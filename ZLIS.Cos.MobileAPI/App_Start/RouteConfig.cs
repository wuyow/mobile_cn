﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace ZLIS.Cos.MobileAPI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetListsContent",
                 routeTemplate: "api/GetListsContent",
                 defaults: new { controller = "ContentAPI", action = "GetContent" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_Login",
                 routeTemplate: "api/Login",
                 defaults: new { controller = "LoginAPI", action = "Login" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_Logout",
                 routeTemplate: "api/Logout",
                 defaults: new { controller = "LogoutAPI", action = "Logout" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetContractDetails",
                 routeTemplate: "api/GetContractDetails",
                 defaults: new { controller = "ContractDetailsAPI", action = "ContractDetails" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetStatementOfAccount",
                 routeTemplate: "api/GetStatementOfAccount",
                 defaults: new { controller = "StatementOfAccountAPI", action = "StatementOfAccount" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetStatementOfAccountPdf",
                 routeTemplate: "api/GetStatementOfAccountPdf",
                 defaults: new { controller = "StatementOfAccountPdfAPI", action = "StatementOfAccountPdf" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetRepaymentPlan",
                 routeTemplate: "api/GetRepaymentPlan",
                 defaults: new { controller = "RepaymentPlanAPI", action = "RepaymentPlan" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetRepaymentPlanPdf",
                 routeTemplate: "api/GetRepaymentPlanPdf",
                 defaults: new { controller = "RepaymentPlanPdfAPI", action = "RepaymentPlanPdf" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetReceipts",
                 routeTemplate: "api/GetReceipts",
                 defaults: new { controller = "ReceiptAPI", action = "Receipts" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetReceiptPdf",
                 routeTemplate: "api/GetReceiptPdf",
                 defaults: new { controller = "ReceiptPdfAPI", action = "ReceiptPdf" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetETEstimate",
                 routeTemplate: "api/GetETEstimate",
                 defaults: new { controller = "ETEstimateAPI", action = "ETEstimate" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_ProcessET",
                 routeTemplate: "api/ProcessET",
                 defaults: new { controller = "ETEstimateAPI", action = "ProcessET" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetEarlyterminatePDF",
                 routeTemplate: "api/GetEarlyterminatePDF",
                 defaults: new { controller = "ETPdfAPI", action = "GetEarlyTerminatePDF" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetDirectDebitInfo",
                 routeTemplate: "api/GetDirectDebitInfo",
                 defaults: new { controller = "DirectDebitAPI", action = "DirectDebitInfo" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetMessageThreads",
                 routeTemplate: "api/GetMessageThreads",
                 defaults: new { controller = "MessageThreadsAPI", action = "MessageThreads" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetMessages",
                 routeTemplate: "api/GetMessages",
                 defaults: new { controller = "MessagesAPI", action = "Messages" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetMessageAttachment",
                 routeTemplate: "api/GetMessageAttachment",
                 defaults: new { controller = "MessageAttachmentAPI", action = "MessageAttachment" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_SendMessage",
                 routeTemplate: "api/SendMessage",
                 defaults: new { controller = "SendMessageAPI", action = "SendMessage" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_SendEnquiry",
                 routeTemplate: "api/SendEnquiry",
                 defaults: new { controller = "SendEnquiryAPI", action = "SendEnquiry" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_ChangePassword",
                 routeTemplate: "api/ChangePassword",
                 defaults: new { controller = "ChangePasswordAPI", action = "ChangePassword" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_ChangeEmail",
                 routeTemplate: "api/ChangeEmail",
                 defaults: new { controller = "ChangeEmailAPI", action = "ChangeEmail" });

            routes.MapHttpRoute(
                 name: "CosMobileApi_GetOtpCode",
                 routeTemplate: "api/GetOtpCode",
                 defaults: new { controller = "GetOtpCodeAPI", action = "GetOtpCode" });

            //routes.MapHttpRoute(
            //     name: "CosMobileApi",
            //     routeTemplate: "api/{controller}/{id}",
            //     defaults: new { id = RouteParameter.Optional });

            routes.MapHttpRoute(
                name: "CosMobileApi_ValidateCustomer",
                routeTemplate: "api/ValidateCustomer",
                defaults: new { controller = "RegistrationAPI", action = "ValidateCustomer" });
            routes.MapHttpRoute(
               name: "CosMobileApi_RegisterCustomer",
               routeTemplate: "api/RegisterCustomer",
               defaults: new { controller = "RegistrationAPI", action = "RegisterCustomer" });
            routes.MapHttpRoute(
               name: "CosMobileApi_ForgotPassword",
               routeTemplate: "api/ForgotPassword",
               defaults: new { controller = "AccountAPI", action = "ForgotPassword" });
            routes.MapHttpRoute(
               name: "CosMobileApi_ForgotUsername",
               routeTemplate: "api/ForgotUsername",
               defaults: new { controller = "AccountAPI", action = "ForgotUsername" });
            routes.MapHttpRoute(
               name: "CosMobileApi_GetStaticPage",
               routeTemplate: "api/GetStaticPage",
               defaults: new { controller = "StaticPageAPI", action = "GetStaticPage" });
            routes.MapHttpRoute(
              name: "CosMobileApi_ValidateOtpCode",
              routeTemplate: "api/ValidateOtpCode",
              defaults: new { controller = "GetOtpCodeAPI", action = "ValidateOtpCode" });
            routes.MapHttpRoute(
             name: "CosMobileApi_GetOverdueBreakdownPDF",
             routeTemplate: "api/GetOverdueBreakdownPDF",
             defaults: new { controller = "OverDuePdfAPI", action = "GetOverdueBreakdownPDF" });

            routes.MapHttpRoute(
            name: "CosMobileApi_GetBanner",
            routeTemplate: "api/GetBanner",
            defaults: new { controller = "BannerAPI", action = "GetBanner" });

            routes.MapHttpRoute(
           name: "CosMobileApi_DownloadGiroForm",
           routeTemplate: "api/DownloadGiroForm",
           defaults: new { controller = "Download", action = "DownloadGiroForm" });
        }
    }
}