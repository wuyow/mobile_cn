﻿using System.Web.Http;

namespace ZLIS.Cos.MobileAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MessageHandlers.Add(new MessageHandler());
        }
    }
}
