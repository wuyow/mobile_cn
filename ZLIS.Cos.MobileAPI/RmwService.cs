﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Cos.RmwService;
using System.Configuration;

namespace ZLIS.Cos.MobileAPI
{
    public class RmwServiceContext : IRmwServiceContext 
    {
        public int RegionId
        {
            get;
            set;
        }

        public string WcfServiceUrl
        {
            get;
            set;
        }

        public string[] Hosts
        {
            get;
            set;
        }

        public RmwServiceContext()
        {
            WcfServiceUrl = ConfigurationManager.AppSettings["RmwWcfServiceUrl"];
            string hosts = ConfigurationManager.AppSettings["RmwWcfHosts"];
            string regionId = ConfigurationManager.AppSettings["Cos.RegionId"];
            if (hosts != null)
                Hosts = hosts.Split(';');
            //_systemSettingService = systemSettingService;
            //_unitOfWorkAccessor = unitOfWorkAccessor;
            RegionId = 1;

            if (regionId != null)
            {
                int i;
                if (int.TryParse(regionId, out i))
                    RegionId = i;
            }
        }
    }
}