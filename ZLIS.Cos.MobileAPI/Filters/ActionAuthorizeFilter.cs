﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Audit.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Framework.Common;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Filters
{
    public class ActionAuthorizeFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            object request = filterContext.ActionArguments["request"];
            if (request != null)
            {
                var _mobileLoginInfoService = CosApplicationContext.Current.GetWorkContext().Resolve<IMobileLoginInfoService>();
                bool haserror = false;
                BaseResponse err_response = new BaseResponse();
                Error errorInfo = new Error();
                err_response.error = errorInfo;

                HttpResponseMessage response = new HttpResponseMessage();

                var propertyInfo = request.GetType().GetProperty("token");
                var token = propertyInfo.GetValue(request, null).ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var loginInfo = _mobileLoginInfoService.GetByToken(token);
                    if (loginInfo == null)
                    {
                        response.StatusCode = HttpStatusCode.Unauthorized;
                        errorInfo.code = (int)GetContractDetailsErrorCode.InvalidToken;
                        errorInfo.message = "Invalid token";
                        haserror = true;
                    }
                    else if (!loginInfo.Valid)
                    {
                        response.StatusCode = HttpStatusCode.MethodNotAllowed;
                        errorInfo.code = (int)GetContractDetailsErrorCode.HaveBeenLoggedOut;
                        errorInfo.message = "Have been logged out";
                        haserror = true;
                    }
                    else if (DateTime.Now > loginInfo.ExpiryTime)
                    {
                        response.StatusCode = HttpStatusCode.Forbidden;
                        errorInfo.code = (int)GetContractDetailsErrorCode.SessionTimeout;
                        errorInfo.message = "Session timeout";
                        haserror = true;
                    }
                    else if (loginInfo.CustomerActivity != null)
                    {
                        if (!loginInfo.CustomerActivity.Customer.ActiveSessionID.Equals(
                           loginInfo.CustomerActivity.SessionID))
                        {
                            response.StatusCode = HttpStatusCode.NotFound;
                            errorInfo.code = (int)GetContractDetailsErrorCode.ForcedLogout;
                            errorInfo.message = "Forced logout";
                            haserror = true;
                        }
                    }

                    if (haserror)
                    {
                        var _unitOfWorkAccessor = CosApplicationContext.Current.GetWorkContext().Resolve<IUnitOfWorkAccessor>();
                        var _logService = CosApplicationContext.Current.GetWorkContext().Resolve<IActionLogService>();
                        var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();
                        try
                        {
                            using (ITransaction tran = unitOfWork.BeginTransaction())
                            {
                                _logService.Log("MobileApi Authorize Failed", Core.Audit.ActionAuditLevel.Warn, Core.Audit.ActionLogLevel.Audit, "Filter", 
                                    string.Format("code:{0} message:{1} (controller:{2} action:{3})", errorInfo.code.ToString(),
                                    errorInfo.message,filterContext.ControllerContext.ControllerDescriptor.ControllerName,filterContext.ActionDescriptor.ActionName), "System");
                                tran.Commit();
                            }
                        }
                        catch
                        { 
                        
                        }
                        var newContent = new ObjectContent<BaseResponse>(err_response, new JsonMediaTypeFormatter());
                        response.Content = newContent;
                        filterContext.Response = response;
                    }
                    else
                        base.OnActionExecuting(filterContext);

                }
                else
                    base.OnActionExecuting(filterContext);

            }
            else
            {
                base.OnActionExecuting(filterContext);
            }
        }
    }
}