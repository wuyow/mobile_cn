﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.RmwService.DomainObjects;

namespace ZLIS.Cos.MobileAPI.CountrySpecific
{
    public class TWSpecialLogic : DefaultSpecialLogic,ISpecialLogic
    {
        public override bool ShowBankDetail(string paymentMode, string paymentCode, string giroStatus, CmsBank bank = null)
        {
            return (bank != null && !string.IsNullOrEmpty(bank.Bank));
        }

        public override bool ShowGiroLink(string paymentMode, string paymentCode, CmsBank bank = null)
        {
            return "Cheque".Equals(paymentMode, StringComparison.OrdinalIgnoreCase)
                    && (bank != null && (bank.GiroStatusCode == "Rejected"));
        }
    }
}
