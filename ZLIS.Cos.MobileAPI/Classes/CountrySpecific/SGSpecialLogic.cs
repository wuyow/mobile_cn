﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.RmwService.DomainObjects;

namespace ZLIS.Cos.MobileAPI.CountrySpecific
{
    public class SGSpecialLogic : DefaultSpecialLogic,ISpecialLogic
    {
        public override bool IsCar(CmsContractDetail contract)
        {
            return contract.ContractNO.IndexOf("HP") > -1;
        }

        public override bool ShowBankDetail(string paymentMode, string paymentCode, string giroStatus, CmsBank bank = null)
        {
            return (bank != null
                        && "Inter Bank Giro".Equals(paymentMode, StringComparison.OrdinalIgnoreCase)
                        && !"Rejected".Equals(bank.GiroStatusCode, StringComparison.OrdinalIgnoreCase));
        }

        public override bool ShowGiroLink(string paymentMode, string paymentCode, CmsBank bank = null)
        {
            return "Cheque".Equals(paymentMode, StringComparison.OrdinalIgnoreCase)
                    && (bank != null && (bank.GiroStatusCode == "Rejected"));
        }
    }
}
