﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.RmwService.DomainObjects;

namespace ZLIS.Cos.MobileAPI.CountrySpecific
{
    public class THSpecialLogic : DefaultSpecialLogic,ISpecialLogic
    {
        public override bool HideGiroStatus(string paymentMode, string paymentCode)
        {
            return (("DirectDebit-SCB".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00046".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-BAY".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00048".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-KBank".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00049".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-Tanachart".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00050".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-DTB".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00051".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                       );
        }

        public override bool ShowBankDetail(string paymentMode, string paymentCode, string giroStatus, CmsBank bank = null)
        {
            return (bank != null
                        &&
                        (("Direct Debit(BBLSaving)".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00003".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-SCB".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00046".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-BAY".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00048".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-KBank".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00049".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-Tanachart".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00050".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-DTB".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00051".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                       ) && "Approved".Equals(giroStatus, StringComparison.OrdinalIgnoreCase));
        }

        public override bool ShowGiroLink(string paymentMode, string paymentCode, CmsBank bank = null)
        {
            return !(bank != null
                       &&
                       (("Direct Debit(BBLSaving)".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00003".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                       || ("DirectDebit-SCB".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00046".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                       || ("DirectDebit-BAY".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00048".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                       || ("DirectDebit-KBank".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00049".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                       || ("DirectDebit-Tanachart".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00050".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                       || ("DirectDebit-DTB".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00051".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                      ));
        }
    }
}
