﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.RmwService.DomainObjects;

namespace ZLIS.Cos.MobileAPI.CountrySpecific
{
    public class MYSpecialLogic : DefaultSpecialLogic,ISpecialLogic
    {
        public override bool IsCar(CmsContractDetail contract)
        {
            bool isCar = true;
            if (contract.ContractAsset.Count() > 0)
            {
                var vehicle = contract.ContractAsset.First();
                isCar = vehicle.VehicleTypeCode == "00001" && !string.IsNullOrEmpty(vehicle.VehicleTypeDescription);
            }
            return isCar;
        }

        public override bool ShowBankDetail(string paymentMode, string paymentCode, string giroStatus, CmsBank bank = null)
        {
            return (bank != null
                       && "Direct Debit".Equals(paymentMode, StringComparison.OrdinalIgnoreCase)
                       && "00003".Equals(paymentCode, StringComparison.OrdinalIgnoreCase));
        }

        public override bool ShowGiroLink(string paymentMode, string paymentCode, CmsBank bank = null)
        {
            return !(bank != null
                           && "Direct Debit".Equals(paymentMode, StringComparison.OrdinalIgnoreCase)
                           && "00003".Equals(paymentCode, StringComparison.OrdinalIgnoreCase));
        }
    }
}
