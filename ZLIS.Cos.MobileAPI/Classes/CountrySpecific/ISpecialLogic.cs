﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.RmwService.DomainObjects;

namespace ZLIS.Cos.MobileAPI.CountrySpecific
{
    public interface ISpecialLogic
    {
        bool IsCar(CmsContractDetail contract);
        bool HideGiroStatus(string paymentMode, string paymentCode);
        bool ShowBankDetail(string paymentMode, string paymentCode, string giroStatus, CmsBank bank = null);
        bool ShowGiroLink(string paymentMode, string paymentCode, CmsBank bank = null);
        bool ShowLocalStatus();
        bool ShowLocalPaymentMode();
        bool ShowLocalFinancialProduct();
        bool CheckFilteredContractDuringRegistration();
    }

    public class DefaultSpecialLogic : ISpecialLogic
    {
        public virtual bool IsCar(CmsContractDetail contract)
        {
            return true;
        }

        public virtual bool HideGiroStatus(string paymentMode, string paymentCode)
        {
            return false;
        }

        public virtual bool ShowBankDetail(string paymentMode, string paymentCode, string giroStatus, CmsBank bank = null)
        {
            return bank != null && "Approved".Equals(giroStatus, StringComparison.OrdinalIgnoreCase);
        }

        public virtual bool ShowGiroLink(string paymentMode, string paymentCode, CmsBank bank = null)
        {
            return bank == null;
        }
        public virtual bool ShowLocalStatus()
        {
            return false;
        }

        public virtual bool ShowLocalPaymentMode()
        {
            return false;
        }

        public virtual bool ShowLocalFinancialProduct()
        {
            return false;
        }

        public virtual bool CheckFilteredContractDuringRegistration()
        {
            return false;
        }
    }
}
