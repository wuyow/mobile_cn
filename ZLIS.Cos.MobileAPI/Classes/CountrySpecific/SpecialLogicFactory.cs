﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Cos.MobileAPI.CountrySpecific;

namespace ZLIS.Cos.MobileAPI
{
    public class SpecialLogicFactory
    {
        public static ISpecialLogic GetSpecialLogic(string countryCode)
        {
            switch (countryCode.ToUpper())
            {
               
                case "SG":
                    return new SGSpecialLogic();
                case "MY":
                    return new MYSpecialLogic();
                case "TH":
                    return new THSpecialLogic();
                case "TW":
                    return new TWSpecialLogic();
                case "CN":
                    return new CNSpecialLogic();
                //case "IN":
                //    return new INSpecialLogic();
                //case "JP":
                //    return new JPSpecialLogic();
                default:
                    return new DefaultSpecialLogic();

            }
        }
    }
}