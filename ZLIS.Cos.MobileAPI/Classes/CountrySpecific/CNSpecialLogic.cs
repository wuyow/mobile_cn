﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.RmwService.DomainObjects;

namespace ZLIS.Cos.MobileAPI.CountrySpecific
{
    public class CNSpecialLogic : DefaultSpecialLogic,ISpecialLogic
    {
        public override bool IsCar(CmsContractDetail contract)
        {
            return contract.ContractNO.IndexOf("IC") == -1;
        }

        public override bool ShowBankDetail(string paymentMode, string paymentCode, string giroStatus, CmsBank bank = null)
        {
            return bank != null && !string.IsNullOrEmpty(paymentMode) && !"Cheque".Equals(paymentMode, StringComparison.OrdinalIgnoreCase);
        }

        public override bool ShowGiroLink(string paymentMode, string paymentCode, CmsBank bank = null)
        {
            return bank == null || "Cheque".Equals(paymentMode, StringComparison.OrdinalIgnoreCase);
        }

        public override bool HideGiroStatus(string paymentMode, string paymentCode)
        {
            return string.IsNullOrEmpty(paymentMode) || !paymentMode.Equals("xxx", StringComparison.OrdinalIgnoreCase);
        }

        public override bool ShowLocalStatus()
        {
            return true;
        }

        public override bool ShowLocalPaymentMode()
        {
            return true;
        }
    }
}
