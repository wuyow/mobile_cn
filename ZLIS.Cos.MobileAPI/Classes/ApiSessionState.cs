﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService;

namespace ZLIS.Cos.MobileAPI
{
    public class ApiSessionState:ISessionState
    {
        private IRmwService _rmwService;
        public ApiSessionState()
        {
            _rmwService =CosApplicationContext.Current.GetWorkContext().Resolve<IRmwService>();
        }

        public string ID
        {
            get { throw new NotImplementedException(); }
        }

        public Core.Account.DomainModel.Customer CurrentCustomer
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Cos.RmwService.DomainObjects.CmsCustomer CurrentCmsCustomer
        {
            get
            {
                return _rmwService.ValidateCustomer(CurrentCmsCustomerProfile.BusinessPartner.OldNricNO);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Cos.RmwService.DomainObjects.CmsCustomerProfile CurrentCmsCustomerProfile
        {
            get;
            set;
        }

        public Guid? CurrentMenuId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string CurrentCountry
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int FailedLoginAttempt
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Welcome
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string WelcomeTitle
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string WelcomeContent
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public T Get<T>(string key) where T : class
        {
            throw new NotImplementedException();
        }

        public void Set<T>(string key, T value) where T : class
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public void Reomove(string key)
        {
            throw new NotImplementedException();
        }

        public bool IsOtpAuthenticated
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public OtpSrcPageInfo OtpSrcPageInfo
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public System.Globalization.CultureInfo CurrentCulture
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool FailedLoginReset
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public IList<string> FailedLoginCustomerName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}