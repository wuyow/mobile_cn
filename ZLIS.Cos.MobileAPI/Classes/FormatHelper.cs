﻿using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Settings;
using System.Globalization;
using ZLIS.Cos.Core.Const;

namespace ZLIS.Cos.MobileAPI
{
    public class FormatHelper
    {
        //public static string GetFormat(string formatCode)
        //{
        //    ISystemSettingService _service = CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();
        //    _service.Cachable = false;

        //    switch (formatCode)
        //    {
        //        case "CURRENCY":
        //            return _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_SYMBOL).Value;
        //        case "CURRENCY_CODE":
        //            return _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_CODE).Value;
        //        case "DATE":
        //            return _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT).Value;
        //        default:
        //            return "";
        //    }
        //}

        //public static string FormatKendo(string format, string data)
        //{
        //    ISystemSettingService _service = CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();
        //    _service.Cachable = false;
        //    string formattedString;

        //    switch (format)
        //    {
        //        case "CURRENCY":
        //            formattedString = string.Format("{1}#= kendo.toString({0},'n') #", data, _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_SYMBOL).Value);
        //            break;
        //        case "CURRENCY_CODE":
        //            formattedString = string.Format("{1}#= kendo.toString({0},'n') #", data, _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_CODE).Value);
        //            break;
        //        case "DATE":
        //            formattedString = string.Format("#= kendo.toString(toDate({0}),'{1}') #", data, _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT).Value);
        //            break;
        //        case "DATE2":
        //            formattedString = string.Format("#= kendo.toString(toDate({0}),'{1}') #", data, "dd-MMM-yyyy");
        //            break;
        //        case "YEAR":
        //            formattedString = string.Format("#= kendo.toString(toDate({0}),'{1}') #", data, "yyyy");
        //            break;
        //        default:
        //            formattedString = string.Format("#= {0} #", data);
        //            break;
        //    }

        //    return string.Format("# if ({0} == null)  {{ # &nbsp; # }} else {{ # {1} # }} #" , data, formattedString);
        //}

        public static string FormatData(ISystemSettingService _service, string format, object data, bool useCode = true)
        {
            //ISystemSettingService _service = CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();
            _service.Cachable = false;

            //ISystemSettingService _tmpservice = CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();

            string formattedString, template;

            if (data != null)
            {
                if (useCode)
                {
                    switch (format)
                    {
                        case "CURRENCY":
                        case "CURRENCY_SOA":
                            formattedString = string.Format("{1}{0:N2}", data, _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_SYMBOL).Value);
                            break;
                        case "CURRENCY_CODE":
                            formattedString = string.Format("{1}{0:N2}", data, _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_CODE).Value);
                            break;
                        case "DATE":
                            //Thread.CurrentThread.CurrentCulture = defaultCulture;
                            template = string.Format("{{0:{0}}}", _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT).Value);
                            formattedString = string.Format(template, data);
                            //Thread.CurrentThread.CurrentCulture = sessionState.CurrentCulture;
                            break;
                        case "DATE2":
                            //Thread.CurrentThread.CurrentCulture = defaultCulture;
                            template = string.Format("{{0:{0}}}", "dd-MMM-yyyy");
                            formattedString = string.Format(template, data);
                            //Thread.CurrentThread.CurrentCulture = sessionState.CurrentCulture;
                            break;
                        case "DATETIME":
                            //Thread.CurrentThread.CurrentCulture = defaultCulture;
                            template = string.Format("{{0:{0}}}", _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATETIME_FORMAT).Value);
                            formattedString = string.Format(template, data);
                            //Thread.CurrentThread.CurrentCulture = sessionState.CurrentCulture;
                            break;
                        case "YEAR":
                            //Thread.CurrentThread.CurrentCulture = defaultCulture;
                            template = string.Format("{{0:{0}}}", "yyyy");
                            formattedString = string.Format(template, data);
                            //Thread.CurrentThread.CurrentCulture = sessionState.CurrentCulture;
                            break;
                        default:
                            formattedString = string.Format("{0}", data);
                            break;
                    }

                    if (string.IsNullOrEmpty(formattedString))
                    {
                        formattedString = "-";
                    }
                }
                else
                {
                    template = string.Format("{{0:{0}}}", format);
                    formattedString = string.Format(template, data);
                }
                return formattedString;
            }
            else
            {
                return "-";
            }
        }

        public static string FormatData(string format, object data, bool useCode = true)
        {
            ISessionState sessionState = CosApplicationContext.Current.GetWorkContext().Resolve<ISessionState>();
            CultureInfo defaultCulture = new CultureInfo(ConstValue.DEFAULT_CULTURE_NAME);

            if (data != null)
            {
                ISystemSettingService _service = CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();
                string formattedString, template;
                if (useCode)
                {
                    switch (format)
                    {
                        case "CURRENCY":
                            formattedString = string.Format("{1}{0:N2}", data, _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_SYMBOL).Value);
                            break;
                        case "CURRENCY_CODE":
                            formattedString = string.Format("{1}{0:N2}", data, _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_CODE).Value);
                            break;
                        case "DATE":
                            //Thread.CurrentThread.CurrentCulture = defaultCulture;
                            template = string.Format("{{0:{0}}}", _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT).Value);
                            formattedString = string.Format(template, data);
                            //Thread.CurrentThread.CurrentCulture = sessionState.CurrentCulture;
                            break;
                        case "DATE2":
                            //Thread.CurrentThread.CurrentCulture = defaultCulture;
                            template = string.Format("{{0:{0}}}", "dd-MMM-yyyy");
                            formattedString = string.Format(template, data);
                            //Thread.CurrentThread.CurrentCulture = sessionState.CurrentCulture;
                            break;
                        case "DATETIME":
                            //Thread.CurrentThread.CurrentCulture = defaultCulture;
                            template = string.Format("{{0:{0}}}", _service.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATETIME_FORMAT).Value);
                            formattedString = string.Format(template, data);
                            //Thread.CurrentThread.CurrentCulture = sessionState.CurrentCulture;
                            break;
                        case "YEAR":
                            //Thread.CurrentThread.CurrentCulture = defaultCulture;
                            template = string.Format("{{0:{0}}}", "yyyy");
                            formattedString = string.Format(template, data);
                            //Thread.CurrentThread.CurrentCulture = sessionState.CurrentCulture;
                            break;
                        default:
                            formattedString = string.Format("{0}", data);
                            break;
                    }

                    if (string.IsNullOrEmpty(formattedString))
                    {
                        formattedString = "-";
                    }
                }
                else
                {
                    template = string.Format("{{0:{0}}}", format);
                    formattedString = string.Format(template, data);
                }
                return formattedString;
            }
            else
            {
                return "-";
            }
        }
    }
}