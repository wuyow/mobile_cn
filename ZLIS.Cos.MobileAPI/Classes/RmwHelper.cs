﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;

namespace ZLIS.Cos.MobileAPI.Classes.RmwService
{
    public class RmwHelper
    {
        private IRmwService _rmwService;
        private ISessionState _session;

        #region Keys
        // IF ADDING IN NEW KEY, MUST MAKE SURE TO MENTION IN ISessionState
        private const string SESSION_KEY_ACCOUNT_SUMMARIES = "CMS.Account_Summaries";
        private const string SESSION_KEY_CONTRACT_DETAILS = "CMS.Contract_Details";
        private const string SESSION_KEY_BANK_DETAILS = "CMS.Bank_Details";

        #endregion

        private string errorFormat = " Data returns 'NULL' from RMW Service - RmwHelper:{0} ({1})";

        #region Storage

        private IList<CmsAccountSummary> AccountSummaries
        {
            get { return _session.Get<IList<CmsAccountSummary>>(SESSION_KEY_ACCOUNT_SUMMARIES); }
            set { _session.Set<IList<CmsAccountSummary>>(SESSION_KEY_ACCOUNT_SUMMARIES, value); }
        }

        private IList<CmsContractDetail> ContractDetails
        {
            get { return _session.Get<IList<CmsContractDetail>>(SESSION_KEY_CONTRACT_DETAILS); }
            set { _session.Set<IList<CmsContractDetail>>(SESSION_KEY_CONTRACT_DETAILS, value); }
        }

        private IList<CmsBank> BankDetails
        {
            get { return _session.Get<IList<CmsBank>>(SESSION_KEY_BANK_DETAILS); }
            set { _session.Set<IList<CmsBank>>(SESSION_KEY_BANK_DETAILS, value); }
        }

        #endregion

        public RmwHelper(IRmwService rmwService)
        {
            _rmwService = rmwService;
            _session = CosApplicationContext.Current.GetWorkContext().Resolve<ISessionState>();
        }

        public IList<CmsAccountSummary> GetAccountSummary(int Bpid, string role = "Borrower")
        {
            //if (AccountSummaries == null)
            //{
            //this._rmwService = CosApplicationContext.Current.GetWorkContext().Resolve<IRmwService>();
            var accountSummaries = _rmwService.GetAccountsSummary(Bpid, null);
            if (accountSummaries == null)
                throw new RmwServiceException(string.Format(errorFormat, "GetAccountSummary", "BPID=" + Bpid + ", role=" + role));
            //}

            if (role.Equals("all", StringComparison.OrdinalIgnoreCase))
                return accountSummaries.ToList();

            return accountSummaries.Where(x => x.RoleDescriptionCode == role).ToList();
        }

        public CmsContractDetail GetContractDetails(int Bpid, string ContractNo)
        {
            CmsContractDetail contract;
            //if (ContractDetails == null)
            //{
            //    this._rmwService = CosApplicationContext.Current.GetWorkContext().Resolve<IRmwService>();
            //    contract = _rmwService.GetContractDetails(Bpid, ContractNo);
            //    if (contract == null) throw new RmwServiceException(string.Format(errorFormat, "GetContractDetails", "BPID=" + Bpid + ", ContractNo=" + ContractNo));
            //    ContractDetails = new List<CmsContractDetail>();
            //    ContractDetails.Add(contract);
            //}
            //else
            //{
            //    contract = ContractDetails.Where(x => x.ContractNO == ContractNo).FirstOrDefault();
            //    if (contract == null)
            //    {
            //        this._rmwService = CosApplicationContext.Current.GetWorkContext().Resolve<IRmwService>();
            //        contract = _rmwService.GetContractDetails(Bpid, ContractNo);
            //        if (contract == null) throw new RmwServiceException(string.Format(errorFormat, "GetContractDetails", "BPID=" + Bpid + ", ContractNo=" + ContractNo));
            //        ContractDetails.Add(contract);
            //    }
            //}

            //this._rmwService = CosApplicationContext.Current.GetWorkContext().Resolve<IRmwService>();
            contract = _rmwService.GetContractDetails(Bpid, ContractNo);
            if (contract == null) throw new RmwServiceException(string.Format(errorFormat, "GetContractDetails", "BPID=" + Bpid + ", ContractNo=" + ContractNo));

            return contract;
        }

        public IList<CmsBank> GetBanks(int Bpid)
        {
            //if (BankDetails == null)
            //{
            //this._rmwService = CosApplicationContext.Current.GetWorkContext().Resolve<IRmwService>();
            var bankDetails = _rmwService.GetBanks(Bpid);
            if (bankDetails == null) throw new RmwServiceException(string.Format(errorFormat, "GetBanks", "BPID=" + Bpid));
            //}
            return bankDetails;
        }

        public IList<CmsBank> GetAssetDetails(int Bpid)
        {
            //if (BankDetails == null)
            //{
            //this._rmwService = CosApplicationContext.Current.GetWorkContext().Resolve<IRmwService>();
            var bankDetails = _rmwService.GetBanks(Bpid);
            if (bankDetails == null) throw new RmwServiceException(string.Format(errorFormat, "GetBanks", "BPID=" + Bpid));
            //}

            return bankDetails;
        }
    }


}