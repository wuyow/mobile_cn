﻿using ZLIS.Cos.RmwService;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.Settings;

namespace ZLIS.Cos.MobileAPI.RmwService
{
    public class RmwSpecContextFactory
    {
        public static IRmwSpecContext CreateRmwSpecContext()
        {
            var systemSettingService = CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();
            string countryCode = systemSettingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_COUNTRY_CODE).Value;
            string cc = countryCode.ToLower();

            if (cc == "tw")
            {
                return new TWRmwSpecContextImpl();
            }

            if (cc == "sg")
            {
                return new SgRmwSpecContextImpl();
            }

            if (cc == "my")
            {
                return new MyRmwSpecContextImpl();
            }

            if (cc == "th")
            {
                return new ThRmwSpecContextImpl();
            }

            if (cc == "in")
            {
                return new INRmwSpecContextImpl();
            }

            return new DefaultRmwSpecContextImpl();
        }
    }
}