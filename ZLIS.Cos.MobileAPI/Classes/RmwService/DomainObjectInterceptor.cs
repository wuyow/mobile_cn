﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZLIS.Cos.RmwService;
using Castle.DynamicProxy;
using System.Reflection;
using ZLIS.Cos.Core.Const;

namespace ZLIS.Cos.MobileAPI.RmwService
{
    [Serializable]
    public class DomainObjectInterceptor : CmsObjectInterceptor
    {
        public override void Intercept(IInvocation invocation)
        {
            if (IsGetter(invocation.Method))
            {
                string methodName = invocation.Method.Name;
                var accessor = invocation.Proxy as IProxyTargetAccessor;
                var obj = accessor.DynProxyGetTarget();
                string propertyName = methodName.Substring(4);
                Dictionary<string, PropertyInfo> properties = invocation.TargetType.GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => x.Name, x => x);

                //var value = properties[string.Format("{0}_Local", propertyName)].GetValue(obj, null);

                //ISessionState sessionState = CosApplicationContext.Current.Resolve<ISessionState>();

                //System.Globalization.CultureInfo cultureInfo = sessionState.CurrentCulture;
                System.Globalization.CultureInfo cultureInfo =
                    new System.Globalization.CultureInfo(ConstValue.DEFAULT_CULTURE_NAME);

                if (cultureInfo.TwoLetterISOLanguageName == ConstValue.DEFAULT_TWO_LETTER_ISO_LANGUAGE_NAME)
                {
                    invocation.Proceed();
                }
                else
                {
                    string propertyNameLocal = propertyName + "_Local";
                    if (!properties.ContainsKey(propertyNameLocal))
                    {
                        invocation.Proceed();
                    }
                    else
                    {
                        string textValueLocal = properties[propertyNameLocal].GetValue(obj, null) as string;
                        if (!string.IsNullOrEmpty(textValueLocal))
                        {
                            invocation.ReturnValue = textValueLocal;
                        }
                        else
                        {
                            invocation.Proceed();
                        }
                    }
                }
            }
            else
            {
                invocation.Proceed();
            }
        }

        private bool IsGetter(MethodInfo method)
        {
            return method.IsSpecialName && method.Name.StartsWith("get_", StringComparison.Ordinal);
        }
    }
}