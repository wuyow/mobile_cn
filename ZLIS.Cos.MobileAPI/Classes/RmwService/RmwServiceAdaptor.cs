﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;

namespace ZLIS.Cos.MobileAPI.RmwService
{
    public class RmwServiceAdaptor : IRmwService
    {
        private IRmwService _rmwService;
        private ICacheManager _cacheManager ;
        private ISignals _signals;
        
        #region Keys
        // IF ADDING IN NEW KEY, MUST MAKE SURE TO MENTION IN ISessionState
        private const string KEY_ACCOUNT_SUMMARIES_PREFIX = "CMS.Account_Summaries";
        private const string KEY_CONTRACT_DETAILS_PREFIX = "CMS.Contract_Details";
        private const string KEY_CUSTOMER_PROFILE_PREFIX = "CMS.Customer_Profile";
        private const string KEY_BANK_DETAILS_PREFIX = "CMS.Bank_Details";

        #endregion

        private string errorFormat = " Data returns 'NULL' from RMW Service - RmwHelper:{0} ({1})";

        private ILogger _logger = NullLogger.Instance;

        public ILogger Logger
        {
            get
            {
                if (_rmwService is RmwServiceImpl)
                {
                    return ((RmwServiceImpl) _rmwService).Logger;
                }

                return _logger;
            }
            set
            {
                if (_rmwService is RmwServiceImpl)
                {
                    ((RmwServiceImpl) _rmwService).Logger = value;
                }
                _logger = value;

            }
        }

        public RmwServiceAdaptor(IRmwService rmwService, ISignals signals)
        {
            _rmwService = rmwService;
            _signals = signals;

            Logger = NullLogger.Instance;
        }

        public void SetCacheManager(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        private IList<CmsAccountSummary> GetAccountSummaryInternal(int bpId)
        {
            string key = string.Format("{0}.{1}", KEY_ACCOUNT_SUMMARIES_PREFIX, bpId);

            var accountSummaries = _cacheManager.Get(key, ctx =>
            {
                Logger.Debug("Get Account Summary from RMW web service for {0}", bpId);
                ctx.Monitor(_signals.When(AppCacheContainer.GetToken(Constants.TOKEN_ACCOUNT_SUMMARY_PREFIX, bpId)));
                return _rmwService.GetAccountsSummary(bpId, null);
            });

            if (accountSummaries == null)
                throw new RmwServiceException(string.Format(errorFormat, "GetAccountSummary", "BPID=" + bpId.ToString()));// + ", role=" + role));

            return accountSummaries;
        }

        public IList<CmsAccountSummary> GetAccountSummary(int Bpid, string role = "Borrower;Contractor")
        {
            Logger.Debug("RmwServiceAdaptor - GetAccountSummary is called, BPID {0}", Bpid);

            var accountSummaries = GetAccountSummaryInternal(Bpid);

            if (role.Equals("all", StringComparison.OrdinalIgnoreCase))
                return accountSummaries.ToList();

            if (role.Contains(';'))
            {
                string[] arrRole = role.Split(';');
                return accountSummaries.Where(x => arrRole.Contains(x.RoleDescriptionCode)).ToList();
            }

            Logger.Debug("RmwServiceAdaptor - GetAccountSummary ends");

            return accountSummaries.Where(x => x.RoleDescriptionCode == role).ToList();
        }

        public IList<CmsAccountSummary> GetAccountsSummary(int bpId, string contractNo)
        {
            Logger.Debug("RmwServiceAdaptor - GetAccountsSummary is called, BPID: {0}, Contract NO: {1}", bpId, contractNo);

            var accountSummaries = GetAccountSummaryInternal(bpId);

            if (!string.IsNullOrEmpty(contractNo))
                return accountSummaries.Where(x => x.ContractNO == contractNo).ToList();

            Logger.Debug("RmwServiceAdaptor - GetAccountsSummary ends.");

            return accountSummaries.ToList();
        }


        public CmsContractDetail GetContractDetails(int Bpid, string ContractNo)
        {
            Logger.Debug("RmwServiceAdaptor - GetContractDetails is called, BPID: {0}, Contract NO: {1}", Bpid, ContractNo);

            string key = string.Format("{0}.{1}.{2}", KEY_CONTRACT_DETAILS_PREFIX, Bpid, ContractNo);

            CmsContractDetail contract = _cacheManager.Get(key, ctx =>
            {
                Logger.Debug("Get Contract Details from RMW web service for BPID: {0}, Contract NO {1}", Bpid, ContractNo);

                ctx.Monitor(_signals.When(AppCacheContainer.GetToken(Constants.TOKEN_CONTRACT_DETAIL_PREFIX, Bpid, ContractNo)));
                return _rmwService.GetContractDetails(Bpid, ContractNo);
            });

           
            if (contract == null) throw new RmwServiceException(string.Format(errorFormat, "GetContractDetails", "BPID=" + Bpid + ", ContractNo=" + ContractNo));

            Logger.Debug("RmwServiceAdaptor - GetContractDetails ends");

            return contract;
        }

        public IList<CmsBank> GetBanks(int Bpid)
        {
            Logger.Debug("RmwServiceAdaptor - GetBanks is called, BPID: {0}", Bpid);

            string key = string.Format("{0}.{1}", KEY_BANK_DETAILS_PREFIX, Bpid);

            var bankDetails = _cacheManager.Get(key, ctx =>
            {
                Logger.Debug("Get Bank Details from RMW web service for {0}", Bpid);

                ctx.Monitor(_signals.When(AppCacheContainer.GetToken(Constants.TOKEN_BANKS_PREFIX, Bpid)));
                return _rmwService.GetBanks(Bpid);
            });

            if (bankDetails == null) throw new RmwServiceException(string.Format(errorFormat, "GetBanks", "BPID=" + Bpid));

            return bankDetails;
        }

        public IList<CmsBank> GetAssetDetails(int Bpid)
        {
            return GetBanks(Bpid);
        }

        
        public IList<CmsAddress> GetAddressDetails(int bpId)
        {
            var profile = GetCustomerProfile(bpId);

            if (profile.Address != null && profile.Address.Count > 0)
                return profile.Address;

            return null;
            //return _rmwService.GetAddressDetails(bpId);
        }

        public IList<CmsAsset> GetAssetDetails(int bpId, string contractNo)
        {
            return GetContractDetails(bpId, contractNo).ContractAsset;
        }

        public CmsETEstimate GetETQuotation(string contractNo, DateTime? requestDate)
        {
            return _rmwService.GetETQuotation(contractNo, requestDate); 
        }

        public IList<CmsCoBorrower> GetCoborrower(string contractNo)
        {
            return _rmwService.GetCoborrower(contractNo); 
        }

        public IList<CmsGaurantor> GetGaurantors(string contractNo)
        {
            return _rmwService.GetGaurantors(contractNo); 
        }

        public IList<CmsEmail> GetEmails(int bpId)
        {
            var profile = GetCustomerProfile(bpId);

            return profile.Emails;
        }

        public IList<CmsFinancialDetail> GetFinancialDetails(int bpId, string contractNo)
        {
            return _rmwService.GetFinancialDetails(bpId, contractNo); 
        }

        public IList<CmsMiscCharge> GetMiscChargesDetails(int bpId, string contractNo)
        {
            return GetContractDetails(bpId, contractNo).MiscellaneousCharges;
        }

        public IList<CmsOverDueRental> GetOverdueRentalDetail(int bpId, string contractNo)
        {
            return GetContractDetails(bpId, contractNo).OverdueRentals;
        }

        public IList<CmsOverDueInterest> GetOverdueRentalInterest(int bpId, string contractNo)
        {
            Logger.Debug("RmwServiceAdaptor - GetOverdueRentalInterest is called, BPID: {0}, Contract NO: {1}", bpId, contractNo);

            var summary = GetAccountsSummary(bpId, contractNo).FirstOrDefault();
            if (summary != null && summary.OverdueInterest != null)
            {
                return summary.OverdueInterest;
            }
            else
            {
                return _rmwService.GetOverdueRentalInterest(bpId, contractNo);
            }
        }

        public CmsPaymentScheduleResultSet GetPaymentSchedule(int bpId, string contractNo, int pageIndex, int numOfPage, DateTime? startDate, DateTime? endDate)
        {
            return _rmwService.GetPaymentSchedule(bpId, contractNo, pageIndex, numOfPage, startDate, endDate);
        }

        public IList<CmsPhone> GetPhone(int bpId)
        {
            return _rmwService.GetPhone(bpId); 
        }

        public CmsCustomerProfile GetCustomerProfile(int bpId)
        {
            Logger.Debug("RmwServiceAdaptor - GetCustomerProfile is called, BPID: {0}", bpId);

            string key = string.Format("{0}.{1}", KEY_CUSTOMER_PROFILE_PREFIX, bpId);
            CmsCustomerProfile cmsCustomerProfile = _cacheManager.Get(key, ctx =>
            {
                Logger.Debug("Get Customer Profile from RMW web service for {0}", bpId);

                ctx.Monitor(_signals.When(AppCacheContainer.GetToken(Constants.TOKEN_CUSTOMER_PROFILE_PREFIX, bpId)));
                return _rmwService.GetCustomerProfile(bpId);
            });

            Logger.Debug("RmwServiceAdaptor - GetCustomerProfile ends.");
            return cmsCustomerProfile;
        }

        public CmsSoaResultSet GetSOA(int bpId, string contractNo, int transactionTypeGroupCode, int pageIndex, int numOfPage, DateTime? startDate, DateTime? endDate)
        {
            return _rmwService.GetSOA(bpId, contractNo, transactionTypeGroupCode, pageIndex, numOfPage, startDate, endDate); 
        }

        public CmsReceiptResultSet GetReceipts(int bpId, string contractNo, int pageIndex, int numOfPage, DateTime? startDate, DateTime? endDate)
        {
            return _rmwService.GetReceipts(bpId, contractNo, pageIndex, numOfPage, startDate, endDate);
        }

        public CmsReceiptDetail GetReceipt(int bpId, string contractNo, string receiptNo)
        {
            return _rmwService.GetReceipt(bpId, contractNo, receiptNo); 
        }

        public CmsCustomer ValidateCustomer(string customerId, string customerName = null)
        {
            return _rmwService.ValidateCustomer(customerId, customerName);
        }

        public CmsAssetInsurance GetAssetInsurance(int assetId)
        {
            return _rmwService.GetAssetInsurance(assetId); 
        }

        public IList<CmsMasterData> GetMasterData(CmsMasterDataType type)
        {
            return _rmwService.GetMasterData(type); 
        }

        public IList<CmsCustomer> SearchCustomer(string NRIC, string MobileNo, string ContractNo, string PassportNo, string VehicleNo, string CompanyNo, string Name)
        {
            return _rmwService.SearchCustomer(NRIC, MobileNo, ContractNo, PassportNo, VehicleNo, CompanyNo, Name);
        }

        public CmsTaxInvoiceResultSet GetTaxInvoices(int bpId, string contractNo, int pageIndex, int numOfPage, DateTime? startDate, DateTime? endDate)
        {
            return _rmwService.GetTaxInvoices(bpId, contractNo, pageIndex, numOfPage, startDate, endDate);
        }

        public CmsTaxInvoiceResultSet GetTaxInvoice(int bpId, string contractNo, string invoiceNo)
        {
            return _rmwService.GetTaxInvoice(bpId, contractNo, invoiceNo);
        }

        public CmsPaymentHistoryResultSet GetPaymentHistory(string contractNo, string receiptNo, int pageIndex, int numOfPage, DateTime? startDate, DateTime? endDate)
        {
            return _rmwService.GetPaymentHistory(contractNo, receiptNo, pageIndex, numOfPage, startDate, endDate);
        }
    }


}