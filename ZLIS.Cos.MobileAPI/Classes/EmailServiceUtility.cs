﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using System.Configuration;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.Settings.Services;

namespace ZLIS.Cos.MobileAPI
{
    public class EmailServiceUtility
    {
        private IAppDataFolder _appDataFolder;
        private ISystemSettingService _settingService;

        public EmailServiceUtility(IAppDataFolder appDataFolder, ISystemSettingService settingService)
        {
            _appDataFolder = appDataFolder;
            _settingService = settingService;
            _settingService.Cachable = false;
        }

        public string GetImageUrl(string rootUrl, string content, string id)
        {
            return string.Format("{0}{1}/EmailReader/GetEmailImages?templateId={2}&image=", rootUrl, content, id);
        }

        public string GetStringFromTemplateFile(EmailTemplate template, string culture)
        {
            if (template != null)
            {
                try
                {
                    //check if culture folder exists

                    //string culture = CosApplicationContext.Current.CurrentCulture.Name;
                    //ISessionState sessionState = CosApplicationContext.Current.Resolve<ISessionState>();
                    //System.Globalization.CultureInfo cultureInfo = sessionState.CurrentCulture;
                    //string culture = cultureInfo.Name;

                    if (!System.IO.Directory.Exists(_appDataFolder.MapPath(string.Format("PublishedResources/EmailTemplates/{0}/{1}",
                        /*CosApplicationContext.Current.CurrentCulture.Name*/culture,
                        template.TemplatePath))))
                        culture = ConstValue.DEFAULT_CULTURE_NAME;

                    string path =
                        _appDataFolder.MapPath(string.Format("PublishedResources/EmailTemplates/{0}/{1}/{2}",
                                                             culture,
                                                             template.TemplatePath, template.TemplateFileName));

                    var contents = File.ReadAllText(path);
                    return contents;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            return null;
        }

        public string GetAttachmentPathFromTemplateFile(EmailTemplate template)
        {
            if (template != null)
            {
                try
                {
                    //check if culture folder exists

                    //string culture = CosApplicationContext.Current.CurrentCulture.Name;
                    ISessionState sessionState = CosApplicationContext.Current.GetWorkContext().Resolve<ISessionState>();
                    System.Globalization.CultureInfo cultureInfo = sessionState.CurrentCulture;
                    string culture = cultureInfo.Name;

                    if (!System.IO.Directory.Exists(_appDataFolder.MapPath(string.Format("PublishedResources/EmailTemplates/{0}/{1}",
                        /*CosApplicationContext.Current.CurrentCulture.Name*/cultureInfo.Name,
                        template.TemplatePath))))
                        culture = ConstValue.DEFAULT_CULTURE_NAME;/*"en-US"*/;

                    string path =
                        _appDataFolder.MapPath(string.Format("PublishedResources/EmailTemplates/{0}/{1}/attachments",
                                                             culture,
                                                             template.TemplatePath));
                    return path;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            return null;
        }

        public string GetMessageBodyReplacement(string messageBody, string emailImageURL, string customerName, string topicType = "", string message = "", string contractNo = "", string contactPreference = "", string contactEmail = "", string contactMobile = "", string contactHours = "", string fileList = "", string activationUrl = "", string activationUrlName = "", string oldUsername = "", string newUsername = "", string ticketNumber = "")
        {
            string msg = messageBody;

            if (string.IsNullOrEmpty(messageBody))
                return "";

            if (!string.IsNullOrEmpty(emailImageURL))
                msg = ReplaceNoCase(msg, "{GetEmailImageURL}", emailImageURL);// msg.Replace("{GetEmailImageURL}", emailImageURL);

            if (!string.IsNullOrEmpty(customerName))
                msg = ReplaceNoCase(msg, "(Customer Name)", customerName);// msg.Replace("(Customer Name)", customerName);

            if (!string.IsNullOrEmpty(topicType))
                msg = ReplaceNoCase(msg, "(topic type)", topicType);// msg.Replace("(topic type)", topicType);

            if (!string.IsNullOrEmpty(message))
                msg = ReplaceNoCase(msg, "(customer's message)", message.Replace("\r\n", "<br />"));// msg.Replace("(customer's message)", message);

            if (!string.IsNullOrEmpty(contractNo))
                msg = ReplaceNoCase(msg, "(contract no)", contractNo);// msg.Replace("(contract no)", contractNo);

            if (!string.IsNullOrEmpty(contactPreference))
                msg = ReplaceNoCase(msg, "(contact preference)", contactPreference);// msg.Replace("(contact preference)", contactPreference);
            else
                msg = ReplaceNoCase(msg, "(contact preference)", "-");

            if (!string.IsNullOrEmpty(contactEmail))
                msg = ReplaceNoCase(msg, "(contact hours)", contactEmail);// msg.Replace("(customer email)", contactEmail);
            else
                msg = ReplaceNoCase(msg, "(contact hours)", "-");// msg.Replace("(contact hours)", "-");

            if (!string.IsNullOrEmpty(contactMobile))
                msg = ReplaceNoCase(msg, "(customer mobile)", contactMobile);// msg.Replace("(customer mobile)", contactMobile);
            else
                msg = ReplaceNoCase(msg, "(customer mobile)", "-");// msg.Replace("(contact hours)", "-");

            if (!string.IsNullOrEmpty(contactHours))
                msg = ReplaceNoCase(msg, "(contact hours)", contactHours);// msg.Replace("(contact hours)", contactHours);
            else
                msg = ReplaceNoCase(msg, "(contact hours)", "-");// msg.Replace("(contact hours)", "-");

            if (!string.IsNullOrEmpty(ticketNumber))
                msg = ReplaceNoCase(msg, "(ticket number)", ticketNumber);
            else
                msg = ReplaceNoCase(msg, "(ticket number)", "-");

            if (!string.IsNullOrEmpty(fileList))
                msg = msg.Replace("(files)", fileList);
            else
                msg = msg.Replace("(files)", "-");

            if (!string.IsNullOrEmpty(activationUrl))
                msg = ReplaceNoCase(msg, "{activationUrl}", activationUrl);

            if (!string.IsNullOrEmpty(activationUrlName))
                msg = ReplaceNoCase(msg, "{activationUrlName}", activationUrlName);

            if (!string.IsNullOrEmpty(oldUsername))
                msg = ReplaceNoCase(msg, "(old username)", oldUsername);

            if (!string.IsNullOrEmpty(newUsername))
                msg = ReplaceNoCase(msg, "(new username)", newUsername);

            msg = ReplaceNoCase(msg, "(date)", FormatHelper.FormatData(_settingService, "DATE", /*TimeHelper.Now*/DateTime.Now));   //!!!   // msg.Replace("(date)", FormatHelper.FormatData("DATE", DateTime.Now));

            return msg;
        }

        // instr:  string to search in.
        // oldstr: string to search for, ignoring the case.
        // newstr: string replacing the occurrences of oldstr.
        private string ReplaceNoCase(string instr, string oldstr, string newstr)
        {
            //// temporarily disabled Regex - Pyisoe (21 Mar 2013)
            //System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(oldstr, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            //return regex.Replace(instr, newstr);


            string output;
            output = instr;

            // lowercase-versions to search in.
            string input_lower;
            string oldone_lower;
            input_lower = instr.ToLower();
            oldone_lower = oldstr.ToLower();

            // search in the lowercase versions,
            // replace in the original-case version.
            int pos;
            pos = input_lower.IndexOf(oldone_lower);

            while (pos != -1)
            {

                // need for empty "newstr" cases.
                input_lower = input_lower.Remove(pos, oldstr.Length);
                input_lower = input_lower.Insert(pos, newstr);

                // actually replace.
                output = output.Remove(pos, oldstr.Length);
                output = output.Insert(pos, newstr);

                pos = input_lower.IndexOf(oldone_lower);
            }

            return output;
        }

        public string Hash(string id)
        {
            MD5 md5 = MD5.Create();
            byte[] hashedByte = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(id));

            StringBuilder hex = new StringBuilder(hashedByte.Length * 2);
            foreach (byte b in hashedByte)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static string GetEmailAddress(string email)
        {
            try
            {
                if (ConfigurationManager.AppSettings["TestEmailAddress"] != null)
                    return ConfigurationManager.AppSettings["TestEmailAddress"].ToString();
            }
            catch
            {
            }

            return email;
        }
    }
}
