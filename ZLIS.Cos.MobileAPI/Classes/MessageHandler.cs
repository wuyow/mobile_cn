﻿using System.Web;
using System.Net.Http;
using Autofac.Integration.WebApi;

namespace ZLIS.Cos.MobileAPI
{
    public class MessageHandler : DelegatingHandler
    {
        protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
           
            HttpContextBase httpContext=request.Properties["MS_HttpContext"] as System.Web.HttpContextBase;
            var dependencyScope = request.GetDependencyScope();
            httpContext.Items["CosAppLifetimeScope"] = dependencyScope.GetRequestLifetimeScope();
            
            // SETUP A CALLBACK FOR CATCHING THE RESPONSE - AFTER ROUTING HANDLER, AND AFTER CONTROLLER ACTIVITY
            return base.SendAsync(request, cancellationToken).ContinueWith(
                        task =>
                        {
                            httpContext.Items["CosAppLifetimeScope"] = null;

                            // RETURN THE ORIGINAL RESULT
                            var response = task.Result;
                            return response;
                        }
            );
        }
    }
}