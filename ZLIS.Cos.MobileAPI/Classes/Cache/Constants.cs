﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZLIS.Cos.MobileAPI.Cache
{
    public class Constants
    {
        public static readonly string TRIGGER_NAME = "trigger_session_expired";
        public static readonly string TOKEN_ACCOUNT_SUMMARY_PREFIX = "Token.AccountSummary";
        public static readonly string TOKEN_CONTRACT_DETAIL_PREFIX = "Token.ContractDetail";
        public static readonly string TOKEN_CUSTOMER_PROFILE_PREFIX = "Token.CustomerProfile";
        public static readonly string TOKEN_BANKS_PREFIX = "Token.Banks";
    }
}