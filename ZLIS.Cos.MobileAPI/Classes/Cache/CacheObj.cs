﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZLIS.Cos.MobileAPI.Cache
{
    [Serializable]
    public class CacheObj
    {
        public string SessionId { get; set; }
        public string Token { get; set; }
        public int BpId { get; set; }
        public DateTime LastAccessTime { get; set; }
    }
}