﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using ZLIS.Cos.Core;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Framework.Common.Tasks;

namespace ZLIS.Cos.MobileAPI.Cache
{
    public class ClearObsoleteTask : IBackgroundTask
    {
        public ILogger Logger { get; set; }


        public void Sweep()
        {
            Logger.Debug("Clear obsolete cached object. Total Cached Objects: {0}", AppCacheContainer.GetCachedObjects().Count);
            CosApplicationContext appCtx = CosApplicationContext.Current;

            ISignals signal = appCtx.BackgroundTaskScope.Resolve<ISignals>();
            var removedCacheObjs = AppCacheContainer.ClearObsoleteCachedObjects();

            foreach (var obj in removedCacheObjs)
            {
                Logger.Debug("Remove cached objects for BPID {0}", obj.BpId);
                AppCacheContainer.Trigger(signal, obj.BpId);
            }

            Logger.Debug("Cached object cleared. Count: {0} ", removedCacheObjs.Count());
        }
    }
}