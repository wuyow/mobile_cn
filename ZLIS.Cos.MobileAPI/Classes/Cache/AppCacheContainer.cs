﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Framework.Common.Caching;

namespace ZLIS.Cos.MobileAPI.Cache
{
    public class AppCacheContainer
    {
        private static readonly ConcurrentDictionary<string, CacheObj> _entries =
            new ConcurrentDictionary<string, CacheObj>();

        private static readonly IList<string> _tokens = new List<string>();

        public static void AddCache(CacheObj obj)
        {
            obj.LastAccessTime = DateTime.Now;
            _entries[obj.Token] = obj;
        }

        public static CacheObj Remove(string key)
        {
            if (_entries.ContainsKey(key))
            {
                CacheObj obj;
                _entries.TryRemove(key, out obj);

                return obj;
            }

            return null;
        }

        public static void UpdateCachedObjectActiveDate(string token)
        {
            if (_entries.ContainsKey(token))
            {
                var obj = _entries[token];
                obj.LastAccessTime = DateTime.Now;
                _entries[token] = obj;
            }
        }

        public static CacheObj[] ClearObsoleteCachedObjects()
        {
            IList<CacheObj> removedObjs = new List<CacheObj>();
            var objects = _entries.Values.Where(o => o.LastAccessTime < DateTime.Now.AddHours(-1)).ToArray();

            foreach (var cacheObj in objects)
            {
                CacheObj obj;
                if(_entries.TryRemove(cacheObj.Token, out obj))
                    removedObjs.Add(obj);
            }

            return removedObjs.ToArray();
        }

        public static IDictionary<string, CacheObj> GetCachedObjects()
        {
            return _entries;
        }

        public static void Trigger(ISignals signals, int bpId)
        {
            var tokens =
                _tokens.Where(t => t.StartsWith(string.Format("{0}.{1}", Constants.TOKEN_ACCOUNT_SUMMARY_PREFIX, bpId)));
            foreach (var token in tokens)
            {
                signals.Trigger(token);
            }

            tokens =
                _tokens.Where(t => t.StartsWith(string.Format("{0}.{1}", Constants.TOKEN_CONTRACT_DETAIL_PREFIX, bpId)));
            foreach (var token in tokens)
            {
                signals.Trigger(token);
            }

            tokens =
                _tokens.Where(t => t.StartsWith(string.Format("{0}.{1}", Constants.TOKEN_CUSTOMER_PROFILE_PREFIX, bpId)));
            foreach (var token in tokens)
            {
                signals.Trigger(token);
            }

            tokens =
                _tokens.Where(t => t.StartsWith(string.Format("{0}.{1}", Constants.TOKEN_BANKS_PREFIX, bpId)));
            foreach (var token in tokens)
            {
                signals.Trigger(token);
            }
        }

        public static string GetToken(string prefix, int bpid)
        {
            string token = string.Format("{0}.{1}", prefix, bpid);
            if (!_tokens.Contains(token))
                _tokens.Add(token);

            return token;
        }

        public static string GetToken(string prefix, int bpid, string contractNo)
        {
            string token = string.Format("{0}.{1}.{2}", prefix, bpid, contractNo);
            if(!_tokens.Contains(token))
                _tokens.Add(token);

            return token;
        }


    }
}