﻿using System;
using System.Configuration;
using System.Web.Http;
using ZLIS.Cos.Core.MobileLogin.DomainModel;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.Core.Common;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Compilation;
using System.Globalization;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.DomainModel;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Cos.Core.Account;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.MobileAPI.CountrySpecific;

namespace ZLIS.Cos.MobileAPI
{
    public static class ControllerExtensions
    {
        public static bool CheckSession(this ApiController apiController, MobileLoginInfo loginInfo, out CheckSessionErrorCode errorCode)
        {
            if (!loginInfo.Valid)
            {
                errorCode = CheckSessionErrorCode.HaveBeenLoggedOut;
                return false;
            }

            if (DateTime.Now > loginInfo.ExpiryTime)
            {
                errorCode = CheckSessionErrorCode.SessionTimeOut;
                return false;
            }
            if (loginInfo.CustomerActivity == null)
            {
                errorCode = CheckSessionErrorCode.SessionTimeOut;
                return false;
            }
            if (!loginInfo.CustomerActivity.Customer.ActiveSessionID.Equals(
                loginInfo.CustomerActivity.SessionID))
            {
                errorCode = CheckSessionErrorCode.ForcedLogout;
                return false;
            }

            errorCode = CheckSessionErrorCode.NoError;
            return true;
        }

        public static int LoginExpiryMinutes(this ApiController apiController)
        {
            string strLoginExpiryMinutes;
            ISystemSettingService _settingService = ZLIS.Cos.Core.CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();
            var timeoutsetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_LOGIN,
               SystemConfigurationConstants.KEY_LOGIN_MOBLIEAPITIMEOUTMINUTES);
            if (timeoutsetting != null)
                strLoginExpiryMinutes = timeoutsetting.Value;
            else
                strLoginExpiryMinutes = ConfigurationManager.AppSettings["LoginExpiryMinutes"];

            int loginExpiryMinutes = 30;
            if (!string.IsNullOrEmpty(strLoginExpiryMinutes))
            {
                int.TryParse(strLoginExpiryMinutes, out loginExpiryMinutes);
            }

            return loginExpiryMinutes;
        }

        public static string SoaHeadingField(this ApiController apiController)
        {
            string strSoaHeadingField = ConfigurationManager.AppSettings["SOA.HeadingField"];

            return strSoaHeadingField;
        }

        public static string RepaymentPlanHeadingField(this ApiController apiController)
        {
            string strRepaymentPlanHeadingField = ConfigurationManager.AppSettings["RepaymentPlan.HeadingField"];

            return strRepaymentPlanHeadingField;
        }

        public static string ReceiptsHeadingField(this ApiController apiController)
        {
            string strReceiptsHeadingField = ConfigurationManager.AppSettings["Receipts.HeadingField"];

            return strReceiptsHeadingField;
        }

        public static DateTime Now(this ApiController controller)
        {
            return TimeHelper.Now;
        }

        private static string GetResourceString(HttpContextBase httpContext, string expression, string virtualPath,
                                                object[] args)
        {
            //ISessionState sessionState = CosApplicationContext.Current.Resolve<ISessionState>();
            //System.Globalization.CultureInfo cultureInfo = sessionState.CurrentCulture;
            CultureInfo cultureInfo = new CultureInfo(ConstValue.DEFAULT_CULTURE_NAME);

            ExpressionBuilderContext context = new ExpressionBuilderContext(virtualPath);
            ResourceExpressionBuilder builder = new ResourceExpressionBuilder();
            //ResourceExpressionFields fields =
            //    (ResourceExpressionFields) builder.ParseExpression(expression, typeof (string), context);

            string classKey = "";
            string resourceKey = "";

            if (expression.Contains(","))
            {
                classKey = expression.Split(',')[0].Trim();
                resourceKey = expression.Split(',')[1].Trim();
            }
            else
            {
                resourceKey = expression;
            }


            if (!string.IsNullOrEmpty(classKey))
            {
                if (args != null && args.Count() > 0)
                    return string.Format((string)httpContext.GetGlobalResourceObject(classKey, resourceKey, cultureInfo), args);
                else
                    return (string)httpContext.GetGlobalResourceObject(classKey, resourceKey, cultureInfo);
            }

            return
                string.Format(
                    (string)
                    httpContext.GetLocalResourceObject(virtualPath, resourceKey, cultureInfo),
                    args);
        }

        public static void GetDisplayValue(this ApiController controller, IPageResourceService pageResourceService,
            ISystemSettingService settingService,
            ILanguageService languageService,
            ISpecialLogic specialLogic,
            object obj, Dictionary<string, PropertyInfo> properties,
            string propertyName, string propertyFormat,
            bool overdue, bool showLink,
            string contractId, out string value, out string valueLoc,
            bool? individual = null, int position = 1, bool hideGiroStatus = false)
        {
            bool enableLocalizedText = EnableLocalizedText(controller, settingService, languageService);
            CultureInfo cultureInfo = new CultureInfo(ConstValue.DEFAULT_CULTURE_NAME);
            CultureInfo cultureInfoLoc = null;
            if (enableLocalizedText)
                cultureInfoLoc = new CultureInfo(GetLanguageCode(controller, settingService, languageService));

            value = null;
            valueLoc = null;
            try
            {
                if (propertyName == "CmsAccountSummary_ContractStatus")
                {
                    propertyName = "CmsAccountSummary_ContractStatusDisplay";
                }
                else if (propertyName == "CmsContractDetail_ContractStatus")
                {
                    propertyName = "CmsContractDetail_ContractStatusDisplay";
                }

                if (propertyName == "CmsBank_GiroStatus" || propertyName == "CmsContractDetail_GiroStatus")
                {
                    if (!hideGiroStatus)
                    {
                        //var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
                        if (obj == null)
                        {
                            //value = "<span><a href='" + urlHelper.Action("MakePayment", "MyAccount",
                            //    new { id = contractId }) + "'>" +
                            //    Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO") + "</a></span>";
                            value = Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO", cultureInfo);
                            if (enableLocalizedText)
                            {
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO", cultureInfoLoc);
                            }
                        }
                        else
                        {
                            string rawValue = properties[propertyName].GetValue(obj, null) as string;
                            value = (FormatHelper.FormatData(settingService, propertyFormat, rawValue));
                            if (string.IsNullOrEmpty(rawValue))
                            {
                                //value = "<span><a href='" + urlHelper.Action("MakePayment", "MyAccount", new { id = contractId }) + "'>" + Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO") + "</a></span>";
                                value = Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO", cultureInfo);
                            }
                            else if (showLink)
                            {
                                //value = string.Format("<span>{0} - <a href='{1}'>" +
                                //    Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO") +
                                //    "</a></span>", value, urlHelper.Action("MakePayment", "MyAccount", new { area = "", id = contractId }));
                                value = string.Format("<span>{0} - " +
                                    Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO", cultureInfo) +
                                    "</span>", value);
                            }
                            else
                            {
                                //value = "Pending".Equals(value, StringComparison.OrdinalIgnoreCase) ||
                                //    "Batched".Equals(value, StringComparison.OrdinalIgnoreCase) ?
                                //    Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_PendingForApproval") : value;
                                value = "Pending".Equals(value, StringComparison.OrdinalIgnoreCase) ||
                                    "Batched".Equals(value, StringComparison.OrdinalIgnoreCase) ?
                                    Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_PendingForApproval", cultureInfo) : value;
                            }

                            if (enableLocalizedText)
                            {
                                string objectPropertyLocal = string.Format("{0}_Local", propertyName);
                                if (properties.ContainsKey(objectPropertyLocal))
                                {
                                    string rawValueLoc = properties[objectPropertyLocal].GetValue(obj, null) as string;
                                    valueLoc = (FormatHelper.FormatData(settingService, propertyFormat, rawValueLoc));
                                    if (string.IsNullOrEmpty(rawValueLoc))
                                    {
                                        //value = "<span><a href='" + urlHelper.Action("MakePayment", "MyAccount", new { id = contractId }) + "'>" + Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO") + "</a></span>";
                                        valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO", cultureInfoLoc);
                                    }
                                    else if (showLink)
                                    {
                                        //value = string.Format("<span>{0} - <a href='{1}'>" +
                                        //    Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO") +
                                        //    "</a></span>", value, urlHelper.Action("MakePayment", "MyAccount", new { area = "", id = contractId }));
                                        valueLoc = string.Format("<span>{0} - " +
                                            Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_ApplyForGIRO", cultureInfoLoc) +
                                            "</span>", valueLoc);
                                    }
                                    else
                                    {
                                        //value = "Pending".Equals(value, StringComparison.OrdinalIgnoreCase) ||
                                        //    "Batched".Equals(value, StringComparison.OrdinalIgnoreCase) ?
                                        //    Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_PendingForApproval") : value;
                                        valueLoc = "Pending".Equals(value, StringComparison.OrdinalIgnoreCase) ||
                                            "Batched".Equals(value, StringComparison.OrdinalIgnoreCase) ?
                                            Resource(pageResourceService, "MyAccount_MyAccount_MakePayment,PageLabel_PendingForApproval", cultureInfoLoc) : valueLoc;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        value = "<span class='HideGiroStatus'>-</span>";
                        if (enableLocalizedText)
                        {
                            valueLoc = "<span class='HideGiroStatus'>-</span>";
                        }
                    }
                }
                else if (obj == null)
                {
                    //value = "<span class='nullobj'>-</span>";
                    //if (enableLocalizedText)
                    //{
                    //    valueLoc = "<span class='nullobj'>-</span>";
                    //}
                    value = "-";
                    if (enableLocalizedText)
                    {
                        valueLoc = "-";
                    }
                }
                else if ((propertyName == "CmsFinancialDetail_OverdueAmount" || (propertyName == "OverdueAmount" && obj.GetType().Name == "CmsFinancialDetail"))
                    || (propertyName == "CmsAccountSummary_AmountOverdue" || (propertyName == "AmountOverdue" && obj.GetType().Name == "CmsAccountSummary"))
                    || (propertyName == "CmsAccountSummary_OverDuePayment" || (propertyName == "OverDuePayment" && obj.GetType().Name == "CmsAccountSummary")))
                {
                    decimal overdueAmount = (decimal)properties[propertyName].GetValue(obj, null);

                    value = FormatHelper.FormatData(settingService, propertyFormat, overdueAmount);
                    //if (overdue || overdueAmount > 0)
                    //{
                    //    value = string.Format("<a href=\"javascript:OpenReport('{0}')\">{1}</a> <span class='warning-sign'></span>", contractId, value);
                    //}
                }
                else if (propertyName == "CmsContractDetail_DealerName" || propertyName == "CmsContractDetail_SalePersonName")
                {
                    value = properties[propertyName].GetValue(obj, null) as string;

                    if (enableLocalizedText)
                    {
                        string objectPropertyLocal = string.Format("{0}_Local", propertyName);
                        valueLoc = properties[objectPropertyLocal].GetValue(obj, null) as string; 
                    }
                }
                else if ((propertyName == "CmsBusinessPartner_FirstName" ||
                    (propertyName == "FirstName" && obj.GetType().Name == "CmsBusinessPartner")) &&
                    individual.HasValue && individual.Value)
                {
                    string FirstName = properties[propertyName].GetValue(obj, null) as string;

                    string MiddleName = propertyName.IndexOf('_') > 0 ?
                        properties["CmsBusinessPartner_MiddleName"].GetValue(obj, null) as string :
                        properties["MiddleName"].GetValue(obj, null) as string;

                    string LastName = propertyName.IndexOf('_') > 0 ?
                        properties["CmsBusinessPartner_LastName"].GetValue(obj, null) as string :
                        properties["LastName"].GetValue(obj, null) as string;

                    value = (string.Format("{0}{1}{2}{3}{4}",
                        FirstName,
                        string.IsNullOrEmpty(FirstName) ? "" : " ",
                        MiddleName, string.IsNullOrEmpty(MiddleName) ? "" : " ",
                        LastName).Trim());

                    if (enableLocalizedText)
                    {
                        string objectPropertyLocal = string.Format("{0}_Local", propertyName);
                        string FirstNameLoc = properties[objectPropertyLocal].GetValue(obj, null) as string;

                        string MiddleNameLoc = propertyName.IndexOf('_') > 0 ?
                            properties["CmsBusinessPartner_MiddleName_Local"].GetValue(obj, null) as string :
                            properties["MiddleName_Local"].GetValue(obj, null) as string;

                        string LastNameLoc = propertyName.IndexOf('_') > 0 ?
                            properties["CmsBusinessPartner_LastName_Local"].GetValue(obj, null) as string :
                            properties["LastName_Local"].GetValue(obj, null) as string;

                        valueLoc = (string.Format("{0}{1}{2}{3}{4}",
                            FirstNameLoc,
                            string.IsNullOrEmpty(FirstNameLoc) ? "" : " ",
                            MiddleNameLoc, string.IsNullOrEmpty(MiddleNameLoc) ? "" : " ",
                            LastNameLoc).Trim());
                    }
                }
                else if ((propertyName == "CmsBusinessPartner_CompanyRegNO" || (propertyName == "FirstName" && obj.GetType().Name == "CmsBusinessPartner")) && individual.HasValue && individual.Value)
                {
                    string companyRegNo = properties[propertyName].GetValue(obj, null) as string;

                    if (string.IsNullOrEmpty(companyRegNo))
                    {
                        companyRegNo = propertyName.IndexOf('_') > 0 ?
                            properties["CmsBusinessPartner_PassportNO"].GetValue(obj, null) as string :
                            properties["PassportNO"].GetValue(obj, null) as string;
                    }

                    value = companyRegNo;

                    if (enableLocalizedText)
                    {
                        string companyRegNoLoc = properties[propertyName + "_Local"].GetValue(obj, null) as string;

                        if (string.IsNullOrEmpty(companyRegNoLoc))
                        {
                            companyRegNoLoc = propertyName.IndexOf('_') > 0 ?
                                properties["CmsBusinessPartner_PassportNO_Local"].GetValue(obj, null) as string :
                                properties["PassportNO_Local"].GetValue(obj, null) as string;
                        }

                        valueLoc = companyRegNoLoc;
                    }
                }
                else if (propertyName == "CmsCustomer_HandPhones")
                {

                }
                else if (propertyName == "CmsCustomerProfile_Emails")
                {

                }
                else if (obj.GetType().GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>)))
                {
                    var objlist = obj as System.Collections.IList;
                    List<string> values = new List<string>();
                    List<string> valuesLoc = new List<string>();
                    foreach (var subObj in objlist)
                    {
                        if (subObj != null)
                        {
                            GetValue(controller, pageResourceService, settingService, languageService,specialLogic, enableLocalizedText, subObj, properties, propertyName, propertyFormat, overdue, showLink, contractId, out value, out valueLoc, individual, position);
                            values.Add(value);

                            if (enableLocalizedText)
                            {
                                valuesLoc.Add(valueLoc);
                            }
                        }
                    }

                    value = string.Join(", ", values);
                    if (enableLocalizedText)
                    {
                        valueLoc = string.Join(", ", valuesLoc);
                    }
                }
                else
                {
                    GetValue(controller, pageResourceService, settingService, languageService,specialLogic, enableLocalizedText, obj, properties, propertyName, propertyFormat,
                        overdue, showLink, contractId, out value, out valueLoc, individual, position);
                }

                 if (propertyName == "CmsContractDetail_ContractStatusDisplay" && specialLogic.ShowLocalStatus())
                {
                    if ((!enableLocalizedText&& cultureInfo.TwoLetterISOLanguageName != ConstValue.DEFAULT_TWO_LETTER_ISO_LANGUAGE_NAME)
                        ||(enableLocalizedText&& cultureInfoLoc.TwoLetterISOLanguageName != ConstValue.DEFAULT_TWO_LETTER_ISO_LANGUAGE_NAME))//local
                    {
                        if (value == ZLIS.Cos.RmwService.CmsContractStatus.Current)
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusCurrent", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusCurrent", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.FlatCancelled )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusFlatCancelled",  cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusFlatCancelled", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.Overdue )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusOverdue", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusOverdue", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.Default )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusDefault", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusDefault", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.Litigation )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusLitigation", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusLitigation", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.EarlyTerminated )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusEarlyTerminated", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusEarlyTerminated", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.ForcedTerminated )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusForcedTerminated", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusForcedTerminated", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.PaidInFull )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusPaidInFull",  cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusPaidInFull", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.DefaultCollectionOfficer )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusDefaultCollectionOfficer",  cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusDefaultCollectionOfficer", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsContractStatus.Payout )
                        {
                            value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusPayout", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusPayout", cultureInfoLoc);
                        }
                    }
                }
                else if (propertyName == "CmsFinancialDetail_PaymentMode" && specialLogic.ShowLocalPaymentMode())
                {

                    if ((!enableLocalizedText && cultureInfo.TwoLetterISOLanguageName != ConstValue.DEFAULT_TWO_LETTER_ISO_LANGUAGE_NAME)
                        || (enableLocalizedText && cultureInfoLoc.TwoLetterISOLanguageName != ConstValue.DEFAULT_TWO_LETTER_ISO_LANGUAGE_NAME))//local
                    {
                        if (value == ZLIS.Cos.RmwService.CmsPaymentMode.EFTCCB )
                        {
                            value = Resource(pageResourceService, "MyAccount_SOA_SOA,PageLabel_EFTCCB", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_SOA_SOA,PageLabel_EFTCCB", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsPaymentMode.EFTICBC )
                        {
                            value = Resource(pageResourceService, "MyAccount_SOA_SOA,PageLabel_EFTICBC", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_SOA_SOA,PageLabel_EFTICBC", cultureInfoLoc );
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsPaymentMode.DealerDeduction )
                        {
                            value = Resource(pageResourceService, "MyAccount_SOA_SOA,PageLabel_DealerDeduction", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_SOA_SOA,PageLabel_DealerDeduction", cultureInfoLoc);
                        }
                        else if (value == ZLIS.Cos.RmwService.CmsPaymentMode.DirectDebit)
                        {
                            value = Resource(pageResourceService, "MyAccount_SOA_SOA,PageLabel_DirectDebit", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_SOA_SOA,PageLabel_DirectDebit", cultureInfoLoc);
                        }
                    }
                }
                 else if (propertyName == "CmsAccountSummary_FinancialProductTitle" && specialLogic.ShowLocalFinancialProduct())
                 {

                     if ((!enableLocalizedText && cultureInfo.TwoLetterISOLanguageName != ConstValue.DEFAULT_TWO_LETTER_ISO_LANGUAGE_NAME)
                        || (enableLocalizedText && cultureInfoLoc.TwoLetterISOLanguageName != ConstValue.DEFAULT_TWO_LETTER_ISO_LANGUAGE_NAME))//local
                     {
                         if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.OpenEndLease )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG1",cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG1", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.OpenEndLeaseMaintenance )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG2", cultureInfo);
                            if(enableLocalizedText)
                                valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG2", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseFL)
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG4", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG4", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMileageplanPCVBFL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG6",  cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG6", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMileagePlanCVMFTBCFL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG8",  cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG8", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMileagePlanCVNotMFTBCFL)
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG9", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG9", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMaintenanceCVMFTBCFL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG12", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG12", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMaintenanceCVNotMFTBCFL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG13", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG13", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.DemoCarLease )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG15", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG15", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CompanyCarFL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG17", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG17", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CompanyCarMaintenanceCVFL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG19", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG19", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.EquipmentLease )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG20", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG20", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG3", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG3", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMileageplanPCVBOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG5", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG5", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMileagePlanCVMFTBCOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG7", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG7", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMileagePlanCVNotMFTBCOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG10", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG10", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMaintenanceCVMFTBCOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG11", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG11", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMaintenanceCVNotMFTBCOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG14", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG14", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CompanyCarOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG16", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG16", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CompanyCarMaintenanceCVOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG18", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG18", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.RentaCar )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG21", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG21", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CarSharing )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG22", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG22", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.KappuIC )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG10001",  cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG10001", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.KappuDP )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG20001", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG20001", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.Loan )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG30001",  cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG30001", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.SaimuBensai )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG20002",cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG20002", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.KappuDPEmployee )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG20003",  cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG20003", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.LoanEmployee )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG30002", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG30002", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMileageplanPCVAOL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG25",  cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG25", cultureInfoLoc);
                         }
                         else if (value == ZLIS.Cos.RmwService.CmsFinancialProductGroup.CloseEndLeaseMileageplanPCVAFL )
                         {
                             value = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG26", cultureInfo);
                             if (enableLocalizedText)
                                 valueLoc = Resource(pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_FPG26", cultureInfoLoc);
                         }
                     }
                 }
                 if (enableLocalizedText)
                 {
                     if (string.IsNullOrEmpty(valueLoc) || string.IsNullOrEmpty(valueLoc.Trim()))
                         valueLoc = value;
                     else if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(value.Trim()))
                         value = valueLoc;
                 }
                 if (value == null)
                     value = "";
                 if (valueLoc == null)
                     valueLoc = "";
            }
            catch (Exception ex)
            {
                //ILogger logger = CosApplicationContext.Current.GetWorkContext().Resolve<ILogger>();
                //logger.Error(ex, "Error occurs while getting display value");
                value = "<span class='erroccured'>-</span>";
                if (enableLocalizedText)
                {
                    valueLoc = "<span class='erroccured'>-</span>";
                }
            }

            //return value;
        }

        private static void GetValue(ApiController controller, IPageResourceService pageResourceService,
            ISystemSettingService settingService, ILanguageService languageService,ISpecialLogic specialLogic, bool enableLocalizedText, object obj,
            Dictionary<string, PropertyInfo> properties, string propertyName, string propertyFormat, bool overdue, bool showLink, string contractId,
            out string value, out string valueLoc, bool? individual = null, int position = 1)
        {
            string[] props = propertyName.Split('_');
            if (props.Length > 1 && (position > 1 || props.Length > 2))
            {
                Dictionary<string, PropertyInfo> nestedProperties = new Dictionary<string, PropertyInfo>();

                var abc = string.Join("_", props.Take(position == 1 ? position + 1 : 1));
                var nestedObj = properties[abc].GetValue(obj, null);

                if (nestedObj == null)
                {
                    //return "<span class='nullnestedobj'>-</span>";
                    value = "<span class='nullnestedobj'>-</span>";
                    valueLoc = null;
                    if (enableLocalizedText)
                    {
                        valueLoc = "<span class='nullnestedobj'>-</span>";
                    }
                }
                else if (nestedObj.GetType().GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>)))
                {
                    var objlist = nestedObj as System.Collections.IList;
                    List<string> values = new List<string>();
                    foreach (var subObj in objlist)
                    {
                        if (subObj != null)
                        {
                            nestedProperties = subObj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => x.Name, x => x);
                            break;
                        }
                    }
                }
                else
                {
                    nestedProperties = nestedObj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => x.Name, x => x);
                }

                GetDisplayValue(controller, pageResourceService, settingService, languageService,specialLogic, nestedObj,
                    nestedProperties, string.Join("_", props.Skip(position == 1 ? position + 1 : 1)), propertyFormat, overdue, showLink, contractId,
                    out value, out valueLoc, individual, position + 1);
            }
            else
            {
                //if (string.IsNullOrEmpty(propertyFormat))
                //{
                //    return (ZLIS.Cos.Web.FormatHelper.FormatData(
                //        propertyFormat,
                //        GetTextValue(htmlHelper, properties, propertyName, obj)));
                //}
                //else
                //{
                //return (FormatHelper.FormatData(settingService,
                //    propertyFormat, properties[propertyName].GetValue(obj, null))
                //    );
                value = (FormatHelper.FormatData(settingService,
                    propertyFormat, properties[propertyName].GetValue(obj, null))
                    );

                valueLoc = null;
                if (enableLocalizedText)
                {
                    string objectPropertyLocal = string.Format("{0}_Local", propertyName);
                    if (properties.ContainsKey(objectPropertyLocal))
                    {
                        object v = properties[objectPropertyLocal].GetValue(obj, null);
                        if (v != null)
                        {
                            valueLoc = (FormatHelper.FormatData(settingService,
                        propertyFormat, properties[objectPropertyLocal].GetValue(obj, null))
                        );
                           
                        }
                    }
                }

                //}
            }
            if (enableLocalizedText)
            {
                if (string.IsNullOrEmpty(valueLoc) || valueLoc == "-")
                    valueLoc = value;
                else if (string.IsNullOrEmpty(value) || value == "-")
                    value = valueLoc;
            }
        }

        private static string Resource(IPageResourceService pageResourceService, string key, CultureInfo cultureInfo)
        {
            string[] arrKey = key.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arrKey.Length == 2)
            {
                return pageResourceService.GetText(arrKey[0], arrKey[1], cultureInfo);
            }

            return null;
        }

        public static string Resource(this ApiController controller, IPageResourceService pageResourceService, string key)
        {
            CultureInfo mobileUiCulture = new CultureInfo(ConstValue.DEFAULT_CULTURE_NAME);

            string[] arrKey = key.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arrKey.Length == 2)
            {
                return pageResourceService.GetText(arrKey[0], arrKey[1], mobileUiCulture);
            }

            return null;
        }

        public static string Resource(this ApiController controller, IPageResourceService pageResourceService, string key, CultureInfo cultureInfo)
        {
            string[] arrKey = key.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (arrKey.Length == 2)
            {
                return pageResourceService.GetText(arrKey[0], arrKey[1], cultureInfo);
            }

            return null;
        }

        public static string GetLanguageCode(this ApiController controller,
            ISystemSettingService _settingService, ILanguageService _languageService)
        {
            string strLanguage = "";
            var language = _languageService.GetLocalLanguage();

            if (language != null)
            {

                strLanguage = string.Format("{0}-{1}", language.PriLan, language.SubLan);
            }

            return strLanguage;
        }

        public static string GetCurrentLanguageCode(this ApiController controller,
            ISystemSettingService _settingService, ILanguageService _languageService, string languageCode)
        {
            if (!string.IsNullOrEmpty(languageCode) && !languageCode.ToUpper().StartsWith("EN"))
            {
                var language = _languageService.GetLocalLanguage();

                if (language != null)
                {
                    string strLanguage = "";

                    strLanguage = string.Format("{0}-{1}", language.PriLan, language.SubLan);

                    if (strLanguage.Equals(languageCode, StringComparison.OrdinalIgnoreCase))
                    {
                        return strLanguage;
                    }
                }
            }

            return GetDefaultLanguageCode(controller, _settingService, _languageService);
        }

        public static bool EnableLocalizedText(this ApiController controller,
            ISystemSettingService _settingService, ILanguageService _languageService)
        {
            var language = GetLanguageCode(controller, _settingService, _languageService);
            if (!string.IsNullOrEmpty(language) &&
                !language.Equals(ConstValue.DEFAULT_CULTURE_NAME))
            {
                return true;
            }

            return false;
        }

        public static string LoginMode(this ApiController controller,
            ISystemSettingService _settingService)
        {
            string loginMode = ConstValue.LOGIN_MODE_EMAIL;
            SystemSettingItem objLoginMode = _settingService.GetSystemSetting(
                SystemConfigurationConstants.CATEGORY_LOGIN,
                SystemConfigurationConstants.KEY_LOGIN_MODE);
            if (objLoginMode != null)
            {
                if (loginMode.Equals(ConstValue.LOGIN_MODE_EMAIL, StringComparison.OrdinalIgnoreCase) ||
                    loginMode.Equals(ConstValue.LOGIN_MODE_NRIC, StringComparison.OrdinalIgnoreCase))
                {
                    loginMode = objLoginMode.Value;
                }
            }

            return loginMode;
        }

        public static Language GetDefaultLanguage(this ApiController controller,
            ISystemSettingService _settingService, ILanguageService _languageService)
        {
            var defaultLanguageSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SYSTEM, SystemConfigurationConstants.KEY_SYSTEM_DEFAULT_LANGUAGE);
            Language defaultLanguage;
            if (defaultLanguageSetting != null)
            {
                if (!string.IsNullOrEmpty(defaultLanguageSetting.Value))
                {
                    string[] arrCodes = defaultLanguageSetting.Value.Split('-');
                    defaultLanguage = _languageService.GetByLanguageCodes(arrCodes[0], arrCodes[1]);
                    if (defaultLanguage == null)
                    {
                        defaultLanguage = _languageService.GetByLanguageCodes("en", "US");
                    }
                }
                else
                {
                    defaultLanguage = _languageService.GetByLanguageCodes("en", "US");
                }
            }
            else
            {
                defaultLanguage = _languageService.GetByLanguageCodes("en", "US");
            }

            return defaultLanguage;
        }

        public static string GetDefaultLanguageCode(this ApiController controller,
            ISystemSettingService _settingService, ILanguageService _languageService)
        {
            var language = GetDefaultLanguage(controller, _settingService, _languageService);

            return string.Format("{0}-{1}", language.PriLan, language.SubLan);
        }

        public static string GetEmailAddress(this ApiController controller, string email)
        {
            try
            {
                if (ConfigurationManager.AppSettings["TestEmailAddress"] != null)
                    return ConfigurationManager.AppSettings["TestEmailAddress"].ToString();
            }
            catch
            {
            }

            return email;
        }

        public static string GetDateFormat(this ApiController controller,
            ISystemSettingService _settingService)
        {
            var dateFormat = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY,
                SystemConfigurationConstants.KEY_DATE_FORMAT);
            if (dateFormat != null)
            {
                return dateFormat.Value;
            }

            return null;
        }

        public static string GetCountryCode(this ApiController controller,
            ISystemSettingService _settingService)
        {
            var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY,
                SystemConfigurationConstants.KEY_COUNTRY_CODE);
            if (countryCode != null)
            {
                return countryCode.Value;
            }

            return null;
        }

        public static string GetDateTimeFormat(this ApiController controller,
            ISystemSettingService _settingService)
        {
            var dateTimeFormat = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATETIME_FORMAT);
            if (dateTimeFormat != null)
            {
                return dateTimeFormat.Value;
            }

            return null;
        }

        public static bool IsPassOtp(this ApiController controller, ISystemSettingService _settingService, MobileLoginInfo logininfo,
            string settingkey, string id, string type)
        {
            var otpsetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, settingkey);
            bool passotp = true;
            if (otpsetting != null && otpsetting.Value.ToLower() == "true")
            {
                passotp = logininfo.OtpValid ?? false;
            }
            return passotp;
        }

    }
}
