﻿using System;
using System.Collections.Generic;
using System.IO;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.Const;

namespace ZLIS.Cos.MobileAPI
{
    public class MailerHelper
    {
        public static string GetMailContent(EmailTemplate template, Language lan, string messageid, string baseurl, string templatefolder, IDictionary<string, string> replacements)
        {
            try
            {
                string strcontent = string.Empty;
                //string path =
                //   string.Format(templatefolder, 
                //   lan.PriLan + "-" + lan.SubLan, template.TemplatePath, template.TemplateFileName);
                string path = path = string.Format("{0}\\{1}\\{2}\\{3}", templatefolder,
                        lan.PriLan + "-" + lan.SubLan, template.TemplatePath,
                                         template.TemplateFileName);
                if (!File.Exists(path))
                    path = string.Format("{0}\\{1}\\{2}\\{3}", templatefolder,
                        ConstValue.DEFAULT_CULTURE_NAME/*"en-US"*/, template.TemplatePath,
                                         template.TemplateFileName);

                byte[] content = System.IO.File.ReadAllBytes(path);
                using (var ms = new MemoryStream(content))
                {
                    var sr = new StreamReader(ms);
                    strcontent = sr.ReadToEnd();
                }
                //if (!string.IsNullOrEmpty(strcontent))
                //{
                //    string serverimagepath = baseurl + "/EmailReader/GetEmailImages?templateId=" + template.Id.ToString() + "&image=";
                //    strcontent = strcontent.Replace("{GetEmailImage}", serverimagepath);
                //}
                strcontent = ReplaceMailTextString(strcontent, replacements);

                return strcontent;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public static string ReplaceMailTextString(string strtext, IDictionary<string, string> replacements)
        {
            if (replacements != null)
            {
                foreach (var replacement in replacements)
                {
                    strtext = ReplaceNoCase(strtext, replacement.Key, replacement.Value);// strtext.Replace(replacement.Key, replacement.Value);
                }
            }
            return strtext;
        }

        private static string ReplaceNoCase(string instr, string oldstr, string newstr)
        {
            //// temporarily disabled Regex - Pyisoe (21 Mar 2013)
            //System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(oldstr, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            //return regex.Replace(instr, newstr);


            string output;
            output = instr;

            // lowercase-versions to search in.
            string input_lower;
            string oldone_lower;
            input_lower = instr.ToLower();
            oldone_lower = oldstr.ToLower();

            // search in the lowercase versions,
            // replace in the original-case version.
            int pos;
            pos = input_lower.IndexOf(oldone_lower);

            while (pos != -1)
            {

                // need for empty "newstr" cases.
                input_lower = input_lower.Remove(pos, oldstr.Length);
                input_lower = input_lower.Insert(pos, newstr);

                // actually replace.
                output = output.Remove(pos, oldstr.Length);
                output = output.Insert(pos, newstr);

                pos = input_lower.IndexOf(oldone_lower);
            }

            return output;
        }
    }
}