﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web;
using System.Web.Http.WebHost;
using System.Web.SessionState;
using Autofac;
using Autofac.Integration.WebApi;
using ZLIS.Cos.Core;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Framework.Common;
using ZLIS.Cos.Core.Formatter;
using ZLIS.Cos.PdfGenerator;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Tasks;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Framework.Common.FileSystems.LockFile;
using ZLIS.Framework.Common.FileSystems.VirtualPath;
using ZLIS.Framework.Common.Logging;

namespace ZLIS.Cos.MobileAPI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        private static CosApplicationContext _cosContext;
        private static IContainer _container;

        /// <summary>
        /// Gets the container.
        /// </summary>
        public IContainer Container
        {
            get { return _container; }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Start()
        {
            var builder = new ContainerBuilder();
            WorkContext.WorkScopeTag = AutofacWebApiDependencyResolver.ApiRequestTag;

            
            //CosApplicationContext.Register(builder);
            Register(builder);

            _container = builder.Build();

            // Create a nested lifetime scope for the web app that registers
            // things as InstancePerHttpRequest, etc. Pass that scope
            // as the basis for the MVC dependency resolver.
            var webScope = _container.BeginLifetimeScope(
              b => b.RegisterType<NhUnitOfWorkAccessor>()
                   .As<IUnitOfWorkAccessor>()
                   .InstancePerMatchingLifetimeScope(WorkContext.WorkScopeTag));
            
            AutofacWebApiDependencyResolver dependencyResolver = new AutofacWebApiDependencyResolver(webScope);
            _cosContext = new CosApplicationContext(dependencyResolver, CacheOption.None);

            GlobalConfiguration.Configuration.DependencyResolver = dependencyResolver;

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());

            ISweepGenerator sweepGenerator = _cosContext.ApplicationScope.Resolve<ISweepGenerator>();
            sweepGenerator.Activate();

        }

        protected void Application_PostAuthorizeRequest()
        {
            // WebApi SessionState
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));
            if (routeData != null && routeData.RouteHandler is HttpControllerRouteHandler)
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        }

        public void Register(ContainerBuilder builder)
        {
            builder.RegisterModule(new LoggingModule());
            builder.RegisterModule(new CacheModule());

            builder.Register(ctx => new WorkContext(ctx.Resolve<IComponentContext>())).InstancePerApiRequest();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiModelBinderProvider();

            //Assembly assembly = Assembly.GetAssembly(typeof(WorkContext));
            //builder.RegisterAssemblyModules(assembly);

            //RegisterDependencies(assembly, builder);

            Assembly assembly = Assembly.GetAssembly(typeof(CosApplicationContext));
            builder.RegisterAssemblyModules(assembly);
            RegisterDependencies(assembly, builder);

            builder.RegisterType<CacheHolder>().As<ICacheHolder>().SingleInstance();
            builder.RegisterType<DefaultCacheContextAccessor>().As<ICacheContextAccessor>().SingleInstance();
            builder.RegisterType<DefaultParallelCacheContext>().As<IParallelCacheContext>().SingleInstance();
            builder.RegisterType<DefaultAsyncTokenProvider>().As<IAsyncTokenProvider>().SingleInstance();
            builder.RegisterType<AppDataFolderRoot>().As<IAppDataFolderRoot>().SingleInstance();

            RegisterVolatileProvider<AppDataFolder, IAppDataFolder>(builder);
            RegisterVolatileProvider<DefaultLockFileManager, ILockFileManager>(builder);
            RegisterVolatileProvider<Clock, IClock>(builder);
            RegisterVolatileProvider<Signals, ISignals>(builder);
            RegisterVolatileProvider<DefaultVirtualPathMonitor, IVirtualPathMonitor>(builder);
            builder.RegisterType<SweepGenerator>().As<ISweepGenerator>().SingleInstance();

            var cmsDomainObjectsProxy = new ProxyBuilder(new[] { new DomainObjectInterceptor() });
            builder.RegisterType<RmwServiceContext>().As<IRmwServiceContext>();

            builder.Register(
               c =>
               new RmwServiceAdaptor(
                   new RmwServiceImpl(c.Resolve<IRmwServiceContext>(),
                                      RmwService.RmwSpecContextFactory.CreateRmwSpecContext(),
                                      cmsDomainObjectsProxy),
                   c.Resolve<ISignals>()))
                  .As<IRmwService>().InstancePerLifetimeScope();


            builder.RegisterType<ZLIS.Cos.Core.Formatter.StringFormatter>().As<IStringFormatter>().InstancePerLifetimeScope();
            builder.Register<ReportContext>(c => new ReportContext()).InstancePerLifetimeScope();
            builder.Register<ReportContext>(c => new ReportContext(c.Resolve<IStringFormatter>(), c.Resolve<IRmwService>())).InstancePerLifetimeScope();
            builder.RegisterType<ApiSessionState>().As<ISessionState>().SingleInstance();
            builder.RegisterType<SmsSender>().As<ISmsSender>().InstancePerLifetimeScope();

            builder.Register((context, parameters) =>
            {
                IList<IBackgroundTask> tasks = new List<IBackgroundTask>();
                tasks.Add(new ClearObsoleteTask());
                return new BackgroundService(tasks);
            }).As<IBackgroundService>().SingleInstance();
        }

        private void RegisterDependencies(Assembly assembly, ContainerBuilder builder)
        {
            var dependencies = assembly.GetTypes().Where(t => typeof(IDependency).IsAssignableFrom(t) && t.IsClass);

            foreach (var item in dependencies)
            {
                var registration = builder.RegisterType(item).InstancePerLifetimeScope();

                foreach (
                    var interfaceType in
                        item.GetInterfaces().Where(itf => typeof(IDependency).IsAssignableFrom(itf)))
                {
                    if (interfaceType != typeof(ISessionState))
                        registration = registration.As(interfaceType);

                    if (typeof(ISingletonDependency).IsAssignableFrom(interfaceType) && interfaceType != typeof(ISessionState))
                    {
                        //shell scope
                        registration = registration.SingleInstance();
                    }
                    else if (typeof(IUnitOfWorkDependency).IsAssignableFrom(interfaceType))
                    {
                        //If it is unitofwork dependency, use Autofac http request scope
                        registration = registration.InstancePerMatchingLifetimeScope(WorkContext.WorkScopeTag);
                    }
                    else if (typeof(ITransientDependency).IsAssignableFrom(interfaceType))
                    {
                        registration = registration.InstancePerDependency();
                    }
                }
            }
        }

        private void RegisterVolatileProvider<TRegister, TService>(ContainerBuilder builder) where TService : IVolatileProvider
        {
            builder.RegisterType<TRegister>()
                .As<TService>()
                .As<IVolatileProvider>()
                .SingleInstance();
        }
    }
}