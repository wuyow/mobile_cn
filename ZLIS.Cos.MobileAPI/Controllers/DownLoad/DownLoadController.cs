﻿using System;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.Modules.FormManagement.Services;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Framework.Common.Logging;

namespace ZLIS.Cos.MobileAPI.Controllers.DownLoad
{
    [ActionAuthorizeFilter]
    public class DownLoadController : ApiController
    {
        public ILogger Logger { get; set; }
        private IFormService _formService;

        public DownLoadController(IFormService formService)
        {
            Logger = NullLogger.Instance;
            _formService = formService;
        }

        [System.Web.Http.HttpPost]
        public DownloadGiroFormResponse DownloadGiroForm(DownloadGiroFormRequest request)
        {
            if (request == null)
                request = new DownloadGiroFormRequest();
            Logger.Debug(string.Format("-------- DownloadGiroForm API started. token:{0} ", request.token ?? string.Empty));
            var response = new DownloadGiroFormResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)DownloadGiroFormErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;
            try
            {
                int giroFormId = 24;
                string rawId = System.Configuration.ConfigurationManager.AppSettings["GiroFormId"];
                if (!string.IsNullOrEmpty(rawId))
                {
                    if (!Int32.TryParse(rawId, out giroFormId))
                    {
                        giroFormId = 24;
                    }
                }
                var form = _formService.GetById(giroFormId);
                if (form != null)
                {
                    FileContent fileContent = new FileContent();
                    if (form.MediaResource != null)
                    {
                        fileContent.fileName = form.MediaResource.MediaFileName;
                        fileContent.fileSize = form.MediaResource.Content.Length;
                        fileContent.mimeType = form.MediaResource.MimeType;
                        fileContent.content = Convert.ToBase64String(form.MediaResource.Content);
                    }
                    response.data = fileContent;
                }
                else {
                    errorInfo.code = (int)DownloadGiroFormErrorCode.InvaildGiroForm;
                    errorInfo.message = "Giro form not available";
                
                }
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)DownloadGiroFormErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when download giro form");
            }

            return response;
        }
    }

}
