﻿using System;
using System.Configuration;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Account;
using ZLIS.Cos.Core.Account.DomainModel;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.DomainModel;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using System.Linq;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    public class GetOtpCodeAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerAccountService _accService;
        private ISystemSettingService _settingService;

        private IRmwService _rmwService;
        private ISmsSender _smsSender;

        public ILogger Logger { get; set; }

        public GetOtpCodeAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                       IMobileLoginInfoService mobileLoginInfoService,
                                       ICustomerAccountService accService,
                                       ISystemSettingService settingService,
                                       IRmwService rmwService,
                                       ISmsSender smsSender,
                                       ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;

            _mobileLoginInfoService = mobileLoginInfoService;
            _accService = accService;
            _settingService = settingService;

            _rmwService = rmwService;
            ((RmwServiceAdaptor) _rmwService).SetCacheManager(cacheManager);

            _settingService.Cachable = false;
            _smsSender = smsSender;
        }

        [System.Web.Http.HttpPost]
        public GetOtpCodeResponse GetOtpCode(GetOtpCodeRequest request)
        {
            if (request == null)
                request = new GetOtpCodeRequest();
            Logger.Debug(string.Format("-------- GetOtpCode API started. token:{0} type:{1} mobileNo{2}", request.token ?? string.Empty, request.type ?? string.Empty, request.mobileNo ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetOtpCodeResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetOtpCodeErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) || string.IsNullOrEmpty(request.type))
                {
                    errorInfo.code = (int)GetOtpCodeErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                OtpType otype = OtpType.L;
                switch (request.type.ToLower())
                {
                    case "registration":
                        otype = OtpType.R;
                        break;
                    case "forgotpassword":
                        otype = OtpType.P;
                        break;
                    case "forgotusername":
                        otype = OtpType.U;
                        break;
                    case "soa":
                        otype = OtpType.S;
                        break;
                    case "repaymentplan":
                        otype = OtpType.E;
                        break;
                    default:
                        otype = OtpType.L;
                        break;
                }
                //create otp
                SystemSettingItem setting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_OTPEXPIRY);
                TimeSpan timeSpan = new TimeSpan(0, 0, GetOtpExpiry(setting));
                CustomerOtp customerOtp = null;
                //TODO:Need pass select language to get loc setting
                var otpMessageSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_OTPSETTING_OTPMESSAGE);
                int bpid = 0;
                
                if (request.type.ToLower() != "registration" && request.type.ToLower() != "forgotpassword" && request.type.ToLower() != "forgotusername")
                {
                    var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                    if (loginInfo == null)
                    {
                        errorInfo.code = (int)GetOtpCodeErrorCode.InvalidToken;
                        errorInfo.message = "Invalid token";

                        return response;
                    }

                    
                    CheckSessionErrorCode errorCode;
                    if (!this.CheckSession(loginInfo, out errorCode))
                    {
                        switch (errorCode)
                        {
                            case CheckSessionErrorCode.HaveBeenLoggedOut:
                                errorInfo.code = (int)GetOtpCodeErrorCode.HaveBeenLoggedOut;
                                errorInfo.message = "Have been logged out";
                                break;
                            case CheckSessionErrorCode.SessionTimeOut:
                                errorInfo.code = (int)GetOtpCodeErrorCode.SessionTimeout;
                                errorInfo.message = "Session timeout";
                                break;
                            case CheckSessionErrorCode.ForcedLogout:
                                errorInfo.code = (int)GetOtpCodeErrorCode.ForcedLogout;
                                errorInfo.message = "Forced logout";
                                break;
                        }

                        return response;
                    }

                    var customer = loginInfo.CustomerActivity.Customer;

                    //DateTime now = this.Now();

                    using (ITransaction tran = _unitOfWorkAccessor.GetUnitOfWork().BeginTransaction())
                    {
                        customerOtp = _accService.CreateCustomerOtp(
                            customer.Bpid,
                            Guid.NewGuid().ToString(), request.mobileNo, timeSpan, otype);
                        tran.Commit();
                    }

                    //string sms = string.Format(otpMessageSetting.Value, 
                    //OtpTypeName.Login, customerOtp.Pin, 
                    //FormatHelper.FormatData("DATETIME", customerOtp.CreateDate.ToCosLocalTime()), 
                    //GetOtpExpiry(setting));

                    OtpCode otpCode = new OtpCode();
                    otpCode.otpCode = customerOtp.Pin;
                    otpCode.expiryDate = FormatHelper.FormatData(_settingService, "DATETIME",
                        customerOtp.CreateDate.AddSeconds(GetOtpExpiry(setting)));

                    response.data = otpCode;
                    bpid = customer.Bpid;
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        loginInfo.LastActionTime = DateTime.Now;
                        loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                        _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                        tran.Commit();
                    }
                }
                else
                {
                    var loginfo = _mobileLoginInfoService.GetByToken(request.token);
                    if (loginfo == null)
                    {
                        errorInfo.code = (int)GetOtpCodeErrorCode.InvalidToken;
                        errorInfo.message = "Invalid session";
                        return response;
                    }
                    else
                    {
                        using (ITransaction tran = _unitOfWorkAccessor.GetUnitOfWork().BeginTransaction())
                        {
                            customerOtp = _accService.CreateCustomerOtp(
                                loginfo.BpId ?? 0,
                                request.token, request.mobileNo, timeSpan, otype);
                            tran.Commit();
                        }
                        OtpCode otpCode = new OtpCode();
                        otpCode.otpCode = customerOtp.Pin;
                        otpCode.expiryDate = FormatHelper.FormatData(_settingService, "DATETIME",
                            customerOtp.CreateDate.AddSeconds(GetOtpExpiry(setting)));

                        response.data = otpCode;
                        bpid = loginfo.BpId ?? 0;
                    }
                }
                bool bIsOTPTesting = false;
                if (ConfigurationManager.AppSettings["IsOtpTesting"] != null)
                    bIsOTPTesting = bool.Parse(ConfigurationManager.AppSettings["IsOtpTesting"].ToString());
                
                if (!bIsOTPTesting)
                {
                    var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_COUNTRY_CODE);
                    if (countryCode != null)
                    {
                        var profile = _rmwService.GetCustomerProfile(bpid);

                        CmsPhone selectedHandphone = (from a in profile.PhoneNOs
                                                      where a.PhoneNO == request.mobileNo
                                                      select a).FirstOrDefault();
                        
                        if (selectedHandphone == null)
                        {
                            selectedHandphone = new CmsPhone();
                            selectedHandphone.PhoneNO = request.mobileNo;
                            selectedHandphone.CountryCode = countryCode.Value;
                        }
                        
                        string sms = string.Format(otpMessageSetting.Value, OtpTypeName.ForgotUsername, customerOtp.Pin, FormatHelper.FormatData("DATETIME", customerOtp.CreateDate), GetOtpExpiry(setting));
                        string success = _smsSender.GetProvider(countryCode.Value).SendSMS(GetHandphoneForOTP(selectedHandphone), sms, bpid.ToString(), false);
                        
                        if (success == "false")
                        {
                            errorInfo.code = (int)GetOtpCodeErrorCode.SendSMSFailed;
                            errorInfo.message = "Send otp by sms failed";
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetOtpCodeErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get otp code");
            }

            return response;
        }

        public CmsPhone GetHandphoneForOTP(CmsPhone handphone)
        {
            try
            {
                if (ConfigurationManager.AppSettings["OTPTestMobileNumber"] != null)
                {
                    handphone.PhoneNO = ConfigurationManager.AppSettings["OTPTestMobileNumber"].ToString();
                    handphone.CountryCode = "";
                }
            }
            catch
            {
            }

            return handphone;
        }

        private int GetOtpExpiry(SystemSettingItem setting)
        {
            int expiry = 100;
            try
            {
                expiry = int.Parse(setting.Value);
            }
            catch
            {
            }

            return expiry;
        }

        [System.Web.Http.HttpPost]
        public ValidateOtpCodeResponse ValidateOtpCode(ValidateOtpCodeRequest request)
        {
            Logger.Debug(string.Format("-------- ValidateOtpCode API started. token:{0} otpPin:{1} type{2}", request.token, request.otpPin, request.type));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new ValidateOtpCodeResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)OtpValidateErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.type) ||
                    string.IsNullOrEmpty(request.otpPin))
                {
                    errorInfo.code = (int)OtpValidateErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                var loginfo = _mobileLoginInfoService.GetByToken(request.token);
                if (loginfo == null)
                {
                    errorInfo.code = (int)OtpValidateErrorCode.InvalidToken;
                    errorInfo.message = "Invalid session";

                    return response;
                }
                OtpStatus status = OtpStatus.Successful;
                OtpType otype = OtpType.L;
                switch (request.type.ToLower())
                {
                    case "registration":
                        otype = OtpType.R;
                        break;
                    case "forgotpassword":
                        otype = OtpType.P;
                        break;
                    case "forgotusername":
                        otype = OtpType.U;
                        break;
                    case "soa":
                        otype = OtpType.S;
                        break;
                    case "repaymentplan":
                        otype = OtpType.E;
                        break;
                    default:
                        otype = OtpType.L;
                        break;
                }
                if (request.type.ToLower() == "registration" || request.type.ToLower() == "forgotpassword" || request.type.ToLower() == "forgotusername")
                {
                    status = _accService.AuthenticateOTP(loginfo.BpId ?? 0, otype, request.otpPin);
                }
                else
                {
                    status = _accService.AuthenticateOTP(loginfo.CustomerActivity.Customer.Bpid, otype, request.otpPin);
                }
                if (status == OtpStatus.Successful)
                {
                    loginfo.OtpValid = true;
                }
                else if(status==OtpStatus.OtpTimeOut)
                {
                    loginfo.OtpValid = false;
                    errorInfo.code = (int)OtpValidateErrorCode.Expired;
                    errorInfo.message = "OTP Code Expired";
                }
                else
                {
                    loginfo.OtpValid = false;
                    errorInfo.code = (int)OtpValidateErrorCode.InvalidOtpCode;
                    errorInfo.message = "Invalid OTP code";
                }
                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginfo);

                    tran.Commit();
                }
                
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)OtpValidateErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when validate otp code");
            }

            return response;
        }

    }
}
