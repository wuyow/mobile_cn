﻿using System;
using System.Linq;
using System.Web.Http;
using ZLIS.Framework.Common;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Framework.Common.Logging;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.ClientModel.Models;
using System.Configuration;

namespace ZLIS.Cos.MobileAPI.Controllers.StaticPage
{
    public class StaticPageAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private IMenuSettingService _menuService;
        private IMenuPageService _menuPageService;
        public ILogger Logger { get; set; }
        private IMobileLoginInfoService _mobileLoginInfoService;

        public StaticPageAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                       IMobileLoginInfoService mobileLoginInfoService,
                                       IMenuSettingService menuService,
                                       IMenuPageService menuPageService)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _menuService = menuService;
            _menuPageService = menuPageService;
            _mobileLoginInfoService = mobileLoginInfoService;
        }

        [System.Web.Http.HttpPost]
        public GetStaticPageResponse GetStaticPage(GetStaticPageRequest request)
        {
            if (request == null)
                request = new GetStaticPageRequest();
            Logger.Debug(string.Format("-------- GetStaticPage API started. token:{0} country:{1} pageName{2}", request.token ?? string.Empty, request.cc ?? string.Empty, request.pageName ?? string.Empty));
            
            var response = new GetStaticPageResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetStaticPageErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;
            try
            {
                if (string.IsNullOrEmpty(request.pageName)||
                    string.IsNullOrEmpty(request.cc))
                {
                    errorInfo.code = (int)GetStaticPageErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";
                    return response;
                }
                Core.UIManagement.DomainModel.MenuSetting menu=null;
                if(request.pageName=="About_myMBFS")
                {
                    Guid mid=Guid.NewGuid();
                    Guid.TryParse(ConfigurationManager.AppSettings["Page_AboutMBFS_ID"], out mid);
                    menu = _menuService.GetMenuSetting(mid);
                }
                else
                    menu= _menuService.GetMenuSettingByTitle(request.pageName);
                if (menu == null)
                {
                    errorInfo.code = (int)GetStaticPageErrorCode.InvalidPageName;
                    errorInfo.message = "Invalid page name";

                    return response;
                }
                if (!menu.Public)
                {
                    var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                    if (loginInfo == null)
                    {
                        errorInfo.code = (int)GetStaticPageErrorCode.InvalidToken;
                        errorInfo.message = "Invalid token";

                        return response;
                    }

                    CheckSessionErrorCode errorCode;
                    if (!this.CheckSession(loginInfo, out errorCode))
                    {
                        switch (errorCode)
                        {
                            case CheckSessionErrorCode.HaveBeenLoggedOut:
                                errorInfo.code = (int)GetStaticPageErrorCode.ForcedLogout;
                                errorInfo.message = "Have been logged out";
                                break;
                            case CheckSessionErrorCode.SessionTimeOut:
                                errorInfo.code = (int)GetStaticPageErrorCode.SessionTimeout;
                                errorInfo.message = "Session timeout";
                                break;
                            case CheckSessionErrorCode.ForcedLogout:
                                errorInfo.code = (int)GetStaticPageErrorCode.ForcedLogout;
                                errorInfo.message = "Forced logout";
                                break;
                        }

                        return response;
                    }
                }
                var menupages = _menuPageService.GetMenuPagesByMenuId(menu.Id).ToList();
                if (menupages != null&&menupages.Count==3)
                {
                    var pageinfo = new ClientModel.Models.StaticPageInfo();
                    pageinfo.title = menupages[0].ControlContent;
                    pageinfo.titleLoc = menupages[0].ControlContentLoc == null ? string.Empty : (menupages[0].ControlContentLoc.Text == null ? menupages[0].ControlContentLoc.BigText : menupages[0].ControlContentLoc.Text);
                    pageinfo.bodyContent = menupages[2].ControlContent;
                    pageinfo.bodyContentLoc = menupages[2].ControlContentLoc == null ? string.Empty : (menupages[2].ControlContentLoc.Text == null ? menupages[2].ControlContentLoc.BigText : menupages[2].ControlContentLoc.Text);
                    FileContent fileContent = new FileContent();
                    if (menupages[1].MediaResource != null)
                    {
                        fileContent.fileName = menupages[1].MediaResource.MediaFileName;
                        fileContent.fileSize = menupages[1].MediaResource.Content.Length;
                        fileContent.mimeType = menupages[1].MediaResource.MimeType;
                        fileContent.content = Convert.ToBase64String(menupages[1].MediaResource.Content);
                    }
                    pageinfo.image = fileContent;
                    response.data = pageinfo;
                }
               
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetStaticPageErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get static page");
            }

            return response;
        }
    }
}
