﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.UIManagement;
using ZLIS.Cos.Core.UIManagement.DomainModel;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.CountrySpecific;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class DirectDebitAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IPageResourceService _pageResourceService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        private IModuleSettingService _msService;

        private ISpecialLogic _specialLogic;

        private bool _enableLocalizedText;

        public ILogger Logger { get; set; }

        public DirectDebitAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                        ILanguageService languageService,
                                        ISystemSettingService settingService,
                                        IPageResourceService pageResourceService,
                                        IMobileLoginInfoService mobileLoginInfoService,
                                        IRmwService rmwService,
                                        IModuleSettingService msService,
                                        ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _pageResourceService = pageResourceService;
            _mobileLoginInfoService = mobileLoginInfoService;

            _rmwService = rmwService;
            ((RmwServiceAdaptor) _rmwService).SetCacheManager(cacheManager);

            _msService = msService;
            _specialLogic = SpecialLogicFactory.GetSpecialLogic(this.GetCountryCode(_settingService));
            _enableLocalizedText = this.EnableLocalizedText(_settingService, _languageService);
        }

        [System.Web.Http.HttpPost]
        public GetDirectDebitInfoResponse DirectDebitInfo(GetDirectDebitInfoRequest request)
        {
            if (request == null)
                request = new GetDirectDebitInfoRequest();
            Logger.Debug(string.Format("-------- DirectDebitInfo API started. token:{0} contractNo:{1}", request.token ?? string.Empty, request.contractNo ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetDirectDebitInfoResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetDirectDebitInfoErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo))
                {
                    errorInfo.code = (int)GetDirectDebitInfoErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);

                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }

                response.data = GetDirectDebitInfo(request.contractNo, loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetDirectDebitInfoErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get direct debit info");
            }

            return response;
        }

        private DirectDebit GetDirectDebitInfo(string contractNo,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            DirectDebit directDebit = new DirectDebit();

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == contractNo).FirstOrDefault();
            CmsContractDetail detail = GetContractDetails(summary.BpId, summary.ContractNO);

            IList<CmsBank> bankList = rmwServiceAdaptor.GetBanks(customer.Bpid);
            if (bankList != null)
            {
                int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;
                var lstModuleSetting = _msService.GetModuleSettings_NoCache(ModuleSettingConstants.MAKE_PAYMENT,
                    ModuleSettingConstants.FILTER_TYPE_CONTRACT_STATUS, summary.ContractStatusCode, fpgId).Where(x => x.Visible).OrderBy(x => x.Order).ToList();

                CmsBank bank = bankList.Where(x => x.ContractNO == summary.ContractNO).FirstOrDefault();
                var showDetail = _specialLogic.ShowBankDetail(detail.FinancialDetail.PaymentMode, detail.FinancialDetail.PaymentCode, detail.GiroStatusCode, bank);

                directDebit.showBankInfo = showDetail;

                if (lstModuleSetting != null && lstModuleSetting.Any())
                {
                    var ms = lstModuleSetting.First();
                    directDebit.sectionTitle = ms.Name;
                    directDebit.sectionTitleLocid = _enableLocalizedText ? (ms.NameLoc != null ? ms.NameLoc.Id.ToString() : null) : null;
                        //(ms.NameLoc != null ? ms.NameLoc.Id : (int?)null) : (int?)null;
                }
                else
                {
                    directDebit.sectionTitle = "Bank Information";
                }

                directDebit.bankInfo =
                    CNRT_DETAIL(customer, ModuleSettingConstants.MAKE_PAYMENT_PAYMENT_INFO, contractNo, null);

                directDebit.languageContent = _enableLocalizedText ?
                    GetLanguageContent(contractNo, customer) : null;
            }
            
            return directDebit;
        }

        private Field[] CNRT_DETAIL(ZLIS.Cos.Core.Account.DomainModel.Customer customer,
            string module, string ContractNo/*, string Title*/, CmsAsset vehicle)
        {
            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            var summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == ContractNo).First();

            int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

            IEnumerable<GridModuleSetting> details = _msService.GetGridModuleSettings_NoCache(module, fpgId)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();
            if (details.Count() > 0)
            {
                CmsContractDetail conDetail = GetContractDetails(customer.Bpid, ContractNo);
                CmsBank bank = rmwServiceAdaptor.GetBanks(customer.Bpid)
                    .Where(x => x.ContractNO == ContractNo).FirstOrDefault();

                if (conDetail != null)
                {
                    Dictionary<string, PropertyInfo> conDetailProperties = typeof(CmsContractDetail).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsContractDetail_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> finDetailProperties = typeof(CmsFinancialDetail).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsFinancialDetail_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> accSummaryProperties = typeof(CmsAccountSummary).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsAccountSummary_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> assetProperties = typeof(CmsAsset).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsAsset_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> assetInsurance = typeof(CmsAssetInsurance).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsAssetInsurance_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> bankProperties = typeof(CmsBank).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsBank_" + x.Name, x => x);

                    CmsFinancialDetail finDetail = conDetail.FinancialDetail;
                    Dictionary<string, PropertyInfo> objectProperties = conDetailProperties.Union(finDetailProperties)
                        .Union(accSummaryProperties)
                        .Union(assetProperties)
                        .Union(bankProperties).ToDictionary(x => x.Key, x => x.Value);

                    Dictionary<string, object> objectList = new Dictionary<string, object>();
                    objectList.Add("CmsContractDetail", conDetail);
                    objectList.Add("CmsFinancialDetail", finDetail);
                    objectList.Add("CmsAccountSummary", summary);

                    if ("Cheque".Equals(conDetail.FinancialDetail.PaymentModeCode, StringComparison.OrdinalIgnoreCase)
                        && bank != null && (bank.GiroStatusCode == "Approved" || bank.GiroStatusCode == "New"))
                    {
                        objectList.Add("CmsBank", null);
                    }
                    else
                    {
                        objectList.Add("CmsBank", bank);
                    }

                    IList<CmsAsset> assetList = conDetail.ContractAsset;

                    objectList.Add("CmsAsset", assetList);
                    
                    bool overdue = summary.ContractStatusDisplayCode == CmsContractStatus.Overdue;
                    string contractNo = summary.ContractNO;
                    string contractId = summary.ContractId;
                    bool showLink = _specialLogic.ShowGiroLink(conDetail.FinancialDetail.PaymentMode, conDetail.FinancialDetail.PaymentCode, bank);
                    bool hideGiroStatus = _specialLogic.HideGiroStatus(conDetail.FinancialDetail.PaymentMode, conDetail.FinancialDetail.PaymentCode);

                    IList<Field> lst = new List<Field>();
                    lst = GetFields(details, objectList, objectProperties, vehicle, overdue, showLink, contractId, hideGiroStatus);
                    //foreach (var d in details)
                    //{
                    //    var field = new Field()
                    //    {
                    //        id = d.ResKey,
                    //        module = d.ModuleName,
                    //        label = d.Name,
                    //        labelLocId = d.NameLoc != null ? d.NameLoc.Id : (int?)null,
                    //        order = d.Order,
                    //    };
                    //    lst.Add(field);
                    //}

                    return lst.ToArray();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        private IList<Field> GetFields(IEnumerable<GridModuleSetting> lstModuleSetting,
            Dictionary<string, object> ObjectList,
            Dictionary<string, System.Reflection.PropertyInfo> ObjectProperties,
            CmsAsset vehicle, bool overdue, bool showLink, string contractId, bool hideGiroStatus)
        {
            IList<Field> lst = new List<Field>();

            string value;
            string valueLoc;

            //int i = 0;
            foreach (ZLIS.Cos.Core.UIManagement.DomainModel.GridModuleSetting column in lstModuleSetting)
            {
                //if (ObjectProperties.ContainsKey(column.ObjectProperty))
                {
                    if (column.ObjectProperty.StartsWith("CmsAsset_"))
                    {
                        this.GetDisplayValue(_pageResourceService, _settingService, _languageService,_specialLogic,
                            (object)vehicle, ObjectProperties, column.ObjectProperty, column.ObjectFormat,
                            overdue, showLink, contractId, out value, out valueLoc);
                        //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")">
                        //    <span id="contract_left">@(column.GetDisplayName(cultureInfo))</span> 
                        //   <span id="contract_right">@Html.Raw(value)</span>
                        //</li>

                        var field = new Field()
                        {
                            id = column.ResKey,
                            module = column.ModuleName,
                            label = column.Name,
                            labelLocId = _enableLocalizedText ? (column.NameLoc != null ? column.NameLoc.Id.ToString() : null) : null,
                            //(column.NameLoc != null ? column.NameLoc.Id : (int?)null) : (int?)null,
                            order = column.Order,
                            value = value,
                            valueLoc = valueLoc,
                        };
                        lst.Add(field);
                    }
                    else
                    {
                        foreach (var objectName in ObjectList.Keys)
                        {
                            if (column.ObjectProperty.StartsWith(objectName + "_"))
                            {
                                this.GetDisplayValue(_pageResourceService, _settingService, _languageService,_specialLogic,
                                    ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat,
                                    overdue, showLink, contractId, out value, out valueLoc, null, 1, hideGiroStatus);
                                //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")">
                                //    <span id="contract_left">@(column.GetDisplayName(cultureInfo))</span> 
                                //    <span id="contract_right">@Html.Raw(value)</span>
                                //</li>

                                var field = new Field()
                                {
                                    id = column.ResKey,
                                    module = column.ModuleName,
                                    label = column.Name,
                                    labelLocId = _enableLocalizedText ? (column.NameLoc != null ? column.NameLoc.Id.ToString() : null) : null,
                                    //(column.NameLoc != null ? column.NameLoc.Id : (int?)null) : (int?)null,
                                    order = column.Order,
                                    value = value,
                                    valueLoc = valueLoc,
                                };
                                lst.Add(field);
                            }
                        }
                    }
                }
            }

            return lst;
        }

        private LanguageContent[] GetLanguageContent(string contractNo, ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            IList<LanguageContent> lstLC = new List<LanguageContent>();

            LanguageContent lc = new LanguageContent()
            {
                languageCode = this.GetLanguageCode(_settingService, _languageService),
            };
            lstLC.Add(lc);

            List<LangContent> lst = new List<LangContent>();

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == contractNo).FirstOrDefault();
            CmsContractDetail detail = GetContractDetails(summary.BpId, summary.ContractNO);

            IList<CmsBank> bankList = rmwServiceAdaptor.GetBanks(customer.Bpid);
            if (bankList != null)
            {
                int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;
                var lstModuleSetting = _msService.GetModuleSettings_NoCache(ModuleSettingConstants.MAKE_PAYMENT,
                    ModuleSettingConstants.FILTER_TYPE_CONTRACT_STATUS, summary.ContractStatusCode, fpgId).Where(x => x.Visible).OrderBy(x => x.Order).ToList();

                CmsBank bank = bankList.Where(x => x.ContractNO == summary.ContractNO).FirstOrDefault();
                var showDetail = _specialLogic.ShowBankDetail(detail.FinancialDetail.PaymentMode, detail.FinancialDetail.PaymentCode, detail.GiroStatusCode, bank);

                if (lstModuleSetting != null && lstModuleSetting.Any())
                {
                    var moduleSetting = lstModuleSetting.First();
                    //decimal? labelLocid = moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (decimal?)null;
                    string labelLocid = moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id.ToString() : null;
                    if (labelLocid != null)
                    {
                        string strTitle = moduleSetting.NameLoc.Text;

                        LangContent langContent = new LangContent()
                        {
                            id = labelLocid,
                            value = strTitle,
                        };
                        lst.Add(langContent);
                    }
                }

                GetLangContentForFields(customer, lst, ModuleSettingConstants.MAKE_PAYMENT_PAYMENT_INFO, contractNo);
            }
            
            lc.contents = lst.ToArray();

            return lstLC.ToArray();
        }

        private void GetLangContentForFields(ZLIS.Cos.Core.Account.DomainModel.Customer customer,
            IList<LangContent> lst,
            string module, string ContractNo)
        {
            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            var summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == ContractNo).First();

            int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

            IEnumerable<GridModuleSetting> details = _msService.GetGridModuleSettings_NoCache(module, fpgId)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();
            if (details.Count() > 0)
            {
                foreach (var d in details)
                {
                    //decimal? labelLocId = d.NameLoc != null ? d.NameLoc.Id : (int?)null;
                    string labelLocId = d.NameLoc != null ? d.NameLoc.Id.ToString() : null;

                    if (labelLocId != null)
                    {
                        lst.Add(new LangContent()
                        {
                            id = labelLocId,
                            value = d.NameLoc.Text,
                        });
                    }
                }
            }
        }

        #region Helper

        private CmsContractDetail GetContractDetails(int Bpid, string ContractNo)
        {
            return _rmwService.GetContractDetails(Bpid, ContractNo);
        }

        #endregion

    }
}
