﻿using System;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Framework.Common.IO;
using ZLIS.Framework.Common.Logging;

namespace ZLIS.Cos.MobileAPI.Controllers.Banner
{
    [ActionAuthorizeFilter]
    public class BannerAPIController : ApiController
    {
        private IAppDataFolder _appDataFolder;
        private IBannerService _bannerService;

        public ILogger Logger { get; set; }

        public BannerAPIController( IAppDataFolder appDataFolder,
                                    IBannerService bannerService)
        {
            Logger = NullLogger.Instance;
            _appDataFolder = appDataFolder;
            _bannerService = bannerService;
        }

        [System.Web.Http.HttpPost]
        public GetBannerResponse GetBanner(GetBannerRequest request)
        {
            if (request == null)
                request = new GetBannerRequest();
            Logger.Debug(string.Format("-------- GetBanner API started. token:{0} bannerId:{1} ", request.token ?? string.Empty, request.bannerId ?? string.Empty));
             var response = new GetBannerResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetStaticPageErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;
            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.bannerId))
                {
                    errorInfo.code = (int)GetBannerErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";
                    return response;
                }
                var bannerguid = Guid.Empty;
                Guid.TryParse(request.bannerId, out bannerguid);
                var banner = _bannerService.GetBySyncId(bannerguid);
                if (banner == null||banner.BannerImage==null)
                {
                    string actualPath = _appDataFolder.Combine(_appDataFolder.MapPath("Banner"), request.bannerId);
                    if (System.IO.File.Exists(actualPath))
                    {
                        byte[] content = IOHelper.GetMediaFileConent(actualPath);
                        response.fileSize = content.Length;
                        response.content = Convert.ToBase64String(content);
                        response.mimeType = "image/png";
                    }
                    else
                    {
                        errorInfo.code = (int)GetBannerErrorCode.InvalidBannerId;
                        errorInfo.message = "Invalid BannerId";
                    }
                }
                else
                {
                    response.fileSize =Convert.ToInt32( banner.BannerImage.MediaSize);
                    response.content = Convert.ToBase64String(banner.BannerImage.Content);
                    response.mimeType = banner.BannerImage.MimeType;
                }
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetBannerErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get static page");
            }

            return response;
        }

    }
}
