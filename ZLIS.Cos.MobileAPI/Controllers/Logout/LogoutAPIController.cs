﻿using System;
using System.Web;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.Account;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    public class LogoutAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        public ITextContentService _textContentService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerActivityService _activityService;
        private ISignals _signals;

        public ILogger Logger { get; set; }

        public LogoutAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                   ITextContentService textContentService,
                                   ISystemSettingService settingService,
                                   IMobileLoginInfoService mobileLoginInfoService,
                                   ICustomerActivityService activityService,
                                   ISignals signals)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _textContentService = textContentService;
            _settingService = settingService;
            _settingService.Cachable = false;

            _mobileLoginInfoService = mobileLoginInfoService;

            _activityService = activityService;
            _signals = signals;
        }

        [System.Web.Http.HttpPost]
        public LogoutResponse Logout(LogoutRequest request)
        {
            if (request == null)
                request = new LogoutRequest();
            Logger.Debug(string.Format("-------- Logout API started. token:{0}",request.token??string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new LogoutResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)LogoutErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token))
                {
                    errorInfo.code = (int) LogoutErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                if (loginInfo == null)
                {
                    errorInfo.code = (int) LogoutErrorCode.InvalidToken;
                    errorInfo.message = "Invalid token";

                    return response;
                }

                //var customer = loginInfo.CustomerActivity.Customer;
                //try
                //{
                //    //update active session id
                //    using (ITransaction tran = unitOfWork.BeginTransaction())
                //    {
                //        customer.ActiveSessionID = string.Empty;

                //        _accountService.UpdateCustomer(customer);

                //        tran.Commit();
                //    }
                //}
                //catch (Exception ex)
                //{
                //    Logger.Error("Error occurs when updating customer activities.", ex);
                //}

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.Valid = false;

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    _activityService.CreateActivity(loginInfo.CustomerActivity.Customer,
                                                    RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                    string.Format("Mobile/{0}", loginInfo.DeviceType),
                                                    loginInfo.CustomerActivity.SessionID, 0, "User logout",
                                                    "Logout", ModuleName.Logout.ToString(), PageName.Logout.ToString());

                    tran.Commit();
                }

                AppCacheContainer.Remove(request.token);
                AppCacheContainer.Trigger(_signals, loginInfo.CustomerActivity.Customer.Bpid);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int) LogoutErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when logout");
            }

            return response;
        }
    }
}
