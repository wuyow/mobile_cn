﻿using System;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Modules.Enquiry.DomainModel;
using ZLIS.Cos.Core.Modules.Enquiry.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class MessageAttachmentAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerTicketService _ticketService;

        public ILogger Logger { get; set; }

        public MessageAttachmentAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                              IMobileLoginInfoService mobileLoginInfoService,
                                              ICustomerTicketService ticketService)
        {
            Logger = NullLogger.Instance;

            _unitOfWorkAccessor = unitOfWorkAccessor;
            _mobileLoginInfoService = mobileLoginInfoService;
            _ticketService = ticketService;
        }

        [System.Web.Http.HttpPost]
        public GetMessageAttachmentResponse MessageAttachment(GetMessageAttachmentRequest request)
        {
            if (request == null)
                request = new GetMessageAttachmentRequest();
            Logger.Debug(string.Format("-------- MessageAttachment API started. token:{0} attachmentId:{1} ", request.token ?? string.Empty, request.attachmentId ));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetMessageAttachmentResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetMessageAttachmentErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token))
                {
                    errorInfo.code = (int)GetMessageAttachmentErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
               
                var data = _ticketService.GetTicketAttachmentById(request.attachmentId);
                if (data == null)
                {
                    errorInfo.code = (int)GetMessageAttachmentErrorCode.InvalidAttachmentId;
                    errorInfo.message = "Invalid attachment ID";

                    return response;
                }
                else
                {
                    if (loginInfo.CustomerActivity.Customer.Id != data.CustomerTicketMessage.Ticket.CreatedBy.Id)
                    {
                        errorInfo.code = (int)SendMessageErrorCode.InvalidThreadId;
                        errorInfo.message = "Invalid attachment ID";

                        return response;
                    }
                }

                response.data = GetMessageAttachment(request, loginInfo.CustomerActivity.Customer, data);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetMessageAttachmentErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get message threads");
            }

            return response;
        }

        private FileContent GetMessageAttachment(GetMessageAttachmentRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer, CustomerTicketAttachment data)
        {
            FileContent fileContent = new FileContent();
            //var data = _ticketService.GetTicketAttachmentById(request.attachmentId);
            if (data != null)
            {
                fileContent.fileName = data.MediaFileName;
                fileContent.fileSize = data.Content.Length;
                fileContent.mimeType = data.MimeType;
                fileContent.content = Convert.ToBase64String(data.Content);
            }

            return fileContent;
        }

    }
}
