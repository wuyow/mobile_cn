﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.ContentManagement;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Modules.Enquiry;
using ZLIS.Cos.Core.Modules.Enquiry.DomainModel;
using ZLIS.Cos.Core.Modules.Enquiry.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class SendEnquiryAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IEnquiryService _enquiryService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerTicketService _ticketService;

        private IEmailTemplateService _templateService;
        private IAppDataFolder _appDataFolder;
        private IMailer _mailer;

        private IMediaResourceService _mediaResourceService;

        public ILogger Logger { get; set; }

        public SendEnquiryAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                        ILanguageService languageService,
                                        ISystemSettingService settingService,
                                        IEnquiryService enquiryService,
                                        IMobileLoginInfoService mobileLoginInfoService,
                                        ICustomerTicketService ticketService,
                                        IEmailTemplateService templateService,
                                        IAppDataFolder appDataFolder,
                                        IMailer mailer,
                                        IMediaResourceService mediaResourceService)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _enquiryService = enquiryService;
            _mobileLoginInfoService = mobileLoginInfoService;
            _ticketService = ticketService;

            _templateService = templateService;
            _appDataFolder = appDataFolder;
            _mailer = mailer;

            _mediaResourceService = mediaResourceService;
        }

        [System.Web.Http.HttpPost]
        public SendEnquiryResponse SendEnquiry(SendEnquiryRequest request)
        {
            if (request == null)
                request = new SendEnquiryRequest();
            Logger.Debug(string.Format("-------- SendEnquiry API started. token:{0} contractNo:{1} contactMode{2} topicId{3}", request.token ?? string.Empty, request.contractNo ?? string.Empty, request.contactMode ?? string.Empty, request.topicId));
            //Logger.Debug("------SendEnquiry Request Begin------");
            //Logger.Debug(ZLIS.Framework.Common.Helper.JsonUtils.ObjectToJsonString(request));
            //Logger.Debug("------SendEnquiry Request End------");
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new SendEnquiryResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)SendEnquiryErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contactMode) ||
                    string.IsNullOrEmpty(request.content))
                {
                    errorInfo.code = (int)SendEnquiryErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                

                if (request.contactMode.Equals("by Email", StringComparison.OrdinalIgnoreCase))
                {
                    if (string.IsNullOrEmpty(request.email))
                    {
                        errorInfo.code = (int)SendEnquiryErrorCode.InvalidEmail;
                        errorInfo.message = "Invalid email";

                        return response;
                    }
                }
                else if (request.contactMode.Equals("by Phone", StringComparison.OrdinalIgnoreCase))
                {
                    if (string.IsNullOrEmpty(request.mobile))
                    {
                        errorInfo.code = (int)SendEnquiryErrorCode.InvalidPhone;
                        errorInfo.message = "Invalid phone";

                        return response;
                    }

                    if (string.IsNullOrEmpty(request.contactTime) ||(
                        !request.contactTime.Equals("Anytime", StringComparison.OrdinalIgnoreCase) &&
                        !request.contactTime.Equals("Morning", StringComparison.OrdinalIgnoreCase) &&
                        !request.contactTime.Equals("Afternoon", StringComparison.OrdinalIgnoreCase))
                        )
                    {
                        errorInfo.code = (int)SendEnquiryErrorCode.InvalidContactTime;
                        errorInfo.message = "Invalid contact time";

                        return response;
                    }
                }
                else
                {
                    errorInfo.code = (int)SendEnquiryErrorCode.InvalidContactMode;
                    errorInfo.message = "Invalid contact mode";

                    return response;
                }

                var enquiryTopic = _enquiryService.GetTopicById(request.topicId);
                if (enquiryTopic == null)
                {
                    errorInfo.code = (int)SendEnquiryErrorCode.InvalidTopicId;
                    errorInfo.message = "Invalid topic ID";

                    return response;
                }

                SaveEnquiry(request, loginInfo.CustomerActivity.Customer, enquiryTopic);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)SendEnquiryErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when send message");
            }

            return response;
        }

        private void SaveEnquiry(SendEnquiryRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer, EnquiryTopic enquiryTopic)
        {
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();
            ZLIS.Cos.Core.Modules.Enquiry.DomainModel.Enquiry enquiry = new Core.Modules.Enquiry.DomainModel.Enquiry();
            if (request.contactMode.Equals("by Email", StringComparison.OrdinalIgnoreCase))
            {
                enquiry.Email = request.email;
            }
            else
            {
                enquiry.Mobile = request.mobile;
                EnquiryContactOption contactTime = _enquiryService
                    .GetEnquiryContactOptionByCriteria("ContactTimeOptions", request.contactTime);
                enquiry.ContactTime = contactTime;
            }

            enquiry.ContactMode = _enquiryService
                    .GetEnquiryContactOptionByCriteria("ContactModeOptions", request.contactMode);

            enquiry.ContractNo = string.IsNullOrEmpty(request.contractNo) ? "All" : request.contractNo;
            enquiry.Topic = enquiryTopic;
            enquiry.Content = request.content;
            enquiry.CreatedBy = customer;
            enquiry.CreatedDate = DateTime.Now;

            //System.Globalization.CultureInfo cultureInfo = 
            //    ZLIS.Cos.Core.CosApplicationContext.Current.CurrentCulture;
            string languageCode = this.GetCurrentLanguageCode(_settingService, _languageService, request.language);
            CultureInfo cultureInfo = new CultureInfo(languageCode);

            using (ITransaction tran = unitOfWork.BeginTransaction())
            {
                _enquiryService.CreateEnquiry(enquiry);
                string actualDir = string.Format("{0}/{1:yyyyMMdd}/{2}",
                    this.GetCountryCode(_settingService), enquiry.CreatedDate, enquiry.Id);

                if (request.attachments != null && request.attachments.Length > 0 && enquiryTopic.HasAttachment)
                {
                    //if (!_appDataFolder.DirectoryExists(actualDir))
                    //    _appDataFolder.CreateDirectory(actualDir);

                    foreach (var att in request.attachments)
                    {
                        //if (!string.IsNullOrEmpty(mr.TmpPath))
                        //{
                        //string actualPath = _appDataFolder.Combine(_appDataFolder.MapPath(actualDir), att.fileName);
                        //if (!System.IO.Directory.Exists(_appDataFolder.MapPath(actualDir)))
                        //{
                        //    System.IO.Directory.CreateDirectory(_appDataFolder.MapPath(actualDir));
                        //}
                        //System.IO.Stream target = _appDataFolder.CreateFile(actualPath);

                        byte[] content = Convert.FromBase64String(att.content);
                        //target.Write(content, 0, content.Length);
                        //target.Close();
                        //}

                        MediaResource mr = new MediaResource()
                        {
                            MediaName = att.fileName,
                            MediaType = new FileInfo(att.fileName).Extension,
                            MediaFileName = att.fileName,
                            MimeType = att.mimeType,
                            Content = Convert.FromBase64String(att.content),
                            Tags = null,
                            Category1 = "Enquiry",
                            CreatedBy = string.Format("CustomerId_{0}", customer.Id),
                            CreatedDate = DateTime.Now,
                        };

                        _mediaResourceService.Create(mr);

                        //mr.CreatedBy = string.Format("CustomerId_{0}", customer.Id);
                        //mr.CreatedDate = DateTime.Now;
                        //_mediaResourceService.Create(mr);

                        EnquiryBill enquiryBill = new EnquiryBill()
                        {
                            Enquiry = enquiry,
                            Bill = mr,
                            CreatedBy = customer
                        };

                        _enquiryService.CreateEnquiryBill(enquiryBill);
                    }
                }

                //create ticket per enquiry
                var ticket = new CustomerTicket()
                {
                    TicketNumber = "",
                    Enquiry = enquiry,
                    EnquiryTopic = enquiryTopic,
                    Severity = (int)TicketSeverity.Low,
                    TicketFromFlag = TicketFromFlag.Customer,
                    AssignedTo = TicketAssignedTo.CustomerService,
                    Status = TicketStatus.Open,
                    CreatedBy = customer,
                    CreatedDate = DateTime.Now
                };

                _ticketService.CreateCustomerTicket(ticket);

                //create ticket message
                var ticketMessage = new CustomerTicketMessage()
                {
                    Ticket = ticket,
                    Comment = request.content,
                    CreatorRoleCode = "CR", //CR=customer
                    ReadFlag = true,
                    SentTo = "", //customer service email
                    CreatedBy = customer.Id.ToString(),
                    CreatedDate = DateTime.Now
                };
                _ticketService.CreateCustomerTicketMessage(ticketMessage);

                //create ticket attachment
                if (request.attachments != null && request.attachments.Length > 0 && enquiryTopic.HasAttachment)
                {
                    foreach (var mr in request.attachments)
                    {
                        //if (!string.IsNullOrEmpty(mr.TmpPath))
                        //{
                        //    System.IO.MemoryStream target = new System.IO.MemoryStream();
                        //    _appDataFolder.OpenFile(mr.TmpPath).CopyTo(target);
                        //    mr.Content = target.ToArray();
                        //}

                        CustomerTicketAttachment attachment = new CustomerTicketAttachment()
                        {
                            CustomerTicketMessage = ticketMessage,
                            MediaName = mr.fileName,
                            MediaType = new FileInfo(mr.fileName).Extension,
                            MediaFileName = mr.fileName,
                            MimeType = mr.mimeType,
                            MediaSize = mr.fileSize,
                            //Thumbnail = mr.Thumbnail,
                            Content = Convert.FromBase64String(mr.content),
                            //Tags = mr.Tags,
                            Category1 = "Enquiry",
                            //Category2 = mr.Category2,
                            //Category3 = mr.Category3,
                            //Category4 = mr.Category4,
                            //Category5 = mr.Category5,
                            CreatedBy = customer.Id.ToString(),
                            CreatedDate = DateTime.Now
                        };

                        _ticketService.CreateCustomerTicketAttachment(attachment);
                    }
                }

                EmailTemplate template = _templateService.GetEmailTemplate(EmailTemplateName.ENQUIRY_EMAIL_REQUEST,
                    DeliveryChannel.EMAIL);
                if (template != null)
                {
                    string subject = template.Subject;
                    if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                    {
                        subject = template.SubjectLocID.Text;
                    }
                    EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                    string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                    string rootUrl = "";

                    var s = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SYSTEM, SystemConfigurationConstants.KEY_SYSTEM_EXTERNAL_ROOTURL);
                    if (s != null)
                    {
                        rootUrl = s.Value;
                    }

                    string imageURL = emailUtility.GetImageUrl(rootUrl, "", template.Id.ToString());//Url.Content("~")
                    messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, customer.DisplayName,
                        enquiryTopic.GetName(cultureInfo),
                        enquiry.Content, "", "", "", "", "", "", "", "", "", "", ticket.TicketNumber);
                    if (request.contactMode.Equals("by Email", StringComparison.OrdinalIgnoreCase))
                        _mailer.SendEmail(template, this.GetEmailAddress(request.email), subject, messageBody);
                    else
                        _mailer.SendEmail(template, this.GetEmailAddress(customer.Email), subject, messageBody);
                }

                EmailTemplate template_admin = _templateService.GetEmailTemplate(EmailTemplateName.ENQUIRY_EMAIL_REQUEST_ADMIN, DeliveryChannel.EMAIL);
                if (template_admin != null)
                {
                    string subject = template_admin.Subject;
                    if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template_admin.SubjectLocID != null)
                    {
                        subject = template_admin.SubjectLocID.Text;
                    }
                    EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                    string messageBody = emailUtility.GetStringFromTemplateFile(template_admin, languageCode);
                    string rootUrl = "";

                    var s = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SYSTEM, SystemConfigurationConstants.KEY_SYSTEM_EXTERNAL_ROOTURL);
                    if (s != null)
                    {
                        rootUrl = s.Value;
                    }

                    string imageURL = emailUtility.GetImageUrl(rootUrl, "", template_admin.Id.ToString());//Url.Content("~")

                    IPHostEntry ip = Dns.GetHostEntry(Dns.GetHostName());

                    string fileList = "-";
                    //IList<int> fileIdList = new List<int>();
                    string fileDir = "";
                    if (request.attachments != null &&
                        request.attachments.Length > 0 && enquiryTopic.HasAttachment)
                    {
                        fileList = string.Join("<br />", request.attachments.Select(x => string.Format(@"\\{0}\{1}\{2:yyyyMMdd}\{3}\{4}",
                            ip.HostName, this.GetCountryCode(_settingService), enquiry.CreatedDate, enquiry.Id, x.fileName)).ToArray());
                        //fileIdList = lstMR.Select(x => x.Id).ToList();
                        fileDir = _appDataFolder.MapPath(actualDir);
                    }

                    messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, customer.DisplayName,
                        enquiryTopic.GetName(cultureInfo), enquiry.Content, enquiry.ContractNo, enquiry.ContactMode.GetText(cultureInfo), enquiry.Email, enquiry.Mobile, enquiry.ContactMode.GetText(cultureInfo), fileList, "", "", "", "", ticket.TicketNumber);

                    string csEmailAddress = string.IsNullOrEmpty(enquiryTopic.CustomerserviceEmail) ?
                                      this.GetEmailAddress(_settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_MAILER, SystemConfigurationConstants.KEY_MAILER_CUSTOMERSERVICE).Value)
                                      : EmailServiceUtility.GetEmailAddress(enquiryTopic.CustomerserviceEmail);

                    _mailer.SendEmail(template_admin,
                        csEmailAddress, subject, "",
                                      messageBody, null, fileDir);
                }

                tran.Commit();
            }
        }

    }
}
