﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Modules.Enquiry.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class MessagesAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ICustomerAccountService _accountService;
        private ISystemSettingService _settingService;
        private IPageResourceService _pageResourceService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerTicketService _ticketService;

        
        public ILogger Logger { get; set; }

        public MessagesAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                     ICustomerAccountService accountService,
                                     ISystemSettingService settingService,
                                     IPageResourceService pageResourceService,
                                     IMobileLoginInfoService mobileLoginInfoService,
                                     ICustomerTicketService ticketService)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _accountService = accountService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _pageResourceService = pageResourceService;
            _mobileLoginInfoService = mobileLoginInfoService;
            _ticketService = ticketService;
        }

        [System.Web.Http.HttpPost]
        public GetMessagesResponse Messages(GetMessagesRequest request)
        {
            if (request == null)
                request = new GetMessagesRequest();
            Logger.Debug(string.Format("-------- Messages API started. token:{0} threadId:{1} ", request.token ?? string.Empty, request.threadId ));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetMessagesResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetMessagesErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token))
                {
                    errorInfo.code = (int)GetMessagesErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                
                var data = _ticketService.GetTicketById(request.threadId);
                if (data == null)
                {
                    errorInfo.code = (int)SendMessageErrorCode.InvalidThreadId;
                    errorInfo.message = "Invalid thread ID";

                    return response;
                }
                else
                {
                    if (loginInfo.CustomerActivity.Customer.Id != data.CreatedBy.Id)
                    {
                        errorInfo.code = (int)SendMessageErrorCode.InvalidThreadId;
                        errorInfo.message = "Invalid thread ID";

                        return response;
                    }
                }

                response.data = GetMessages(request, loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetMessagesErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get message threads");
            }

            return response;
        }

        private Messages GetMessages(GetMessagesRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            Messages messages = new Messages();

            //ISessionState sessionState = CosApplicationContext.Current.Resolve<ISessionState>();
            //ZLIS.Cos.Core.Account.DomainModel.Customer customer = sessionState.CurrentCustomer;
            var data = (_ticketService.GetTickectMessagesByTicketId(request.threadId)
                .OrderByDescending(o => o.CreatedDate).Select(o => new Message()
            {
                id = o.Id,
                content = o.Comment.Replace("\r\n", "<br />"),
                createdDate = o.CreatedDate.ToString(this.GetDateTimeFormat(_settingService)),
                createdBy = o.CreatorRoleCode == "CS" ? this.Resource(_pageResourceService,"Message_Message_Edit,CustomerService") : o.CreatedBy,
                readFlag = o.ReadFlag,

                //Id = o.Id,
                //ReadFlag = o.ReadFlag,
                //Name = o.CreatorRoleCode == "CS" ? this.Resource("Message_Message_Edit,CustomerService") : customer.DisplayName,
                //Message = o.Comment.Replace("\r\n", "<br />"),
                //DateTime = o.CreatedDate.ToCosLocalTime()
            })).ToList();

            foreach (var item in data)
            {
                if (!string.IsNullOrEmpty(item.createdBy))
                {
                    int cid = 0;
                    if (int.TryParse(item.createdBy, out cid))
                    {
                        var cust = _accountService.GetCustomerById(cid);
                        if (cust != null)
                        {
                            item.createdBy = cust.DisplayName;
                        }
                    }
                }

                var att = _ticketService.GetTicketAttachmentsByMessageId(item.id);

                if (att != null)
                {
                    //string attachments = "";
                    IList<Attachment> lst = new List<Attachment>();
                    foreach (var a in att)
                    {
                        var attachment = new Attachment()
                        {
                            id = a.Id,
                            fileName = a.MediaFileName,
                            mimeType = a.MimeType,
                            fileSize = a.Content.Length,
                        };

                        lst.Add(attachment);
                        //attachments = attachments == "" ? string.Format("{0},{1}", a.Id.ToString(), a.MediaFileName) : string.Format("{0};{1}", attachments, string.Format("{0},{1}", a.Id.ToString(), a.MediaFileName));
                    }
                    item.attachments = lst.ToArray();
                    //item.Attachments = attachments;
                }
            }

            messages.messages = data.ToArray();

            //update ReadFlag=true
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();
            using (ITransaction tran = unitOfWork.BeginTransaction())
            {
                _ticketService.UpdateCustomerTicketMessageToRead(request.threadId);
                tran.Commit();
            }
            

            return messages;
        }

    }
}
