﻿using System;
using System.Linq;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Modules.Enquiry.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class MessageThreadsAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerTicketService _ticketService;
        private bool _enableLocalizedText;

        public ILogger Logger { get; set; }

        public MessageThreadsAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                           ILanguageService languageService,
                                           ISystemSettingService settingService,
                                           IMobileLoginInfoService mobileLoginInfoService,
                                           ICustomerTicketService ticketService)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _mobileLoginInfoService = mobileLoginInfoService;

            _ticketService = ticketService;
            _enableLocalizedText = this.EnableLocalizedText(_settingService, _languageService);
        }

        [System.Web.Http.HttpPost]
        public GetMessageThreadsResponse MessageThreads(GetMessageThreadsRequest request)
        {
            if (request == null)
                request = new GetMessageThreadsRequest();
            Logger.Debug(string.Format("-------- MessageThreads API started. token:{0} ", request.token ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetMessageThreadsResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetMessageThreadsErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token))
                {
                    errorInfo.code = (int)GetMessageThreadsErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                
                response.data = GetMessageThreads(loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetMessageThreadsErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get message threads");
            }

            return response;
        }

        private MessageThreads GetMessageThreads(
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            MessageThreads messageThreads = new MessageThreads();

            string dateTimeFormat = this.GetDateTimeFormat(_settingService);

            messageThreads.messageThreads = (_ticketService.GetTicketsByCustomerId(customer.Id)
                .OrderByDescending(x => x.TotalUnreadMessage)
                .ThenByDescending(x => x.CreatedDate).Select(o => new MessageThread()
            {
                id = o.Id,
                title = _enableLocalizedText ? ((string.IsNullOrEmpty(o.EnquiryTopic.Name) || string.IsNullOrEmpty(o.EnquiryTopic.Name.Trim())) ? (o.EnquiryTopic.NameLoc == null ? o.EnquiryTopic.Name : (o.EnquiryTopic.NameLoc.Text == null ? o.EnquiryTopic.NameLoc.BigText : o.EnquiryTopic.NameLoc.Text)) :  o.EnquiryTopic.Name) : o.EnquiryTopic.Name,
                titleLoc = _enableLocalizedText ? (o.EnquiryTopic.NameLoc == null? o.EnquiryTopic.Name : (o.EnquiryTopic.NameLoc.Text == null ? ((string.IsNullOrEmpty(o.EnquiryTopic.NameLoc.BigText) || string.IsNullOrEmpty(o.EnquiryTopic.NameLoc.BigText.Trim())) ? o.EnquiryTopic.Name : o.EnquiryTopic.NameLoc.BigText) : ((string.IsNullOrEmpty(o.EnquiryTopic.NameLoc.Text) || string.IsNullOrEmpty(o.EnquiryTopic.NameLoc.Text.Trim())) ? o.EnquiryTopic.Name : o.EnquiryTopic.NameLoc.Text))) : o.EnquiryTopic.Name,
                //createdDate = o.CreatedDate.ToCosLocalTime().ToString(dateTimeFormat),    //!!
                createdDate = o.CreatedDate.ToString(dateTimeFormat),
                createdBy = o.CreatedBy.DisplayName,
                //lastReplyDate = o.LastRepliedDate != null ? o.LastRepliedDate.Value.ToCosLocalTime().ToString(dateTimeFormat) : (string)null, //!!
                lastReplyDate = o.LastRepliedDate != null ? o.LastRepliedDate.Value.ToString(dateTimeFormat) : (string)null,
                unreadMessageNum = o.TotalUnreadMessage,
            })).ToArray();

            return messageThreads;
        }

    }
}
