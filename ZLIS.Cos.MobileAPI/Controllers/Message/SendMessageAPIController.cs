﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.ContentManagement;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Modules.Enquiry.DomainModel;
using ZLIS.Cos.Core.Modules.Enquiry.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using System.Linq;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class SendMessageAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerTicketService _ticketService;

        private IEmailTemplateService _templateService;
        private IAppDataFolder _appDataFolder;
        private IMailer _mailer;

        public ILogger Logger { get; set; }

        public SendMessageAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                        ILanguageService languageService,
                                        ISystemSettingService settingService,
                                        IMobileLoginInfoService mobileLoginInfoService,
                                        IEmailTemplateService templateService,
                                        IAppDataFolder appDataFolder,
                                        IMailer mailer,
                                        ICustomerTicketService ticketService)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _mobileLoginInfoService = mobileLoginInfoService;

            _ticketService = ticketService;
            _templateService = templateService;
            _appDataFolder = appDataFolder;
            _mailer = mailer;
        }

        [System.Web.Http.HttpPost]
        public SendMessageResponse SendMessage(SendMessageRequest request)
        {
            if (request == null)
                request = new SendMessageRequest();
            Logger.Debug(string.Format("-------- SendMessage API started. token:{0} threadId:{1} content{2}", request.token ?? string.Empty, request.threadId, request.content ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new SendMessageResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)SendMessageErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.content))
                {
                    errorInfo.code = (int)SendMessageErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
               
                var data = _ticketService.GetTicketById(request.threadId);
                if (data == null)
                {
                    errorInfo.code = (int)SendMessageErrorCode.InvalidThreadId;
                    errorInfo.message = "Invalid thread ID";

                    return response;
                }
                else
                {
                    if (loginInfo.CustomerActivity.Customer.Id != data.CreatedBy.Id)
                    {
                        errorInfo.code = (int)SendMessageErrorCode.InvalidThreadId;
                        errorInfo.message = "Invalid thread ID";

                        return response;
                    }
                }

                SaveMessage(request, loginInfo.CustomerActivity.Customer, data);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)SendMessageErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when send message");
            }

            return response;
        }

        private void SaveMessage(SendMessageRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer, CustomerTicket data)
        {
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();
            using (ITransaction tran = unitOfWork.BeginTransaction())
            {
                CustomerTicketMessage ctm = new CustomerTicketMessage()
                {
                    Ticket = data,
                    Comment = request.content,
                    CreatedBy = customer.Id.ToString(),
                    CreatedDate = DateTime.Now,
                    ReadFlag = true,
                    CreatorRoleCode = "CR", //CR=customer
                    SentTo = _settingService.GetSystemSetting(
                                                         SystemConfigurationConstants.CATEGORY_MAILER,
                                                         SystemConfigurationConstants.KEY_MAILER_CUSTOMERSERVICE).Value, //customer service email
                };

                int ctmId = _ticketService.CreateCustomerTicketMessage(ctm);

                if (request.attachments != null &&
                    request.attachments.Length > 0)
                {
                    foreach (var att in request.attachments)
                    {
                        CustomerTicketAttachment cta = new CustomerTicketAttachment()
                        {
                            CustomerTicketMessage = ctm,
                            MediaName = att.fileName,
                            MediaType = new FileInfo(att.fileName).Extension,
                            MediaFileName = att.fileName,
                            MimeType = att.mimeType,
                            Content = Convert.FromBase64String(att.content),
                            Tags = null,
                            Category1 = "Message",
                            CreatedBy = customer.Id.ToString(),
                            CreatedDate = DateTime.Now,
                        };

                        _ticketService.CreateCustomerTicketAttachment(cta);
                    }
                }

                string languageCode = this.GetCurrentLanguageCode(_settingService, _languageService, request.language);
                CultureInfo cultureInfo = new CultureInfo(languageCode);
                string actualDir = string.Format("{0}/{1:yyyyMMdd}/{2}",
                   this.GetCountryCode(_settingService), data.Enquiry.CreatedDate, data.Enquiry.Id);

                EmailTemplate template = _templateService.GetEmailTemplate(EmailTemplateName.ENQUIRY_EMAIL_REQUEST,
                    DeliveryChannel.EMAIL);
                if (template != null)
                {
                    string subject = template.Subject;
                    if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                    {
                        subject = template.SubjectLocID.Text;
                    }
                    EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                    string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                    string rootUrl = "";

                    var s = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SYSTEM, SystemConfigurationConstants.KEY_SYSTEM_EXTERNAL_ROOTURL);
                    if (s != null)
                    {
                        rootUrl = s.Value;
                    }

                    string imageURL = emailUtility.GetImageUrl(rootUrl, "", template.Id.ToString());//Url.Content("~")
                    messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, customer.DisplayName,
                        data.EnquiryTopic.GetName(cultureInfo),
                        request.content, "", "", "", "", "", "", "", "", "", "", data.TicketNumber);

                    _mailer.SendEmail(template, this.GetEmailAddress(customer.Email), subject, messageBody);
                }

                EmailTemplate template_admin = _templateService.GetEmailTemplate(EmailTemplateName.ENQUIRY_EMAIL_REQUEST_ADMIN, DeliveryChannel.EMAIL);
                if (template_admin != null)
                {
                    string subject = template_admin.Subject;
                    if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template_admin.SubjectLocID != null)
                    {
                        subject = template_admin.SubjectLocID.Text;
                    }
                    EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                    string messageBody = emailUtility.GetStringFromTemplateFile(template_admin, languageCode);
                    string rootUrl = "";

                    var s = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SYSTEM, SystemConfigurationConstants.KEY_SYSTEM_EXTERNAL_ROOTURL);
                    if (s != null)
                    {
                        rootUrl = s.Value;
                    }

                    string imageURL = emailUtility.GetImageUrl(rootUrl, "", template_admin.Id.ToString());//Url.Content("~")

                    IPHostEntry ip = Dns.GetHostEntry(Dns.GetHostName());

                    string fileList = "-";
                    string fileDir = "";
                    if (request.attachments != null &&
                        request.attachments.Length > 0 && data.EnquiryTopic.HasAttachment)
                    {
                        fileList = string.Join("<br />", request.attachments.Select(x => string.Format(@"\\{0}\{1}\{2:yyyyMMdd}\{3}\{4}",
                            ip.HostName, this.GetCountryCode(_settingService), data.Enquiry.CreatedDate, data.Enquiry.Id, x.fileName)).ToArray());
                        fileDir = _appDataFolder.MapPath(actualDir);
                    }

                    messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, customer.DisplayName,
                        data.EnquiryTopic.GetName(cultureInfo), data.Enquiry.Content, data.Enquiry.ContractNo, data.Enquiry.ContactMode.GetText(cultureInfo),
                        data.Enquiry.Email, data.Enquiry.Mobile, data.Enquiry.ContactMode.GetText(cultureInfo), fileList, "", "", "", "", data.TicketNumber);

                    string csEmailAddress = string.IsNullOrEmpty(data.EnquiryTopic.CustomerserviceEmail) ?
                                      this.GetEmailAddress(_settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_MAILER, SystemConfigurationConstants.KEY_MAILER_CUSTOMERSERVICE).Value)
                                      : EmailServiceUtility.GetEmailAddress(data.EnquiryTopic.CustomerserviceEmail);

                    _mailer.SendEmail(template_admin,
                        csEmailAddress, subject, "",
                                      messageBody, null, fileDir);
                }
                tran.Commit();
            }

        }

    }
}
