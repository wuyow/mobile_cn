﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.UIManagement;
using ZLIS.Cos.Core.UIManagement.DomainModel;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.MobileAPI.Filters;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class ReceiptAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        private IModuleSettingService _msService;

        private bool _enableLocalizedText;

        public ILogger Logger { get; set; }

        public ReceiptAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                    ILanguageService languageService,
                                    ISystemSettingService settingService,
                                    IMobileLoginInfoService mobileLoginInfoService,
                                    IRmwService rmwService,
                                    IModuleSettingService msService,
                                    ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _mobileLoginInfoService = mobileLoginInfoService;

            _rmwService = rmwService;
            ((RmwServiceAdaptor)_rmwService).SetCacheManager(cacheManager);

            _msService = msService;
            _enableLocalizedText = this.EnableLocalizedText(_settingService, _languageService);
        }

        [System.Web.Http.HttpPost]
        public GetReceiptsResponse Receipts(GetReceiptsRequest request)
        {
            if (request == null)
                request = new GetReceiptsRequest();
            Logger.Debug(string.Format("-------- Receipts API started. token:{0} contractNo:{1} fromDate{2} endDate{3}", request.token ?? string.Empty,
                request.contractNo ?? string.Empty, request.fromDate ?? string.Empty, request.endDate ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetReceiptsResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetReceiptsErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo) ||
                    request.pageIndex == null ||
                    request.itemCount == null)
                {
                    errorInfo.code = (int)GetReceiptsErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
               
                if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_RECEIPT_OTP_ENABLED, request.token, "Receipt"))
                {
                    errorInfo.code = (int)GetReceiptsErrorCode.InvalidToken;
                    errorInfo.message = "Validate Otp Failed";
                    return response;
                }

                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetReceiptsErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }

                IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
                string dateFormat = this.GetDateFormat(_settingService);

                DateTime sDate, eDate;
                if (!DateTime.TryParseExact(request.fromDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out sDate)
                    || !DateTime.TryParseExact(request.endDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out eDate))
                {
                    errorInfo.code = (int)GetReceiptsErrorCode.InvalidFromDate_EndDate;
                    errorInfo.message = "Invalid from date/end date";

                    return response;
                }
                                
                response.data = GetReceipts(request, loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetReceiptsErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get repayment plan");
            }

            return response;
        }

        private Receipt GetReceipts(GetReceiptsRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            Receipt receipts =
                new Receipt();

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
            int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

            var lstModuleSetting = _msService.GetGridModuleSettings_NoCache(ModuleSettingConstants.RECEIPT_TABLE, fpgId)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();
            var headerItems = _msService.GetGridModuleSettings_NoCache(ModuleSettingConstants.RECEIPT_HEADER, fpgId)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();

            CmsContractDetail contract = rmwServiceAdaptor.GetContractDetails(customer.Bpid, summary.ContractNO);
            CmsReceiptResultSet receipt = _rmwService.GetReceipts(customer.Bpid, summary.ContractNO, 1, 10, null, null);

            if (contract != null && receipt != null)
            {
                Dictionary<string, PropertyInfo> conDetailProperties = typeof(CmsContractDetail).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsContractDetail_" + x.Name, x => x);
                Dictionary<string, PropertyInfo> objectProperties = conDetailProperties.ToDictionary(x => x.Key, x => x.Value);

                Dictionary<string, object> objectList = new Dictionary<string, object>();
                objectList.Add("CmsContractDetail", contract);

                receipts.settings = GetSettings(headerItems, lstModuleSetting, objectProperties, objectList);

                int total;
                receipts.tableData = GetTableData(request, customer, lstModuleSetting, out total);
                receipts.totalCount = total;

                receipts.languageContent = _enableLocalizedText ?
                    GetLanguageContent(headerItems, lstModuleSetting) : null;
            }

            return receipts;
        }

        private Settings GetSettings(List<GridModuleSetting> headerItems, List<GridModuleSetting> lstModuleSetting,
            Dictionary<string, PropertyInfo> objectProperties, Dictionary<string, object> objectList)
        {
            var settings = new Settings();

            settings.headerSettings = GetHeaderSettings(headerItems, objectProperties, objectList);

            settings.tableSettings = GetTableSettings(lstModuleSetting);

            return settings;
        }

        private HeaderSetting[] GetHeaderSettings(List<GridModuleSetting> headerItems,
            Dictionary<string, PropertyInfo> objectProperties, Dictionary<string, object> objectList)
        {
            IList<HeaderSetting> lstHeaderSettings =
                new List<HeaderSetting>();

            foreach (var item in headerItems)
            {
                if (objectProperties.ContainsKey(item.ObjectProperty))
                {
                    foreach (var objectName in objectList.Keys)
                    {
                        if (item.ObjectProperty.Contains(objectName))
                        {
                            string property = item.ObjectProperty;
                            var Object = objectList[objectName];

                            HeaderSetting headerSetting = new HeaderSetting()
                            {
                                id = item.ResKey,
                                label = item.Name,
                                order = item.Order,
                                labelLocid = _enableLocalizedText ? (item.NameLoc != null ? item.NameLoc.Id.ToString() : null) : null,
                                //(item.NameLoc != null ? item.NameLoc.Id : (int?)null) : (int?)null,
                                value = FormatHelper.FormatData(_settingService, item.ObjectFormat, objectProperties[property].GetValue(Object, null))
                            };

                            if (_enableLocalizedText)
                            {
                                string objectPropertyLocal = string.Format("{0}_Local", property);
                                if (objectProperties.ContainsKey(objectPropertyLocal))
                                {
                                    object v = objectProperties[objectPropertyLocal].GetValue(Object, null);
                                    if (v != null)
                                    {
                                        headerSetting.valueLoc = FormatHelper.FormatData(_settingService, item.ObjectFormat, v);
                                    }
                                }
                            }

                            lstHeaderSettings.Add(headerSetting);
                        }
                    }
                }
            }

            return lstHeaderSettings.ToArray();
        }

        private TableSetting[] GetTableSettings(List<GridModuleSetting> lstModuleSetting)
        {
            IList<TableSetting> lstTableSettings =
                new List<TableSetting>();

            string receiptsHeadingField = this.ReceiptsHeadingField();
            string[] arrReceiptsHeadingField = receiptsHeadingField.Split(';');

            foreach (var item in lstModuleSetting)
            {
                TableSetting tableSetting = new TableSetting()
                {
                    id = item.ResKey,
                    label = item.Name,
                    order = item.Order,
                    labelLocid = _enableLocalizedText ? (item.NameLoc != null ? item.NameLoc.Id.ToString() : null) : null,
                    //(item.NameLoc != null ? item.NameLoc.Id : (int?)null) : (int?)null,
                    isHeading = arrReceiptsHeadingField.Contains(item.ResKey),
                };

                lstTableSettings.Add(tableSetting);
            }

            return lstTableSettings.ToArray();
        }

        private TableDataRow[] GetTableData(GetReceiptsRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer, List<GridModuleSetting> lstModuleSetting,
            out int total)
        {
            IList<TableDataRow> lstTableDataRow = new List<TableDataRow>();

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
            int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

            IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
            //string dateFormat = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY,
            //    SystemConfigurationConstants.KEY_DATE_FORMAT).Value;
            string dateFormat = this.GetDateFormat(_settingService);

            DateTime sDate, eDate;
            if (DateTime.TryParseExact(request.fromDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out sDate)
                && DateTime.TryParseExact(request.endDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out eDate))
            {
                CmsReceiptResultSet resultSet = _rmwService
                    .GetReceipts(customer.Bpid,
                    summary.ContractNO, request.pageIndex.Value, request.itemCount.Value, sDate/*.ToCosLocalTime()*/, eDate/*.ToCosLocalTime()*/);

                total = 0;
                if (resultSet != null && resultSet.ReceiptDetails != null)
                {
                    IList<CmsReceiptDetail> receipt = resultSet.ReceiptDetails;
                    var data = _msService.GetObjectDataNoCache<CmsReceiptDetail>(ModuleSettingConstants.RECEIPT_TABLE, receipt, fpgId);

                    total = resultSet.Total;
                    int i = 0;
                    foreach (var d in data)
                    {
                        Dictionary<string, PropertyInfo> objectProperties = d.GetType()
                       .GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => x.Name, x => x);

                        TableDataRow tableDataRow = new TableDataRow();
                        tableDataRow.id = receipt[i].ReceiptNo;
                        i++;
                        IList<GridDataRow> lstSoaDataRow = new List<GridDataRow>();
                        foreach (var column in lstModuleSetting)
                        {
                            var gridDataRow = new GridDataRow();
                            gridDataRow.settingId = column.ResKey;
                            gridDataRow.dataType = column.ObjectFormat;
                            gridDataRow.value = FormatHelper.FormatData(_settingService, column.ObjectFormat, objectProperties[column.ObjectProperty].GetValue(d, null));

                            if (_enableLocalizedText)
                            {
                                string objectPropertyLocal = string.Format("{0}_Local", column.ObjectProperty);
                                if (objectProperties.ContainsKey(objectPropertyLocal))
                                {
                                    object v = objectProperties[objectPropertyLocal].GetValue(d, null);
                                    if (v != null)
                                    {
                                        gridDataRow.valueLoc = FormatHelper.FormatData(_settingService, column.ObjectFormat, objectProperties[objectPropertyLocal].GetValue(d, null));
                                    }
                                }
                            }

                            lstSoaDataRow.Add(gridDataRow);
                        }

                        tableDataRow.tableDataRow = lstSoaDataRow.ToArray();
                        lstTableDataRow.Add(tableDataRow);
                    }
                }

                //DataSourceResult result = new DataSourceResult()
                //{
                //    Data = data,
                //    Total = total
                //};
                //return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                total = 0;
                //DataSourceResult result = new DataSourceResult()
                //{
                //    Data = new object[0],
                //    Total = total
                //};
                //return Json(result, JsonRequestBehavior.AllowGet);
            }

            return lstTableDataRow.ToArray();
        }

        private LanguageContent[] GetLanguageContent(List<GridModuleSetting> headerItems, List<GridModuleSetting> lstModuleSetting)
        {
            IList<LanguageContent> lstLC = new List<LanguageContent>();

            LanguageContent lc = new LanguageContent()
            {
                languageCode = this.GetLanguageCode(_settingService, _languageService),
            };
            lstLC.Add(lc);

            List<LangContent> lst = new List<LangContent>();

            foreach (var item in headerItems)
            {
                //decimal? labelLocid = item.NameLoc != null ? item.NameLoc.Id : (decimal?)null;
                string labelLocid = item.NameLoc != null ? item.NameLoc.Id.ToString() : null;
                if (labelLocid != null)
                {
                    string strTitle = item.NameLoc.Text;

                    LangContent langContent = new LangContent()
                    {
                        id = labelLocid,
                        value = strTitle,
                    };
                    lst.Add(langContent);
                }
            }

            foreach (var item in lstModuleSetting)
            {
                //decimal? labelLocid = item.NameLoc != null ? item.NameLoc.Id : (decimal?)null;
                string labelLocid = item.NameLoc != null ? item.NameLoc.Id.ToString() : null;
                if (labelLocid != null)
                {
                    string strTitle = item.NameLoc.Text;

                    LangContent langContent = new LangContent()
                    {
                        id = labelLocid,
                        value = strTitle,
                    };
                    lst.Add(langContent);
                }
            }

            lc.contents = lst.ToArray();

            return lstLC.ToArray();
        }
    }
}
