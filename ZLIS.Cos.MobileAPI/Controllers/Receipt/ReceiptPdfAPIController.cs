﻿using System;
using System.Linq;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Cos.PdfGenerator;
using ZLIS.Cos.Core;
using ZLIS.Framework.Common.Encrypt;
using ZLIS.Cos.MobileAPI.Filters;
using System.Web;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class ReceiptPdfAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        
        public ILogger Logger { get; set; }

        public ReceiptPdfAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                       ILanguageService languageService,
                                       ISystemSettingService settingService,
                                       IMobileLoginInfoService mobileLoginInfoService,
                                       IRmwService rmwService,
                                       ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _rmwService = rmwService;
            ((RmwServiceAdaptor)_rmwService).SetCacheManager(cacheManager);
            _mobileLoginInfoService = mobileLoginInfoService;
        }

        [System.Web.Http.HttpPost]
        public GetReceiptPdfResponse ReceiptPdf(GetReceiptPdfRequest request)
        {
            if (request == null)
                request = new GetReceiptPdfRequest();
            Logger.Debug(string.Format("-------- Receipts API started. token:{0} contractNo:{1} receiptId{2}", request.token ?? string.Empty, request.contractNo ?? string.Empty, request.receiptId ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetReceiptPdfResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetReceiptPdfErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo))
                {
                    errorInfo.code = (int)GetReceiptPdfErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                string cc = this.GetCountryCode(_settingService).ToUpper();
                if (cc == "SG" || cc == "CN" || cc == "JP" || cc == "IN")
                {
                    errorInfo.code = (int)GetReceiptPdfErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                if (!string.IsNullOrEmpty(request.language) && !request.language.ToUpper().StartsWith("EN"))
                {
                    var language = _languageService.GetLocalLanguage();
                    if (language.PriLan.ToLower() + "-" + language.SubLan.ToLower() != request.language.ToLower())
                    {
                        errorInfo.code = (int)GetReceiptPdfErrorCode.InvalidRequest;
                        errorInfo.message = "Invalid language.";

                        return response;
                    }
                }
                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                
                if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_RECEIPT_OTP_ENABLED, request.token, "Receipt"))
                {
                    errorInfo.code = (int)GetReceiptsErrorCode.InvalidToken;
                    errorInfo.message = "Validate Otp Failed";
                    return response;
                }

                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetReceiptPdfErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }

                //TODO Check Receipt ID
                                
                CmsCustomerProfile profile = null;
                if (!string.IsNullOrEmpty(loginInfo.CustomerInfo))
                {
                    try
                    {
                        BlowFish bf = new BlowFish(CosApplicationContext.Current.EncryptionKey);
                        profile = ZLIS.Framework.Common.Helper.JsonUtils.JsonStringToObject<CmsCustomerProfile>(HttpUtility.UrlDecode(bf.Decrypt_ECB(loginInfo.CustomerInfo)));
                    }
                    catch { }
                }
                if (profile == null)
                    profile = _rmwService.GetCustomerProfile(loginInfo.CustomerActivity.Customer.Bpid);
                ISessionState session = CosApplicationContext.Current.Session;
                session.CurrentCmsCustomerProfile = profile;
                response.data = GetReceiptPdf(request, loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetReceiptPdfErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get receipt pdf");
            }

            return response;
        }

        private FileContent GetReceiptPdf(GetReceiptPdfRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            FileContent result = new FileContent();
            string reportFormat = ReportConstants.FORMAT_PDF;
            string receiptNo = request.receiptId;

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid).Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
            
            int bpId = customer.Bpid;

            string ext = "";
            string mimeType = "";
            switch (reportFormat)
            {
                case ReportConstants.FORMAT_PDF:
                    ext = ".pdf";
                    mimeType = "application/pdf";
                    break;
                default:
                    ext = ".pdf";
                    mimeType = "application/pdf";
                    break;
            }
            string cc = GetCountryCode();
            string downloadName = string.Format("{0}_{1}_{2:yyyyMMddHHmmss}{3}", ReportConstants.KEY_RECEIPT, summary.ContractNO, this.Now(), ext);
            result.fileName = downloadName;
            byte[] reportcontent = null;
            if (string.IsNullOrEmpty(request.language) || request.language.ToUpper().StartsWith("EN"))
                reportcontent = ReportGeneraterFactory.GetReporter(cc).GenerateReceipt(ReportConstants.TEMPLATE_RECEIPT_REPORT, downloadName, summary.ContractNO, cc,
                 "EN", receiptNo, false);
            else
                reportcontent = ReportGeneraterFactory.GetReporter(cc).GenerateReceipt(ReportConstants.TEMPLATE_RECEIPT_REPORT, downloadName, summary.ContractNO, cc,
                                 cc, receiptNo, true);
            result.fileSize = reportcontent.Length;
            result.mimeType = mimeType;
            result.content = Convert.ToBase64String(reportcontent);
            //ZLIS.Framework.Common.IO.IOHelper.SaveMediaFile("D:\\ReportTemp\\" + downloadName, reportcontent);
            return result;
        }

        #region Helper

        private string GetCountryCode()
        {
            var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY,
                SystemConfigurationConstants.KEY_COUNTRY_CODE);
            if (countryCode != null)
            {
                return countryCode.Value;
            }

            return null;
        }

        #endregion

    }
}
