﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.UIManagement;
using ZLIS.Cos.Core.UIManagement.DomainModel;
using ZLIS.Cos.MobileAPI;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.MobileAPI.Filters;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class StatementOfAccountAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        private IModuleSettingService _msService;

        private bool _enableLocalizedText;

        public ILogger Logger { get; set; }

        public StatementOfAccountAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                               ILanguageService languageService,
                                               ISystemSettingService settingService,
                                               IMobileLoginInfoService mobileLoginInfoService,
                                               IRmwService rmwService,
                                               IModuleSettingService msService,
                                               ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _mobileLoginInfoService = mobileLoginInfoService;

            _rmwService = rmwService;
            ((RmwServiceAdaptor) _rmwService).SetCacheManager(cacheManager);

            _msService = msService;
            _enableLocalizedText = this.EnableLocalizedText(_settingService, _languageService);
        }

        [System.Web.Http.HttpPost]
        public GetStatementOfAccountResponse StatementOfAccount(GetStatementOfAccountRequest request)
        {
            if (request == null)
                request = new GetStatementOfAccountRequest();
            Logger.Debug(string.Format("-------- GetStatementOfAccount API started. token:{0} contractNo:{1} fromDate{2} endDate{3}",
                request.token ?? string.Empty, request.contractNo ?? string.Empty, request.fromDate ?? string.Empty, request.endDate ?? string.Empty));

            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetStatementOfAccountResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetStatementOfAccountErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo) ||
                    string.IsNullOrEmpty(request.fromDate) ||
                    string.IsNullOrEmpty(request.endDate) ||
                    request.pageIndex == null ||
                    request.itemCount == null)
                {
                    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);

                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }
                if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_SOA_OTP_ENABLED, request.token, "SOA"))
                {
                    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidToken;
                    errorInfo.message = "Validate Otp Failed";
                    return response;
                }
                IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
                string dateFormat = this.GetDateFormat(_settingService);

                DateTime sDate, eDate;
                if (!DateTime.TryParseExact(request.fromDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out sDate)
                    || !DateTime.TryParseExact(request.endDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out eDate))
                {
                    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidFromDate_EndDate;
                    errorInfo.message = "Invalid from date/end date";

                    return response;
                }
                               
                response.data = GetStatementOfAccount(request, loginInfo.CustomerActivity.Customer,sDate,eDate);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetStatementOfAccountErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get statement of account");
            }

            Logger.Debug("-------- GetStatementOfAccount API Ends --------------------");
            return response;
        }

        private StatementOfAccount GetStatementOfAccount(GetStatementOfAccountRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer, DateTime sDate, DateTime eDate)
        {
            StatementOfAccount statementOfAccount =
                new StatementOfAccount();

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
            int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

            var lstModuleSetting = _msService.GetGridModuleSettings_NoCache(ModuleSettingConstants.SOA_TABLE, fpgId)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();
            var headerItems = _msService.GetGridModuleSettings_NoCache(ModuleSettingConstants.SOA_HEADER, fpgId)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();

            CmsContractDetail contract = GetContractDetails(customer.Bpid, summary.ContractNO);

            CmsSoaResultSet soa = _rmwService.GetSOA(customer.Bpid, summary.ContractNO,
                (int)CmsGroupTxnType.All, 1, 10, sDate, eDate);
            CmsSoaResultSet soa2 = _rmwService.GetSOA(customer.Bpid, summary.ContractNO,
                (int)CmsGroupTxnType.All, 1, 10, null, null);

            if (contract != null && soa != null)
            {
                //ZLIS.Cos.Web.Models.MyAccount.MyAccountSOADisplayModel model = new Models.MyAccount.MyAccountSOADisplayModel()
                //{
                //    StartDate = contract.StartDate,
                //    ContractNo = contract.ContractNO,
                //    ContractId = summary.ContractId,
                //    OpeningBalance = soa.OpeningBalance,
                //    ClosingBalance = soa.ClosingBalance,
                //    MiscCharges = contract.MiscellaneousCharges == null || contract.MiscellaneousCharges.Count == 0 ? 0 : contract.MiscellaneousCharges.Select(x => x.DueAmount).Sum(),
                //    DisplayRowNo = nDisplayRowNo,
                //    DefaultFromToMonthDuration = nDefaultFromToMonthDuration,
                //    TransactionDisplayDropdownEnabled = bTransactionDisplayDropdownEnabled,
                //    ExportPDFEnabled = bExportPDFEnabled,
                //    ExportExcelEnabled = bExportExcelEnabled,
                //};

                Dictionary<string, PropertyInfo> conDetailProperties =
                    typeof (CmsContractDetail).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                              .ToDictionary(x => "CmsContractDetail_" + x.Name, x => x);
                Dictionary<string, PropertyInfo> soaResultProperties =
                    typeof (CmsSoaResultSet).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                            .ToDictionary(x => "CmsSoaResultSet_" + x.Name, x => x);
                Dictionary<string, PropertyInfo> objectProperties =
                    conDetailProperties.Union(soaResultProperties).ToDictionary(x => x.Key, x => x.Value);

                Dictionary<string, object> objectList = new Dictionary<string, object>();
                objectList.Add("CmsContractDetail", contract);
                objectList.Add("CmsSoaResultSet", soa);
                Dictionary<string, object> objectList2 = new Dictionary<string, object>();
                objectList2.Add("CmsSoaResultSet", soa2);

                Logger.Debug("Get settings started.");
                statementOfAccount.settings = GetSettings(headerItems, lstModuleSetting, objectProperties, objectList,
                                                          objectList2, contract, sDate, eDate);
                Logger.Debug("Get settings succeeded.");

                int total;
                Logger.Debug("Get tableData started.");
                statementOfAccount.tableData = GetTableData(request, customer, lstModuleSetting, out total);
                Logger.Debug("Get tableData succeeded.");
                statementOfAccount.totalCount = total;

                Logger.Debug("Get language content started.");
                statementOfAccount.languageContent = _enableLocalizedText
                                                         ? GetLanguageContent(headerItems, lstModuleSetting, contract)
                                                         : null;
                Logger.Debug("Get language content succeeded.");

            }

            return statementOfAccount;
        }

        private Settings GetSettings(List<GridModuleSetting> headerItems, List<GridModuleSetting> lstModuleSetting,
            Dictionary<string, PropertyInfo> objectProperties, Dictionary<string, object> objectList, 
            Dictionary<string, object> objectList2,
            CmsContractDetail contract,DateTime sDate,DateTime eDate)
        {
            var settings = new Settings();

            settings.headerSettings = GetHeaderSettings(headerItems, objectProperties, objectList, objectList2, contract, sDate, eDate);

            settings.tableSettings = GetTableSettings(lstModuleSetting);

            return settings;
        }

        private HeaderSetting[] GetHeaderSettings(List<GridModuleSetting> headerItems,
            Dictionary<string, PropertyInfo> objectProperties, Dictionary<string, object> objectList,
            Dictionary<string, object> objectList2,
            CmsContractDetail contract, DateTime sDate,DateTime eDate)
        {
            IList<HeaderSetting> lstHeaderSettings =
                new List<HeaderSetting>();

            foreach (var item in headerItems)
            {
                if (objectProperties.ContainsKey(item.ObjectProperty))
                {
                    foreach (var objectName in objectList.Keys)
                    {
                        if (item.ObjectProperty.Contains(objectName))
                        {
                            string property = item.ObjectProperty;
                            var Object = objectList[objectName];
                            var Object2 = objectList2[objectName];

                            HeaderSetting headerSetting = new HeaderSetting()
                            {
                                id = item.ResKey,
                                label = item.Name,
                                order = item.Order,
                                labelLocid = _enableLocalizedText ? (item.NameLoc != null ? item.NameLoc.Id.ToString() : null) : null,
                                //(item.NameLoc != null ? item.NameLoc.Id : (int?)null) : (int?)null,
                                value = FormatHelper.FormatData(_settingService, item.ObjectFormat, objectProperties[property].GetValue(Object, null))
                            };
                            if (item.ResKey.Equals("SOA.OpeningBalance", StringComparison.OrdinalIgnoreCase))
                            {
                                if (contract != null)
                                {
                                    headerSetting.label = string.Format("{0} ({1})",
                                        headerSetting.label, FormatHelper.FormatData(_settingService,
                                        "DATE", contract.StartDate));//sDate
                                }

                                if (Object2 != null)
                                {
                                    headerSetting.value = FormatHelper.FormatData(_settingService, item.ObjectFormat, objectProperties[property].GetValue(Object2, null));
                                }
                            }
                            if (item.ResKey.Equals("SOA.ClosingBalance", StringComparison.OrdinalIgnoreCase))
                            {
                                headerSetting.label = string.Format("{0} ({1})",
                                    headerSetting.label, FormatHelper.FormatData(_settingService,
                                    "DATE", DateTime.Now));//eDate
                                if (Object2 != null)
                                {
                                    headerSetting.value = FormatHelper.FormatData(_settingService, item.ObjectFormat, objectProperties[property].GetValue(Object2, null));
                                }
                            }

                            if (_enableLocalizedText)
                            {
                                string objectPropertyLocal = string.Format("{0}_Local", property);
                                if (objectProperties.ContainsKey(objectPropertyLocal))
                                {
                                    object v = objectProperties[objectPropertyLocal].GetValue(Object, null);
                                    if (v != null)
                                    {
                                        headerSetting.valueLoc = FormatHelper.FormatData(_settingService, item.ObjectFormat, v);
                                    }
                                }
                            }

                            lstHeaderSettings.Add(headerSetting);
                        }
                    }
                }
            }

            return lstHeaderSettings.ToArray();
        }

        private TableSetting[] GetTableSettings(List<GridModuleSetting> lstModuleSetting)
        {
            IList<TableSetting> lstTableSettings = new List<TableSetting>();

            string soaHeadingField = this.SoaHeadingField();
            string[] arrSoaHeadingField = soaHeadingField.Split(';');

            foreach (var item in lstModuleSetting)
            {
                TableSetting tableSetting = new TableSetting()
                {
                    id = item.ResKey,
                    label = item.Name,
                    order = item.Order,
                    labelLocid = _enableLocalizedText ? (item.NameLoc != null ? item.NameLoc.Id.ToString() : null) : null,
                    //(item.NameLoc != null ? item.NameLoc.Id : (int?)null) : (int?)null,
                    isHeading = arrSoaHeadingField.Contains(item.ResKey),
                };

                lstTableSettings.Add(tableSetting);
            }

            return lstTableSettings.ToArray();
        }

        private TableDataRow[] GetTableData(GetStatementOfAccountRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer, List<GridModuleSetting> lstModuleSetting, out int total)
        {
            IList<TableDataRow> lstTableDataRow = new List<TableDataRow>();

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
            int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

            IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
            //string dateFormat = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY,
            //    SystemConfigurationConstants.KEY_DATE_FORMAT).Value;
            string dateFormat = this.GetDateFormat(_settingService);

            DateTime sDate, eDate;
            if (DateTime.TryParseExact(request.fromDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None,
                                       out sDate)
                &&
                DateTime.TryParseExact(request.endDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None,
                                       out eDate))
            {
                CmsSoaResultSet resultSet = _rmwService.GetSOA(customer.Bpid,
                                                               summary.ContractNO, 0, request.pageIndex.Value,
                                                               request.itemCount.Value,
                                                               sDate /*.ToCosLocalTime()*/, eDate /*.ToCosLocalTime()*/);

                IList<CmsSoaDetail> soa = resultSet.SoaDetails;
                var data = _msService.GetObjectDataNoCache<CmsSoaDetail>(ModuleSettingConstants.SOA_TABLE, soa, fpgId);

                total = resultSet.Total;

                foreach (var d in data)
                {
                    Dictionary<string, PropertyInfo> objectProperties = d.GetType()
                                                                         .GetProperties(BindingFlags.Public |
                                                                                        BindingFlags.Instance)
                                                                         .ToDictionary(x => x.Name, x => x);

                    TableDataRow tableDataRow = new TableDataRow();
                    IList<GridDataRow> lstSoaDataRow = new List<GridDataRow>();
                    foreach (var column in lstModuleSetting)
                    {
                        var soaDataRow = new GridDataRow();
                        soaDataRow.settingId = column.ResKey;
                        soaDataRow.dataType = column.ObjectFormat;
                        soaDataRow.value = FormatHelper.FormatData(_settingService, column.ObjectFormat,
                                                                   objectProperties[column.ObjectProperty].GetValue(d,
                                                                                                                    null));

                        if (_enableLocalizedText)
                        {
                            string objectPropertyLocal = string.Format("{0}_Local", column.ObjectProperty);
                            if (objectProperties.ContainsKey(objectPropertyLocal))
                            {
                                object v = objectProperties[objectPropertyLocal].GetValue(d, null);
                                if (v != null)
                                {
                                    soaDataRow.valueLoc = FormatHelper.FormatData(_settingService, column.ObjectFormat,
                                                                                  v);
                                }
                            }
                        }

                        lstSoaDataRow.Add(soaDataRow);
                    }

                    tableDataRow.tableDataRow = lstSoaDataRow.ToArray();
                    lstTableDataRow.Add(tableDataRow);
                }
            }
            else
            {
                total = 0;
            }

            return lstTableDataRow.ToArray();
        }

        private LanguageContent[] GetLanguageContent(List<GridModuleSetting> headerItems,
            List<GridModuleSetting> lstModuleSetting, CmsContractDetail contract)
        {
            IList<LanguageContent> lstLC = new List<LanguageContent>();

            LanguageContent lc = new LanguageContent()
            {
                languageCode = this.GetLanguageCode(_settingService, _languageService),
            };
            lstLC.Add(lc);

            List<LangContent> lst = new List<LangContent>();

            foreach (var item in headerItems)
            {
                //decimal? labelLocid = item.NameLoc != null ? item.NameLoc.Id : (decimal?)null;
                string labelLocid = item.NameLoc != null ? item.NameLoc.Id.ToString() : null;
                if (labelLocid != null)
                {
                    string strTitle = item.NameLoc.Text;

                    LangContent langContent = new LangContent()
                    {
                        id = labelLocid,
                        value = strTitle,
                    };

                    if (item.ResKey.Equals("SOA.OpeningBalance", StringComparison.OrdinalIgnoreCase))
                    {
                        langContent.value = string.Format("{0} ({1})",
                            langContent.value, FormatHelper.FormatData(_settingService,
                            "DATE", contract.StartDate));
                    }
                    if (item.ResKey.Equals("SOA.ClosingBalance", StringComparison.OrdinalIgnoreCase))
                    {
                        langContent.value = string.Format("{0} ({1})",
                            langContent.value, FormatHelper.FormatData(_settingService,
                            "DATE", DateTime.Now));
                    }

                    lst.Add(langContent);
                }
            }

            foreach (var item in lstModuleSetting)
            {
                //decimal? labelLocid = item.NameLoc != null ? item.NameLoc.Id : (decimal?)null;
                string labelLocid = item.NameLoc != null ? item.NameLoc.Id.ToString() : null;
                if (labelLocid != null)
                {
                    string strTitle = item.NameLoc.Text;

                    LangContent langContent = new LangContent()
                    {
                        id = labelLocid,
                        value = strTitle,
                    };
                    lst.Add(langContent);
                }
            }

            lc.contents = lst.ToArray();

            return lstLC.ToArray();
        }

        #region Helper

        private CmsContractDetail GetContractDetails(int Bpid, string ContractNo)
        {
            return _rmwService.GetContractDetails(Bpid, ContractNo);
        }

        #endregion

    }
}
