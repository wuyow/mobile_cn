﻿using System;
using System.Linq;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using Aspose.Words;
using ZLIS.Cos.Core.Common;
using ZLIS.Cos.PdfGenerator;
using ZLIS.Framework.Common.Encrypt;
using ZLIS.Cos.Core;
using ZLIS.Cos.MobileAPI.Filters;
using System.Web;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class StatementOfAccountPdfAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        public ILogger Logger { get; set; }

        public StatementOfAccountPdfAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                                  ILanguageService languageService,
                                                  ISystemSettingService settingService,
                                                  IMobileLoginInfoService mobileLoginInfoService,
                                                  IRmwService rmwService,
                                                  ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _rmwService = rmwService;
            ((RmwServiceAdaptor)_rmwService).SetCacheManager(cacheManager);
            _mobileLoginInfoService = mobileLoginInfoService;
        }

        [System.Web.Http.HttpPost]
        public GetStatementOfAccountPdfResponse StatementOfAccountPdf(GetStatementOfAccountPdfRequest request)
        {
            if (request == null)
                request = new GetStatementOfAccountPdfRequest();
            Logger.Debug(string.Format("-------- GetStatementOfAccountPDF API started. token:{0} contractNo:{1} fromDate{2} endDate{3}",
                request.token ?? string.Empty, request.contractNo ?? string.Empty, request.fromDate ?? string.Empty, request.endDate ?? string.Empty));

            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetStatementOfAccountPdfResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetStatementOfAccountPdfErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo) ||
                    string.IsNullOrEmpty(request.fromDate) ||
                    string.IsNullOrEmpty(request.endDate))
                {
                    errorInfo.code = (int)GetStatementOfAccountPdfErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                string cc = this.GetCountryCode(_settingService).ToUpper();
                if (cc ==  "JP" )
                {
                    errorInfo.code = (int)GetReceiptPdfErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                if (!string.IsNullOrEmpty(request.language) && !request.language.ToUpper().StartsWith("EN"))
                {
                    var language = _languageService.GetLocalLanguage();
                    if (language.PriLan.ToLower() + "-" + language.SubLan.ToLower() != request.language.ToLower())
                    {
                        errorInfo.code = (int)GetStatementOfAccountPdfErrorCode.InvalidRequest;
                        errorInfo.message = "Invalid language.";

                        return response;
                    }
                }
                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);

                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetStatementOfAccountPdfErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }
                if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_SOA_OTP_ENABLED, request.token, "SOA"))
                {
                    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidToken;
                    errorInfo.message = "Validate Otp Failed";
                    return response;
                }
                IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
                string dateFormat = GetDateFormat();

                DateTime sDate, eDate;
                if (!DateTime.TryParseExact(request.fromDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out sDate)
                    || !DateTime.TryParseExact(request.endDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out eDate))
                {
                    errorInfo.code = (int)GetStatementOfAccountPdfErrorCode.InvalidFromDate_EndDate;
                    errorInfo.message = "Invalid from date/end date";

                    return response;
                }

                CmsCustomerProfile profile = null;
                if (!string.IsNullOrEmpty(loginInfo.CustomerInfo))
                {
                    try
                    {
                        BlowFish bf = new BlowFish(CosApplicationContext.Current.EncryptionKey);
                        profile = ZLIS.Framework.Common.Helper.JsonUtils.JsonStringToObject<CmsCustomerProfile>(HttpUtility.UrlDecode(bf.Decrypt_ECB(loginInfo.CustomerInfo)));
                    }
                    catch { }
                }
                if (profile == null)
                    profile = _rmwService.GetCustomerProfile(loginInfo.CustomerActivity.Customer.Bpid);
                ISessionState session = CosApplicationContext.Current.Session;
                session.CurrentCmsCustomerProfile = profile;
                response.data = GetStatementOfAccountPdf(request, loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetStatementOfAccountPdfErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get statement of account pdf");
            }

            Logger.Debug("-------- GetStatementOfAccountPDF API Ends --------------------");

            return response;
        }

        private FileContent GetStatementOfAccountPdf(GetStatementOfAccountPdfRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            FileContent result = new FileContent();
            string reportFormat = ReportConstants.FORMAT_PDF;


            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            var summaries = rmwServiceAdaptor.GetAccountSummary(customer.Bpid);
           
            string contractno = request.contractNo;
            var contract = GetContractDetails(customer.Bpid, contractno);
            IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
            //string dateFormat = _ssService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT).Value;
            string dateFormat = GetDateFormat();
            DateTime sDate, eDate;
            if (DateTime.TryParseExact(request.fromDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out sDate)
                && DateTime.TryParseExact(request.endDate, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out eDate))
            {
                SaveFormat sf;

                string ext = "";
                string mimeType = "";
                switch (reportFormat)
                {
                    case ReportConstants.FORMAT_PDF:
                        ext = ".pdf";
                        mimeType = "application/pdf";
                        sf = SaveFormat.Pdf;
                        break;
                    default:
                        ext = ".pdf";
                        mimeType = "application/pdf";
                        sf = SaveFormat.Pdf;
                        break;
                }

                //string cc = CosApplicationContext.Current.CurrentCountry;
                string cc = GetCountryCode();
                string downloadName = string.Format("{0}_{1}_{2:yyyyMMddHHmmss}{3}",
                   ReportConstants.KEY_SOA, contractno, TimeHelper.Now, ext);
                result.fileName = downloadName;
                CmsAccountSummary summary = summaries.Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
                string fctitle = string.Empty;
                if (summary.FinancialProductGroup != null)
                {
                    fctitle = summary.FinancialProductGroup.Title;
                }
                byte[] reportcontent = null;
                if (string.IsNullOrEmpty(request.language)||request.language.ToUpper().StartsWith("EN"))
                    reportcontent = ReportGeneraterFactory.GetReporter(cc).GenerateSOAReport(ReportConstants.TEMPLATE_SOA_REPORT, downloadName, contract, fctitle,
                    cc, "EN" ,0, sDate, eDate,false);//, sf);
                else
                    reportcontent = ReportGeneraterFactory.GetReporter(cc).GenerateSOAReport(ReportConstants.TEMPLATE_SOA_REPORT, downloadName, contract, fctitle,
                    cc, cc, 0, sDate, eDate, true);
                result.fileSize = reportcontent.Length;
                result.mimeType = mimeType;
                result.content= Convert.ToBase64String(reportcontent);
                //ZLIS.Framework.Common.IO.IOHelper.SaveMediaFile("D:\\ReportTemp\\" + downloadName, reportcontent);
                return result;
            }

            return null;
        }

        #region Helper

        private string GetDateFormat()
        {
            var dateFormat = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT);
            if (dateFormat != null)
            {
                return dateFormat.Value;
            }

            return null;
        }

        private string GetCountryCode()
        {
            var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY,
                SystemConfigurationConstants.KEY_COUNTRY_CODE);
            if (countryCode != null)
            {
                return countryCode.Value;
            }

            return null;
        }

        private string GetLanguage()
        {
            string strLanguage = "";
            var language = _languageService.GetLocalLanguage();
            if (language != null)
            {

                strLanguage = string.Format("{0}-{1}", language.PriLan, language.SubLan);
            }

            return strLanguage;
        }

        private CmsContractDetail GetContractDetails(int Bpid, string ContractNo)
        {
            return _rmwService.GetContractDetails(Bpid, ContractNo);
        }

        #endregion

    }
}
