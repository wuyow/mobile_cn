﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.UIManagement;
using ZLIS.Cos.Core.UIManagement.DomainModel;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.CountrySpecific;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class ContractDetailsAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IPageResourceService _pageResourceService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        private IModuleSettingService _msService;
        private ISpecialLogic _specialLogic;
        private bool _enableLocalizedText;

        public ILogger Logger { get; set; }


        public ContractDetailsAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                            ILanguageService languageService,
                                            ISystemSettingService settingService,
                                            IPageResourceService pageResourceService,
                                            IMobileLoginInfoService mobileLoginInfoService,
                                            IRmwService rmwService,
                                            IModuleSettingService msService,
                                            ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _pageResourceService = pageResourceService;
            _mobileLoginInfoService = mobileLoginInfoService;
            _rmwService = rmwService;
            ((RmwServiceAdaptor) _rmwService).SetCacheManager(cacheManager);

            _msService = msService;
            _specialLogic = SpecialLogicFactory.GetSpecialLogic(this.GetCountryCode(_settingService));
            _enableLocalizedText = this.EnableLocalizedText(_settingService, _languageService);
        }

        [System.Web.Http.HttpPost]
        public GetContractDetailsResponse ContractDetails(GetContractDetailsRequest request)
        {
            if (request == null)
                request = new GetContractDetailsRequest();
            Logger.Debug(string.Format("-------- GetContractDetails API started.token:{0} contractNo:{1}", request.token ?? string.Empty, request.contractNo ?? string.Empty));

            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetContractDetailsResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetContractDetailsErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo))
                {
                    errorInfo.code = (int)GetContractDetailsErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                //if (loginInfo == null)
                //{
                //    errorInfo.code = (int)GetContractDetailsErrorCode.InvalidToken;
                //    errorInfo.message = "Invalid token";

                //    return response;
                //}

                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }

                //CheckSessionErrorCode errorCode;
                //if (!this.CheckSession(loginInfo, out errorCode))
                //{
                //    switch (errorCode)
                //    {
                //        case CheckSessionErrorCode.HaveBeenLoggedOut:
                //            errorInfo.code = (int)GetContractDetailsErrorCode.HaveBeenLoggedOut;
                //            errorInfo.message = "Have been logged out";
                //            break;
                //        case CheckSessionErrorCode.SessionTimeOut:
                //            errorInfo.code = (int)GetContractDetailsErrorCode.SessionTimeout;
                //            errorInfo.message = "Session timeout";
                //            break;
                //        case CheckSessionErrorCode.ForcedLogout:
                //            errorInfo.code = (int)GetContractDetailsErrorCode.ForcedLogout;
                //            errorInfo.message = "Forced logout";
                //            break;
                //    }

                //    return response;
                //}

                response.data = GetContractDetails(request.contractNo, loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetContractDetailsErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get contract details");
            }

            Logger.Debug("-------- GetContractDetails API Ends.");

            return response;
        }

        private ContractDetails GetContractDetails(string contractNo,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            CmsContractDetail contract = GetContractDetails(customer.Bpid, contractNo);

            ContractDetails contractDetails = new ClientModel.Models.ContractDetails();
            contractDetails.contractNo = contractNo;
            contractDetails.contractStatus = contract.ContractStatusDisplay;
            contractDetails.contractStatusLoc = GetLocContractStatus(contract.ContractStatusDisplay);
           
            Logger.Debug("Get sections started.");
            contractDetails.sections = GetSections(contractNo, customer);
            Logger.Debug("Get sections succeeded.");

            Logger.Debug("Get language content started.");
            contractDetails.languageContent = _enableLocalizedText ?
                GetLanguageContent(contractNo, customer) : null;
            Logger.Debug("Get language content succeeded.");

            return contractDetails;
        }

        private string GetLocContractStatus(string status)
        {
            CultureInfo mobileUiCulture = new CultureInfo(this.GetLanguageCode(_settingService,_languageService));

            if (status == ZLIS.Cos.RmwService.CmsContractStatus.Current)
            {
                return this.Resource(_pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusCurrent", mobileUiCulture);
            }
            else if (status == ZLIS.Cos.RmwService.CmsContractStatus.PaidInFull)
            {
                return this.Resource(_pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusPaidInFull", mobileUiCulture);
            }
            else if (status == ZLIS.Cos.RmwService.CmsContractStatus.Overdue)
            {
                return this.Resource(_pageResourceService, "MyAccount_MyAccount_ContractSummary,PageLabel_ContractStatusOverdue", mobileUiCulture);
            }
            else
                return string.Empty;
        }

        private Section[] GetSections(string contractNo,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            IList<Section> lst = new List<Section>();

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            var summaries = rmwServiceAdaptor.GetAccountSummary(customer.Bpid);

            CmsAccountSummary summary = null;

            summary = summaries.Where(x => x.ContractNO == contractNo).FirstOrDefault();

            if (summary != null)
            {
                int? fpgId = 0;// summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

                #region TW Requirement of show bank detail in Contract detail
                bool showBankInfo = false;
                CmsContractDetail contract = GetContractDetails(customer.Bpid, summary.ContractNO);

                var bankList = rmwServiceAdaptor.GetBanks(customer.Bpid);
                if (bankList != null)
                {
                    CmsBank bank = bankList.Where(x => x.ContractNO == summary.ContractNO).FirstOrDefault();
                    showBankInfo = _specialLogic.ShowBankDetail(contract.FinancialDetail.PaymentMode, contract.FinancialDetail.PaymentCode, contract.GiroStatusCode, bank);
                }
                #endregion

                var lstModuleSetting = _msService.GetModuleSettings_NoCache(ModuleSettingConstants.CONTRACT_DETAIL, ModuleSettingConstants.FILTER_TYPE_CONTRACT_STATUS, summary.ContractStatusCode, fpgId)
                     .Where(x => x.Visible && (x.ResKey != ModuleSettingConstants.MAKE_PAYMENT_PAYMENT_INFO || showBankInfo))
                     .OrderBy(x => x.Order).ToList();
                
                    foreach (var moduleSetting in lstModuleSetting)
                    {
                        if (!moduleSetting.ResKey.Equals("CNRT_DETAIL_VEHICLE", StringComparison.OrdinalIgnoreCase))
                        {
                            IList<CmsAsset> assetList = null;
                            if (contract != null)
                            {
                                assetList = contract.ContractAsset;
                                
                            }
                            Section section = new Section()
                            {
                                id = moduleSetting.ResKey,
                                label = moduleSetting.Name,
                                order = moduleSetting.Order,
                                labelLocid = _enableLocalizedText ? (moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id.ToString() : null) : null,
                                //(moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (int?)null) : (int?)null,
                                fields = CNRT_DETAIL(customer, moduleSetting.ResKey, contractNo, (object)assetList)
                            };

                            lst.Add(section);
                        }
                        else
                        {
                            int idx = 1;
                            if (contract != null)
                            {
                                IList<CmsAsset> assetList = contract.ContractAsset;
                                foreach (var vehicle in assetList)
                                {
                                    Section section = new Section()
                                    {
                                        id = string.Format("{0}_{1}", moduleSetting.ResKey, idx++),
                                        label = moduleSetting.Name,
                                        order = moduleSetting.Order,
                                        labelLocid = _enableLocalizedText ? (moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id.ToString() : null) : null,
                                        //(moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (int?)null) : (int?)null,
                                        fields = CNRT_DETAIL(customer, moduleSetting.ResKey, contractNo, (object)vehicle)
                                    };

                                    lst.Add(section);
                                }
                            }
                        }
                }

                //ViewBag.Setting = _mfsService.GetModuleFilterSettings(ModuleSettingConstants.CONTRACT_DETAIL, ModuleSettingConstants.FILTER_TYPE_CONTRACT_STATUS, summary.ContractStatusCode)
                //    .ToDictionary(x => string.Format("{0}_{1}", summary.ContractNO, x.ResKey), x => x.Visible);

                //return View("ContractDetails", summary);
            }
            //else
            //{
            //    return RedirectToAction("ContractSummary");
            //}

            return lst.ToArray();
        }

        private Field[] CNRT_DETAIL(ZLIS.Cos.Core.Account.DomainModel.Customer customer,
            string module, string ContractNo/*, string Title*/, object vehicle)
        {
            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            var summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == ContractNo).First();

            int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

            IEnumerable<GridModuleSetting> details = _msService.GetGridModuleSettings_NoCache(module, fpgId)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();
            if (details.Count() > 0)
            {
                CmsContractDetail conDetail = GetContractDetails(customer.Bpid, ContractNo);
                CmsBank bank = rmwServiceAdaptor.GetBanks(customer.Bpid)
                    .Where(x => x.ContractNO == ContractNo).FirstOrDefault();

                if (conDetail != null)
                {
                    Dictionary<string, PropertyInfo> conDetailProperties = typeof(CmsContractDetail).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsContractDetail_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> finDetailProperties = typeof(CmsFinancialDetail).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsFinancialDetail_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> accSummaryProperties = typeof(CmsAccountSummary).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsAccountSummary_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> assetProperties = typeof(CmsAsset).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsAsset_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> assetInsurance = typeof(CmsAssetInsurance).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsAssetInsurance_" + x.Name, x => x);
                    Dictionary<string, PropertyInfo> bankProperties = typeof(CmsBank).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsBank_" + x.Name, x => x);

                    CmsFinancialDetail finDetail = conDetail.FinancialDetail;
                    Dictionary<string, PropertyInfo> objectProperties = conDetailProperties.Union(finDetailProperties)
                        .Union(accSummaryProperties)
                        .Union(assetProperties)
                        .Union(bankProperties).ToDictionary(x => x.Key, x => x.Value);

                    Dictionary<string, object> objectList = new Dictionary<string, object>();
                    objectList.Add("CmsContractDetail", conDetail);
                    objectList.Add("CmsFinancialDetail", finDetail);
                    objectList.Add("CmsAccountSummary", summary);

                    if ("Cheque".Equals(conDetail.FinancialDetail.PaymentModeCode, StringComparison.OrdinalIgnoreCase)
                        && bank != null && (bank.GiroStatusCode == "Approved" || bank.GiroStatusCode == "New"))
                    {
                        objectList.Add("CmsBank", null);
                    }
                    else
                    {
                        objectList.Add("CmsBank", bank);
                    }

                    IList<CmsAsset> assetList = conDetail.ContractAsset;

                    objectList.Add("CmsAsset", assetList);
                    //ViewBag.Object = assetList;

                    //ViewBag.ObjectList = objectList;
                    //ViewBag.ObjectProperties = objectProperties;
                    //ViewBag.ErrorMessage = "";


                    //ViewBag.Title = Title;
                    //ViewBag.lstModuleSetting = details;
                    //ViewBag.Overdue = summary.ContractStatusDisplayCode == CmsContractStatus.Overdue;
                    //ViewBag.ContractNo = summary.ContractNO;
                    //ViewBag.ContractId = summary.ContractId;
                    //ViewBag.ShowLink = ShowGiroLink(conDetail.FinancialDetail.PaymentMode, conDetail.FinancialDetail.PaymentCode, bank);
                    //ViewBag.HideGiroStatus = HideGiroStatus(conDetail.FinancialDetail.PaymentMode, conDetail.FinancialDetail.PaymentCode);

                    bool overdue = summary.ContractStatusDisplayCode == CmsContractStatus.Overdue;
                    string contractNo = summary.ContractNO;
                    string contractId = summary.ContractId;
                    bool showLink = _specialLogic.ShowGiroLink(conDetail.FinancialDetail.PaymentMode, conDetail.FinancialDetail.PaymentCode, bank);
                    bool hideGiroStatus = _specialLogic.HideGiroStatus(conDetail.FinancialDetail.PaymentMode, conDetail.FinancialDetail.PaymentCode);

                    IList<LoginField> lst = new List<LoginField>();
                    lst = GetFields(details, objectList, objectProperties, vehicle, overdue, showLink, contractId, hideGiroStatus);
                    //foreach (var d in details)
                    //{
                    //    var field = new Field()
                    //    {
                    //        id = d.ResKey,
                    //        module = d.ModuleName,
                    //        label = d.Name,
                    //        labelLocId = d.NameLoc != null ? d.NameLoc.Id : (int?)null,
                    //        order = d.Order,
                    //    };
                    //    lst.Add(field);
                    //}

                    return lst.ToArray();
                }
                else
                {
                    return null;
                    //ViewBag.ObjectList = null;
                    //ViewBag.ObjectProperties = null;
                    //ViewBag.ErrorMessage = "Service unavailable.";
                }
            }
            else
            {
                return null;
                //return false;
            }
        }

        private IList<LoginField> GetFields(IEnumerable<GridModuleSetting> lstModuleSetting,
            Dictionary<string, object> ObjectList,
            Dictionary<string, System.Reflection.PropertyInfo> ObjectProperties,
            object vehicle, bool overdue, bool showLink, string contractId, bool hideGiroStatus)
        {
            IList<LoginField> lst = new List<LoginField>();

            string value;
            string valueLoc;

            //int i = 0;
            foreach (ZLIS.Cos.Core.UIManagement.DomainModel.GridModuleSetting column in lstModuleSetting)
            {
                //if (ObjectProperties.ContainsKey(column.ObjectProperty))
                {
                    if (column.ObjectProperty.StartsWith("CmsAsset_"))
                    {
                        this.GetDisplayValue(_pageResourceService, _settingService, _languageService,_specialLogic,
                            vehicle, ObjectProperties, column.ObjectProperty, column.ObjectFormat,
                            overdue, showLink, contractId, out value, out valueLoc);
                        //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")">
                        //    <span id="contract_left">@(column.GetDisplayName(cultureInfo))</span> 
                        //   <span id="contract_right">@Html.Raw(value)</span>
                        //</li>

                        var field = new LoginField()
                        {
                            id = column.ResKey,
                            module = column.ModuleName,
                            label = column.Name,
                            labelLocId = _enableLocalizedText ? (column.NameLoc != null ? column.NameLoc.Id.ToString() : null) : null,
                            //(column.NameLoc != null ? column.NameLoc.Id : (int?)null) : (int?)null,
                            order = column.Order,
                            value = value,
                            valueLoc = valueLoc,
                        };
                        lst.Add(field);
                    }
                    else
                    {
                        foreach (var objectName in ObjectList.Keys)
                        {
                            if (column.ObjectProperty.StartsWith(objectName + "_"))
                            {
                                this.GetDisplayValue(_pageResourceService, _settingService, _languageService,_specialLogic,
                                    ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat,
                                    overdue, showLink, contractId, out value, out valueLoc, null, 1, hideGiroStatus);
                                //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")">
                                //    <span id="contract_left">@(column.GetDisplayName(cultureInfo))</span> 
                                //    <span id="contract_right">@Html.Raw(value)</span>
                                //</li>

                                var field = new LoginField()
                                {
                                    id = column.ResKey,
                                    module = column.ModuleName,
                                    label = column.Name,
                                    labelLocId = _enableLocalizedText ? (column.NameLoc != null ? column.NameLoc.Id.ToString() : null) : null,
                                    //(column.NameLoc != null ? column.NameLoc.Id : (int?)null) : (int?)null,
                                    order = column.Order,
                                    value = value,
                                    valueLoc = valueLoc,
                                };
                                lst.Add(field);
                            }
                        }
                    }
                }
            }

            return lst;
        }

        private LanguageContent[] GetLanguageContent(string contractNo, ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            IList<LanguageContent> lstLC = new List<LanguageContent>();

            LanguageContent lc = new LanguageContent()
            {
                languageCode = this.GetLanguageCode(_settingService, _languageService),
            };
            lstLC.Add(lc);

            List<LangContent> lst = new List<LangContent>();

            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            var summaries = rmwServiceAdaptor.GetAccountSummary(customer.Bpid);

            CmsAccountSummary summary = null;

            summary = summaries.Where(x => x.ContractNO == contractNo).FirstOrDefault();

            if (summary != null)
            {
                int? fpgId = 0;// summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

                #region TW Requirement of show bank detail in Contract detail
                bool showBankInfo = false;
                CmsContractDetail contract = GetContractDetails(customer.Bpid, summary.ContractNO);

                var bankList = rmwServiceAdaptor.GetBanks(customer.Bpid);
                if (bankList != null)
                {
                    CmsBank bank = bankList.Where(x => x.ContractNO == summary.ContractNO).FirstOrDefault();
                    showBankInfo = _specialLogic.ShowBankDetail(contract.FinancialDetail.PaymentMode, contract.FinancialDetail.PaymentCode, contract.GiroStatusCode, bank);
                }
                #endregion

                var lstModuleSetting = _msService.GetModuleSettings_NoCache(ModuleSettingConstants.CONTRACT_DETAIL, ModuleSettingConstants.FILTER_TYPE_CONTRACT_STATUS, summary.ContractStatusCode, fpgId)
                     .Where(x => x.Visible && (x.ResKey != ModuleSettingConstants.MAKE_PAYMENT_PAYMENT_INFO || showBankInfo)).OrderBy(x => x.Order).ToList();

                foreach (var moduleSetting in lstModuleSetting)
                {
                    //decimal? labelLocid = moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (decimal?)null;
                    string labelLocid = moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id.ToString() : null;
                    if (labelLocid != null)
                    {
                        string strTitle = moduleSetting.NameLoc.Text;

                        LangContent langContent = new LangContent()
                        {
                            id = labelLocid,
                            value = strTitle,
                        };
                        lst.Add(langContent);
                    }

                    GetLangContentForFields(customer, lst, moduleSetting.ResKey, contractNo);
                }
            }

            lc.contents = lst.ToArray();

            return lstLC.ToArray();
        }

        private void GetLangContentForFields(ZLIS.Cos.Core.Account.DomainModel.Customer customer,
            IList<LangContent> lst,
            string module, string ContractNo)
        {
            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            var summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == ContractNo).First();

            int? fpgId = summary.FinancialProductGroup == null ? 0 : summary.FinancialProductGroup.Code;

            IEnumerable<GridModuleSetting> details = _msService.GetGridModuleSettings_NoCache(module, fpgId)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();
            if (details.Count() > 0)
            {
                foreach (var d in details)
                {
                    //decimal? labelLocId = d.NameLoc != null ? d.NameLoc.Id : (int?)null;
                    string labelLocId = d.NameLoc != null ? d.NameLoc.Id.ToString() : null;

                    if (labelLocId != null)
                    {
                        lst.Add(new LangContent()
                        {
                            id = labelLocId,
                            value = d.NameLoc.Text,
                        });
                    }
                }
            }
        }

        #region Helper

        private CmsContractDetail GetContractDetails(int Bpid, string ContractNo)
        {
            return _rmwService.GetContractDetails(Bpid, ContractNo);
        }

        private bool ShowBankDetail(string paymentMode, string paymentCode, string giroStatus, CmsBank bank = null)
        {
            bool showDetail = false;
            switch (this.GetCountryCode(_settingService))
            {
                case "SG":
                    if (bank != null
                        && "Inter Bank Giro".Equals(paymentMode, StringComparison.OrdinalIgnoreCase)
                        && !"Rejected".Equals(bank.GiroStatusCode, StringComparison.OrdinalIgnoreCase))
                    {
                        showDetail = true;
                    }
                    break;

                case "MY":
                    if (bank != null
                       && "Direct Debit".Equals(paymentMode, StringComparison.OrdinalIgnoreCase)
                       && "00003".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                    {
                        showDetail = true;
                    }
                    break;
                case "TH":
                    if (bank != null
                        &&
                        (("Direct Debit(BBLSaving)".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00003".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-SCB".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00046".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-BAY".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00048".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-KBank".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00049".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-Tanachart".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00050".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                        || ("DirectDebit-DTB".Equals(paymentMode, StringComparison.OrdinalIgnoreCase) && "00051".Equals(paymentCode, StringComparison.OrdinalIgnoreCase))
                       ) && "Approved".Equals(giroStatus, StringComparison.OrdinalIgnoreCase))
                    {
                        showDetail = true;
                    }
                    break;
                case "TW":
                    if (bank != null
                       && !"Cheque".Equals(paymentMode, StringComparison.OrdinalIgnoreCase))
                    {
                        showDetail = true;
                    }
                    break;
            }
            return showDetail;
        }

        #endregion

    }
}
