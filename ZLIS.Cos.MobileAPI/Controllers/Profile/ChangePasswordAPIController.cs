﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Cos.Core.Audit.Services;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.ContentManagement;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.DomainModel;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class ChangePasswordAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerAccountService _accService;
        private IActionLogService _logService;
        private ISystemSettingService _settingService;

        private ILanguageService _languageService;

        private IEmailTemplateService _templateService;
        private IAppDataFolder _appDataFolder;
        private IMailer _mailer;
        private IPageResourceService _pageResourceService;

        public ILogger Logger { get; set; }

        public ChangePasswordAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                            IMobileLoginInfoService mobileLoginInfoService,
                                            ICustomerAccountService accService,
                                            IActionLogService logService,
                                            ISystemSettingService settingService,
                                            ILanguageService languageService,
                                            IEmailTemplateService templateService,
                                            IAppDataFolder appDataFolder,
                                            IMailer mailer,
                                            IPageResourceService pageResourceService)
        {
            Logger = NullLogger.Instance;

            _unitOfWorkAccessor = unitOfWorkAccessor;
            _mobileLoginInfoService = mobileLoginInfoService;
            _accService = accService;
            _logService = logService;
            _settingService = settingService;
            _languageService = languageService;
            _templateService = templateService;
            _appDataFolder = appDataFolder;
            _mailer = mailer;
            _pageResourceService = pageResourceService;
            _settingService.Cachable = false;
        }

        [System.Web.Http.HttpPost]
        public ChangePasswordResponse ChangePassword(ChangePasswordRequest request)
        {
            if (request == null)
                request = new ChangePasswordRequest();
            Logger.Debug(string.Format("-------- ChangeEmail API started. token:{0} currentPassword:{1} newPassword{2}", request.token ?? string.Empty, string.IsNullOrEmpty(request.currentPassword) ? string.Empty: "**********", string.IsNullOrEmpty(request.newPassword) ? string.Empty : "**********"));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new ChangePasswordResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)ChangePasswordErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.currentPassword) ||
                    string.IsNullOrEmpty(request.newPassword))
                {
                    errorInfo.code = (int)ChangePasswordErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                
                //if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_FORGOTPASSWORD_OTPENABLED, request.token, "ForgotPassword"))
                //{
                //    errorInfo.code = (int)ChangePasswordErrorCode.InvalidToken;
                //    errorInfo.message = "Validate Otp Failed";
                //    return response;
                //}
                var customer = loginInfo.CustomerActivity.Customer;

                bool success = false;

                string loginMode = this.LoginMode(_settingService);
                
                if (_accService.Login(
                    loginMode.Equals(ConstValue.LOGIN_MODE_EMAIL, StringComparison.OrdinalIgnoreCase) ?
                    customer.Email : customer.Nric,
                    request.currentPassword,
                    loginMode) == null)
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER RESET_PASSWORD", Core.Audit.ActionAuditLevel.Normal, Core.Audit.ActionLogLevel.Audit, "Update", string.Format("Incorrect Old Password;"), customer.Bpid.ToString());
                        tran.Commit();
                    }

                    errorInfo.code = (int)ChangePasswordErrorCode.InvalidCurrentPassword;
                    //errorInfo.message = "Invalid current password or new password";
                    errorInfo.message = this.Resource(_pageResourceService, "Profile_Profile_ChangePassword,Validation_OldPasswordIncorrect");

                    return response;
                }

                //check if new password equals old password
                if (_accService.IsPasswordSame(request.newPassword, customer))
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER RESET_PASSWORD", Core.Audit.ActionAuditLevel.Normal, Core.Audit.ActionLogLevel.Audit, "Update", string.Format("New password equals old password;"), customer.Bpid.ToString());
                        tran.Commit();
                    }

                    errorInfo.code = (int)ChangePasswordErrorCode.NewPasswordSameAsCurrentPassword;
                    errorInfo.message = this.Resource(_pageResourceService, "Profile_Profile_ChangePassword,Validation_NewPasswordSameAsOldPassword");

                    return response;
                }

                //TODO: validate password
                int required = 0;
                //check length>=8
                if (request.newPassword.Length < this.MinPwdLength())
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER RESET_PASSWORD", Core.Audit.ActionAuditLevel.Normal, Core.Audit.ActionLogLevel.Audit, "Update", string.Format("New password length < {0}", this.MinPwdLength()), customer.Bpid.ToString());
                        tran.Commit();
                    }

                    errorInfo.code = (int)ChangePasswordErrorCode.InvalidNewPassword;
                    errorInfo.message = string.Format(this.Resource(_pageResourceService,
                        "Profile_Profile_ChangePassword,Validation_PasswordIncorrect"), this.MinPwdLength());
                    //"Invalid current password or new password";

                    return response;
                }

                //check fulfilment of 3 out of 4 requirements
                //at least 1 digit
                if (Regex.IsMatch(request.newPassword, @"^(?=.*?[0-9])", RegexOptions.IgnorePatternWhitespace))
                {
                    required = required + 1;
                }

                //at least 1 capital letter
                if (Regex.IsMatch(request.newPassword, @"^(?=.*?[A-Z])", RegexOptions.IgnorePatternWhitespace))
                {
                    required = required + 1;
                }

                //at least 1 small letter
                if (Regex.IsMatch(request.newPassword, @"^(?=.*?[a-z])", RegexOptions.IgnorePatternWhitespace))
                {
                    required = required + 1;
                }

                //at least one special character !*#$%&'()'+,./;:?_@>-
                if (Regex.IsMatch(request.newPassword, @"^(?=.*?[!*#$%&'()'+,./;:?_@>-])", RegexOptions.IgnorePatternWhitespace))
                {
                    required = required + 1;
                }

                if (required < 3)
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER RESET_PASSWORD", Core.Audit.ActionAuditLevel.Normal, Core.Audit.ActionLogLevel.Audit,
                                        "Update", string.Format("New password does not meet 3 out of 4 requirements"),
                                        customer.Bpid.ToString());
                        tran.Commit();
                    }

                    errorInfo.code = (int)ChangePasswordErrorCode.InvalidNewPassword;
                    //errorInfo.message = "Invalid current password or new password";
                    errorInfo.message = string.Format(this.Resource(_pageResourceService, "Profile_Profile_ChangePassword,Validation_PasswordIncorrect"), this.MinPwdLength());

                    return response;
                }

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    success = _accService.ResetPassword(customer.Id, request.newPassword);

                    tran.Commit();
                }

                string languageCode = this.GetCurrentLanguageCode(_settingService, _languageService, request.language);
                CultureInfo cultureInfo = new CultureInfo(languageCode);

                if (success)
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER RESET_PASSWORD", Core.Audit.ActionAuditLevel.Normal,
                                        Core.Audit.ActionLogLevel.Audit, "Update",
                                        string.Format("Status: Successful; Old Password: {0}; New Password: {1};",
                                                      "**********", "**********"), customer.Bpid.ToString());
                        tran.Commit();
                    }

                    //send email to customer
                    string toEmail = this.GetEmailAddress(customer.Email);
                    EmailTemplate template = _templateService.GetEmailTemplate(EmailTemplateName.RESET_PASSWORD, DeliveryChannel.EMAIL);
                    if (template != null)
                    {
                        string subject = template.Subject;
                        if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                        {
                            subject = template.SubjectLocID.Text;
                        }

                        EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                        string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                        string rootUrl = "";

                        var s = _settingService.GetSystemSetting("System", "ExternalRootUrl");
                        if (s != null)
                        {
                            rootUrl = s.Value;
                        }

                        string imageURL = emailUtility.GetImageUrl(rootUrl, "", template.Id.ToString());//Url.Content("~")
                        messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, customer.DisplayName);
                        _mailer.SendEmail(template, toEmail, subject, messageBody);
                    }
                }

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    _logService.Log("CUSTOMER RESET_PASSWORD", Core.Audit.ActionAuditLevel.Normal,
                                    Core.Audit.ActionLogLevel.Audit, "Update",
                                    string.Format("Status: Failed; Old Password: {0}; New Password: {1};", "**********",
                                                  "**********"), customer.Bpid.ToString());
                    tran.Commit();
                }

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());
                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)ChangePasswordErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when change password");
            }

            return response;
        }

        private int MinPwdLength()
        {
            int minPwdLength = ConstValue.DEFAULT_MIN_PWD_LENGTH;
            SystemSettingItem objMinPwdLength = _settingService.GetSystemSetting(
                SystemConfigurationConstants.CATEGORY_SYSTEM,
                SystemConfigurationConstants.KEY_MIN_PWD_LENGTH);
            if (objMinPwdLength != null)
            {
                int value;
                if (int.TryParse(objMinPwdLength.Value, out value))
                {
                    minPwdLength = value;
                }
            }

            return minPwdLength;
        }

    }
}
