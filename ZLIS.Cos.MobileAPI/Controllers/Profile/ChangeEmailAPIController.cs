﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Cos.Core.Audit.Services;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.ContentManagement;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.DomainModel;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class ChangeEmailAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ICustomerAccountService _accService;
        private IActionLogService _logService;
        private ISystemSettingService _settingService;

        private ILanguageService _languageService;

        private IEmailTemplateService _templateService;
        private IAppDataFolder _appDataFolder;
        private IMailer _mailer;

        private IRmwService _rmwService;

        private IPageResourceService _pageResourceService;

        public ILogger Logger { get; set; }

        public ChangeEmailAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                            IMobileLoginInfoService mobileLoginInfoService,
                                            ICustomerAccountService accService,
                                            IActionLogService logService,
                                            ISystemSettingService settingService,
                                            ILanguageService languageService,
                                            IEmailTemplateService templateService,
                                            IAppDataFolder appDataFolder,
                                            IMailer mailer,
                                            IRmwService rmwService,
                                            IPageResourceService pageResourceService,
            ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;

            _unitOfWorkAccessor = unitOfWorkAccessor;
            _mobileLoginInfoService = mobileLoginInfoService;
            _accService = accService;
            _logService = logService;
            _settingService = settingService;
            _languageService = languageService;
            _templateService = templateService;
            _appDataFolder = appDataFolder;
            _mailer = mailer;
            _rmwService = rmwService;
            ((RmwServiceAdaptor)_rmwService).SetCacheManager(cacheManager);

            _pageResourceService = pageResourceService;
            _settingService.Cachable = false;
        }

        [System.Web.Http.HttpPost]
        public ChangeEmailResponse ChangeEmail(ChangeEmailRequest request)
        {
            if (request == null)
                request = new ChangeEmailRequest();
            Logger.Debug(string.Format("-------- ChangeEmail API started. token:{0} newEmail:{1}", request.token ?? string.Empty, request.newEmail ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new ChangeEmailResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)ChangeEmailErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.newEmail))
                {
                    errorInfo.code = (int)ChangeEmailErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
               
                if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_UPDATE_USERNAME_OTP_ENABLED, request.token, "ForgotUserName"))
                {
                    errorInfo.code = (int)ChangeEmailErrorCode.InvalidToken;
                    errorInfo.message = "Validate Otp Failed";
                    return response;
                }
                var customer = loginInfo.CustomerActivity.Customer;

                bool success = false;
                //string email = model.Email;// (string)collection["email"];
                //string email2 = model.ConfirmEmail;// (string)collection["confirm_email"];
                string email = request.newEmail;
                string oldEmail = customer.Email;

                //check if new email same as current email
                if (email.Equals(customer.Email, StringComparison.OrdinalIgnoreCase))
                {
                    //model.ErrorMessage = this.Resource("Profile_Profile_UpdateEmail,Validation_NewUsernameSameAsCurrentUsername");
                    //TempData["UpdateEmail"] = model;

                    //return View("UpdateEmail", model);
                    errorInfo.code = (int)ChangeEmailErrorCode.InvalidNewEmail;
                    //errorInfo.message = "Invalid new email";
                    errorInfo.message = this.Resource(_pageResourceService, "Profile_Profile_UpdateEmail,Validation_NewUsernameSameAsCurrentUsername");

                    return response;
                }

                ZLIS.Cos.Core.Account.DomainModel.Customer customerCheck = _accService.GetCustomerByEmail(email);

                if (customerCheck != null && customerCheck.Bpid != customer.Bpid)
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER CHANGE_EMAIL", Core.Audit.ActionAuditLevel.Normal,
                                        Core.Audit.ActionLogLevel.Audit, "Update",
                                        string.Format("Email is in use: {0};", email), customer.Bpid.ToString());
                        tran.Commit();
                    }

                    errorInfo.code = (int)ChangeEmailErrorCode.InvalidNewEmail;
                    //errorInfo.message = "Invalid new email";
                    errorInfo.message = this.Resource(_pageResourceService, "Profile_Profile_UpdateEmail,Validation_EmailIsInUse");

                    return response;
                }

                //TODO: validate email

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    success = _accService.UpdateEmail(customer.Id, email, customer.Nric);

                    tran.Commit();
                }

                string languageCode = this.GetCurrentLanguageCode(_settingService, _languageService, request.language);
                CultureInfo cultureInfo = new CultureInfo(languageCode);

                if (success)
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER CHANGE_EMAIL", Core.Audit.ActionAuditLevel.Normal,
                                        Core.Audit.ActionLogLevel.Audit, "Update",
                                        string.Format("Status: Successful; New Email: {0};", email),
                                        customer.Bpid.ToString());
                        tran.Commit();
                    }

                    //send email to customer
                    EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                    string toEmail = this.GetEmailAddress(email);
                    EmailTemplate template = _templateService.GetEmailTemplate(EmailTemplateName.UPDATE_USERNAME, DeliveryChannel.EMAIL);
                    if (template != null)
                    {
                        string subject = template.Subject;
                        
                        if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                        {
                            subject = template.SubjectLocID.Text;
                        }

                        string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                        string rootUrl = "";

                        var s = _settingService.GetSystemSetting("System", "ExternalRootUrl");
                        if (s != null)
                        {
                            rootUrl = s.Value;
                        }

                        string imageURL = emailUtility.GetImageUrl(rootUrl, "", template.Id.ToString());//Url.Content("~")
                        messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, customer.DisplayName);
                        _mailer.SendEmail(template, toEmail, subject, messageBody);
                    }

                    //send email to customer service
                    toEmail = this.GetEmailAddress(_settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_MAILER, SystemConfigurationConstants.KEY_MAILER_CUSTOMERSERVICE).Value);
                    template = _templateService.GetEmailTemplate(EmailTemplateName.UPDATE_USERNAME_ADMIN, DeliveryChannel.EMAIL);
                    if (template != null)
                    {
                        var cmsCust = _rmwService.GetCustomerProfile(customer.Bpid);//sessionState.CurrentCmsCustomer;
                        string mobileNo = "";

                        if (cmsCust != null && cmsCust.PhoneNOs != null && cmsCust.PhoneNOs.Length > 0)
                        {
                            SystemSettingItem mobileTypeSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_OTPSETTING_MOBILETYPE);
                            string[] mobileTypes = new string[] { CmsPhoneType.Mobile };
                            if (mobileTypeSetting != null)
                            {
                                if (!string.IsNullOrEmpty(mobileTypeSetting.Value))
                                {
                                    mobileTypes = mobileTypeSetting.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                        .Select(x => x.Trim()).ToArray();
                                }
                            }

                            foreach (CmsPhone phone in cmsCust.PhoneNOs)
                            {
                                if (mobileTypes.Contains(phone.PhoneTypeCode, StringComparer.OrdinalIgnoreCase))//if (phone.PhoneType == CmsPhoneType.Mobile)
                                {
                                    if (mobileNo == "")
                                        mobileNo = phone.PhoneNO;
                                    else
                                        mobileNo = string.Format("{0}, {1}", mobileNo, phone.PhoneNO);
                                }
                            }
                        }

                        string subject = template.Subject;
                        if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                        {
                            subject = template.SubjectLocID.Text;
                        }
                        string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode); //template.TemplateContent;
                        messageBody = emailUtility.GetMessageBodyReplacement(messageBody, "",
                            customer.DisplayName, "", "", "", "", "",
                            mobileNo, "", "", "", "", oldEmail, email);
                        _mailer.SendEmail(template, toEmail, subject, messageBody);
                    }

                    //ZLIS.Cos.Core.Account.DomainModel.Customer newCustomer = _accService.GetCustomerByBPID(customer.Bpid);
                    //sessionState.CurrentCustomer = newCustomer;
                    //model = new UpdateEmailModel();
                    //model.OldEmail = customer.Email;
                    //model.ErrorMessage = "";
                    //TempData["showUpdateEmailSuccessful"] = "true";
                    //TempData["UpdateEmail"] = null;

                    //if (HttpContext.GetOverriddenBrowser().IsMobileDevice)
                    //if (this.GetDisplayModeId().ToLower() == "mobile")
                    //{
                    //    return View("UpdateEmailSuccessful", model);
                    //}
                    //else
                    //    return View("UpdateEmail", model);
                }
                else
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER CHANGE_EMAIL", Core.Audit.ActionAuditLevel.Normal,
                                        Core.Audit.ActionLogLevel.Audit, "Update",
                                        string.Format("Status: Failed; New Email: {0};", email), customer.Bpid.ToString());
                        tran.Commit();
                    }
                }

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)ChangeEmailErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when change email");
            }

            return response;
        }

    }
}
