﻿using System;
using System.Web.Http;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Framework.Common;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Framework.Common.Logging;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Audit.Services;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.Settings.DomainModel;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using System.Text.RegularExpressions;
using System.Globalization;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.ContentManagement;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Cos.Core;
using ZLIS.Cos.MobileAPI.Filters;

namespace ZLIS.Cos.MobileAPI.Controllers.Profile
{
    [ActionAuthorizeFilter]
    public class AccountAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ICustomerAccountService _accountService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IActionLogService _logService;
        private IPageResourceService _pageResourceService;
        private ISystemSettingService _settingService;
        private ILanguageService _languageService;
        private IEmailTemplateService _templateService;
        private IAppDataFolder _appDataFolder;
        private IMailer _mailer;


        public ILogger Logger { get; set; }

        public AccountAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                    IMobileLoginInfoService mobileLoginInfoService,
                                    ICustomerAccountService accountService,
                                    IActionLogService logService,
                                    ISystemSettingService settingService,
                                    ILanguageService languageService,
                                    IEmailTemplateService templateService,
                                    IAppDataFolder appDataFolder,
                                    IMailer mailer,
                                    IPageResourceService pageResourceService)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _accountService = accountService;
            _mobileLoginInfoService = mobileLoginInfoService;
            _logService = logService;
            _pageResourceService = pageResourceService;
            _settingService = settingService;
            _languageService = languageService;
            _templateService = templateService;
            _appDataFolder = appDataFolder;
            _mailer = mailer;
            _settingService.Cachable = false;
        }

        [System.Web.Http.HttpPost]
        public ForgotUserNameResponse ForgotUserName(ForgotUserNameRequest request)
        {
            if (request == null)
                request = new ForgotUserNameRequest();
            Logger.Debug(string.Format("-------- ForgotUserName API started. token:{0} ", request.token ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new ForgotUserNameResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)ForgotUserNameErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;
            try
            {
                if (string.IsNullOrEmpty(request.token) )
                {
                    errorInfo.code = (int)ForgotUserNameErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";
                    return response;
                }
                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
                
                if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_FORGOTUSERNAME_OTPENABLED, request.token, "ForgotUsername"))
                {
                    errorInfo.code = (int)ForgotUserNameErrorCode.InvalidToken;
                    errorInfo.message = "Validate Otp Failed";
                    return response;
                }
                var customer = _accountService.GetCustomerByBPID(loginInfo.BpId ?? 0);
                if (customer == null)
                {
                    errorInfo.code = (int)ForgotUserNameErrorCode.CustomerDoNotExist;
                    errorInfo.message = "Customer Do Not Exists";

                    return response;
                }
                var uninfo = new ClientModel.Models.UserNameInfo();
                uninfo.username = customer.Email;
                response.data = uninfo;
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)ForgotUserNameErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get forgot username");
            }

            return response;
        }

        [System.Web.Http.HttpPost]
        public ForgotPasswordResponse ForgotPassword(ForgotPasswordRequest request)
        {
            if (request == null)
                request = new ForgotPasswordRequest();
            Logger.Debug(string.Format("-------- ForgotPassword API started. token:{0} newPassword:{1}", request.token ?? string.Empty, string.IsNullOrEmpty(request.newPassword) ? string.Empty : "**********"));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new ForgotPasswordResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)ForgotPasswordErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;
            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                        string.IsNullOrEmpty(request.newPassword))
                {
                    errorInfo.code = (int)ForgotPasswordErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";
                    return response;
                }
               
                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);
               
                if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_FORGOTPASSWORD_OTPENABLED, request.token, "ForgotPassword"))
                {
                    errorInfo.code = (int)ForgotPasswordErrorCode.InvalidToken;
                    errorInfo.message = "Validate Otp Failed";
                    return response;
                }
                var customer = _accountService.GetCustomerByBPID(loginInfo.BpId ?? 0);
                if (customer == null)
                {
                    errorInfo.code = (int)ForgotPasswordErrorCode.CustomerDoNotExist;
                    errorInfo.message = "Customer Do Not Exists";

                    return response;
                }

                bool success = false;

                //check if new password equals old password
                if (_accountService.IsPasswordSame(request.newPassword, customer))
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER FORGOT_PASSWORD", Core.Audit.ActionAuditLevel.Normal, Core.Audit.ActionLogLevel.Audit, "Update", string.Format("New password equals old password;"), customer.Bpid.ToString());
                        tran.Commit();
                    }

                    errorInfo.code = (int)ForgotPasswordErrorCode.NewPasswordSameAsCurrentPassword;
                    errorInfo.message = this.Resource(_pageResourceService, "Profile_Profile_ChangePassword,Validation_NewPasswordSameAsOldPassword");

                    return response;
                }

                //TODO: validate password
                int required = 0;
                //check length>=8
                if (request.newPassword.Length < this.MinPwdLength())
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER FORGOT_PASSWORD", Core.Audit.ActionAuditLevel.Normal, Core.Audit.ActionLogLevel.Audit, "Update", string.Format("New password length < {0}", this.MinPwdLength()), customer.Bpid.ToString());
                        tran.Commit();
                    }

                    errorInfo.code = (int)ForgotPasswordErrorCode.InvaildPassword;
                    errorInfo.message = string.Format(this.Resource(_pageResourceService,
                        "Profile_Profile_ChangePassword,Validation_PasswordIncorrect"), this.MinPwdLength());
                    //"Invalid current password or new password";

                    return response;
                }

                //check fulfilment of 3 out of 4 requirements
                //at least 1 digit
                if (Regex.IsMatch(request.newPassword, @"^(?=.*?[0-9])", RegexOptions.IgnorePatternWhitespace))
                {
                    required = required + 1;
                }

                //at least 1 capital letter
                if (Regex.IsMatch(request.newPassword, @"^(?=.*?[A-Z])", RegexOptions.IgnorePatternWhitespace))
                {
                    required = required + 1;
                }

                //at least 1 small letter
                if (Regex.IsMatch(request.newPassword, @"^(?=.*?[a-z])", RegexOptions.IgnorePatternWhitespace))
                {
                    required = required + 1;
                }

                //at least one special character !*#$%&'()'+,./;:?_@>-
                if (Regex.IsMatch(request.newPassword, @"^(?=.*?[!*#$%&'()'+,./;:?_@>-])", RegexOptions.IgnorePatternWhitespace))
                {
                    required = required + 1;
                }

                if (required < 3)
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER FORGOT_PASSWORD", Core.Audit.ActionAuditLevel.Normal, Core.Audit.ActionLogLevel.Audit,
                                        "Update", string.Format("New password does not meet 3 out of 4 requirements"),
                                        customer.Bpid.ToString());
                        tran.Commit();
                    }

                    errorInfo.code = (int)ForgotPasswordErrorCode.InvaildPassword;
                    //errorInfo.message = "Invalid current password or new password";
                    errorInfo.message = string.Format(this.Resource(_pageResourceService, "Profile_Profile_ChangePassword,Validation_PasswordIncorrect"), this.MinPwdLength());

                    return response;
                }

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    success = _accountService.ResetPassword(customer.Id, request.newPassword);

                    tran.Commit();
                }

                string languageCode = this.GetCurrentLanguageCode(_settingService, _languageService, request.language);
                CultureInfo cultureInfo = new CultureInfo(languageCode);

                if (success)
                {
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _logService.Log("CUSTOMER FORGOT_PASSWORD", Core.Audit.ActionAuditLevel.Normal,
                                        Core.Audit.ActionLogLevel.Audit, "Update",
                                        string.Format("Status: Successful;  New Password: {0};",
                                            "**********"), customer.Bpid.ToString());
                        tran.Commit();
                    }

                    //send email to customer
                    string toEmail = this.GetEmailAddress(customer.Email);
                    EmailTemplate template = _templateService.GetEmailTemplate(EmailTemplateName.RESET_PASSWORD, DeliveryChannel.EMAIL);
                    if (template != null)
                    {
                        string subject = template.Subject;
                        if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                        {
                            subject = template.SubjectLocID.Text;
                        }

                        EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                        string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                        string rootUrl = "";

                        var s = _settingService.GetSystemSetting("System", "ExternalRootUrl");
                        if (s != null)
                        {
                            rootUrl = s.Value;
                        }

                        string imageURL = emailUtility.GetImageUrl(rootUrl, "", template.Id.ToString());//Url.Content("~")
                        messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, customer.DisplayName);
                        _mailer.SendEmail(template, toEmail, subject, messageBody);
                    }

                }

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)ForgotPasswordErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when call forgot password");
            }

            return response;
        }

        private int MinPwdLength()
        {
            int minPwdLength = ConstValue.DEFAULT_MIN_PWD_LENGTH;
            SystemSettingItem objMinPwdLength = _settingService.GetSystemSetting(
                SystemConfigurationConstants.CATEGORY_SYSTEM,
                SystemConfigurationConstants.KEY_MIN_PWD_LENGTH);
            if (objMinPwdLength != null)
            {
                int value;
                if (int.TryParse(objMinPwdLength.Value, out value))
                {
                    minPwdLength = value;
                }
            }

            return minPwdLength;
        }
    }
}
