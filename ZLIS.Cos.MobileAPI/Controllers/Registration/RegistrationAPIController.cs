﻿using System;
using System.Linq;
using System.Web.Http;
using ZLIS.Cos.MobileAPI.CountrySpecific;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Framework.Common;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Cos.RmwService;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.Settings.DomainModel;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.ContentManagement;
using System.Globalization;
using ZLIS.Cos.Core.MobileLogin.DomainModel;
using ZLIS.Cos.Core.MobileLogin.Services;

namespace ZLIS.Cos.MobileAPI.Controllers.Registration
{
    public class RegistrationAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ICustomerAccountService _accountService;
        private IRmwService _rmwService;
        private ISystemSettingService _settingService;
        private ILanguageService _languageService;
        public ILogger Logger { get; set; }
        private IEmailTemplateService _templateService;
        private IAppDataFolder _appDataFolder;
        private IMailer _mailer;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private ISpecialLogic _specialLogic;

        private bool _enableLocalizedText;

        public RegistrationAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                         ICustomerAccountService accountService,
                                         ILanguageService languageService,
                                         IMobileLoginInfoService mobileLoginInfoService,
                                         ISystemSettingService settingService,
                                         IEmailTemplateService templateService,
                                         IAppDataFolder appDataFolder,
                                         IMailer mailer,
                                         IRmwService rmwService,
                                         ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _accountService = accountService;
            _rmwService = rmwService;
            ((RmwServiceAdaptor) _rmwService).SetCacheManager(cacheManager);
            _settingService = settingService;
            _settingService.Cachable = false;
            _languageService = languageService;
            _enableLocalizedText = this.EnableLocalizedText(_settingService, _languageService);
            _templateService = templateService;
            _appDataFolder = appDataFolder;
            _mailer = mailer;
            _mobileLoginInfoService = mobileLoginInfoService;
            _specialLogic = SpecialLogicFactory.GetSpecialLogic(this.GetCountryCode(_settingService));
        }

        [System.Web.Http.HttpPost]
        public ValidateCustomerResponse ValidateCustomer(ValidateCustomerRequest request)
        {
            if (request == null)
                request = new ValidateCustomerRequest();
            Logger.Debug(string.Format("-------- ValidateCustomer API started. country:{0} nric:{1}", request.cc ?? string.Empty, request.nric ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new ValidateCustomerResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)ValidateCustomerErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;
            var data = new CustomerInfo();
            try
            {
                if (string.IsNullOrEmpty(request.cc) ||
                    string.IsNullOrEmpty(request.nric)||
                    string.IsNullOrEmpty(request.type))
                {
                    errorInfo.code = (int)ValidateCustomerErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_COUNTRY_CODE);
                if (countryCode != null)
                {
                    if (!request.cc.Equals(countryCode.Value, StringComparison.OrdinalIgnoreCase))
                    {
                        errorInfo.code = (int)ValidateCustomerErrorCode.InvalidRequest;
                        errorInfo.message = "Bad parameters";

                        return response;
                    }
                }
                else
                {
                    throw new ApplicationException("No country code setting");
                }
                var cust = _accountService.GetCustomerByNric(request.nric);
                if (request.type.ToLower() == "registration")
                {
                    if (cust != null)
                    {
                        errorInfo.code = (int)ValidateCustomerErrorCode.CustomerExist;
                        errorInfo.message = "Customer already registered";

                        return response;
                    }
                }
                else
                {
                    if (cust == null)
                    {
                        errorInfo.code = (int)ValidateCustomerErrorCode.CustomerDoNotExist;
                        errorInfo.message = "Customer not registered";

                        return response;
                    }

                    //check if account is locked
                    if (cust.AccountLocked ?? false)
                    {
                        errorInfo.code = (int)ValidateCustomerErrorCode.AccountIsLocked;
                        errorInfo.message = "Customer account is locked";                        

                        return response;
                    }
                }
                var custresponse = _rmwService.ValidateCustomer(request.nric);
                if (custresponse == null)
                {
                    data.isValid = false;
                    errorInfo.code = (int)ValidateCustomerErrorCode.InvalidCustomer;
                    errorInfo.message = "Invalid customer";

                    return response;
                }
                else
                {
                    data.isValid = true;
                    data.sessionId = Guid.NewGuid().ToString();
                    if (custresponse.HandPhones != null)
                    {
                        var mobileType = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_OTPSETTING_MOBILETYPE);

                        if (mobileType != null)
                        {
                            string[] types = mobileType.Value.Split(',');
                            if (!_enableLocalizedText)
                                data.mobileNOs = custresponse.HandPhones.Where(h => types.Contains(h.PhoneType)).Select(h => h.PhoneNO).ToArray();
                            else
                            {
                                data.mobileNOs = custresponse.HandPhones.Where(h => h.PhoneNO_Local != null && types.Contains(h.PhoneType)).Select(h => h.PhoneNO_Local).ToArray();
                                if (data.mobileNOs.Count() == 0)
                                    data.mobileNOs = custresponse.HandPhones.Where(h => types.Contains(h.PhoneType)).Select(h => h.PhoneNO).ToArray();

                            }
                        }
                    }

                    if (data.mobileNOs == null || data.mobileNOs.Length == 0)
                    {
                        errorInfo.code = (int)ValidateCustomerErrorCode.InvalidMobileNo;
                        errorInfo.message = "Invalid mobile no.";

                        return response;
                    }

                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        MobileLoginInfo mobileLoginInfo = _mobileLoginInfoService.RegisterMobileLoginInfo(
                             "string", "mobile", "string", "string", "string",
                             this.LoginExpiryMinutes(), data.sessionId, custresponse.BpId);
                        tran.Commit();
                    }
                }
                //}
                response.data = data;
                return response;
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)ChangeEmailErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when change email");
            }

            return response;
        }

        [System.Web.Http.HttpPost]
        public RegisterCustomerResponse RegisterCustomer(RegisterCustomerRequest request)
        {
            if (request == null)
                request = new RegisterCustomerRequest();
            Logger.Debug(string.Format("-------- RegisterCustomer API started. sessionId:{0} email:{1}", request.sessionId ?? string.Empty, request.email ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new RegisterCustomerResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)RegisterErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;
            try
            {
                if (string.IsNullOrEmpty(request.password) ||
                    string.IsNullOrEmpty(request.sessionId))
                {
                    errorInfo.code = (int)RegisterErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                if (string.IsNullOrEmpty(request.email))
                {
                    errorInfo.code = (int)RegisterErrorCode.InvalidEmailAddress;
                    errorInfo.message = "Invalid email";

                    return response;
                }
                if (request.password.Length < this.MinPwdLength())
                {
                    errorInfo.code = (int)RegisterErrorCode.InvaildPassword;
                    errorInfo.message = string.Format("Password not meeting minimum password requirement {0}", this.MinPwdLength());
                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.sessionId);
                if (loginInfo == null)
                {
                    errorInfo.code = (int)RegisterErrorCode.InvalidSession;
                    errorInfo.message = "Invalid session";

                    return response;
                }

                int bpid = loginInfo.BpId ?? 0;
                var cust = _accountService.GetCustomerByBPID(bpid);
                if (cust != null)
                {
                    errorInfo.code = (int)RegisterErrorCode.CustomerExist;
                    errorInfo.message = "Customer already exists";

                    return response;
                }

                CmsCustomerProfile profile = _rmwService.GetCustomerProfile(bpid);
                if (profile == null)
                {
                    errorInfo.code = (int)RegisterErrorCode.InvalidSession;
                    errorInfo.message = "Invalid BpId";

                    return response;
                }
                //must have contract during registration
                if (_specialLogic.CheckFilteredContractDuringRegistration())
                {
                    RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                    var summaries = rmwServiceAdaptor.GetAccountSummary(bpid);

                    if (summaries == null || summaries.Count == 0)
                    {
                        errorInfo.code = (int)RegisterErrorCode.InvalidRequest;
                        errorInfo.message = "Must have contract during registration";
                        return response;
                    }
                }
                if (this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.KEY_REGISTRATION_OTPENABLED, request.sessionId, "Registration"))
                {
                    ZLIS.Cos.Core.Account.DomainModel.Customer customer = new Core.Account.DomainModel.Customer();
                    bool success = false;
                    //ZLIS.Cos.Core.Account.DomainModel.Customer newCustomer = null;
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        //CustomerName will represent Salutation || FirstName || LastName in case of Individual and will represent CompanyName in case of corporate customers
                        if (profile.BusinessPartner != null)
                        {
                            if (profile.BusinessPartner.CustomerType == CmsCustomerType.Corporate)
                            {
                                customer.Name = profile.BusinessPartner.FirstName ?? profile.BusinessPartner.FirstName_Local;
                                customer.FirstName = "";
                                customer.LastName = "";
                                customer.Salutation = "";
                            }
                            else if (profile.BusinessPartner.CustomerType == CmsCustomerType.Individual)
                            {
                                customer.Name = "";
                                customer.FirstName = profile.BusinessPartner.FirstName ?? profile.BusinessPartner.FirstName_Local;
                                customer.MiddleName = profile.BusinessPartner.MiddleName ?? profile.BusinessPartner.MiddleName_Local;
                                customer.LastName = profile.BusinessPartner.LastName ?? profile.BusinessPartner.LastName_Local;
                                customer.Salutation = profile.BusinessPartner.Title ?? profile.BusinessPartner.Title_Local;
                            }
                            else
                            {
                                customer.FirstName = profile.BusinessPartner.FirstName == null ? "" : profile.BusinessPartner.FirstName;
                                customer.Name = profile.BusinessPartner.MiddleName == null ? (profile.BusinessPartner.FirstName == null ? "" : profile.BusinessPartner.FirstName) : profile.BusinessPartner.MiddleName;
                                customer.LastName = profile.BusinessPartner.LastName == null ? "" : profile.BusinessPartner.LastName;
                                customer.Salutation = profile.BusinessPartner.Title ?? profile.BusinessPartner.Title_Local;
                            }
                        }

                        //CustomerName will represent Salutation || FirstName || LastName in case of Individual and will represent CompanyName in case of corporate customers
                        //string[] names = model.CmsCustomer.Name.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

                        customer.AccountLocked = false;
                        customer.Bpid = bpid;
                        customer.BpType = profile.BusinessPartner.CustomerType.ToString();
                        customer.Email = request.email;
                        customer.Enabled = true;

                        customer.LastLoginDate = DateTime.Now;
                        customer.LoginAttempts = 0;
                        customer.Nric = profile.BusinessPartner.CompanyRegNO;
                        customer.Password = request.password;
                        customer.PasswordSalt = "";
                        customer.Status = 1;
                        customer.TmpPassword = "";
                        customer.TmpPasswordCreateDate = DateTime.Now;
                        customer.TmpPasswordExpired = false;
                        customer.CreatedBy = profile.BusinessPartner.OldNricNO;
                        customer.CreatedDate = DateTime.Now;

                        success = _accountService.CreateAccount(customer);
                        //newCustomer = _accountService.CreateAccount(customer);
                        loginInfo.Valid =false;
                        _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);
                        tran.Commit();
                    }

                    if (success)
                    //if (newCustomer != null)
                    {
                        string languageCode = this.GetCurrentLanguageCode(_settingService, _languageService, request.language);
                        CultureInfo cultureInfo = new CultureInfo(languageCode);

                        //send email to customer
                        string toEmail = this.GetEmailAddress(request.email);
                        EmailTemplate template = _templateService.GetEmailTemplate(EmailTemplateName.SUCCESSFUL_REGISTRATION, DeliveryChannel.EMAIL);
                        if (template != null)
                        {
                            string subject = template.Subject;
                            if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                            {
                                subject = template.SubjectLocID.Text;
                            }

                            EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                            string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                            string rootUrl = "";

                            var s = _settingService.GetSystemSetting("System", "ExternalRootUrl");
                            if (s != null)
                            {
                                rootUrl = s.Value;
                            }

                            string imageURL = emailUtility.GetImageUrl(rootUrl, "", template.Id.ToString());//Url.Content("~")
                            messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, customer.DisplayName);
                            _mailer.SendEmail(template, toEmail, subject, messageBody);
                        }

                        //_activityService.CreateActivity(newCustomer,
                        //                                        RequestHelpers.GetClientIpAddress(this.HttpContext.Request),
                        //                                        RequestHelpers.GetBrowserAgent(this.HttpContext.Request),
                        //                                        RequestHelpers.GetSessionID(this.HttpContext),
                        //                                        LoginStatus.Registration_Successful, "Account registration successful", "AccountRegistration", ModuleName.Registration,
                        //                                        PageName.AccountRegistration);
                        /*_activityService.CreateActivity(newCustomer,
                                                            RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                            string.Format("Mobile/{0}", request.deviceType),
                                                            Guid.NewGuid().ToString(),
                                                            LoginStatus.Registration_Successful, "Account registration successful", "AccountRegistration", ModuleName.Registration,
                                                            PageName.AccountRegistration);*/

                    }
                    else
                    {
                        errorInfo.code = (int)RegisterErrorCode.CustomerExist;
                        errorInfo.message = "Customer already registered";
                        return response;
                    }

                }
                else
                {
                    errorInfo.code = (int)RegisterErrorCode.InvalidSession;
                    errorInfo.message = "Otp failed";
                    return response;
                }
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)RegisterErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when registion customer");
            }

            return response;
        }

        private int MinPwdLength()
        {
            int minPwdLength = ConstValue.DEFAULT_MIN_PWD_LENGTH;
            SystemSettingItem objMinPwdLength = _settingService.GetSystemSetting(
                SystemConfigurationConstants.CATEGORY_SYSTEM,
                SystemConfigurationConstants.KEY_MIN_PWD_LENGTH);
            if (objMinPwdLength != null)
            {
                int value;
                if (int.TryParse(objMinPwdLength.Value, out value))
                {
                    minPwdLength = value;
                }
            }

            return minPwdLength;
        }
    }
}
