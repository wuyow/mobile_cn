﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.Modules.Enquiry.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Logging;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    public class ContentAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        public ITextContentService _textContentService;
        private ISystemSettingService _settingService;
        private IMenuSettingService _menuSettingService;
        private IEnquiryService _enquiryService;
        private IPageResourceService _pageResourceService;

        private bool _enableLocalizedText;

        public ILogger Logger { get; set; }

        public ContentAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                    ILanguageService languageService,
                                    ITextContentService textContentService,
                                    ISystemSettingService settingService,
                                    IMenuSettingService menuSettingService,
                                    IEnquiryService enquiryService,
                                    IPageResourceService pageResourceService)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _textContentService = textContentService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _menuSettingService = menuSettingService;

            _enquiryService = enquiryService;
            _pageResourceService = pageResourceService;

            _enableLocalizedText = this.EnableLocalizedText(_settingService, _languageService);
        }

        [System.Web.Http.HttpPost]
        public GetContentResponse GetContent(GetContentRequest request)
        {
            if (request == null)
                request = new GetContentRequest();
            Logger.Debug(string.Format("-------- GetListsContent API started. country:{0}",request.cc??string.Empty));

            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetContentResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetListsContentErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.cc))
                {
                    errorInfo.code = (int)GetListsContentErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_COUNTRY_CODE);
                if (countryCode != null)
                {
                    if (!request.cc.Equals(countryCode.Value, StringComparison.OrdinalIgnoreCase))
                    {
                        errorInfo.code = (int)GetListsContentErrorCode.BadParameters;
                        errorInfo.message = "Bad parameters";

                        return response;
                    }
                }
                else
                {
                    throw new ApplicationException("No country code setting");
                }

                response.data = GetContent();
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetListsContentErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when getting lists content");
            }

            return response;
        }

        private Content GetContent()
        {
            Content content = new Content();

            Logger.Debug("Get settings started.");
            content.settings = GetSettings();
            Logger.Debug("Get settings succeeded.");

            Logger.Debug("Get menu items started.");
            content.menuItems = GetMenuItems();
            Logger.Debug("Get menu items succeeded.");

            Logger.Debug("Get enquiry topics started.");
            content.enquiryTopics = GetEnquiryTopics();
            Logger.Debug("Get enquiry topics succeeded.");

            Logger.Debug("Get page resources started.");
            content.pageResources = GetPageResources();
            Logger.Debug("Get page resources succeeded.");

            Logger.Debug("Get language content started.");
            content.languageContent = _enableLocalizedText ? GetLanguageContent() : null;
            Logger.Debug("Get language content succeeded.");

            return content;
        }

        private Setting GetSettings()
        {
            Setting setting = new Setting();

            var dateFormat = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT);
            if (dateFormat != null)
            {
                setting.dateFormat = dateFormat.Value;
            }

            var currencySymbol = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_CURRENCY_SYMBOL);
            if (currencySymbol != null)
            {
                setting.currencySymbol = currencySymbol.Value;
            }

            var timeZone = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_TIMEZONE);
            if (timeZone != null)
            {
                int nTimeZone;
                if (int.TryParse(timeZone.Value, out nTimeZone))
                {
                    setting.timeZone = nTimeZone;
                }
            }

            //var language = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_LANGUAGE);
            //if (language != null)
            //{
            //    int nLanguage;
            //    if (int.TryParse(language.Value, out nLanguage))
            //    {
            //        var lan = _languageService.GetByLanguageId(nLanguage);
            //        if (lan != null)
            //        {
            //            setting.language = string.Format("{0}-{1}", lan.PriLan, lan.SubLan);
            //        }
            //    }
            //}
            setting.language = this.GetLanguageCode(_settingService, _languageService);

            var maxFileSize = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_ENQUIRY, SystemConfigurationConstants.KEY_MAX_FILESIZE);
            if (maxFileSize != null)
            {
                int nMaxFileSize;
                if (int.TryParse(maxFileSize.Value, out nMaxFileSize))
                {
                    setting.maxAttachmentSize = nMaxFileSize;
                }
            }

            var maxFileCount = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_ENQUIRY, SystemConfigurationConstants.KEY_MAX_FILECOUNT);
            if (maxFileCount != null)
            {
                int nMaxFileCount;
                if (int.TryParse(maxFileCount.Value, out nMaxFileCount))
                {
                    setting.maxAttachmentNumber = nMaxFileCount;
                }
            }

            var minPwdLength = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SYSTEM, SystemConfigurationConstants.KEY_MIN_PWD_LENGTH);
            if (minPwdLength != null)
            {
                int nMinPwdLength;
                if (int.TryParse(minPwdLength.Value, out nMinPwdLength))
                {
                    setting.minPasswordLength = nMinPwdLength;
                }
            }

            var loginMode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_LOGIN,
                SystemConfigurationConstants.KEY_LOGIN_MODE);
            if (loginMode != null)
            {
                setting.loginMode = loginMode.Value;
            }
            else
            {
                setting.loginMode = ConstValue.LOGIN_MODE_EMAIL;
            }

            var loginOtpEnabled = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_LOGIN_OTPENABLED);
            if (loginOtpEnabled != null)
            {
                bool bLoginOtpEnabled;
                if (bool.TryParse(loginOtpEnabled.Value, out bLoginOtpEnabled))
                {
                    setting.loginOtpEnabled = bLoginOtpEnabled;
                }
            }

            var soaOtpEnabled = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_SOA_OTP_ENABLED);
            if (soaOtpEnabled != null)
            {
                bool bSoaOtpEnabled;
                if (bool.TryParse(soaOtpEnabled.Value, out bSoaOtpEnabled))
                {
                    setting.soaOtpEnabled = bSoaOtpEnabled;
                }
            }

            var repaymentPlanOtpEnabled = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_REPAYMENT_PLAN_OTP_ENABLED);
            if (repaymentPlanOtpEnabled != null)
            {
                bool bRepaymentPlanOtpEnabled;
                if (bool.TryParse(repaymentPlanOtpEnabled.Value, out bRepaymentPlanOtpEnabled))
                {
                    setting.repaymentPlanOtpEnabled = bRepaymentPlanOtpEnabled;
                }
            }

            var receiptOtpEnabled = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_RECEIPT_OTP_ENABLED);
            if (receiptOtpEnabled != null)
            {
                bool bReceiptOtpEnabled;
                if (bool.TryParse(receiptOtpEnabled.Value, out bReceiptOtpEnabled))
                {
                    setting.receiptOtpEnabled = bReceiptOtpEnabled;
                }
            }

            //var forgotPasswordOtpEnabled = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_FORGOTPASSWORD_OTPENABLED);
            //if (forgotPasswordOtpEnabled != null)
            //{
            //    bool bForgotPasswordOtpEnabled;
            //    if (bool.TryParse(forgotPasswordOtpEnabled.Value, out bForgotPasswordOtpEnabled))
            //    {
            //        setting.forgotPasswordOtpEnabled = bForgotPasswordOtpEnabled;
            //    }
            //}

            //var forgotUsernameOtpEnabled = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_FORGOTUSERNAME_OTPENABLED);
            //if (forgotUsernameOtpEnabled != null)
            //{
            //    bool bForgotUsernameOtpEnabled;
            //    if (bool.TryParse(forgotUsernameOtpEnabled.Value, out bForgotUsernameOtpEnabled))
            //    {
            //        setting.forgotUsernameOtpEnabled = bForgotUsernameOtpEnabled;
            //    }
            //}

            var changeUsernameOtpEnabled = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_UPDATE_USERNAME_OTP_ENABLED);
            if (changeUsernameOtpEnabled != null)
            {
                bool bChangeUsernameOtpEnabled;
                if (bool.TryParse(changeUsernameOtpEnabled.Value, out bChangeUsernameOtpEnabled))
                {
                    setting.changeEmailOtpEnabled = bChangeUsernameOtpEnabled;   //!!
                }
            }

            var soa_defaultmonth = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SOA, SystemConfigurationConstants.KEY_DEFAULT_FROM_TO_MONTH_DURATION);
            if (soa_defaultmonth != null)
            {
                int soa_dm;
                if (int.TryParse(soa_defaultmonth.Value, out soa_dm))
                {
                    setting.SOA_DefaultFromToMonthDuration = soa_dm;
                }
            }

            var rec_defaultmonth = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_RECEIPT, SystemConfigurationConstants.KEY_DEFAULT_FROM_TO_MONTH_DURATION);
            if (rec_defaultmonth != null)
            {
                int rec_dm;
                if (int.TryParse(rec_defaultmonth.Value, out rec_dm))
                {
                    setting.Receipt_DefaultFromToMonthDuration = rec_dm;
                }
            }

            var invoice_defaultmonth = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_ENQUIRY, SystemConfigurationConstants.KEY_MAX_FILESIZE);
            if (invoice_defaultmonth != null)
            {
                int inv_dm;
                if (int.TryParse(invoice_defaultmonth.Value, out inv_dm))
                {
                    setting.Invoice_DefaultFromToMonthDuration = inv_dm;
                }
            }

            bool bIsOTPTesting = false;
            if (ConfigurationManager.AppSettings["IsOtpTesting"] != null)
                bIsOTPTesting= bool.Parse(ConfigurationManager.AppSettings["IsOtpTesting"].ToString());
            setting.IsOTPTesting = bIsOTPTesting;

            bool mobile_MM = false;
            var mobile_maintenancemMode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_MOBILE, SystemConfigurationConstants.KEY_LOGIN_MOBLIE_MAINTENANCEMODE);
            if (mobile_maintenancemMode != null)
            {
                bool.TryParse(mobile_maintenancemMode.Value, out mobile_MM);
            }
            setting.maintenanceMode = mobile_MM;

            var mobile_maintenanceMessage = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_MOBILE, SystemConfigurationConstants.KEY_LOGIN_MOBLIE_MAINTENANCEMESSAGE);
            if (mobile_maintenanceMessage != null)
            {
                setting.maintenanceMessage = mobile_maintenanceMessage.Value;
            }
            return setting;
        }

        private MenuItem[] GetMenuItems()
        {
            return (from m in _menuSettingService.GetAllMenuSettings().Where(m => m.Published)
                    select new MenuItem()
                    {
                        id = m.Id.ToString(),
                        parent = m.Parent != null ? m.Parent.Id.ToString() : null,
                        level = m.Level ?? 0,
                        order = m.Order,
                        key = m.ContentSyncId != null ? m.ContentSyncId.Value.ToString() : null,
                        label = m.MenuTitle,
                        labelLocid = _enableLocalizedText ? (m.MenuTitleLoc != null ? m.MenuTitleLoc.Id.ToString() : null) : null
                                //(m.MenuTitleLoc != null ? m.MenuTitleLoc.Id : (int?)null) : (int?)null
                    }).ToArray();
        }

        private EnquiryTopic[] GetEnquiryTopics()
        {
            return (from e in _enquiryService.GetTopics().Where(x => x.Published && (x.Deleted == false || x.Deleted == null)).OrderBy(x => x.Order)
                    select new EnquiryTopic()
                    {
                        id = e.Id,
                        content = e.Name,
                        contentLocid = _enableLocalizedText ? (e.NameLoc != null ? e.NameLoc.Id.ToString() : null) : null,
                                //(e.NameLoc != null ? e.NameLoc.Id : (int?)null) : (int?)null,
                        instruction=e.Instruction,
                        instructionLocid = _enableLocalizedText ? (e.InstructionLoc != null ? e.InstructionLoc.Id.ToString() : null) : null,
                                //(e.InstructionLoc != null ? e.InstructionLoc.Id : (int?)null) : (int?)null,
                        attachmentRequired=e.RequireAttachment,
                        hasAttachment=e.HasAttachment,
                        order = e.Order
                    }).ToArray();
        }

        private PageResource[] GetPageResources()
        {
            IList<PageResource> lstPR = new List<PageResource>();

            List<DataRow> lst = _pageResourceService.GetPageResourcesForMobile();
            foreach (var dr in lst)
            {
                PageResource pageResource = new PageResource()
                {
                    id = Convert.ToInt32(dr["id"]),
                    category = dr["category"] as string,
                    key = dr["key"] as string,
                    content = dr["content"] as string,
                    contentLocid = _enableLocalizedText ? (dr["contentLocid"] != DBNull.Value ? Convert.ToInt32(dr["contentLocid"]).ToString() : null) : null
                        //(dr["contentLocid"] != DBNull.Value ? Convert.ToInt32(dr["contentLocid"]) : (int?)null) : (int?)null,
                };

                lstPR.Add(pageResource);
            }

            return lstPR.ToArray();
        }

        private LanguageContent[] GetLanguageContent()
        {
            IList<LanguageContent> lstLC = new List<LanguageContent>();

            var languages = _languageService.GetAll();

            foreach (var language in languages)
            {
                string langCode = string.Format("{0}-{1}", language.PriLan, language.SubLan);
                if (langCode.Equals(ConstValue.DEFAULT_CULTURE_NAME))
                    continue;

                LanguageContent languageContent = new LanguageContent();
                languageContent.languageCode = langCode;

                IList<LangContent> lstLangContent = new List<LangContent>();

                List<DataRow> lst = _textContentService.GetLanguageContentForMobile(language.Id);
                foreach (var dr in lst)
                {
                    LangContent langContent = new LangContent()
                    {
                        id = Convert.ToInt32(dr["id"]).ToString(),
                        value = dr["value"] as string,
                    };

                    lstLangContent.Add(langContent);
                }

                languageContent.contents = lstLangContent.ToArray();

                lstLC.Add(languageContent);
            }

            return lstLC.ToArray();
        }

    }
}
