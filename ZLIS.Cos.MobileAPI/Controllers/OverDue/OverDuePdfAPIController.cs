﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Framework.Common;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.PdfGenerator;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService.DomainObjects;
using Aspose.Words;
using ZLIS.Cos.Core.Common;
using ZLIS.Framework.Common.Encrypt;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Cos.MobileAPI.Filters;

namespace ZLIS.Cos.MobileAPI.Controllers.OverDue
{
    [ActionAuthorizeFilter]
    public class OverDuePdfAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        public ILogger Logger { get; set; }

        public OverDuePdfAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                       ILanguageService languageService,
                                       ISystemSettingService settingService,
                                       IMobileLoginInfoService mobileLoginInfoService,
                                       IRmwService rmwService,
                                       ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _rmwService = rmwService;
            ((RmwServiceAdaptor)_rmwService).SetCacheManager(cacheManager);

            _mobileLoginInfoService = mobileLoginInfoService;

        }

        [System.Web.Http.HttpPost]
        public GetOverdueBreakdownPdfResponse GetOverdueBreakdownPDF(GetOverdueBreakdownPdfRequest request)
        {
            if (request == null)
                request = new GetOverdueBreakdownPdfRequest();
            Logger.Debug(string.Format("-------- GetStatementOfAccountPDF API started. token:{0} contractNo:{1} ", request.token ?? string.Empty, request.contractNo ?? string.Empty));

            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetOverdueBreakdownPdfResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetOverDuePdfErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo))
                {
                    errorInfo.code = (int)GetOverDuePdfErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                string cc = this.GetCountryCode(_settingService).ToUpper();
                if (cc == "JP")
                {
                    errorInfo.code = (int)GetReceiptPdfErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                if (!string.IsNullOrEmpty(request.language) && !request.language.ToUpper().StartsWith("EN"))
                {
                    var language = _languageService.GetLocalLanguage();
                    if (language.PriLan.ToLower() + "-" + language.SubLan.ToLower() != request.language.ToLower())
                    {
                        errorInfo.code = (int)GetOverDuePdfErrorCode.InvalidRequest;
                        errorInfo.message = "Invalid language.";

                        return response;
                    }
                }
                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);

                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetOverDuePdfErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }
                //if (!this.IsPassOtp(_settingService, loginInfo, SystemConfigurationConstants.otp, request.token, "SOA"))
                //{
                //    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidToken;
                //    errorInfo.message = "Validate Otp Failed";
                //    return response;
                //}
                
               
                CmsCustomerProfile profile = null;
                if (!string.IsNullOrEmpty(loginInfo.CustomerInfo))
                {
                    try
                    {
                        BlowFish bf = new BlowFish(CosApplicationContext.Current.EncryptionKey);
                        profile = ZLIS.Framework.Common.Helper.JsonUtils.JsonStringToObject<CmsCustomerProfile>(HttpUtility.UrlDecode(bf.Decrypt_ECB(loginInfo.CustomerInfo)));
                    }
                    catch { }
                }
                if (profile == null)
                    profile = _rmwService.GetCustomerProfile(loginInfo.CustomerActivity.Customer.Bpid);
                ISessionState session = CosApplicationContext.Current.Session;
                session.CurrentCmsCustomerProfile = profile;
                response.data = GetOverduePdf(request);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetOverDuePdfErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get overdue draft pdf");
            }

            return response;
        }

        private FileContent GetOverduePdf(GetOverdueBreakdownPdfRequest request)
        {
            FileContent result = new FileContent();
            string reportFormat = ReportConstants.FORMAT_PDF;

            ISessionState session = CosApplicationContext.Current.Session;
            SaveFormat sf;
            string ext = "";
            string mimeType = "";
            switch (reportFormat)
            {
                case ReportConstants.FORMAT_PDF:
                    ext = ".pdf";
                    mimeType = "application/pdf";
                    sf = SaveFormat.Pdf;
                    break;
                default:
                    ext = ".pdf";
                    mimeType = "application/pdf";
                    sf = SaveFormat.Pdf;
                    break;
            }
            string cc = GetCountryCode();
            string downloadName = string.Format("{0}_{1}_{2:yyyyMMddHHmmss}.pdf", ReportConstants.KEY_OVERDUE_BREAKDOWN, request.contractNo, TimeHelper.Now);
            byte[] reportcontent=null;
            if (string.IsNullOrEmpty(request.language) || request.language.ToUpper().StartsWith("EN"))
                reportcontent = ReportGeneraterFactory.GetReporter(cc).GenerateOverdueReport(ReportConstants.TEMPLATE_OVERDUE_REPORT, downloadName, request.contractNo,
                cc, "EN", true);
            else
                reportcontent = ReportGeneraterFactory.GetReporter(cc).GenerateOverdueReport(ReportConstants.TEMPLATE_OVERDUE_REPORT, downloadName, request.contractNo,
                cc, cc, true);
            result.fileSize = reportcontent.Length;
            result.mimeType = mimeType;
            result.content = Convert.ToBase64String(reportcontent);
            result.fileName = downloadName;
            //ZLIS.Framework.Common.IO.IOHelper.SaveMediaFile("D:\\ReportTemp\\" + downloadName, reportcontent);
            return result;
        }

        #region Helper

        private string GetCountryCode()
        {
            var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY,
                SystemConfigurationConstants.KEY_COUNTRY_CODE);
            if (countryCode != null)
            {
                return countryCode.Value;
            }

            return null;
        }

        #endregion

    }
}
