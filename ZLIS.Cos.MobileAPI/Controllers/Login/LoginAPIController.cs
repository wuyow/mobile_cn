﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Account;
using ZLIS.Cos.Core.Account.DomainModel;
using ZLIS.Cos.Core.Account.Services;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.ContentManagement;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.DomainModel;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.DomainModel;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.UIManagement;
using ZLIS.Cos.Core.UIManagement.DomainModel;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.CountrySpecific;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Framework.Common.Encrypt;
using ZLIS.Cos.Core.Modules.Enquiry.Services;
using ZLIS.Cos.Core.ModuleFilterSetting.Services;
using ZLIS.Cos.Core.Modules.FormManagement.Services;
using System.Configuration;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    public class LoginAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ICustomerAccountService _accountService;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        private ICustomerActivityService _activityService;
        private IModuleSettingService _msService;

        private IEmailTemplateService _templateService;
        private IAppDataFolder _appDataFolder;
        private IMailer _mailer;
        private IPageResourceService _pageResourceService;
        private ICustomerTicketService _ticketService;
        private IMenuSettingService _menuSettingService;
        private IModuleFilterSettingService _mfsService;
        private bool _enableLocalizedText;
        private IBannerService _bannerService;
        private IFormService _formService;
        private ISpecialLogic _specialLogic;

        public ILogger Logger { get; set; }

        private string[] mobileTypes = new string[] { CmsPhoneType.Mobile };

        Dictionary<string, PropertyInfo> objectProperties = null;

        public LoginAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                  ICustomerAccountService accountService,
                                  ILanguageService languageService,
                                  ISystemSettingService settingService,
                                  IMobileLoginInfoService mobileLoginInfoService,
                                  IRmwService rmwService,
                                  ICustomerActivityService activityService,
                                  IModuleSettingService msService,
                                  IEmailTemplateService templateService,
                                  IAppDataFolder appDataFolder,
                                  IMailer mailer,
                                  IPageResourceService pageResourceService,
                                  ICustomerTicketService ticketService,
                                  IMenuSettingService menuSettingService,
                                  IModuleFilterSettingService mfsService,
                                  IBannerService bannerService,
                                  IFormService formService,
                                  ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _accountService = accountService;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;

            _mobileLoginInfoService = mobileLoginInfoService;

            _rmwService = rmwService;
            ((RmwServiceAdaptor) _rmwService).SetCacheManager(cacheManager);

            _activityService = activityService;
            _msService = msService;
            _templateService = templateService;
            _appDataFolder = appDataFolder;
            _mailer = mailer;
            _pageResourceService = pageResourceService;
            _enableLocalizedText = this.EnableLocalizedText(_settingService, _languageService);
            _ticketService = ticketService;
            _menuSettingService = menuSettingService;
            _mfsService = mfsService;
            _bannerService = bannerService;
            _formService = formService;
            _specialLogic = SpecialLogicFactory.GetSpecialLogic(this.GetCountryCode(_settingService));
        }

        private void InitMobileTypes()
        {
            SystemSettingItem mobileTypeSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_OTPSETTING_MOBILETYPE);
            //string[] mobileTypes = new string[] { CmsPhoneType.Mobile };
            if (mobileTypeSetting != null)
            {
                if (!string.IsNullOrEmpty(mobileTypeSetting.Value))
                {
                    mobileTypes = mobileTypeSetting.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                        .Select(x => x.Trim()).ToArray();
                }
            }
        }

        private void InitObjectProperties()
        {
            Dictionary<string, PropertyInfo> businessPartnerProperties = typeof(CmsBusinessPartner).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsBusinessPartner_" + x.Name, x => x);
            Dictionary<string, PropertyInfo> customerProfileProperties = typeof(CmsCustomerProfile).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsCustomerProfile_" + x.Name, x => x);
            Dictionary<string, PropertyInfo> cmsCustomerProperties = typeof(CmsCustomer).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsCustomer_" + x.Name, x => x);
            Dictionary<string, PropertyInfo> customerProperties = typeof(ZLIS.Cos.Core.Account.DomainModel.Customer).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "Customer_" + x.Name, x => x);
            Dictionary<string, PropertyInfo> addressProperties = typeof(CmsAddress).GetProperties(BindingFlags.Public | BindingFlags.Instance).ToDictionary(x => "CmsAddress_" + x.Name, x => x);
            objectProperties = businessPartnerProperties.Union(customerProfileProperties).Union(customerProperties).Union(addressProperties).Union(cmsCustomerProperties).ToDictionary(x => x.Key, x => x.Value);
        }

        [System.Web.Http.HttpPost]
        public LoginResponse Login(LoginRequest request)
        {
            if (request == null)
                request = new LoginRequest();
            Logger.Debug(string.Format("-------- Login API started. cc:{0} deviceId:{1} deviceType:{2} userId:{3} userIdFormat:{4} sessionId:{5}", 
                request.cc ?? string.Empty, request.deviceId ?? string.Empty, request.deviceType??string.Empty,request.userId??string.Empty,
                request.userIdFormat??string.Empty,request.sessionId??string.Empty));

            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new LoginResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)LoginErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            request.userId = request.userId.Trim();
            request.password = request.password.Trim();

            string sessionId = request.sessionId;
            if (string.IsNullOrEmpty(sessionId))
            {
                sessionId = Guid.NewGuid().ToString();
            }

            response.data = new LoginInfo();
            response.data.sessionId = sessionId;

            try
            {
                if (string.IsNullOrEmpty(request.cc) ||
                    string.IsNullOrEmpty(request.deviceId) ||
                    string.IsNullOrEmpty(request.deviceType) ||
                    string.IsNullOrEmpty(request.userIdFormat) ||
                    string.IsNullOrEmpty(request.appVersion) ||
                    string.IsNullOrEmpty(request.osVersion))
                {
                    errorInfo.code = (int)LoginErrorCode.BadParameters;
                    errorInfo.message = "Bad parameters";

                    return response;
                }

                var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_COUNTRY_CODE);
                if (countryCode != null)
                {
                    if (!request.cc.Equals(countryCode.Value, StringComparison.OrdinalIgnoreCase))
                    {
                        errorInfo.code = (int)GetListsContentErrorCode.BadParameters;
                        errorInfo.message = "Bad parameters";

                        return response;
                    }
                }
                else
                {
                    throw new ApplicationException("No country code setting");
                }

                string loginMode = this.LoginMode(_settingService);
                if (!request.userIdFormat.Equals(loginMode, StringComparison.OrdinalIgnoreCase))
                {
                    errorInfo.code = (int)LoginErrorCode.BadParameters;
                    errorInfo.message = "Bad parameters";

                    return response;
                }

                if (string.IsNullOrEmpty(request.userId) ||
                    string.IsNullOrEmpty(request.password))
                {
                    errorInfo.code = (int)LoginErrorCode.Login_InvalidUserIdPassword;
                    //errorInfo.message = "Invalid userId/password";

                    errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_InvalidUserNameOrPassword");

                    return response;
                }

                ZLIS.Cos.Core.Account.DomainModel.Customer customer =
                    _accountService.Login(request.userId, request.password, loginMode);
                CmsCustomer cmsCustomer = null;
                CmsCustomerProfile profile = null;
                if (customer == null)
                {
                    var cust = _accountService.GetCustomer(request.userId, loginMode);
                    if (cust != null)
                    {
                        if (cust.Email == ConfigurationManager.AppSettings["TestingAccount"])
                            profile = new CmsCustomerProfile();
                        else
                            profile = _rmwService.GetCustomerProfile(cust.Bpid);
                        //check if status=0, i.e. account not yet activated, status=1 means account activated already
                        if (cust.Status == 0)
                        {
                            Logger.Warning("Account is not activated for {0}", cust.Nric);

                            errorInfo.code = (int)LoginErrorCode.Login_AccountNotYetActivated;
                            //errorInfo.message = "Account is not activated";
                            errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_AccountNotYetActivated");

                            return response;
                        }

                        //check if already blocked
                        if (cust.AccountLocked ?? false)
                        {
                            Logger.Warning("Account is locked for {0}", cust.Nric);

                            errorInfo.code = (int)LoginErrorCode.Login_AccountIsLocked;
                            //errorInfo.message = "Account is locked";
                            errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_AccountIsSuspended");

                            return response;
                        }

                        //check if account is unlocked by admin, LOGIN_ATTEMPTS=null
                        if (cust.LoginAttempts == null)
                        {
                            cust.LoginAttempts = 0;
                            SetAppVar(sessionId, 0);
                            //session.FailedLoginAttempt = 0;
                        }

                        //check for the same session
                        IList<CustomerActivity> activities = _activityService.GetLoginActivity(cust, sessionId);

                        using (ITransaction tran = unitOfWork.BeginTransaction())
                        {
                            if (activities != null/* || (cust.AccountLocked ?? false)*/)
                            {
                                //cust.LoginAttempts = cust.LoginAttempts + 1;

                                SystemSettingItem failureAttemptsNo = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_LOGIN,
                                      SystemConfigurationConstants.KEY_PASSWORD_FAILTURE_ATTEMPTS_NO);

                                int nFailureAttemptsNo = 3;
                                if (failureAttemptsNo != null)
                                {
                                    int.TryParse(failureAttemptsNo.Value, out nFailureAttemptsNo);
                                }

                                SetAppVar(sessionId, GetAppVar(sessionId) + 1);
                                cust.AccountLocked = GetAppVar(sessionId) >= nFailureAttemptsNo;

                                //session.FailedLoginAttempt = session.FailedLoginAttempt + 1;
                                //cust.AccountLocked = session.FailedLoginAttempt >= nFailureAttemptsNo;    //!
                            }
                            else
                            {
                                //cust.LoginAttempts = 1;

                                SetAppVar(sessionId, 1);
                                cust.AccountLocked = false;
                                //session.FailedLoginAttempt = 1;   //!!
                                //cust.AccountLocked = false;
                            }

                            _accountService.UpdateCustomer(cust);
                            tran.Commit();
                        }

                        string languageCode = this.GetCurrentLanguageCode(_settingService, _languageService, request.language);
                        CultureInfo cultureInfo = new CultureInfo(languageCode);

                        //account is locked
                        if (cust.AccountLocked ?? false)
                        {
                            //send email to customer
                            EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                            string toEmail = this.GetEmailAddress(cust.Email);
                            EmailTemplate template = _templateService.GetEmailTemplate(EmailTemplateName.ACCOUNT_SUSPENSION, DeliveryChannel.EMAIL);
                            if (template != null)
                            {
                                string subject = template.Subject;
                                if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                                {
                                    subject = template.SubjectLocID.Text;
                                }

                                string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                                string rootUrl = "";

                                var s = _settingService.GetSystemSetting("System", "ExternalRootUrl");
                                if (s != null)
                                {
                                    rootUrl = s.Value;
                                }

                                string imageURL = emailUtility.GetImageUrl(rootUrl, "", template.Id.ToString());//Url.Content("~")
                                messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL, cust.DisplayName);
                                _mailer.SendEmail(template, toEmail, subject, messageBody);

                                //EmailSentMessage sentMessage = new EmailSentMessage();
                                //sentMessage.EmailTemplate = template;
                                //sentMessage.Message = messageBody;
                                //sentMessage.SentDate = DateTime.Now;
                                //sentMessage.ID = emailUtility.Hash(string.Format("{0}|{1}|{2}", cust.Email, template.Id.ToString(), (DateTime.Now).ToString("yyyy-MM-dd hh:mm:ss")));

                                //_emailSentMessageService.Create(sentMessage);
                            }

                            //send email to customer service
                            toEmail = this.GetEmailAddress(_settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_MAILER, SystemConfigurationConstants.KEY_MAILER_CUSTOMERSERVICE).Value);
                            template = _templateService.GetEmailTemplate(EmailTemplateName.ACCOUNT_SUSPENSION_ADMIN, DeliveryChannel.EMAIL);
                            if (template != null)
                            {
                               // CmsCustomer cmsCust = _rmwService.ValidateCustomer(cust.Nric);
                                string mobileNo = "";

                                if (profile != null && profile.PhoneNOs != null && profile.PhoneNOs.Length > 0)
                                {
                                    SystemSettingItem mobileTypeSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_OTPSETTING_MOBILETYPE);
                                    string[] mobileTypes = new string[] { CmsPhoneType.Mobile };
                                    if (mobileTypeSetting != null)
                                    {
                                        if (!string.IsNullOrEmpty(mobileTypeSetting.Value))
                                        {
                                            mobileTypes = mobileTypeSetting.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                                .Select(x => x.Trim()).ToArray();
                                        }
                                    }

                                    foreach (CmsPhone phone in profile.PhoneNOs)
                                    {
                                        if (mobileTypes.Contains(phone.PhoneTypeCode, StringComparer.OrdinalIgnoreCase))
                                        //if (phone.PhoneType == CmsPhoneType.Mobile)
                                        {
                                            if (mobileNo == "")
                                                mobileNo = phone.PhoneNO;
                                            else
                                                mobileNo = string.Format("{0}, {1}", mobileNo, phone.PhoneNO);
                                        }
                                    }
                                }
                                string subject = template.Subject;
                                if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) && template.SubjectLocID != null)
                                {
                                    subject = template.SubjectLocID.Text;
                                }
                                string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                                messageBody = emailUtility.GetMessageBodyReplacement(messageBody, "", cust.DisplayName, "", "", "", "", "", mobileNo);
                                _mailer.SendEmail(template, toEmail, subject, messageBody);
                            }

                            errorInfo.code = (int)LoginErrorCode.Login_ExceedMaximumLoginAttempt;
                            //errorInfo.message = "Exceed maximum login attempt";
                            errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_ExceedMaximumLoginAttempt");

                            //model.ErrorMessage = this.Resource("Account_Login,Validation_ExceedMaximumLoginAttempt");
                        }
                        else
                        {
                            errorInfo.code = (int)LoginErrorCode.Login_InvalidUserIdPassword;
                            //errorInfo.message = "Invalid userId/password";
                            errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_InvalidUserNameOrPassword");

                            //model.ErrorMessage = this.Resource("Account_Login,Validation_InvalidUserNameOrPassword");
                        }

                        _activityService.CreateActivity(cust,
                                                        RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                        string.Format("Mobile/{0}", request.deviceType)/*RequestHelpers.GetBrowserAgent(HttpContext.Current.Request)*/,
                                                        sessionId,
                                                        LoginStatus.Password_Incorrect, "Login failed", "Login", ModuleName.Login,
                                                        PageName.Login);
                    }
                    else
                    {
                        errorInfo.code = (int)LoginErrorCode.Login_NoMatchingRecordsFound;
                        //errorInfo.message = "No matching records found";
                        errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_NoMatchingRecordsFound");
                    }
                }
                else
                {
                    if (customer.Email == ConfigurationManager.AppSettings["TestingAccount"])
                        profile = new CmsCustomerProfile();
                    else
                        profile = _rmwService.GetCustomerProfile(customer.Bpid);
                    if (customer.Status == 0)
                    {
                        _activityService.CreateActivity(customer,
                                                                RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                                string.Format("Mobile/{0}", request.deviceType)/*RequestHelpers.GetBrowserAgent(HttpContext.Current.Request)*/,
                                                                sessionId,
                                                                LoginStatus.Not_Active, "Account is not activated", "Login", ModuleName.Login,
                                                                PageName.Login);

                        Logger.Warning("Account is not activated for {0}", customer.Nric);

                        errorInfo.code = (int)LoginErrorCode.Login_AccountNotYetActivated;
                        //errorInfo.message = "Account is not activated";
                        errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_AccountNotYetActivated");
                    }

                    //check if account is blocked
                    if (customer.AccountLocked ?? false)
                    {
                        _activityService.CreateActivity(customer,
                                                                RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                                string.Format("Mobile/{0}", request.deviceType)/*RequestHelpers.GetBrowserAgent(HttpContext.Current.Request)*/,
                                                                sessionId,
                                                                LoginStatus.Locked, "Account is locked", "Login", ModuleName.Login,
                                                                PageName.Login);

                        Logger.Warning("Account is locked for {0}", customer.Nric);

                        errorInfo.code = (int)LoginErrorCode.Login_AccountIsLocked;
                        //errorInfo.message = "Account is locked";
                        errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_AccountIsSuspended");
                    }
                    if (customer.Email == ConfigurationManager.AppSettings["TestingAccount"])
                        cmsCustomer = new CmsCustomer();
                    else
                        cmsCustomer = _rmwService.ValidateCustomer(customer.Nric);

                    //reset login attempt to 0
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        //commented by pyisoe as we are not suppose to store cms email (2013-04-04)
                        //if (cmsCustomer != null && cmsCustomer.EmailAddresses != null && cmsCustomer.EmailAddresses.Length > 0)
                        //{
                        //    //customer.CmsEmail = cmsCustomer.EmailAddresses.OrderByDescending(x => x.EmailId).Take(1).First().EmailAddress;
                        //    //customer.CmsEmail = cmsCustomer.EmailAddresses.Select(email =>
                        //    //    new ZLIS.Cos.Core.Account.DomainModel.CmsEmail()
                        //    //    {
                        //    //        EmailAddress = email.EmailAddress,
                        //    //        EmailId = email.EmailId
                        //    //    }).ToArray();
                        //}

                        customer.LoginAttempts = 0;
                        _accountService.UpdateCustomer(customer);
                        tran.Commit();
                    }

                    //check if CmsCustomer is null
                    if (cmsCustomer == null)
                    {
                        _activityService.CreateActivity(customer,
                                                                RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                                string.Format("Mobile/{0}", request.deviceType)/*RequestHelpers.GetBrowserAgent(HttpContext.Current.Request)*/,
                                                                sessionId,
                                                                LoginStatus.CmsCustomerNotFound, "CmsCustomer returned null from RMW", "Login", ModuleName.Login,
                                                                PageName.Login);

                        Logger.Warning("RMW Webservice returns NULL for Nric {0}", customer.Nric);

                        errorInfo.code = (int)LoginErrorCode.Login_NoMatchingRecordsFound;
                        //errorInfo.message = string.Format("RMW Webservice returns NULL for Nric {0}", customer.Nric);
                        errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_NoMatchingRecordsFound");
                    }
                    else if(customer.Email != ConfigurationManager.AppSettings["TestingAccount"])
                    {
                        bool isBorrower = cmsCustomer.Roles != null && cmsCustomer.Roles.Length > 0 && (cmsCustomer.Roles.Any(x => x == CmsRole.Borrower || x == CmsRole.Contractor));

                        if (!isBorrower)
                        {
                            _activityService.CreateActivity(customer,
                                                                RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                                string.Format("Mobile/{0}", request.deviceType)/*RequestHelpers.GetBrowserAgent(HttpContext.Current.Request)*/,
                                                                sessionId,
                                                                LoginStatus.UnauthorizedRole, "Role not authorized to login", "Login", ModuleName.Login,
                                                                PageName.Login);

                            errorInfo.code = (int)LoginErrorCode.Login_NotAuthorized;
                            //errorInfo.message = "No permission to login";
                            errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_NoPermissionToLogin");
                        }
                    }
                }

                //must have contract to login
                if (_specialLogic.CheckFilteredContractDuringRegistration())
                {
                    RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                    var summaries = rmwServiceAdaptor.GetAccountSummary(cmsCustomer.BpId);

                    if (summaries == null || summaries.Count == 0)
                    {
                        _activityService.CreateActivity(customer,
                                                                RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                                string.Format("Mobile/{0}", request.deviceType)/*RequestHelpers.GetBrowserAgent(HttpContext.Current.Request)*/,
                                                                sessionId,
                                                                LoginStatus.UnauthorizedRole, "Must have contract to login", "Login", ModuleName.Login,
                                                                PageName.Login);
                        errorInfo.code = (int)LoginErrorCode.Login_NotAuthorized;
                        errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_NoPermissionToLogin");
                    }
                }

                if (errorInfo.code != (int)LoginErrorCode.NoError)
                {
                    return response;
                }


                //If profile is null, show error
                if (profile == null)
                {
                    _activityService.CreateActivity(customer,
                                                            RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                            RequestHelpers.GetBrowserAgent(HttpContext.Current.Request),
                                                            sessionId,
                                                            LoginStatus.CmsCustomerProfileNotFound, "CmsCustomerProfile returned null from RMW", "Login", ModuleName.Login,
                                                            PageName.Login);

                    errorInfo.code = (int)LoginErrorCode.Login_NoCustomerProfileFound;
                    //errorInfo.message = "No customer profile found";
                    errorInfo.message = this.Resource(_pageResourceService, "Account_Login,Validation_NoCustomerProfileFound");
                }

                try
                {
                    //update active session id
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        customer.ActiveSessionID = sessionId;
                        customer.LastLoginDate = DateTime.Now;

                        _accountService.UpdateCustomer(customer);

                        tran.Commit();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Error occurs when updating customer activities.", ex);
                }

                var ca = _activityService.CreateActivity(customer,
                                                         RequestHelpers.GetClientIpAddress(HttpContext.Current.Request),
                                                         string.Format("Mobile/{0}", request.deviceType)/*RequestHelpers.GetBrowserAgent(HttpContext.Current.Request)*/,
                                                         sessionId,
                                                         LoginStatus.Login_Success, "Login Successful", "Login", ModuleName.Login,
                                                         PageName.Login);
                BlowFish bf = new BlowFish(CosApplicationContext.Current.EncryptionKey);
                MobileLoginInfo mobileLoginInfo = _mobileLoginInfoService.CreateMobileLoginInfo(
                    ca, request.deviceId, request.deviceType,
                     request.appVersion, request.osVersion, request.deviceVendorCode,
                     this.LoginExpiryMinutes(), customer.Email == ConfigurationManager.AppSettings["TestingAccount"] ? string.Empty : bf.Encrypt_ECB( HttpUtility.UrlEncode(ZLIS.Framework.Common.Helper.JsonUtils.ObjectToJsonString(GetProfileWithNoProxy(profile)))));

                InitObjectProperties();
                InitMobileTypes();

                response.data = GetLoginInfo(mobileLoginInfo.Token, sessionId, request.cc, customer, profile, cmsCustomer);

                AppCacheContainer.AddCache(new CacheObj()
                {
                    BpId = cmsCustomer.BpId,
                    LastAccessTime = DateTime.Now,
                    SessionId = sessionId,
                    Token = mobileLoginInfo.Token
                });
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)LoginErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when login");
            }

            Logger.Debug("-------- Login API ended.");

            return response;
        }

        private void SetAppVar(string key, int value)
        {
            System.Web.HttpContext.Current.Application[key] = value;
        }

        private int GetAppVar(string key)
        {
            if (System.Web.HttpContext.Current.Application[key] != null)
                return Convert.ToInt32(System.Web.HttpContext.Current.Application[key]);

            return 0;
        }

        private LoginInfo GetLoginInfo(string token, string sessionId, string cc, ZLIS.Cos.Core.Account.DomainModel.Customer customer, CmsCustomerProfile profile, CmsCustomer cmsCustomer)
        {
            LoginInfo loginInfo = new LoginInfo();

            loginInfo.token = token;

            loginInfo.sessionId = sessionId;

            Logger.Debug("Get customer info started.");
            loginInfo.customer = GetCustomer(customer, profile);
            Logger.Debug("Get customer info succeeded.");
            if (customer.Email != ConfigurationManager.AppSettings["TestingAccount"])
            {
                Logger.Debug("Get customer profile started.");
                loginInfo.customerProfile = GetCustomerProfile(cc, customer, profile, cmsCustomer);
                Logger.Debug("Get customer profile succeeded.");

                Logger.Debug("Get contracts started.");
                loginInfo.contracts = GetContracts(customer);
                Logger.Debug("Get contracts succeeded.");

                Logger.Debug("Get language content started.");
                loginInfo.languageContent = _enableLocalizedText ? GetLanguageContent(cc, customer, profile, cmsCustomer) : null;
                Logger.Debug("Get language content ended.");

                Logger.Debug("Get enquiry started.");
                loginInfo.enquiryInfo = GetEnquiry(customer, profile);
                Logger.Debug("Get enquiry succeeded.");
            }
            Logger.Debug("Get banners started.");
            loginInfo.banners = GetBanners();
            Logger.Debug("Get banners succeeded.");

            Logger.Debug("Get unread message count started.");
            var tickets = _ticketService.GetTicketsByCustomerId(customer.Id);
            if (tickets != null)
            {
                loginInfo.unreadMessageNum = tickets.Sum(a => a.TotalUnreadMessage);
            }
            else
                loginInfo.unreadMessageNum = 0;
            int giroFormId = 0;
            string rawId = System.Configuration.ConfigurationManager.AppSettings["GiroFormId"];
            if (!string.IsNullOrEmpty(rawId))
            {
                if (!Int32.TryParse(rawId, out giroFormId))
                {
                    loginInfo.isGiroFormAvailable = false;
                }
                else
                {
                    var form = _formService.GetById(giroFormId);
                    if (form != null)
                    {
                        loginInfo.isGiroFormAvailable = true;
                    }
                    else
                        loginInfo.isGiroFormAvailable = false;
                }
            }
            else
                loginInfo.isGiroFormAvailable = false;

            Logger.Debug("Get unread message count ended.");

            return loginInfo;
        }

        private ZLIS.Cos.ClientModel.Models.Customer GetCustomer(ZLIS.Cos.Core.Account.DomainModel.Customer customer, CmsCustomerProfile customerProfile)
        {
            ZLIS.Cos.ClientModel.Models.Customer cust = new ClientModel.Models.Customer()
            {
                customerType = customerProfile.BusinessPartner==null?"Individual":(customerProfile.BusinessPartner.CustomerType == CmsCustomerType.Corporate) ?
                                    "Corporate" : "Individual",
                email = customer.Email,
                firstName = customer.FirstName,
                middleName = customer.MiddleName,
                lastName = customer.LastName,
            };

            //if (customerProfile.BusinessPartner.CustomerType == CmsCustomerType.Corporate)

            return cust;
        }

        private ZLIS.Cos.ClientModel.Models.CustomerProfile GetCustomerProfile(string cc, ZLIS.Cos.Core.Account.DomainModel.Customer customer, CmsCustomerProfile customerProfile, CmsCustomer cmsCustomer)
        {
            ZLIS.Cos.ClientModel.Models.CustomerProfile cp =
                new ClientModel.Models.CustomerProfile();

            IList<ZLIS.Cos.ClientModel.Models.LoginSection> lst =
                new List<ZLIS.Cos.ClientModel.Models.LoginSection>();

            Logger.Debug("Get customer profile module settings started.");
            var lstModuleSetting = _msService.GetModuleSettings_NoCache(ModuleSettingConstants.PROFILE).Where(x => x.Visible).OrderBy(x => x.Order).ToList();
            Logger.Debug("Get customer profile module settings succeeded.");
            int? addressCnt = GetAddressCnt(cc, customer, customerProfile, lstModuleSetting);

            foreach (var moduleSetting in lstModuleSetting)
            {
                Logger.Debug(string.Format("Get customer profile section for {0} started.", moduleSetting.ResKey));

                //string title = moduleSetting.GetDisplayName(cultureInfo);
                string key = string.Empty;
                string title = moduleSetting.Name;
                string strTitle = string.Empty;

                decimal? labelLocid = null;
                LoginField[] arrFields = null;
                switch (moduleSetting.ResKey)
                {
                    case ModuleSettingConstants.PROFILE_ADDRESS:
                        if (addressCnt.HasValue && addressCnt.Value > 1)
                        {
                            for (int i = 0; i < addressCnt; i++)
                            {
                                key = string.Format("{0}_{1}", moduleSetting.ResKey, i + 1);
                                strTitle = string.Format("{0} {1}", title, i + 1);
                                Logger.Debug(string.Format("Get customer profile fields for {0} started.", key));
                                arrFields = PROFILE(cc, customer, customerProfile,cmsCustomer, moduleSetting.ResKey, strTitle, i);
                                Logger.Debug(string.Format("Get customer profile fields for {0} succeeded.", key));
                                labelLocid = _enableLocalizedText ?
                                    (moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (decimal?)null) : (decimal?)null;

                                if (labelLocid != null)
                                {
                                    labelLocid += (decimal)((i + 1) / 10.0);
                                }

                                LoginSection section = new LoginSection()
                                {
                                    id = key,
                                    label = strTitle,
                                    order = moduleSetting.Order,
                                    labelLocid = labelLocid.ToString(),
                                    fields = arrFields,
                                };

                                lst.Add(section);
                            }
                        }
                        else
                        {
                            key = moduleSetting.ResKey;
                            strTitle = string.Format("{0}", title);
                            Logger.Debug(string.Format("Get customer profile fields for {0} started.", key));
                            arrFields = PROFILE(cc, customer, customerProfile,cmsCustomer, moduleSetting.ResKey, strTitle, 0);
                            Logger.Debug(string.Format("Get customer profile fields for {0} succeeded.", key));

                            labelLocid = _enableLocalizedText ?
                                (moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (decimal?)null) : (decimal?)null;

                            LoginSection section = new LoginSection()
                            {
                                id = key,
                                label = strTitle,
                                order = moduleSetting.Order,
                                labelLocid = labelLocid.ToString(),
                                fields = arrFields,
                            };

                            lst.Add(section);
                        }
                        break;

                    default:
                        {
                            key = moduleSetting.ResKey;
                            strTitle = string.Format("{0}", title);
                            Logger.Debug(string.Format("Get customer profile fields for {0} started.", key));
                            arrFields = PROFILE(cc, customer, customerProfile,cmsCustomer, moduleSetting.ResKey, strTitle);
                            Logger.Debug(string.Format("Get customer profile fields for {0} succeeded.", key));

                            labelLocid = _enableLocalizedText ?
                                (moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (decimal?)null) : (decimal?)null;

                            LoginSection section = new LoginSection()
                            {
                                id = key,
                                label = strTitle,
                                order = moduleSetting.Order,
                                labelLocid = labelLocid.ToString(),
                                fields = arrFields,
                            };

                            lst.Add(section);
                        }
                        break;
                }

                Logger.Debug(string.Format("Get customer profile section for {0} succeeded.", moduleSetting.ResKey));
            }

            cp.sections = lst.ToArray();

            return cp;
        }

        private int? GetAddressCnt(string cc, ZLIS.Cos.Core.Account.DomainModel.Customer customer, CmsCustomerProfile customerProfile, IList<ModuleSetting> lstModuleSetting)
        {
            int? addressCnt;
            IList<CmsAddress> lstAddress = _rmwService.GetAddressDetails(customer.Bpid);

            //var lstModuleSetting = _msService.GetModuleSettings_NoCache(ModuleSettingConstants.PROFILE).Where(x => x.Visible).OrderBy(x => x.Order).ToList();

            if (customerProfile.BusinessPartner.CustomerType == CmsCustomerType.Corporate)
            {
                SystemSettingItem addressSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_PROFILE, SystemConfigurationConstants.KEY_CORPORATE_DEFAULT_ADDRESS);
                if (addressSetting != null)
                {
                    var addresstypes = addressSetting.Value.Split(',').ToList();
                    var query = from ms in lstModuleSetting where ms.ResKey == ModuleSettingConstants.PROFILE_INDIVIDUAL_PROFILE select ms;
                    if (query.Count() > 0) lstModuleSetting.Remove(query.First());

                    if (lstAddress != null)
                    {
                        if (cc.Equals("MY", StringComparison.OrdinalIgnoreCase))
                            lstAddress = lstAddress.OrderBy(x => x.AddressSeq).Take(1).ToList();
                        else
                            lstAddress = lstAddress.Where(x => !string.IsNullOrEmpty(x.AddressType) && (addresstypes.Contains(x.AddressTypeCode))).OrderBy(x => x.AddressSeq).ToList();
                    }
                }
            }
            else
            {
                var query = from ms in lstModuleSetting where ms.ResKey == ModuleSettingConstants.PROFILE_COMPANY_PROFILE select ms;
                if (query.Count() > 0) lstModuleSetting.Remove(query.First());

                if (lstAddress != null)
                {
                    SystemSettingItem addressSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_PROFILE, SystemConfigurationConstants.KEY_INDUDIDUAL_DEFAULT_ADDRESS);
                    if (addressSetting != null)
                    {
                        var addresstypes = addressSetting.Value.Split(',').ToList();
                        if (cc.Equals("MY", StringComparison.OrdinalIgnoreCase))
                            lstAddress = lstAddress.OrderBy(x => x.AddressSeq).Take(1).ToList();
                        else
                            lstAddress = lstAddress.Where(x => !string.IsNullOrEmpty(x.AddressType) &&
                                (addresstypes.Contains(x.AddressTypeCode))).OrderBy(x => x.AddressSeq).ToList();
                    }
                }
            }

            addressCnt = lstAddress != null ? lstAddress.Count : new System.Nullable<int>();

            return addressCnt;
        }

        private LoginField[] PROFILE(string cc, ZLIS.Cos.Core.Account.DomainModel.Customer customer,
            CmsCustomerProfile customerProfile,CmsCustomer cmsCustomer,
            string module, string Title, int? idx = null)
        {
            //CmsCustomerProfile customerProfile = sessionState.CurrentCmsCustomerProfile;
            //ZLIS.Cos.Core.Account.DomainModel.Customer customer = sessionState.CurrentCustomer;
            IEnumerable<GridModuleSetting> details = _msService.GetGridModuleSettings_NoCache(module)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();

            if (details.Count() > 0)
            {
                CmsBusinessPartner cmsBusinessPartner = customerProfile.BusinessPartner;
                //CmsCustomer cmsCustomer = sessionState.CurrentCmsCustomer;

                ////sessionState.DoNotInterceptLocalProperty = true;

                IList<CmsAddress> lstAddress = _rmwService.GetAddressDetails(cmsCustomer.BpId);
                CmsAddress address = null;
                if (lstAddress != null && idx.HasValue)
                {
                    var lstModuleSetting = _msService.GetModuleSettings_NoCache(ModuleSettingConstants.PROFILE).Where(x => x.Visible).OrderBy(x => x.Order).ToList();
                    if (customerProfile.BusinessPartner.CustomerType == CmsCustomerType.Corporate)
                    {
                        SystemSettingItem addressSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_PROFILE, SystemConfigurationConstants.KEY_CORPORATE_DEFAULT_ADDRESS);
                        if (addressSetting != null)
                        {
                            var addresstypes = addressSetting.Value.Split(',').ToList();
                            var query = from ms in lstModuleSetting where ms.ResKey == ModuleSettingConstants.PROFILE_INDIVIDUAL_PROFILE select ms;
                            if (query.Count() > 0) lstModuleSetting.Remove(query.First());

                            if (lstAddress != null)
                            {
                                if (cc.Equals("MY", StringComparison.OrdinalIgnoreCase))
                                    lstAddress = lstAddress.OrderBy(x => x.AddressSeq).Take(1).ToList();
                                else
                                    lstAddress = lstAddress.Where(x => !string.IsNullOrEmpty(x.AddressType) && (addresstypes.Contains(x.AddressTypeCode))).OrderBy(x => x.AddressSeq).ToList();
                            }
                        }
                    }
                    else
                    {
                        var query = from ms in lstModuleSetting where ms.ResKey == ModuleSettingConstants.PROFILE_COMPANY_PROFILE select ms;
                        if (query.Count() > 0) lstModuleSetting.Remove(query.First());

                        if (lstAddress != null)
                        {
                            SystemSettingItem addressSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_PROFILE, SystemConfigurationConstants.KEY_INDUDIDUAL_DEFAULT_ADDRESS);
                            if (addressSetting != null)
                            {
                                var addresstypes = addressSetting.Value.Split(',').ToList();
                                if (cc.Equals("MY", StringComparison.OrdinalIgnoreCase))
                                    lstAddress = lstAddress.OrderBy(x => x.AddressSeq).Take(1).ToList();
                                else
                                    lstAddress = lstAddress.Where(x => !string.IsNullOrEmpty(x.AddressType) && (addresstypes.Contains(x.AddressTypeCode))).OrderBy(x => x.AddressSeq).ToList();
                            }
                        }
                    }

                    if (lstAddress != null && lstAddress.Count > 0)
                    {
                        address = lstAddress[idx.Value];
                    }
                    //ViewBag.Object = address;
                }

                IList<CmsPhone> lstMobiles = cmsCustomer.HandPhones.Where(m => mobileTypes.Contains(m.PhoneTypeCode, StringComparer.OrdinalIgnoreCase)).OrderBy(m => m.PhoneSequence).Select(m => m).Distinct().ToList();

                Dictionary<string, object> objectList = new Dictionary<string, object>();
                objectList.Add("CmsBusinessPartner", cmsBusinessPartner);
                objectList.Add("CmsCustomerProfile", customerProfile);
                objectList.Add("CmsCustomer", cmsCustomer);
                objectList.Add("Customer", customer);
                objectList.Add("CmsAddress", lstAddress);

                //ViewBag.Title = Title;
                //ViewBag.lstModuleSetting = details;
                //ViewBag.ObjectList = objectList;
                //ViewBag.ObjectProperties = objectProperties;
                //ViewBag.MobileList = lstMobiles;

                //var isCompanyCustomer = customerProfile.BusinessPartner.CustomerType == CmsCustomerType.Corporate;

                IList<LoginField> lst = new List<LoginField>();
                lst = GetFields(details, objectList, objectProperties, address, lstMobiles, customerProfile.BusinessPartner.CustomerType == CmsCustomerType.Individual);
                //switch (module)
                //{
                //    case "PROFILE_INDIVIDUAL_PROFILE":
                //        lst = PROFILE_INDIVIDUAL_PROFILE(details, objectList, objectProperties, lstMobiles);
                //        break;
                //    case "PROFILE_COMPANY_PROFILE":
                //        lst = PROFILE_COMPANY_PROFILE(details, objectList, objectProperties, lstMobiles);
                //        break;
                //    case "PROFILE_CONTACT_INFO":
                //        lst = PROFILE_CONTACT_INFO(details, objectList, objectProperties, lstMobiles);
                //        break;
                //    case "PROFILE_ADDRESS":
                //        lst = PROFILE_ADDRESS(details, objectList, objectProperties, address, lstMobiles);
                //        break;
                //    default:
                //        foreach (var d in details)
                //        {
                //            //this.GetDisplayValue(_pageResourceService, _settingService, GetDefaultCultureName(),

                //            var field = new Field()
                //            {
                //                id = d.ResKey,
                //                module = d.ModuleName,
                //                label = d.Name,
                //                labelLocId = d.NameLoc != null ? d.NameLoc.Id : (int?)null,
                //                order = d.Order,
                //            };
                //            lst.Add(field);
                //        }
                //        break;
                //}

                return lst.ToArray();
            }
            else
            {
                return null;
            }
        }

        //private IList<Field> PROFILE_INDIVIDUAL_PROFILE(IEnumerable<GridModuleSetting> lstModuleSetting,
        //    Dictionary<string, object> ObjectList,
        //    Dictionary<string, System.Reflection.PropertyInfo> ObjectProperties, IList<CmsPhone> lstMobiles)
        //{
        //    IList<Field> lst = new List<Field>();

        //    //int i = 0;
        //    string value;
        //    string valueLoc;
        //    foreach (GridModuleSetting column in lstModuleSetting)
        //    {
        //        //if (ObjectProperties.ContainsKey(column.ObjectProperty))
        //        {
        //            foreach (var objectName in ObjectList.Keys)
        //            {
        //                var obj = ObjectList[objectName];
        //                if (column.ObjectProperty.StartsWith(objectName + "_") && obj != null)
        //                {
        //                    if (column.ObjectProperty != "CmsCustomer_HandPhones" &&
        //                        column.ObjectProperty != "CmsCustomerProfile_Emails")
        //                    {
        //                        this.GetDisplayValue(_pageResourceService, _settingService, GetDefaultCultureName(),
        //                             ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat, false, false, "", out value, out valueLoc, true);

        //                        var field = new Field()
        //                        {
        //                            id = column.ResKey,
        //                            module = column.ModuleName,
        //                            label = column.Name,
        //                            labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null,
        //                            order = column.Order,
        //                            value = value,
        //                            valueLoc = valueLoc,
        //                        };
        //                        lst.Add(field);
        //                    }
        //                    else
        //                    {
        //                        if (objectName == "CmsCustomer")
        //                        {
        //                            CmsCustomer cmsCustomer = obj as CmsCustomer;
        //                            if (cmsCustomer.HandPhones != null)
        //                            {
        //                                //IList<ZLIS.Cos.RmwService.DomainObjects.CmsPhone> lstMobiles = ViewBag.MobileList;
        //                                int idx = 0;
        //                                foreach (var ce in lstMobiles)
        //                                {
        //                                    //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                                    //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
        //                                    //    <span id='contract_right'>@Html.Raw(string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : ""))</span>
        //                                    //</li>

        //                                    var field = new Field();

        //                                    field.id = column.ResKey;
        //                                    field.module = column.ModuleName;
        //                                    field.label = string.Format("{0} {1}", column.Name, idx + 1);

        //                                    decimal? labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null;
        //                                    if (labelLocId != null)
        //                                    {
        //                                        labelLocId += (decimal)((idx + 1) / 10.0);
        //                                    }

        //                                    field.labelLocId = labelLocId;
        //                                    field.order = column.Order;
        //                                    field.value = string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : "");

        //                                    lst.Add(field);

        //                                    idx++;
        //                                }
        //                            }
        //                        }
        //                        else if (objectName == "CmsCustomerProfile")
        //                        {
        //                            CmsCustomerProfile cmsCustomerProfile = obj as CmsCustomerProfile;
        //                            if (cmsCustomerProfile.Emails != null)
        //                            {
        //                                int idx = 0;
        //                                foreach (var ce in cmsCustomerProfile.Emails)
        //                                {
        //                                    //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                                    //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
        //                                    //    <span id='contract_right'>@ce.EmailAddress</span> </li>

        //                                    var field = new Field();
        //                                    field.id = column.ResKey;
        //                                    field.module = column.ModuleName;
        //                                    field.label = string.Format("{0} {1}", column.Name, idx + 1);

        //                                    decimal? labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null;
        //                                    if (labelLocId != null)
        //                                    {
        //                                        labelLocId += (decimal)((idx + 1) / 10.0);
        //                                    }

        //                                    field.labelLocId = labelLocId;

        //                                    field.order = column.Order;
        //                                    field.value = ce.EmailAddress;

        //                                    lst.Add(field);

        //                                    idx++;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    //return lst.ToArray();
        //    return lst;
        //}

        //private IList<Field> PROFILE_COMPANY_PROFILE(IEnumerable<GridModuleSetting> lstModuleSetting,
        //    Dictionary<string, object> ObjectList,
        //    Dictionary<string, System.Reflection.PropertyInfo> ObjectProperties, IList<CmsPhone> lstMobiles)
        //{
        //    IList<Field> lst = new List<Field>();

        //    //int i = 0;
        //    string value;
        //    string valueLoc;
        //    foreach (GridModuleSetting column in lstModuleSetting)
        //    {
        //        //if (ObjectProperties.ContainsKey(column.ObjectProperty))
        //        {
        //            foreach (var objectName in ObjectList.Keys)
        //            {
        //                var obj = ObjectList[objectName];
        //                if (column.ObjectProperty.StartsWith(objectName + "_") && obj != null)
        //                {
        //                    if (column.ObjectProperty != "CmsCustomer_HandPhones" &&
        //                        column.ObjectProperty != "CmsCustomerProfile_Emails")
        //                    {
        //                        this.GetDisplayValue(_pageResourceService, _settingService, GetDefaultCultureName(),
        //                             ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat, false, false, "", out value, out valueLoc, true);

        //                        var field = new Field()
        //                        {
        //                            id = column.ResKey,
        //                            module = column.ModuleName,
        //                            label = column.Name,
        //                            labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null,
        //                            order = column.Order,
        //                            value = value,
        //                            valueLoc = valueLoc,
        //                        };
        //                        lst.Add(field);
        //                    }
        //                    else
        //                    {
        //                        if (objectName == "CmsCustomer")
        //                        {
        //                            CmsCustomer cmsCustomer = obj as CmsCustomer;
        //                            if (cmsCustomer.HandPhones != null)
        //                            {
        //                                //IList<ZLIS.Cos.RmwService.DomainObjects.CmsPhone> lstMobiles = ViewBag.MobileList;
        //                                int idx = 0;
        //                                foreach (var ce in lstMobiles)
        //                                {
        //                                    //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                                    //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
        //                                    //    <span id='contract_right'>@Html.Raw(string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : ""))</span>
        //                                    //</li>

        //                                    var field = new Field();

        //                                    field.id = column.ResKey;
        //                                    field.module = column.ModuleName;
        //                                    field.label = string.Format("{0} {1}", column.Name, idx + 1);

        //                                    decimal? labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null;
        //                                    if (labelLocId != null)
        //                                    {
        //                                        labelLocId += (decimal)((idx + 1) / 10.0);
        //                                    }

        //                                    field.labelLocId = labelLocId;
        //                                    field.order = column.Order;
        //                                    field.value = string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : "");

        //                                    lst.Add(field);

        //                                    idx++;
        //                                }
        //                            }
        //                        }
        //                        else if (objectName == "CmsCustomerProfile")
        //                        {
        //                            CmsCustomerProfile cmsCustomerProfile = obj as CmsCustomerProfile;
        //                            if (cmsCustomerProfile.Emails != null)
        //                            {
        //                                int idx = 0;
        //                                foreach (var ce in cmsCustomerProfile.Emails)
        //                                {
        //                                    //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                                    //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
        //                                    //    <span id='contract_right'>@ce.EmailAddress</span> </li>

        //                                    var field = new Field();
        //                                    field.id = column.ResKey;
        //                                    field.module = column.ModuleName;
        //                                    field.label = string.Format("{0} {1}", column.Name, idx + 1);

        //                                    decimal? labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null;
        //                                    if (labelLocId != null)
        //                                    {
        //                                        labelLocId += (decimal)((idx + 1) / 10.0);
        //                                    }

        //                                    field.labelLocId = labelLocId;

        //                                    field.order = column.Order;
        //                                    field.value = ce.EmailAddress;

        //                                    lst.Add(field);

        //                                    idx++;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    //return lst.ToArray();
        //    return lst;
        //}

        //private IList<Field> PROFILE_CONTACT_INFO(IEnumerable<GridModuleSetting> lstModuleSetting,
        //    Dictionary<string, object> ObjectList,
        //    Dictionary<string, System.Reflection.PropertyInfo> ObjectProperties, IList<CmsPhone> lstMobiles)
        //{
        //    IList<Field> lst = new List<Field>();

        //    //int i = 0;
        //    string value;
        //    string valueLoc;
        //    foreach (GridModuleSetting column in lstModuleSetting)
        //    {
        //        //if (ObjectProperties.ContainsKey(column.ObjectProperty))
        //        {
        //            foreach (var objectName in ObjectList.Keys)
        //            {
        //                var obj = ObjectList[objectName];
        //                if (column.ObjectProperty.StartsWith(objectName + "_") && obj != null)
        //                {
        //                    if (column.ObjectProperty != "CmsCustomer_HandPhones" &&
        //                        column.ObjectProperty != "CmsCustomerProfile_Emails")
        //                    {
        //                        this.GetDisplayValue(_pageResourceService, _settingService, GetDefaultCultureName(),
        //                             ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat, false, false, "", out value, out valueLoc, true);

        //                        var field = new Field()
        //                        {
        //                            id = column.ResKey,
        //                            module = column.ModuleName,
        //                            label = column.Name,
        //                            labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null,
        //                            order = column.Order,
        //                            value = value,
        //                            valueLoc = valueLoc,
        //                        };
        //                        lst.Add(field);
        //                    }
        //                    else
        //                    {
        //                        if (objectName == "CmsCustomer")
        //                        {
        //                            CmsCustomer cmsCustomer = obj as CmsCustomer;
        //                            if (cmsCustomer.HandPhones != null)
        //                            {
        //                                //IList<ZLIS.Cos.RmwService.DomainObjects.CmsPhone> lstMobiles = ViewBag.MobileList;
        //                                int idx = 0;
        //                                foreach (var ce in lstMobiles)
        //                                {
        //                                    //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                                    //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
        //                                    //    <span id='contract_right'>@Html.Raw(string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : ""))</span>
        //                                    //</li>

        //                                    var field = new Field();

        //                                    field.id = column.ResKey;
        //                                    field.module = column.ModuleName;
        //                                    field.label = string.Format("{0} {1}", column.Name, idx + 1);

        //                                    decimal? labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null;
        //                                    if (labelLocId != null)
        //                                    {
        //                                        labelLocId += (decimal)((idx + 1) / 10.0);
        //                                    }

        //                                    field.labelLocId = labelLocId;
        //                                    field.order = column.Order;
        //                                    field.value = string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : "");

        //                                    lst.Add(field);

        //                                    idx++;
        //                                }
        //                            }
        //                        }
        //                        else if (objectName == "CmsCustomerProfile")
        //                        {
        //                            CmsCustomerProfile cmsCustomerProfile = obj as CmsCustomerProfile;
        //                            if (cmsCustomerProfile.Emails != null)
        //                            {
        //                                int idx = 0;
        //                                foreach (var ce in cmsCustomerProfile.Emails)
        //                                {
        //                                    //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                                    //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
        //                                    //    <span id='contract_right'>@ce.EmailAddress</span> </li>

        //                                    var field = new Field();
        //                                    field.id = column.ResKey;
        //                                    field.module = column.ModuleName;
        //                                    field.label = string.Format("{0} {1}", column.Name, idx + 1);

        //                                    decimal? labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null;
        //                                    if (labelLocId != null)
        //                                    {
        //                                        labelLocId += (decimal)((idx + 1) / 10.0);
        //                                    }

        //                                    field.labelLocId = labelLocId;

        //                                    field.order = column.Order;
        //                                    field.value = ce.EmailAddress;

        //                                    lst.Add(field);

        //                                    idx++;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    //return lst.ToArray();
        //    return lst;
        //}

        //private IList<Field> PROFILE_ADDRESS(IEnumerable<GridModuleSetting> lstModuleSetting,
        //    Dictionary<string, object> ObjectList,
        //    Dictionary<string, System.Reflection.PropertyInfo> ObjectProperties, CmsAddress address, IList<CmsPhone> lstMobiles)
        //{
        //    IList<Field> lst = new List<Field>();

        //    string value;
        //    string valueLoc;

        //    //int i = 0;
        //    foreach (ZLIS.Cos.Core.UIManagement.DomainModel.GridModuleSetting column in lstModuleSetting)
        //    {
        //        //if (ObjectProperties.ContainsKey(column.ObjectProperty))
        //        {
        //            foreach (var objectName in ObjectList.Keys)
        //            {
        //                var obj = ObjectList[objectName];
        //                if (column.ObjectProperty.StartsWith(objectName + "_") && obj != null)
        //                {
        //                    if (column.ObjectProperty.StartsWith("CmsAddress_"))
        //                    {
        //                        //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                        //    id="contract_left">@(column.GetDisplayName(cultureInfo))</span> <span id="contract_right">@(ZLIS.Cos.Web.FormatHelper
        //                        //                            .FormatData(column.ObjectFormat, 
        //                        //                            ObjectProperties[column.ObjectProperty].GetValue(address, null)
        //                        //                            ))</span>
        //                        //</li>
        //                        value = FormatHelper.FormatData(_settingService, column.ObjectFormat,
        //                                                    ObjectProperties[column.ObjectProperty].GetValue(address, null));

        //                        valueLoc = "";
        //                        string objectPropertyLocal = string.Format("{0}_Local", column.ObjectProperty);
        //                        if (ObjectProperties.ContainsKey(objectPropertyLocal))
        //                        {
        //                            object v = ObjectProperties[objectPropertyLocal].GetValue(address, null);
        //                            if (v != null)
        //                            {
        //                                valueLoc = FormatHelper.FormatData(_settingService, column.ObjectFormat, ObjectProperties[objectPropertyLocal].GetValue(address, null));
        //                            }
        //                        }

        //                        var field = new Field()
        //                        {
        //                            id = column.ResKey,
        //                            module = column.ModuleName,
        //                            label = column.Name,
        //                            labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null,
        //                            order = column.Order,
        //                            value = value,
        //                            valueLoc = valueLoc,
        //                        };
        //                        lst.Add(field);
        //                    }
        //                    else
        //                    {
        //                        if (column.ObjectProperty != "CmsCustomer_HandPhones" && column.ObjectProperty != "CmsCustomerProfile_Emails")
        //                        {
        //                            //string value = Html.GetDisplayValue(ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat, false, false, "");
        //                            //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                            //    id="contract_left">@(column.GetDisplayName(cultureInfo))</span> <span id="contract_right">@Html.Raw(value)</span>
        //                            //</li>
        //                            this.GetDisplayValue(_pageResourceService, _settingService, GetDefaultCultureName(),
        //                             ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat, false, false, "", out value, out valueLoc, true);

        //                            var field = new Field()
        //                            {
        //                                id = column.ResKey,
        //                                module = column.ModuleName,
        //                                label = column.Name,
        //                                labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null,
        //                                order = column.Order,
        //                                value = value,
        //                                valueLoc = valueLoc,
        //                            };
        //                            lst.Add(field);
        //                        }
        //                        else
        //                        {
        //                            if (objectName == "CmsCustomer")
        //                            {
        //                                CmsCustomer cmsCustomer = obj as CmsCustomer;
        //                                if (cmsCustomer.HandPhones != null)
        //                                {
        //                                    //IList<ZLIS.Cos.RmwService.DomainObjects.CmsPhone> lstMobiles = ViewBag.MobileList;
        //                                    int idx = 0;
        //                                    foreach (var ce in lstMobiles)
        //                                    {
        //                                        //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                                        //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
        //                                        //    <span id='contract_right'>@Html.Raw(string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : ""))</span>
        //                                        //</li>
        //                                        var field = new Field();

        //                                        field.id = column.ResKey;
        //                                        field.module = column.ModuleName;
        //                                        field.label = string.Format("{0} {1}", column.Name, idx + 1);

        //                                        decimal? labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null;
        //                                        if (labelLocId != null)
        //                                        {
        //                                            labelLocId += (decimal)((idx + 1) / 10.0);
        //                                        }

        //                                        field.labelLocId = labelLocId;
        //                                        field.order = column.Order;
        //                                        field.value = string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : "");

        //                                        lst.Add(field);

        //                                        idx++;
        //                                    }
        //                                }
        //                            }
        //                            else if (objectName == "CmsCustomerProfile")
        //                            {
        //                                CmsCustomerProfile cmsCustomerProfile = obj as CmsCustomerProfile;
        //                                if (cmsCustomerProfile.Emails != null)
        //                                {
        //                                    int idx = 0;
        //                                    foreach (var ce in cmsCustomerProfile.Emails)
        //                                    {
        //                                        //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
        //                                        //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
        //                                        //    <span id='contract_right'>@ce.EmailAddress</span> </li>
        //                                        var field = new Field();
        //                                        field.id = column.ResKey;
        //                                        field.module = column.ModuleName;
        //                                        field.label = string.Format("{0} {1}", column.Name, idx + 1);

        //                                        decimal? labelLocId = column.NameLoc != null ? column.NameLoc.Id : (int?)null;
        //                                        if (labelLocId != null)
        //                                        {
        //                                            labelLocId += (decimal)((idx + 1) / 10.0);
        //                                        }

        //                                        field.labelLocId = labelLocId;

        //                                        field.order = column.Order;
        //                                        field.value = ce.EmailAddress;

        //                                        lst.Add(field);

        //                                        idx++;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return lst;
        //}

        private IList<LoginField> GetFields(IEnumerable<GridModuleSetting> lstModuleSetting,
            Dictionary<string, object> ObjectList,
            Dictionary<string, System.Reflection.PropertyInfo> ObjectProperties, CmsAddress address, IList<CmsPhone> lstMobiles, bool isindividual=true)
        {
            IList<LoginField> lst = new List<LoginField>();

            string value;
            string valueLoc;

            //int i = 0;
            foreach (GridModuleSetting column in lstModuleSetting)
            {
                //if (ObjectProperties.ContainsKey(column.ObjectProperty))
                {
                    foreach (var objectName in ObjectList.Keys)
                    {
                        var obj = ObjectList[objectName];
                        if (column.ObjectProperty.StartsWith(objectName + "_") && obj != null)
                        {
                            if (column.ObjectProperty.StartsWith("CmsAddress_"))
                            {
                                //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
                                //    id="contract_left">@(column.GetDisplayName(cultureInfo))</span> <span id="contract_right">@(ZLIS.Cos.Web.FormatHelper
                                //                            .FormatData(column.ObjectFormat, 
                                //                            ObjectProperties[column.ObjectProperty].GetValue(address, null)
                                //                            ))</span>
                                //</li>
                                value = FormatHelper.FormatData(_settingService, column.ObjectFormat,
                                                            ObjectProperties[column.ObjectProperty].GetValue(address, null));

                                valueLoc = null;
                                if (_enableLocalizedText)
                                {
                                    string objectPropertyLocal = string.Format("{0}_Local", column.ObjectProperty);
                                    if (ObjectProperties.ContainsKey(objectPropertyLocal))
                                    {
                                        object v = ObjectProperties[objectPropertyLocal].GetValue(address, null);
                                        if (v != null)
                                        {
                                            //valueLoc = FormatHelper.FormatData(_settingService, column.ObjectFormat, ObjectProperties[objectPropertyLocal].GetValue(address, null));
                                            valueLoc = FormatHelper.FormatData(_settingService, column.ObjectFormat, v);
                                           
                                        }
                                    }
                                    if (string.IsNullOrEmpty(valueLoc))
                                        valueLoc = value;
                                    else if (string.IsNullOrEmpty(value))
                                        value = valueLoc;
                                }
                               
                                var field = new LoginField()
                                {
                                    id = column.ResKey,
                                    module = column.ModuleName,
                                    label = column.Name,
                                    labelLocId = _enableLocalizedText ? (column.NameLoc != null ? column.NameLoc.Id.ToString() : null) : null,
                                    //(column.NameLoc != null ? column.NameLoc.Id : (int?)null) : (int?)null,
                                    order = column.Order,
                                    value = value,
                                    valueLoc = valueLoc,
                                };
                                lst.Add(field);
                            }
                            else
                            {
                                if (column.ObjectProperty != "CmsCustomer_HandPhones" && column.ObjectProperty != "CmsCustomerProfile_Emails")
                                {
                                    //string value = Html.GetDisplayValue(ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat, false, false, "");
                                    //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
                                    //    id="contract_left">@(column.GetDisplayName(cultureInfo))</span> <span id="contract_right">@Html.Raw(value)</span>
                                    //</li>
                                    this.GetDisplayValue(_pageResourceService, _settingService, _languageService, _specialLogic,
                                     ObjectList[objectName], ObjectProperties, column.ObjectProperty, column.ObjectFormat, false, false, "", out value, out valueLoc, isindividual);

                                    var field = new LoginField()
                                    {
                                        id = column.ResKey,
                                        module = column.ModuleName,
                                        label = column.Name,
                                        labelLocId = _enableLocalizedText ? (column.NameLoc != null ? column.NameLoc.Id.ToString() : null) : null,
                                        //(column.NameLoc != null ? column.NameLoc.Id : (int?)null) : (int?)null,
                                        order = column.Order,
                                        value = value,
                                        valueLoc = valueLoc,
                                    };
                                    lst.Add(field);
                                }
                                else
                                {
                                    if (objectName == "CmsCustomer")
                                    {
                                        CmsCustomer cmsCustomer = obj as CmsCustomer;
                                        if (cmsCustomer.HandPhones != null)
                                        {
                                            //IList<ZLIS.Cos.RmwService.DomainObjects.CmsPhone> lstMobiles = ViewBag.MobileList;
                                            int idx = 0;
                                            foreach (var ce in lstMobiles)
                                            {
                                                //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
                                                //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
                                                //    <span id='contract_right'>@Html.Raw(string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : ""))</span>
                                                //</li>
                                                var field = new LoginField();

                                                field.id = column.ResKey;
                                                field.module = column.ModuleName;
                                                field.label = string.Format("{0} {1}", column.Name, idx + 1);

                                                decimal? labelLocId = _enableLocalizedText ?
                                                    (column.NameLoc != null ? column.NameLoc.Id : (int?)null) : (int?)null;
                                                if (labelLocId != null)
                                                {
                                                    labelLocId += (decimal)((idx + 1) / 10.0);
                                                }

                                                field.labelLocId = labelLocId.ToString();
                                                field.order = column.Order;
                                                //field.value = string.Format("{0}{1}", ce.AreaCode + ce.PhoneNO, ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value ? "<span id='sms'>SMS</span>" : "");
                                                field.value = ce.AreaCode + ce.PhoneNO;
                                                field.SMSRegistered = ce.IsSmsRegistered.HasValue && ce.IsSmsRegistered.Value;
                                                if (_enableLocalizedText)
                                                    field.valueLoc = field.value;
                                                lst.Add(field);

                                                idx++;
                                            }
                                        }
                                    }
                                    else if (objectName == "CmsCustomerProfile")
                                    {
                                        CmsCustomerProfile cmsCustomerProfile = obj as CmsCustomerProfile;
                                        if (cmsCustomerProfile.Emails != null)
                                        {
                                            int idx = 0;
                                            foreach (var ce in cmsCustomerProfile.Emails)
                                            {
                                                //<li style="@(i++ % 2 == 0 ? "background: #39393b;" : "background: #444447;")"><span
                                                //    id='contract_left'>@string.Format("{0} {1}", (column.GetDisplayName(cultureInfo)), idx++)</span>
                                                //    <span id='contract_right'>@ce.EmailAddress</span> </li>
                                                var field = new LoginField();
                                                field.id = column.ResKey;
                                                field.module = column.ModuleName;
                                                field.label = string.Format("{0} {1}", column.Name, idx + 1);

                                                decimal? labelLocId = _enableLocalizedText ?
                                                    (column.NameLoc != null ? column.NameLoc.Id : (int?)null) : (int?)null;
                                                if (labelLocId != null)
                                                {
                                                    labelLocId += (decimal)((idx + 1) / 10.0);
                                                }

                                                field.labelLocId = labelLocId.ToString();

                                                field.order = column.Order;
                                                field.value = ce.EmailAddress;
                                                if (_enableLocalizedText)
                                                    field.valueLoc = field.value;

                                                lst.Add(field);

                                                idx++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return lst;
        }

        private Contract[] GetContracts(ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            var summaries = rmwServiceAdaptor.GetAccountSummary(customer.Bpid);

            IList<Contract> lst = new List<Contract>();
            var contractmenu = _menuSettingService.GetMenuSetting("MyAccount");

            Dictionary<string, bool> summarysettings = new Dictionary<string, bool>();
            foreach (CmsAccountSummary summary in summaries)
            {
                summarysettings = summarysettings.Union(_mfsService.GetModuleFilterSettings_Nocache(ModuleSettingConstants.CONTRACT_SUMMARY_TABLE, ModuleSettingConstants.FILTER_TYPE_CONTRACT_STATUS, summary.ContractStatusCode)
                    .ToDictionary(x => string.Format("{0}_{1}", summary.ContractNO, x.ResKey), x => x.Visible)).ToDictionary(y => y.Key, y => y.Value);
            }

            foreach (var s in summaries)
            {
                bool showmsgflag = false;
                //CmsContractDetail contract = rmwHelper.GetContractDetails(s.BpId, s.ContractNO);
                var settings = _mfsService.GetModuleFilterSettings_Nocache(ModuleSettingConstants.CONTRACT_DETAIL, ModuleSettingConstants.FILTER_TYPE_CONTRACT_STATUS, s.ContractStatus)
                    .ToDictionary(x => string.Format("{0}_{1}", s.ContractNO, x.ResKey), x => x.Visible);
                if (settings != null && settings.ContainsKey(s.ContractNO + "_CNRT_DETAIL_HIDE") && !settings[s.ContractNO + "_CNRT_DETAIL_HIDE"])
                    showmsgflag = true;
                string ContractStatus = string.Empty;
                if (summarysettings.ContainsKey(s.ContractId + "_CNRT_SUM_TABLE.ContractStatus") && summarysettings[s.ContractId + "_CNRT_SUM_TABLE.ContractStatus"]==false)
                    ContractStatus = "";
                else if (s.ContractStatusDisplayCode == ZLIS.Cos.RmwService.CmsContractStatus.Overdue)
                    ContractStatus = "Overdue";
                else if (s.ContractStatusDisplayCode == null)
                    ContractStatus = "";
                else
                    ContractStatus = s.ContractStatusDisplay;

                lst.Add(new Contract()
                {
                    contractNo = s.ContractNO,
                    nextDueDate = s.DateNextPaymentDue != null ? s.DateNextPaymentDue.Value.ToString(this.GetDateFormat(_settingService)) : "",
                    nextDueAmount = s.AmountNextPayment,
                    terms = s.TotalTerm,
                    termPaid = s.PaidTerm,
                    contractStatus = ContractStatus,
                    vehcileRegNo = s.VehicleRegNO,
                    maturityDate =(s.ContractEndDate != null ? s.ContractEndDate.Value.ToString(this.GetDateFormat(_settingService)) : ""),
                    menus = GetContractMenus(contractmenu.Id, s.ContractStatus, "MyAccount", "ContractDetails",s),
                    startDate =  (s.ContractStartDate != null ? s.ContractStartDate.Value.ToString(this.GetDateFormat(_settingService)) : ""),
                    showMsgFlag=showmsgflag,
                    overdueAmount=s.AmountOverdue,
                });
            }

            return lst.ToArray();
        }

        private string[] GetContractMenus(Guid menuid, string contractStatus, string controller, string action,CmsAccountSummary summary)
        {
            var menusetting = _mfsService.GetModuleFilterSettings_Nocache(ModuleSettingConstants.ET_ESTIMATE, ModuleSettingConstants.FILTER_TYPE_FINANCIAL_PRODUCT, summary.FinancialProduct == null ? "" : summary.FinancialProduct.Code.ToString())
                    .ToDictionary(x => string.Format("{0}_{1}", summary.ContractNO, x.ResKey), x => x.Visible);
            IEnumerable<MenuSetting> lstItems = _menuSettingService.GetSubMenuSettings(menuid);
            Dictionary<string, bool> settings = _mfsService.GetModuleFilterSettings_Nocache(ModuleSettingConstants.MY_ACCOUNT, ModuleSettingConstants.FILTER_TYPE_CONTRACT_STATUS, contractStatus)
                   .ToDictionary(x => x.ResKey, x => x.Visible);
            //Applying Module Filter Setting
            lstItems = lstItems.Where(x => (string.IsNullOrEmpty(x.ControllerAction)
                || !settings.ContainsKey(x.ControllerAction.Replace(";#", "_"))
                || settings[x.ControllerAction.Replace(";#", "_")]) && string.IsNullOrEmpty(x.PageTemplate));
            IList<string> results = new List<string>();
            if (lstItems != null)
            {
                foreach (var item in lstItems)
                {
                    //if (IsActive(item, controller, action))
                    if (item.ControllerAction == "MyAccount;#ETEstimate")
                    {
                        if (menusetting != null && menusetting.ContainsKey(summary.ContractNO + "_ET_ESTIMATE_NOT_ALLOWED") && menusetting[summary.ContractNO + "_ET_ESTIMATE_NOT_ALLOWED"])
                            continue;
                        if (summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.Payout
                                                    || summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.EarlyPayout
                                                    || summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.Repossessed)
                            continue;
                        results.Add(item.Id.ToString());

                    }
                    else
                        results.Add(item.Id.ToString());
                }
            }
            return results.ToArray();
        }

        private bool IsActive(MenuSetting ms, string controller, string action)
        {
            bool active = false;

            if (!string.IsNullOrEmpty(ms.ControllerAction))
            {
                string[] arr = ms.ControllerAction.Split(new char[] { ';', '#' }, StringSplitOptions.RemoveEmptyEntries);
                if (arr.Length >= 2)
                {
                    if (arr.Length == 4)
                    {
                        active = arr[1].Equals(controller, StringComparison.OrdinalIgnoreCase)
                            && (arr[2].Equals(action, StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        active = arr[0].Equals(controller, StringComparison.OrdinalIgnoreCase)
                            && (arr[1].Equals(action, StringComparison.OrdinalIgnoreCase)
                            || "Edit".Equals(action, StringComparison.OrdinalIgnoreCase));
                    }
                }
                else if (!string.IsNullOrEmpty(ms.Url) && "MyAccount/ContractDetails/".Contains(ms.Url))
                    active = true;
            }

            return active;
        }

        private LanguageContent[] GetLanguageContent(string cc, ZLIS.Cos.Core.Account.DomainModel.Customer customer,CmsCustomerProfile customerProfile,CmsCustomer cmsCustomer)
        {
            IList<LanguageContent> lstLC = new List<LanguageContent>();

            LanguageContent lc = new LanguageContent()
            {
                languageCode = this.GetLanguageCode(_settingService, _languageService),
            };
            lstLC.Add(lc);

            List<LangContent> lst = new List<LangContent>();

            var lstModuleSetting = _msService.GetModuleSettings_NoCache(ModuleSettingConstants.PROFILE)
                .Where(x => x.Visible).OrderBy(x => x.Order).ToList();
            int? addressCnt = GetAddressCnt(cc, customer, customerProfile, lstModuleSetting);
            foreach (var moduleSetting in lstModuleSetting)
            {
                string strTitle = string.Empty;

                decimal? labelLocid = null;
                switch (moduleSetting.ResKey)
                {
                    case ModuleSettingConstants.PROFILE_ADDRESS:
                        if (addressCnt.HasValue && addressCnt.Value > 1)
                        {
                            for (int i = 0; i < addressCnt; i++)
                            {
                                labelLocid = moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (decimal?)null;
                                if (labelLocid != null)
                                {
                                    labelLocid += (decimal)((i + 1) / 10.0);
                                    strTitle = string.Format("{0} {1}", moduleSetting.NameLoc.Text, i + 1);

                                    LangContent langContent = new LangContent()
                                    {
                                        id = labelLocid.Value.ToString(),
                                        value = strTitle,
                                    };
                                    lst.Add(langContent);
                                }

                                if (i == 0)
                                {
                                    GetLangContentForFields(customer, lst, moduleSetting.ResKey, customerProfile, cmsCustomer);
                                }
                            }
                        }
                        else
                        {
                            labelLocid = moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (decimal?)null;
                            if (labelLocid != null)
                            {
                                strTitle = moduleSetting.NameLoc.Text;

                                LangContent langContent = new LangContent()
                                {
                                    id = labelLocid.Value.ToString(),
                                    value = strTitle,
                                };
                                lst.Add(langContent);
                            }

                            GetLangContentForFields(customer, lst, moduleSetting.ResKey, customerProfile, cmsCustomer);
                        }
                        break;

                    default:
                        labelLocid = moduleSetting.NameLoc != null ? moduleSetting.NameLoc.Id : (decimal?)null;
                        if (labelLocid != null)
                        {
                            strTitle = moduleSetting.NameLoc.Text;

                            LangContent langContent = new LangContent()
                            {
                                id = labelLocid.Value.ToString(),
                                value = strTitle,
                            };
                            lst.Add(langContent);
                        }

                        GetLangContentForFields(customer, lst, moduleSetting.ResKey, customerProfile, cmsCustomer);

                        break;
                }
            }

            lc.contents = lst.ToArray();

            return lstLC.ToArray();
        }

        private void GetLangContentForFields(ZLIS.Cos.Core.Account.DomainModel.Customer customer,
            IList<LangContent> lst,
            string module, CmsCustomerProfile cmsCustomerProfile,CmsCustomer cmsCustomer)
        {
            IEnumerable<GridModuleSetting> details =
                _msService.GetGridModuleSettings_NoCache(module).Where(x => x.Visible).OrderBy(x => x.Order).ToList();

            if (details.Count() > 0)
            {
                foreach (var d in details)
                {
                    decimal? labelLocId = d.NameLoc != null ? d.NameLoc.Id : (int?)null;
                    string labelLoc = "";
                    if (labelLocId != null)
                    {
                        if (d.ObjectProperty != "CmsCustomer_HandPhones" &&
                                d.ObjectProperty != "CmsCustomerProfile_Emails")
                        {
                            lst.Add(new LangContent()
                            {
                                id = labelLocId.Value.ToString(),
                                value = d.NameLoc.Text,
                            });
                        }
                        else if (d.ObjectProperty != "CmsCustomerProfile_Emails")
                        {

                            SystemSettingItem mobileTypeSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_OTPSETTING_MOBILETYPE);
                            string[] mobileTypes = new string[] { CmsPhoneType.Mobile };
                            if (mobileTypeSetting != null)
                            {
                                if (!string.IsNullOrEmpty(mobileTypeSetting.Value))
                                {
                                    mobileTypes = mobileTypeSetting.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                        .Select(x => x.Trim()).ToArray();
                                }
                            }

                            IList<CmsPhone> lstMobiles = cmsCustomer.HandPhones.Where(m => mobileTypes.Contains(m.PhoneTypeCode, StringComparer.OrdinalIgnoreCase)).OrderBy(m => m.PhoneSequence).Select(m => m).Distinct().ToList();
                            if (cmsCustomer.HandPhones != null)
                            {
                                int idx = 0;
                                foreach (var ce in lstMobiles)
                                {
                                    //labelLocId += (decimal)((idx + 1) / 10.0);
                                    labelLocId += (decimal)0.1;
                                    labelLoc = string.Format("{0} {1}", d.NameLoc.Text, idx + 1);
                                    lst.Add(new LangContent()
                                    {
                                        id = labelLocId.Value.ToString(),
                                        value = labelLoc,
                                    });

                                    idx++;
                                }
                            }
                        }
                        else
                        {
                            
                            if (cmsCustomerProfile.Emails != null)
                            {
                                int idx = 0;
                                foreach (var ce in cmsCustomerProfile.Emails)
                                {
                                    //labelLocId += (decimal)((idx + 1) / 10.0);
                                    labelLocId += (decimal)0.1;
                                    labelLoc = string.Format("{0} {1}", d.NameLoc.Text, idx + 1);
                                    lst.Add(new LangContent()
                                    {
                                        id = labelLocId.Value.ToString(),
                                        value = labelLoc,
                                    });

                                    idx++;
                                }
                            }
                        }
                    }
                }
            }
        }

        private CmsCustomerProfile GetProfileWithNoProxy(CmsCustomerProfile profile)
        {
            CmsCustomerProfile result = new CmsCustomerProfile();
            var bpartner = new CmsBusinessPartner();
            var sourcepartner = profile.BusinessPartner;
            bpartner.BpId = sourcepartner.BpId;
            bpartner.Bumi = sourcepartner.Bumi;
            bpartner.Bumi_Local = sourcepartner.Bumi_Local;
            bpartner.Classification = sourcepartner.Classification;
            bpartner.Classification_Local = sourcepartner.Classification_Local;
            bpartner.CompanyRegDate = sourcepartner.CompanyRegDate;
            bpartner.CompanyRegNO = sourcepartner.CompanyRegNO;
            bpartner.CompanyRegNO_Local = sourcepartner.CompanyRegNO_Local;
            bpartner.CustomerType = sourcepartner.CustomerType;
            bpartner.Dob = sourcepartner.Dob;
            bpartner.DrivingLicenseNo = sourcepartner.DrivingLicenseNo;
            bpartner.DrivingLicenseNo_Local = sourcepartner.DrivingLicenseNo_Local;
            bpartner.FirstName = sourcepartner.FirstName;
            bpartner.FirstName_Local = sourcepartner.FirstName_Local;
            bpartner.Gender = sourcepartner.Gender;
            bpartner.Gender_Local = sourcepartner.Gender_Local;
            bpartner.GstRegistered = sourcepartner.GstRegistered;
            bpartner.IndustrialType = sourcepartner.IndustrialType;
            bpartner.IndustrialType_Local = sourcepartner.IndustrialType_Local;
            bpartner.IndustrySubType = sourcepartner.IndustrySubType;
            bpartner.IndustrySubType_Local = sourcepartner.IndustrySubType_Local;
            bpartner.LastName = sourcepartner.LastName;
            bpartner.LastName_Local = sourcepartner.LastName_Local;
            bpartner.LicenseExpiryDate = sourcepartner.LicenseExpiryDate;
            bpartner.MaritalStatus = sourcepartner.MaritalStatus;
            bpartner.MaritalStatus_Local = sourcepartner.MaritalStatus_Local;
            bpartner.MiddleName = sourcepartner.MiddleName;
            bpartner.MiddleName_Local = sourcepartner.MiddleName_Local;
            bpartner.Nationality = sourcepartner.Nationality;
            bpartner.Nationality_Local = sourcepartner.Nationality_Local;
            bpartner.NonBumi = sourcepartner.NonBumi;
            bpartner.NonBumi_Local = sourcepartner.NonBumi_Local;
            bpartner.Occupation = sourcepartner.Occupation;
            bpartner.Occupation_Local = sourcepartner.Occupation_Local;
            bpartner.OldNricNO = sourcepartner.OldNricNO;
            bpartner.OldNricNO_Local = sourcepartner.OldNricNO_Local;
            bpartner.PassportExpiryDate = sourcepartner.PassportExpiryDate;
            bpartner.PassportIssueDate = sourcepartner.PassportIssueDate;
            bpartner.PassportNO = sourcepartner.PassportNO;
            bpartner.PassportNO_Local = sourcepartner.PassportNO_Local;
            bpartner.Race = sourcepartner.Race;
            bpartner.Race_Local = sourcepartner.Race_Local;
            bpartner.ResidentialStatus = sourcepartner.ResidentialStatus;
            bpartner.ResidentialStatus_Local = sourcepartner.ResidentialStatus_Local;
            bpartner.SuedFlag = sourcepartner.SuedFlag;
            bpartner.SuedFlag_Local = sourcepartner.SuedFlag_Local;
            bpartner.TaxNo = sourcepartner.TaxNo;
            bpartner.TaxNo_Local = sourcepartner.TaxNo_Local;
            bpartner.Title = sourcepartner.Title;
            bpartner.Title_Local = sourcepartner.Title_Local;
            bpartner.VipStatus = sourcepartner.VipStatus;
            bpartner.VipStatus_Local = sourcepartner.VipStatus_Local;
            result.BusinessPartner = bpartner;
            return result;
        }

        private EnquiryInfo GetEnquiry(ZLIS.Cos.Core.Account.DomainModel.Customer customer,CmsCustomerProfile cmsCustomer)
        {
            EnquiryInfo result = new EnquiryInfo();
            //emails
            IList<string> lstEmails = null;

            if (cmsCustomer.Emails != null && cmsCustomer.Emails.Any())
                cmsCustomer.Emails.Select(m => m.EmailAddress).Distinct().ToList();
            if (lstEmails == null)
            {
                lstEmails = new List<string>();
            }
            if (lstEmails.Contains(customer.Email))
            {
                lstEmails.Remove(customer.Email);
                lstEmails.Insert(0, customer.Email);
            }
            else
            {
                lstEmails.Insert(0, customer.Email);
            }
            result.email = lstEmails.ToArray();
            //mobiles
            SystemSettingItem mobileTypeSetting = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_OTPSETTING, SystemConfigurationConstants.KEY_OTPSETTING_MOBILETYPE);
            string[] mobileTypes = new string[] { CmsPhoneType.Mobile };
            if (mobileTypeSetting != null)
            {
                if (!string.IsNullOrEmpty(mobileTypeSetting.Value))
                {
                    mobileTypes = mobileTypeSetting.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                        .Select(x => x.Trim()).ToArray();
                }
            }
            IList<string> lstMobiles = cmsCustomer.PhoneNOs.Where(m => mobileTypes.Contains(m.PhoneTypeCode, StringComparer.OrdinalIgnoreCase)).OrderBy(m => m.PhoneSequence).Select(m => m.PhoneNO).Distinct().ToList();
            result.mobile = lstMobiles.ToArray();
            //contract no
            var summaries = _rmwService.GetAccountsSummary(customer.Bpid, null);
            IList<string> lstContracts = new List<string>();

            foreach (var summary in summaries)
            {
                lstContracts.Add(string.Format("{0} / {1}", summary.ContractNO, summary.VehicleRegNO));
            }
            result.contract = lstContracts.ToArray();
            return result;
        }

        private BannerInfo[] GetBanners()
        {
            IList<BannerInfo> lst = new List<BannerInfo>();
            var mb = _bannerService.GetMobileBanner().FirstOrDefault();
            if (mb == null)
            {
                BannerInfo banner = new BannerInfo();
                banner.name = "ET";
                banner.id = "7F01069A-7CCF-424D-B1EE-0BE1DD14111D";
                SystemSettingItem objTimezone = _settingService.GetSystemSetting(
                    SystemConfigurationConstants.CATEGORY_COUNTRY,
                    SystemConfigurationConstants.KEY_TIMEZONE);

                double addHours = double.Parse(objTimezone == null ? "0" : objTimezone.Value);
                string actualPath = _appDataFolder.Combine(_appDataFolder.MapPath("Banner"), banner.id);
                if (System.IO.File.Exists(actualPath))
                {
                    banner.modifiedDate = System.IO.File.GetLastWriteTimeUtc(actualPath).AddHours(addHours).ToString(this.GetDateFormat(_settingService));
                }
                lst.Add(banner);
            }
            else
            {
                BannerInfo banner = new BannerInfo();
                banner.name = mb.Name;
                banner.id = mb.ContentSyncId.ToString();
                banner.modifiedDate = (mb.ModifiedDate ?? mb.CreatedDate).ToString(this.GetDateFormat(_settingService));
                lst.Add(banner);
            }
            return lst.ToArray();
        }

    }
}
