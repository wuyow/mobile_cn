﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core;
using ZLIS.Cos.Core.Const;
using ZLIS.Cos.Core.ContentManagement;
using ZLIS.Cos.Core.ContentManagement.DomainModel;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.ModuleFilterSetting.Services;
using ZLIS.Cos.Core.Modules.Enquiry;
using ZLIS.Cos.Core.Modules.Enquiry.DomainModel;
using ZLIS.Cos.Core.Modules.Enquiry.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.Core.UIManagement;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.Filters;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class ETEstimateAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        private IModuleFilterSettingService _mfsService;
        private IEnquiryService _enquiryService;
        private ICustomerTicketService _ticketService;
        private IEmailTemplateService _templateService;
        private IAppDataFolder _appDataFolder;
        private IMailer _mailer;
        private IPageResourceService _pageResourceService;

        public ILogger Logger { get; set; }

        public ETEstimateAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                       ILanguageService languageService,
                                       ISystemSettingService settingService,
                                       IMobileLoginInfoService mobileLoginInfoService,
                                       IRmwService rmwService,
                                       IModuleFilterSettingService mfsService,
                                       IEnquiryService enquiryService,
                                       ICustomerTicketService ticketService,
                                       IEmailTemplateService templateService,
                                       IAppDataFolder appDataFolder,
                                       IMailer mailer,
                                       IPageResourceService pageResourceService,
                                       ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _mobileLoginInfoService = mobileLoginInfoService;

            _rmwService = rmwService;
            ((RmwServiceAdaptor)_rmwService).SetCacheManager(cacheManager);

            _mfsService = mfsService;
            _enquiryService = enquiryService;
            _ticketService = ticketService;
            _templateService = templateService;
            _appDataFolder = appDataFolder;
            _mailer = mailer;

            _pageResourceService = pageResourceService;
        }

        [System.Web.Http.HttpPost]
        public GetETEstimateResponse ETEstimate(GetETEstimateRequest request)
        {
            if (request == null)
                request = new GetETEstimateRequest();
            Logger.Debug(string.Format("-------- ETEstimate API started. token:{0} contractNo:{1} date:{2}", request.token ?? string.Empty, request.contractNo ?? string.Empty,request.date??string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetETEstimateResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetETEstimateErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo) ||
                    string.IsNullOrEmpty(request.date))
                {
                    errorInfo.code = (int)GetETEstimateErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);


                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetETEstimateErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }

                IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
                string dateFormat = this.GetDateFormat(_settingService);

                DateTime sDate;
                if (!DateTime.TryParseExact(request.date, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out sDate))
                {
                    errorInfo.code = (int)GetETEstimateErrorCode.InvalidDate;
                    errorInfo.message = "Invalid date";

                    return response;
                }

              
                CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
                
                var settings = _mfsService.GetModuleFilterSettings_Nocache(ModuleSettingConstants.ET_ESTIMATE, ModuleSettingConstants.FILTER_TYPE_FINANCIAL_PRODUCT, summary.FinancialProduct == null ? "" : summary.FinancialProduct.Code.ToString())
                        .ToDictionary(x => string.Format("{0}_{1}", summary.ContractNO, x.ResKey), x => x.Visible);

                if (settings != null && settings.ContainsKey(summary.ContractNO + "_ET_ESTIMATE_NOT_ALLOWED") && settings[summary.ContractNO + "_ET_ESTIMATE_NOT_ALLOWED"])
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_Message")
                    //</p>
                    errorInfo.code = (int)GetETEstimateErrorCode.EtEstimateNotAllowed;
                    errorInfo.message = this.Resource(_pageResourceService, "MyAccount_MyAccount_ETEstimate,PageContent_Message");//"This function is not available for your account type.";

                    return response;
                }
                else if (summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.Payout
                    || summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.EarlyPayout)
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_PaidInFullMessage")
                    //</p>
                    errorInfo.code = (int)GetETEstimateErrorCode.PaidInFull;
                    errorInfo.message = this.Resource(_pageResourceService, "MyAccount_MyAccount_ETEstimate,PageContent_PaidInFullMessage"); //"This function is not available for your account type.";

                    return response;
                }
                else if (summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.Repossessed)
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_ReprocessedMessage")
                    //</p>
                    errorInfo.code = (int)GetETEstimateErrorCode.Repossessed;
                    errorInfo.message = this.Resource(_pageResourceService, "MyAccount_MyAccount_ETEstimate,PageContent_ReprocessedMessage"); //"This function is not available for your account type.";

                    return response;
                }
                string message = string.Empty;
                response.data = GetETEstimate(request, loginInfo.CustomerActivity.Customer,out message);
                if (!string.IsNullOrEmpty(message))
                {
                    errorInfo.code = (int)GetETEstimateErrorCode.EtEstimateNotAllowed;
                    errorInfo.message = message;
                    return response;
                }

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetETEstimateErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get ET Estimate");
            }

            return response;
        }

        private ETEstimate GetETEstimate(GetETEstimateRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer,out string message)
        {
            ETEstimate etEstimate = null;
            message = string.Empty;
            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();

            IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
            //string dateFormat = _ssService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT).Value;
            string dateFormat = this.GetDateFormat(_settingService);

            DateTime tDate;
            if (DateTime.TryParseExact(request.date, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out tDate))
            {
                //Thread.CurrentThread.CurrentCulture = this.DefaultCulture();

                CmsETEstimateExt cmsEtEstimate = (CmsETEstimateExt)_rmwService
                    .GetETQuotation(summary.ContractNO, tDate/*.ToCosLocalTime()*/);

                //Thread.CurrentThread.CurrentCulture = currentCulture;

                if (cmsEtEstimate != null && !string.IsNullOrEmpty(cmsEtEstimate.Message) && !cmsEtEstimate.EarlyTerminationAmount.HasValue)
                {
                    etEstimate = new ETEstimate()
                    {
                        amount = cmsEtEstimate.EarlyTerminationAmount,
                        etStatus = "Error"
                    };
                    message = cmsEtEstimate.Message;
                    //TempData["ShowETEstimateError"] = "True";
                    //return View(new ETEstimateDisplayModel { ContractId = summary.ContractId, ContractNo = summary.ContractNO, ContractStatusDisplay = summary.ContractStatusCode, Message = cmsEtEstimate.Message, MaturityDate = contract.MaturityDate.HasValue ? contract.MaturityDate.Value : (contract.EndDate.HasValue ? contract.EndDate.Value : TimeHelper.Now) });
                }
                else if (cmsEtEstimate == null)
                {
                    etEstimate = new ETEstimate()
                    {
                        //amount = cmsEtEstimate.EarlyTerminationAmount,
                        etStatus = "NoET"
                    };
                    message = "Service unavailable.";
                    //TempData["ShowETEstimateError"] = "True";
                    //message = "Service unavailable.";
                }
                else
                {
                    etEstimate = new ETEstimate()
                    {
                        amount = cmsEtEstimate.EarlyTerminationAmount,
                        etStatus = "OK"
                    };

                    //byte[] pdf = GenerateEtEstimatePdf(cmsEtEstimate, summary.ContractNO);
                    //string downloadName = string.Format("{0}_{1}_{2:yyyyMMddHHmmss}.pdf", ReportConstants.KEY_ET_ESTIMATE, summary.ContractNO, DateTime.Now);
                    //return File(pdf, "application/pdf", downloadName);

                    //Session["TerminationDate"] = model.TerminationDate;
                    ////if (HttpContext.GetOverriddenBrowser().IsMobileDevice)
                    //if (this.GetDisplayModeId().ToLower() == "mobile")
                    //{
                    //    ViewBag.Amount = cmsEtEstimate.EarlyTerminationAmount;
                    //    return View("ETEstimateDetail", new ETEstimateDisplayModel { ContractId = summary.ContractId, ContractNo = summary.ContractNO, ContractStatusDisplay = summary.ContractStatusCode, Message = message, Step = 2, MaturityDate = contract.MaturityDate.HasValue ? contract.MaturityDate.Value : (contract.EndDate.HasValue ? contract.EndDate.Value : TimeHelper.Now) });
                    //}
                    //else
                    //{
                    //    ViewBag.Amount = cmsEtEstimate.EarlyTerminationAmount;
                    //    ViewBag.TerminationDate = model.TerminationDate;
                    //    return View("ETEstimate", new ETEstimateDisplayModel { ContractId = summary.ContractId, ContractNo = summary.ContractNO, ContractStatusDisplay = summary.ContractStatusCode, Message = message, Step = 2, MaturityDate = contract.MaturityDate.HasValue ? contract.MaturityDate.Value : (contract.EndDate.HasValue ? contract.EndDate.Value : TimeHelper.Now) });
                    //}
                }
            }

            return etEstimate;
        }

        [System.Web.Http.HttpPost]
        public ProcessETResponse ProcessET(ProcessETRequest request)
        {
            if (request == null)
                request = new ProcessETRequest();
            Logger.Debug(string.Format("-------- ProcessET API started. token:{0} contractNo:{1} date:{2}", request.token ?? string.Empty, request.contractNo ?? string.Empty, request.date ?? string.Empty));

            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new ProcessETResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)ProcessETErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo) ||
                    string.IsNullOrEmpty(request.date))
                {
                    errorInfo.code = (int)ProcessETErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }

                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);


                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)ProcessETErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }

                IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
                string dateFormat = this.GetDateFormat(_settingService);

                DateTime sDate;
                if (!DateTime.TryParseExact(request.date, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out sDate))
                {
                    errorInfo.code = (int)ProcessETErrorCode.InvalidDate;
                    errorInfo.message = "Invalid date";

                    return response;
                }

                CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
                
                var settings = _mfsService.GetModuleFilterSettings_Nocache(ModuleSettingConstants.ET_ESTIMATE, ModuleSettingConstants.FILTER_TYPE_FINANCIAL_PRODUCT, summary.FinancialProduct == null ? "" : summary.FinancialProduct.Code.ToString())
                        .ToDictionary(x => string.Format("{0}_{1}", summary.ContractNO, x.ResKey), x => x.Visible);

                
                if (settings != null && settings.ContainsKey(summary.ContractNO + "_ET_ESTIMATE_NOT_ALLOWED") && settings[summary.ContractNO + "_ET_ESTIMATE_NOT_ALLOWED"])
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_Message")
                    //</p>
                    errorInfo.code = (int)ProcessETErrorCode.EtEstimateNotAllowed;
                    errorInfo.message = this.Resource(_pageResourceService, "MyAccount_MyAccount_ETEstimate,PageContent_Message");//"This function is not available for your account type.";

                    return response;
                }
                else if (summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.Payout
                    || summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.EarlyPayout)
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_PaidInFullMessage")
                    //</p>
                    errorInfo.code = (int)ProcessETErrorCode.PaidInFull;
                    errorInfo.message = this.Resource(_pageResourceService, "MyAccount_MyAccount_ETEstimate,PageContent_PaidInFullMessage"); //"This function is not available for your account type.";

                    return response;
                }
                else if (summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.Repossessed)
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_ReprocessedMessage")
                    //</p>
                    errorInfo.code = (int)ProcessETErrorCode.Repossessed;
                    errorInfo.message = this.Resource(_pageResourceService, "MyAccount_MyAccount_ETEstimate,PageContent_ReprocessedMessage"); //"This function is not available for your account type.";

                    return response;
                }

                ProcessET(request, loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());

                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)ProcessETErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when process ET");
            }

            return response;
        }

        private void ProcessET(ProcessETRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
            CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(customer.Bpid)
                .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();

            //CmsContractDetail contract = rmwHelper.GetContractDetails(summary.BpId, summary.ContractNO);

            IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
            //string dateFormat = _ssService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT).Value;
            string dateFormat = this.GetDateFormat(_settingService);
            DateTime tDate;
            
            string terminationDateStr = request.date;

            if (DateTime.TryParseExact(terminationDateStr, dateFormat, formatter,
                                       System.Globalization.DateTimeStyles.None, out tDate))
            {
                //Thread.CurrentThread.CurrentCulture = this.DefaultCulture();

                CmsETEstimateExt cmsEtEstimate = (CmsETEstimateExt) _rmwService
                                                                        .GetETQuotation(summary.ContractNO, tDate
                                                                        /*.ToCosLocalTime()*/);

                if (cmsEtEstimate != null && !string.IsNullOrEmpty(cmsEtEstimate.Message) &&
                    !cmsEtEstimate.EarlyTerminationAmount.HasValue)
                {
                    throw new ApplicationException(cmsEtEstimate.Message);
                }
                else if (cmsEtEstimate == null)
                {
                    throw new ApplicationException("Service unavailable.");
                }
                else
                {
                    string enquiryDetails = string.Format("Termination Date: {1}<br />Termination Est Amount : {2}",
                                                          summary.ContractNO,
                                                          FormatHelper.FormatData(_settingService, "DATE",
                                                                                  cmsEtEstimate.EarlyTerminationDate),
                                                          FormatHelper.FormatData(_settingService, "CURRENCY",
                                                                                  cmsEtEstimate.EarlyTerminationAmount));

                    ZLIS.Cos.Core.Modules.Enquiry.DomainModel.Enquiry enquiry =
                        new Core.Modules.Enquiry.DomainModel.Enquiry();
                    EnquiryContactOption contactMode =
                        _enquiryService.GetEnquiryContactOptionByCriteria("ContactModeOptions", "by Email");

                    ZLIS.Cos.Core.Modules.Enquiry.DomainModel.EnquiryTopic topic =
                        _enquiryService.GetTopicByName(ConfigurationManager.AppSettings["ETEnquiryName"]);
                    if (topic == null)
                        throw new ApplicationException("Bad ETEnquiry Setting.");
                    enquiry.ContactMode = contactMode;
                    enquiry.ContractNo = summary.ContractNO;
                    enquiry.Topic = topic;
                    enquiry.Email = customer.Email;

                    enquiry.Content = enquiryDetails;
                    enquiry.CreatedBy = customer;
                    enquiry.CreatedDate = DateTime.Now;

                    //System.Globalization.CultureInfo cultureInfo = ZLIS.Cos.Core.CosApplicationContext.Current.CurrentCulture;
                    //System.Globalization.CultureInfo cultureInfo = sessionState.CurrentCulture;
                    string languageCode = this.GetCurrentLanguageCode(_settingService, _languageService,
                                                                      request.language);
                    CultureInfo cultureInfo = new CultureInfo(languageCode);

                    var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();
                    using (ITransaction tran = unitOfWork.BeginTransaction())
                    {
                        _enquiryService.CreateEnquiry(enquiry);

                        //create ticket per enquiry
                        var ticket = new CustomerTicket()
                            {
                                TicketNumber = "",
                                Enquiry = enquiry,
                                EnquiryTopic = topic,
                                Severity = (int) TicketSeverity.Low,
                                TicketFromFlag = TicketFromFlag.Customer,
                                AssignedTo = TicketAssignedTo.CustomerService,
                                Status = TicketStatus.Open,
                                CreatedBy = customer,
                                CreatedDate = DateTime.Now
                            };
                        _ticketService.CreateCustomerTicket(ticket);

                        //create ticket message
                        var ticketMessage = new CustomerTicketMessage()
                            {
                                Ticket = ticket,
                                Comment = enquiryDetails,
                                CreatorRoleCode = "CR", //CR=customer
                                ReadFlag = true,
                                SentTo = "", //customer service email
                                CreatedBy = customer.Id.ToString(),
                                CreatedDate = DateTime.Now
                            };
                        _ticketService.CreateCustomerTicketMessage(ticketMessage);

                        //send email to customer
                        EmailTemplate template =
                            _templateService.GetEmailTemplate(EmailTemplateName.ENQUIRY_EMAIL_REQUEST,
                                                              DeliveryChannel.EMAIL);
                        if (template != null)
                        {
                            string subject = template.Subject;
                            if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) &&
                                template.SubjectLocID != null)
                            {
                                subject = template.SubjectLocID.Text;
                            }

                            EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                            string messageBody = emailUtility.GetStringFromTemplateFile(template, languageCode);
                            string rootUrl = "";

                            var s = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SYSTEM,
                                                                     SystemConfigurationConstants
                                                                         .KEY_SYSTEM_EXTERNAL_ROOTURL);
                            if (s != null)
                            {
                                rootUrl = s.Value;
                            }

                            string imageURL = emailUtility.GetImageUrl(rootUrl, "", template.Id.ToString());
                                //Url.Content("~")
                            messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL,
                                                                                 customer.DisplayName,
                                                                                 topic.GetName(cultureInfo),
                                                                                 enquiry.Content, enquiry.ContractNo, "",
                                                                                 enquiry.Email, enquiry.Mobile,
                                                                                 enquiry.ContactMode.GetText(cultureInfo),
                                                                                 "", "", "", "", "", ticket.TicketNumber);

                            _mailer.SendEmail(template, this.GetEmailAddress(customer.Email), subject, messageBody);
                        }

                        //send email to admin
                        EmailTemplate template_admin =
                            _templateService.GetEmailTemplate(EmailTemplateName.ENQUIRY_EMAIL_REQUEST_ADMIN,
                                                              DeliveryChannel.EMAIL);
                        if (template_admin != null)
                        {
                            string subject = template_admin.Subject;
                            if (!cultureInfo.Name.Equals(ConstValue.DEFAULT_CULTURE_NAME) &&
                                template_admin.SubjectLocID != null)
                            {
                                subject = template_admin.SubjectLocID.Text;
                            }

                            EmailServiceUtility emailUtility = new EmailServiceUtility(_appDataFolder, _settingService);
                            string messageBody = emailUtility.GetStringFromTemplateFile(template_admin, languageCode);
                            string rootUrl = "";

                            var s = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_SYSTEM,
                                                                     SystemConfigurationConstants
                                                                         .KEY_SYSTEM_EXTERNAL_ROOTURL);
                            if (s != null)
                            {
                                rootUrl = s.Value;
                            }

                            string imageURL = emailUtility.GetImageUrl(rootUrl, "", template_admin.Id.ToString());
                                //Url.Content("~")

                            IPHostEntry ip = Dns.GetHostEntry(Dns.GetHostName());

                            messageBody = emailUtility.GetMessageBodyReplacement(messageBody, imageURL,
                                                                                 customer.DisplayName,
                                                                                 topic.GetName(cultureInfo),
                                                                                 enquiry.Content, enquiry.ContractNo, "",
                                                                                 enquiry.Email, enquiry.Mobile,
                                                                                 enquiry.ContactMode.GetText(cultureInfo),
                                                                                 "", "", "", "", "", ticket.TicketNumber);
                                //enquiry.ContactMode.GetText(cultureInfo)

                            string csEmailAddress = string.IsNullOrEmpty(topic.CustomerserviceEmail)
                                                        ? this.GetEmailAddress(
                                                            _settingService.GetSystemSetting(
                                                                SystemConfigurationConstants.CATEGORY_MAILER,
                                                                SystemConfigurationConstants.KEY_MAILER_CUSTOMERSERVICE)
                                                                           .Value)
                                                        : topic.CustomerserviceEmail;

                            _mailer.SendEmail(template_admin,
                                              csEmailAddress, subject, "",
                                              messageBody, null, "");
                        }
                        tran.Commit();
                    }
                }

            }

        }

    }
}
