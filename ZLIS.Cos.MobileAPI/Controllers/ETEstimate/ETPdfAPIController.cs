﻿using System;
using System.Linq;
using System.Web.Http;
using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;
using ZLIS.Cos.ClientModel.Requests;
using ZLIS.Cos.ClientModel.Responses;
using ZLIS.Cos.Core.ContentManagement.Services;
using ZLIS.Cos.Core.MobileLogin.Services;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.Core.Settings.Services;
using ZLIS.Cos.MobileAPI.Cache;
using ZLIS.Cos.MobileAPI.RmwService;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Framework.Common;
using ZLIS.Framework.Common.Caching;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Cos.Core.UIManagement;
using ZLIS.Cos.Core.ModuleFilterSetting.Services;
using ZLIS.Cos.Core.Common;
using ZLIS.Cos.PdfGenerator;
using ZLIS.Cos.Core;
using ZLIS.Framework.Common.Encrypt;
using ZLIS.Cos.MobileAPI.Filters;
using System.Web;

namespace ZLIS.Cos.MobileAPI.Controllers.Account
{
    [ActionAuthorizeFilter]
    public class ETPdfAPIController : ApiController
    {
        private IUnitOfWorkAccessor _unitOfWorkAccessor;
        private ILanguageService _languageService;
        private ISystemSettingService _settingService;
        private IMobileLoginInfoService _mobileLoginInfoService;
        private IRmwService _rmwService;
        private IModuleFilterSettingService _mfsService;

        public ILogger Logger { get; set; }

        public ETPdfAPIController(IUnitOfWorkAccessor unitOfWorkAccessor,
                                  ILanguageService languageService,
                                  ISystemSettingService settingService,
                                  IMobileLoginInfoService mobileLoginInfoService,
                                  IRmwService rmwsrv,
                                  IModuleFilterSettingService mfsService,
                                  ICacheManager cacheManager)
        {
            Logger = NullLogger.Instance;
            _unitOfWorkAccessor = unitOfWorkAccessor;
            _languageService = languageService;
            _settingService = settingService;
            _settingService.Cachable = false;
            _rmwService = rmwsrv;
            ((RmwServiceAdaptor)_rmwService).SetCacheManager(cacheManager);

            _mobileLoginInfoService = mobileLoginInfoService;

            _mfsService = mfsService;
        }

        [System.Web.Http.HttpPost]
        public GetEtPdfResponse GetEarlyTerminatePDF(GetEtPdfRequest request)
        {
            Logger.Debug(string.Format("-------- GetEarlyTerminatePDF API started. token:{0} contractNo:{1} date:{2}", request.token ?? string.Empty, request.contractNo ?? string.Empty, request.date ?? string.Empty));
            var unitOfWork = _unitOfWorkAccessor.GetUnitOfWork();

            var response = new GetEtPdfResponse();
            Error errorInfo = new Error();
            errorInfo.code = (int)GetEtPdfErrorCode.NoError;
            errorInfo.message = ErrorMsg.NoError;

            response.error = errorInfo;

            try
            {
                if (string.IsNullOrEmpty(request.token) ||
                    string.IsNullOrEmpty(request.contractNo))
                {
                    errorInfo.code = (int)GetEtPdfErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                string cc = this.GetCountryCode(_settingService).ToUpper();
                if (cc == "JP")
                {
                    errorInfo.code = (int)GetReceiptPdfErrorCode.InvalidRequest;
                    errorInfo.message = "Invalid request";

                    return response;
                }
                var loginInfo = _mobileLoginInfoService.GetByToken(request.token);

                RmwServiceAdaptor rmwServiceAdaptor = (RmwServiceAdaptor)_rmwService;
                var queryContractNo = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo);
                if (!queryContractNo.Any())
                {
                    errorInfo.code = (int)GetEtPdfErrorCode.InvalidContractNo;
                    errorInfo.message = "Invalid contract no.";

                    return response;
                }

                IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
                string dateFormat = GetDateFormat();

                DateTime sDate;
                if (!DateTime.TryParseExact(request.date, dateFormat, formatter, System.Globalization.DateTimeStyles.None, out sDate))
                {
                    errorInfo.code = (int)GetStatementOfAccountErrorCode.InvalidFromDate_EndDate;
                    errorInfo.message = "Invalid date";

                    return response;
                }

                CmsAccountSummary summary = rmwServiceAdaptor.GetAccountSummary(loginInfo.CustomerActivity.Customer.Bpid)
                    .Where(x => x.ContractNO == request.contractNo).FirstOrDefault();
                
                var settings = _mfsService.GetModuleFilterSettings_Nocache(ModuleSettingConstants.ET_ESTIMATE, ModuleSettingConstants.FILTER_TYPE_FINANCIAL_PRODUCT, summary.FinancialProduct == null ? "" : summary.FinancialProduct.Code.ToString())
                        .ToDictionary(x => string.Format("{0}_{1}", summary.ContractNO, x.ResKey), x => x.Visible);

                if (settings != null && settings.ContainsKey(summary.ContractNO + "_ET_ESTIMATE_NOT_ALLOWED") && settings[summary.ContractNO + "_ET_ESTIMATE_NOT_ALLOWED"])
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_Message")
                    //</p>
                    errorInfo.code = (int)GetEtPdfErrorCode.EtEstimateNotAllowed;
                    errorInfo.message = "This function is not available for your account type.";

                    return response;
                }
                else if (summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.Payout
                    || summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.EarlyPayout)
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_PaidInFullMessage")
                    //</p>
                    errorInfo.code = (int)GetEtPdfErrorCode.PaidInFull;
                    errorInfo.message = "This function is not available for your account type.";

                    return response;
                }
                else if (summary.ContractStatusCode == ZLIS.Cos.RmwService.CmsContractStatus.Repossessed)
                {
                    //<p class="exception-msg">
                    //    @Html.Resource("MyAccount_MyAccount_ETEstimate,PageContent_ReprocessedMessage")
                    //</p>
                    errorInfo.code = (int)GetEtPdfErrorCode.Repossessed;
                    errorInfo.message = "This function is not available for your account type.";

                    return response;
                }
                CmsCustomerProfile profile = null;
                if (!string.IsNullOrEmpty(loginInfo.CustomerInfo))
                {
                    try
                    {
                        BlowFish bf = new BlowFish(CosApplicationContext.Current.EncryptionKey);
                        profile = ZLIS.Framework.Common.Helper.JsonUtils.JsonStringToObject<CmsCustomerProfile>(HttpUtility.UrlDecode(bf.Decrypt_ECB(loginInfo.CustomerInfo)));
                    }
                    catch { }
                }
                if (profile == null)
                    profile = _rmwService.GetCustomerProfile(loginInfo.CustomerActivity.Customer.Bpid);
                ISessionState session = CosApplicationContext.Current.Session;
                session.CurrentCmsCustomerProfile = profile;
                response.data = GetStatementOfAccountPdf(request, loginInfo.CustomerActivity.Customer);

                using (ITransaction tran = unitOfWork.BeginTransaction())
                {
                    loginInfo.LastActionTime = DateTime.Now;
                    loginInfo.ExpiryTime = DateTime.Now.AddMinutes(this.LoginExpiryMinutes());
                    _mobileLoginInfoService.UpdateMobileLoginInfo(loginInfo);

                    tran.Commit();
                }

                AppCacheContainer.UpdateCachedObjectActiveDate(request.token);
            }
            catch (Exception ex)
            {
                errorInfo.code = (int)GetEtPdfErrorCode.SystemError;
                errorInfo.message = ex.Message;
                Logger.Error(ex, "Error occurred when get ET pdf");
            }

            return response;
        }

        private FileContent GetStatementOfAccountPdf(GetEtPdfRequest request,
            ZLIS.Cos.Core.Account.DomainModel.Customer customer)
        {
            FileContent result = new FileContent();
            string contractNO = request.contractNo;
            string cc = GetCountryCode();
            IFormatProvider formatter = System.Globalization.CultureInfo.InvariantCulture;
            string dateFormat = GetDateFormat();
            DateTime date = DateTime.ParseExact(request.date, dateFormat, formatter, System.Globalization.DateTimeStyles.None);

            string downloadName = string.Format("{0}_{1}_{2:yyyyMMddHHmmss}.pdf", ReportConstants.KEY_ET_ESTIMATE, contractNO, TimeHelper.Now);
            byte[] reportcontent = ReportGeneraterFactory.GetReporter(cc).GenerateETReport(ReportConstants.TEMPLATE_ET_REPORT, downloadName, contractNO,
                cc, "EN", date, false);
            result.fileName = downloadName;
            result.fileSize = reportcontent.Length;
            result.mimeType = "application/pdf";
            result.content = Convert.ToBase64String(reportcontent);
            //ZLIS.Framework.Common.IO.IOHelper.SaveMediaFile("D:\\ReportTemp\\" + downloadName, reportcontent);
            return result;
        }

        #region Helper

        private string GetDateFormat()
        {
            var dateFormat = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY, SystemConfigurationConstants.KEY_DATE_FORMAT);
            if (dateFormat != null)
            {
                return dateFormat.Value;
            }

            return null;
        }

        private string GetCountryCode()
        {
            var countryCode = _settingService.GetSystemSetting(SystemConfigurationConstants.CATEGORY_COUNTRY,
                SystemConfigurationConstants.KEY_COUNTRY_CODE);
            if (countryCode != null)
            {
                return countryCode.Value;
            }

            return null;
        }

        private string GetLanguage()
        {
            string strLanguage = "";
            var language = _languageService.GetLocalLanguage();
            if (language != null)
            {

                strLanguage = string.Format("{0}-{1}", language.PriLan, language.SubLan);
            }

            return strLanguage;
        }

        #endregion

    }
}
