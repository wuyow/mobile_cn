﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class LoginRequest : BaseLanguageRequest
    {
        public string cc
        {
            get;
            set;
        }

        public string deviceId { get; set; }

        public string deviceType { get; set; }

        public string userId { get; set; }

        public string password { get; set; }

        public string sessionId { get; set; }

        public string userIdFormat { get; set; }

        public string appVersion { get; set; }

        public string osVersion { get; set; }

        public string deviceVendorCode { get; set; }
    }
}
