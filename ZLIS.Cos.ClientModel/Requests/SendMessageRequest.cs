﻿using System;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class SendMessageRequest : BaseLanguageRequest
    {
        public string token
        {
            get;
            set;
        }

        public int threadId
        {
            get;
            set;
        }

        public string content
        {
            get;
            set;
        }

        public FileContent[] attachments
        {
            get;
            set;
        }
    }
}
