﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class ChangePasswordRequest : BaseLanguageRequest
    {
        public string token
        {
            get;
            set;
        }

        public string currentPassword
        {
            get;
            set;
        }

        public string newPassword
        {
            get;
            set;
        }
    }
}
