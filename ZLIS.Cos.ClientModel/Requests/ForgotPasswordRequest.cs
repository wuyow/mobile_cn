﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class ForgotPasswordRequest : BaseLanguageRequest
    {
        public string token
        {
            get;
            set;
        }

        public string newPassword
        {
            get;
            set;
        }
    }
}
