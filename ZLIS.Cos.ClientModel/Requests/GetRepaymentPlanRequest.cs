﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetRepaymentPlanRequest
    {
        public string token
        {
            get;
            set;
        }

        public string contractNo
        {
            get;
            set;
        }

        //public string fromDate
        //{
        //    get;
        //    set;
        //}

        //public string endDate
        //{
        //    get;
        //    set;
        //}

        public int? pageIndex
        {
            get;
            set;
        }

        public int? itemCount
        {
            get;
            set;
        }
    }
}
