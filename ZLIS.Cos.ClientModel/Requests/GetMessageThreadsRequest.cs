﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetMessageThreadsRequest
    {
        public string token
        {
            get;
            set;
        }

    }
}
