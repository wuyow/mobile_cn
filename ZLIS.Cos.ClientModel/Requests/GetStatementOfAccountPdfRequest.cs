﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetStatementOfAccountPdfRequest : BaseLanguageRequest
    {
        public string token
        {
            get;
            set;
        }

        public string contractNo
        {
            get;
            set;
        }

        public string fromDate
        {
            get;
            set;
        }

        public string endDate
        {
            get;
            set;
        }
    }
}
