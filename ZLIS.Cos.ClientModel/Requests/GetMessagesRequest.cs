﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetMessagesRequest
    {
        public string token
        {
            get;
            set;
        }

        public int threadId
        {
            get;
            set;
        }

    }
}
