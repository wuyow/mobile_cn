﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class ValidateCustomerRequest
    {
        public string cc
        {
            get;
            set;
        }

        public string nric
        {
            get;
            set;
        }

        public string type
        { get; set; }
    }
}
