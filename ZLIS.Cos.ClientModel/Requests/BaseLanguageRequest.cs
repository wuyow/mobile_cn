﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class BaseLanguageRequest
    {
        public string language { get; set; }
    }
}
