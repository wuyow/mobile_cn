﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetEtPdfRequest
    {
        public string token
        {
            get;
            set;
        }

        public string contractNo
        {
            get;
            set;
        }

        public string date
        {
            get;
            set;
        }

    }
}
