﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class LogoutRequest
    {
        public string token
        {
            get;
            set;
        }
    }
}
