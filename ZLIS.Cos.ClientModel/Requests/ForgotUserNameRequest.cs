﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class ForgotUserNameRequest
    {
        public string token { get; set; }
    }
}
