﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class RegisterCustomerRequest : BaseLanguageRequest
    {
        public string sessionId { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string deviceType { get; set; }
    }
}
