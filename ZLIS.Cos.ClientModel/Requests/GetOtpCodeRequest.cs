﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetOtpCodeRequest
    {
        public string token
        {
            get;
            set;
        }

        public string type
        {
            get;
            set;
        }

        public string mobileNo
        {
            get;
            set;
        }

    }
}
