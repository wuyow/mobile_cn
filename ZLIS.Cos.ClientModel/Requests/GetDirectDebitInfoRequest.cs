﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetDirectDebitInfoRequest
    {
        public string token
        {
            get;
            set;
        }

        public string contractNo
        {
            get;
            set;
        }

    }
}
