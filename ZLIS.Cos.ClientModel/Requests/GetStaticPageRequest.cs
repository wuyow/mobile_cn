﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetStaticPageRequest
    {
        public string cc { get; set; }
        public string token { get; set; }
        public string pageName { get; set; }

    }
}
