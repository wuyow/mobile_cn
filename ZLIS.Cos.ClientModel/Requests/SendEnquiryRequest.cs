﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class SendEnquiryRequest :BaseLanguageRequest
    {
        public string token
        {
            get;
            set;
        }

        public string contactMode
        {
            get;
            set;
        }

        public string email
        {
            get;
            set;
        }

        public string mobile
        {
            get;
            set;
        }

        public string contactTime
        {
            get;
            set;
        }

        public string contractNo
        {
            get;
            set;
        }

        public int topicId
        {
            get;
            set;
        }

        public string content
        {
            get;
            set;
        }

        public FileContent[] attachments
        {
            get;
            set;
        }
    }
}
