﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class ChangeEmailRequest : BaseLanguageRequest
    {
        public string token
        {
            get;
            set;
        }

        public string newEmail
        {
            get;
            set;
        }
    }
}
