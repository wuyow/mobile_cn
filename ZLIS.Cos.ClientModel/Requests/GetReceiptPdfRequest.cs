﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetReceiptPdfRequest : BaseLanguageRequest
    {
        public string token
        {
            get;
            set;
        }

        public string contractNo
        {
            get;
            set;
        }

        public string receiptId
        {
            get;
            set;
        }
    }
}
