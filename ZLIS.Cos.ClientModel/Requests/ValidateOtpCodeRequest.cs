﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class ValidateOtpCodeRequest
    {
        public string token { get; set; }
        public string type { get; set; }
        public string otpPin { get; set; }
    }
}
