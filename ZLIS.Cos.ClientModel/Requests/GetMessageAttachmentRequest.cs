﻿
namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetMessageAttachmentRequest
    {
        public string token
        {
            get;
            set;
        }

        public int attachmentId
        {
            get;
            set;
        }

    }
}
