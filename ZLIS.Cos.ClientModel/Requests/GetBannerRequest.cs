﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetBannerRequest
    {
        public string token
        {
            get;
            set;
        }

        public string bannerId
        {
            get;
            set;
        }

    }
}
