﻿using System;

namespace ZLIS.Cos.ClientModel.Requests
{
    public class GetRepaymentPlanPdfRequest : BaseLanguageRequest
    {
        public string token
        {
            get;
            set;
        }

        public string contractNo
        {
            get;
            set;
        }

    }
}
