﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetMessageThreadsResponse : BaseResponse
    {
        public MessageThreads data
        {
            get;
            set;
        }
    }
}
