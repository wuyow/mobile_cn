﻿using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetStatementOfAccountPdfResponse : BaseResponse
    {
        public FileContent data
        {
            get;
            set;
        }
    }
}
