﻿using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class LoginResponse : BaseResponse
    {
        public LoginInfo data
        {
            get;
            set;
        }
    }
}
