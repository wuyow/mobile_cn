﻿using ZLIS.Cos.ClientModel.Common;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class BaseResponse
    {
        public Error error
        {
            get;
            set;
        }
    }
}
