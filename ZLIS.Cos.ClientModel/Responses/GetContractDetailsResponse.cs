﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetContractDetailsResponse : BaseResponse
    {
        public ContractDetails data
        {
            get;
            set;
        }
    }
}
