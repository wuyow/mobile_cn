﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetRepaymentPlanResponse : BaseResponse
    {
        public RepaymentPlan data
        {
            get;
            set;
        }
    }
}
