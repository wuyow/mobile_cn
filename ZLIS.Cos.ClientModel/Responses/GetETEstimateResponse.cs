﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetETEstimateResponse : BaseResponse
    {
        public ETEstimate data { get; set; }
    }

}
