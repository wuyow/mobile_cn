﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetOtpCodeResponse : BaseResponse
    {
        public OtpCode data { get; set; }
    }
}
