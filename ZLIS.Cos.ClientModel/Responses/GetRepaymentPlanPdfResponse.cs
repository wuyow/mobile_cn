﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetRepaymentPlanPdfResponse : BaseResponse
    {
        public FileContent data
        {
            get;
            set;
        }
    }
}
