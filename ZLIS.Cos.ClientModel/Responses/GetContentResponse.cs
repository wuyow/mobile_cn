﻿using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetContentResponse : BaseResponse
    {
        public Content data
        {
            get;
            set;
        }
    }
}
