﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetReceiptsResponse : BaseResponse
    {
        public Receipt data
        {
            get;
            set;
        }
    }
}
