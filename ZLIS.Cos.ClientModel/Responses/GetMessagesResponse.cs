﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetMessagesResponse : BaseResponse
    {
        public Messages data
        {
            get;
            set;
        }
    }
}
