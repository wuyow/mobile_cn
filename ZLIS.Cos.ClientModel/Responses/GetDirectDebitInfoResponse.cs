﻿using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetDirectDebitInfoResponse : BaseResponse
    {
        public DirectDebit data
        {
            get;
            set;
        }
    }
}
