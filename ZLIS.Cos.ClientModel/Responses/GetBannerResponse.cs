﻿using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetBannerResponse : BaseResponse
    {
        public int fileSize { get; set; }

        public string mimeType { get; set; }

        public string content { get; set; }

    }
}
