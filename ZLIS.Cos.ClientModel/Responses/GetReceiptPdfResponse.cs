﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetReceiptPdfResponse : BaseResponse
    {
        public FileContent data
        {
            get;
            set;
        }
    }
}
