﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class ValidateCustomerResponse : BaseResponse
    {
        public CustomerInfo data
        {
            get;
            set;
        }
    }
}
