﻿using ZLIS.Cos.ClientModel.Common;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetEtPdfResponse : BaseResponse
    {
        public FileContent data
        {
            get;
            set;
        }
    }
}
