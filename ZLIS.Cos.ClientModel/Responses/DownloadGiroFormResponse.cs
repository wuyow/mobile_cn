﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class DownloadGiroFormResponse:BaseResponse
    {
        public FileContent data { get; set; } 
    }
}
