﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetStatementOfAccountResponse : BaseResponse
    {
        public StatementOfAccount data
        {
            get;
            set;
        }
    }
}
