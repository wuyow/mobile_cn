﻿using ZLIS.Cos.ClientModel.Models;

namespace ZLIS.Cos.ClientModel.Responses
{
    public class GetMessageAttachmentResponse : BaseResponse
    {
        public FileContent data
        {
            get;
            set;
        }
    }
}
