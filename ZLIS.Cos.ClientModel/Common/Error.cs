﻿using System;

namespace ZLIS.Cos.ClientModel.Common
{
    public class Error
    {
        public int code
        {
            get;
            set;
        }

        public string message { get; set; }
    }
}
