﻿namespace ZLIS.Cos.ClientModel.Common
{
    public enum GetListsContentErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        BadParameters = 401,

        SystemError = 500,
    }

    public enum LoginErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        BadParameters = 401,

        Login_InvalidUserIdPassword = 403,
        Login_AccountNotYetActivated = 404,
        Login_NoMatchingRecordsFound = 405,

        Login_AccountIsLocked = 406,
        Login_NotAuthorized = 407,
        Login_ExceedMaximumLoginAttempt = 408,
        Login_NoCustomerProfileFound = 409,

        SystemError = 500,
    }

    public enum LogoutErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,

        SystemError = 500,
    }

    public enum GetContractDetailsErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        HaveBeenLoggedOut = 405,

        SystemError = 500,
    }

    public enum GetStatementOfAccountErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidFromDate_EndDate = 405,

        ExceptionCase = 406,    //!!

        HaveBeenLoggedOut = 407,

        SystemError = 500,
    }

    public enum GetStatementOfAccountPdfErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidFromDate_EndDate = 405,

        HaveBeenLoggedOut = 406,

        SystemError = 500,
    }

    public enum GetRepaymentPlanErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidFromDate_EndDate = 405,

        HaveBeenLoggedOut = 406,

        SystemError = 500,
    }

    public enum GetRepaymentPlanPdfErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        HaveBeenLoggedOut = 405,

        SystemError = 500,
    }


    public enum GetReceiptsErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidFromDate_EndDate = 405,

        HaveBeenLoggedOut = 406,

        SystemError = 500,
    }

    public enum GetReceiptPdfErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidReceiptId = 405,

        HaveBeenLoggedOut = 406,

        SystemError = 500,
    }

    public enum GetETEstimateErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidDate = 405,

        EtEstimateNotAllowed = 406,
        PaidInFull = 407,
        Repossessed = 408,

        HaveBeenLoggedOut = 409,

        SystemError = 500,
    }

    public enum ProcessETErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidDate = 405,

        EtEstimateNotAllowed = 406,
        PaidInFull = 407,
        Repossessed = 408,

        HaveBeenLoggedOut = 409,

        SystemError = 500,
    }

    public enum GetEtPdfErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidDate = 405,

        EtEstimateNotAllowed = 406,
        PaidInFull = 407,
        Repossessed = 408,

        HaveBeenLoggedOut = 409,

        SystemError = 500,
    }

    public enum GetDirectDebitInfoErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        HaveBeenLoggedOut = 405,

        SystemError = 500,
    }

    public enum GetMessageThreadsErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        SessionTimeout = 402,
        ForcedLogout = 403,

        HaveBeenLoggedOut = 404,

        SystemError = 500,
    }

    public enum GetMessagesErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidThreadId = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        HaveBeenLoggedOut = 405,

        SystemError = 500,
    }

    public enum GetMessageAttachmentErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidAttachmentId = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        HaveBeenLoggedOut = 405,

        SystemError = 500,
    }

    public enum SendMessageErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidThreadId = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        HaveBeenLoggedOut = 405,

        SystemError = 500,
    }

    public enum SendEnquiryErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidTopicId = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        InvalidContactMode = 405,
        InvalidEmail = 406,
        InvalidPhone = 407,
        InvalidContactTime = 408,

        HaveBeenLoggedOut = 409,

        SystemError = 500,
    }

    public enum ChangePasswordErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidNewPassword = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        InvalidCurrentPassword = 405,
        NewPasswordSameAsCurrentPassword = 406,

        HaveBeenLoggedOut = 407,

        SystemError = 500,
    }

    public enum ChangeEmailErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidNewEmail = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,

        HaveBeenLoggedOut = 405,

        SystemError = 500,
    }

    public enum GetOtpCodeErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,

        SessionTimeout = 403,
        ForcedLogout = 404,

        HaveBeenLoggedOut = 405,
        SendSMSFailed = 406,

        SystemError = 500,
    }

    public class ErrorMsg
    {
        public const string NoError = "The operation is completed successfully.";
    }

    public enum CheckSessionErrorCode
    {
        NoError = 0,
        HaveBeenLoggedOut,
        SessionTimeOut,
        ForcedLogout,
    }

    public enum RegisterErrorCode
    {
        NoError = 200,
        InvalidRequest = 400,
        InvalidSession = 401,
        InvalidEmailAddress = 402,
        InvaildPassword = 403,
        CustomerExist = 404,
        SystemError = 500,
    }

    public enum ForgotPasswordErrorCode
    {
        NoError = 200,
        InvalidRequest = 400,
        InvalidToken = 401,
        InvaildPassword = 402,
        SessionTimeout = 403,
        ForcedLogout=404,
        CustomerDoNotExist = 405,
        NewPasswordSameAsCurrentPassword = 406,
        SystemError = 500,
    }

    public enum ForgotUserNameErrorCode
    {
        NoError = 200,
        InvalidRequest = 400,
        InvalidToken = 401,
        InvaildUserName = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        CustomerDoNotExist = 405,
        SystemError = 500,
    }

    public enum GetStaticPageErrorCode
    {
        NoError = 200,
        InvalidRequest = 400,
        InvalidToken = 401,
        InvaildContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidPageName = 405,
        SystemError = 500,
    }

    public enum OtpValidateErrorCode
    {
        NoError = 200,
        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidOtpCode = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        Expired = 405,
        SystemError = 500,
    }

    public enum GetOverDuePdfErrorCode
    {
        NoError = 200,

        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidContractNo = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidFromDate_EndDate = 405,

        HaveBeenLoggedOut = 406,

        SystemError = 500,
    }

    public enum GetBannerErrorCode
    {
        NoError = 200,
        InvalidRequest = 400,
        InvalidToken = 401,
        InvalidBannerId = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        SystemError = 500,
    }

    public enum DownloadGiroFormErrorCode
    {
        NoError = 200,
        InvalidRequest = 400,
        InvalidToken = 401,
        InvaildGiroForm = 402,
        SessionTimeout = 403,
        ForcedLogout = 404,
        InvalidPageName = 405,
        SystemError = 500,
    }

    public enum ValidateCustomerErrorCode
    {
        NoError = 200,
        InvalidRequest = 400,
        CustomerExist = 401,
        CustomerDoNotExist = 402,
        AccountIsLocked = 406,
        InvalidMobileNo = 407,
        InvalidCustomer = 408,
        SystemError = 500,
    }
}
