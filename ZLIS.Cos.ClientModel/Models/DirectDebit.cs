﻿using System;

namespace ZLIS.Cos.ClientModel.Models
{
    public class DirectDebit
    {
        public bool showBankInfo { get; set; }

        public string sectionTitle { get; set; }

        public string sectionTitleLocid { get; set; }//int?

        public Field[] bankInfo { get; set; }

        public LanguageContent[] languageContent { get; set; }
    }

    //public class BankInfo
    //{
    //    public string id { get; set; }

    //    public string module { get; set; }

    //    public string label { get; set; }

    //    public int? labelLocid { get; set; }

    //    public int order { get; set; }
    //}

}
