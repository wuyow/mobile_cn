﻿using System;

namespace ZLIS.Cos.ClientModel.Models
{
    public class LoginInfo
    {
        public string token { get; set; }

        public string sessionId { get; set; }

        public Customer customer { get; set; }

        public CustomerProfile customerProfile { get; set; }

        public Contract[] contracts { get; set; }

        public LanguageContent[] languageContent { get; set; }

        public EnquiryInfo enquiryInfo { get; set; }

        public BannerInfo[] banners { get; set; }

        public int unreadMessageNum { get; set; }

        public bool isGiroFormAvailable { get; set; }
    }

    public class Customer
    {
        public string firstName { get; set; }

        public string middleName { get; set; }

        public string lastName { get; set; }

        public string email { get; set; }

        public string customerType { get; set; }
    }

    public class CustomerProfile
    {
        public LoginSection[] sections { get; set; }
    }

    public class LoginSection : Section
    {
        public string id { get; set; }

        public string label { get; set; }

        public int order { get; set; }

        public string labelLocid { get; set; }//decimal?

        public LoginField[] fields { get; set; }

    }

    public class Section
    {
        public string id { get; set; }

        public string label { get; set; }

        public int order { get; set; }

        public string labelLocid { get; set; }//decimal?

        public Field[] fields { get; set; }
    }

    public class LoginField : Field
    {
        public bool? SMSRegistered { get; set; }
    }

    public class Field
    {
        public string id { get; set; }

        public string module { get; set; }

        public string label { get; set; }

        public string labelLocId { get; set; }//decimal?

        public int order { get; set; }

        public string value { get; set; }

        public string valueLoc { get; set; }
    }

    public class Contract
    {
        public string contractNo { get; set; }

        public string nextDueDate { get; set; }

        public decimal nextDueAmount { get; set; }

        public int terms { get; set; }

        public decimal termPaid { get; set; }

        public string contractStatus { get; set; }

        public string vehcileRegNo { get; set; }

        public string[] menus { get; set; }
        public string maturityDate { get; set; }
        public string startDate { get; set; }
        public bool showMsgFlag { get; set; }

        public decimal overdueAmount { get; set; }
    }

    public class EnquiryInfo
    {
        public string[] email { get; set; }
        public string[] mobile { get; set; }
        public string[] contract { get; set; }

    }

    public class BannerInfo
    {
        public string name { get; set; }
        public string id { get; set; }
        public string modifiedDate { get; set; }

    }

}
