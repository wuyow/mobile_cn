﻿using System;

namespace ZLIS.Cos.ClientModel.Models
{
    public class ContractDetails
    {
        public string contractNo { get; set; }

        public string contractStatus { get; set; }

        public string contractStatusLoc { get; set; }

        public Section[] sections { get; set; }

        public LanguageContent[] languageContent { get; set; }
    }

}
