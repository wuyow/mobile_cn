﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Models
{
    public class ETEstimate
    {
        public decimal? amount { get; set; }

        public string etStatus { get; set; }
    }
}
