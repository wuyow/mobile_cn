﻿using System;

namespace ZLIS.Cos.ClientModel.Models
{
    public class StatementOfAccount
    {
        public int totalCount { get; set; }

        public Settings settings { get; set; }

        public TableDataRow[] tableData { get; set; }

        public LanguageContent[] languageContent { get; set; }
    }

    public class Settings
    {
        public HeaderSetting[] headerSettings { get; set; }

        public TableSetting[] tableSettings { get; set; }
    }

    public class HeaderSetting
    {
        public string id { get; set; }

        public string label { get; set; }

        public int order { get; set; }

        public string labelLocid { get; set; }//int?

        public string value { get; set; }

        public string valueLoc { get; set; }
    }

    public class TableSetting
    {
        public string id { get; set; }

        public string label { get; set; }

        public int order { get; set; }

        public string labelLocid { get; set; }//int?

        public bool isHeading { get; set; }
    }

    public class TableDataRow
    {
        public string id { get; set; }
        public GridDataRow[] tableDataRow { get; set; }
    }

    public class GridDataRow
    {
        public string settingId { get; set; }

        public string dataType { get; set; }

        public string value { get; set; }

        public string valueLoc { get; set; }
    }

    public class FileContent
    {
        public string fileName { get; set; }

        public int fileSize { get; set; }

        public string mimeType { get; set; }

        public string content { get; set; }
    }

}
