﻿using System;

namespace ZLIS.Cos.ClientModel.Models
{
    public class Content
    {
        public Setting settings { get; set; }

        public MenuItem[] menuItems { get; set; }

        public EnquiryTopic[] enquiryTopics { get; set; }

        public PageResource[] pageResources { get; set; }

        public LanguageContent[] languageContent { get; set; }
    }

    public class Setting
    {
        public string dateFormat { get; set; }

        public string currencySymbol { get; set; }

        public int timeZone { get; set; }

        public string language { get; set; }

        public int maxAttachmentSize { get; set; }

        public int maxAttachmentNumber { get; set; }

        public int minPasswordLength { get; set; }

        public string loginMode { get; set; }

        public bool loginOtpEnabled { get; set; }

        public bool soaOtpEnabled { get; set; }

        public bool repaymentPlanOtpEnabled { get; set; }

        public bool receiptOtpEnabled { get; set; }

        //public bool forgotPasswordOtpEnabled { get; set; }

        //public bool forgotUsernameOtpEnabled { get; set; }

        public bool changeEmailOtpEnabled { get; set; }
        public int SOA_DefaultFromToMonthDuration { get; set; }
        public int Invoice_DefaultFromToMonthDuration { get; set; }
        public int Receipt_DefaultFromToMonthDuration { get; set; }
        public bool IsOTPTesting { get; set; }
        public bool maintenanceMode { get; set; }
        public string maintenanceMessage { get; set; }

    }

    public class MenuItem
    {
        public string id { get; set; }

        public string parent { get; set; }

        public int level { get; set; }

        public int order { get; set; }

        public string key { get; set; }

        public string label { get; set; }

        public string labelLocid { get; set; }//int?
    }

    public class EnquiryTopic
    {
        public int id { get; set; }

        public string content { get; set; }

        public string contentLocid { get; set; }//int?

        public string instruction { get; set; }

        public string instructionLocid { get; set; }//int?

        public bool attachmentRequired { get; set; }

        public bool hasAttachment { get; set; }

        public int order { get; set; }
    }

    public class PageResource
    {
        public int id { get; set; }

        public string category { get; set; }

        public string key { get; set; }

        public string content { get; set; }

        public string contentLocid { get; set; }//int?
    }

    public class LanguageContent
    {
        public string languageCode { get; set; }

        public LangContent[] contents { get; set; }
    }

    public class LangContent
    {
        public string id { get; set; } //decimal

        public string value { get; set; }
    }
}
