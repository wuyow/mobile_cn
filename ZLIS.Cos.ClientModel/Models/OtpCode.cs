﻿
namespace ZLIS.Cos.ClientModel.Models
{

    public class OtpCode
    {
        public string otpCode { get; set; }

        public string expiryDate { get; set; }
    }

}
