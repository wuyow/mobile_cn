﻿using System;

namespace ZLIS.Cos.ClientModel.Models
{
    public class Receipt
    {
        public int totalCount { get; set; }

        public Settings settings { get; set; }

        public TableDataRow[] tableData { get; set; }

        public LanguageContent[] languageContent { get; set; }
    }

}
