﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Cos.ClientModel.Models
{
    public class StaticPageInfo
    {
        public string title { get; set; }
        public string titleLoc { get; set; }
        public FileContent image { get; set; }
        public string bodyContent { get; set; }
        public string bodyContentLoc { get; set; }
    }
}
