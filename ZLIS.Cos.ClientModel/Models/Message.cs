﻿using System;

namespace ZLIS.Cos.ClientModel.Models
{
    public class MessageThreads
    {
        public MessageThread[] messageThreads { get; set; }
    }

    public class MessageThread
    {
        public int id { get; set; }

        public string title { get; set; }
        
        public string titleLoc { get; set; }

        public string createdDate { get; set; }

        public string createdBy { get; set; }

        public string lastReplyDate { get; set; }

        public int unreadMessageNum { get; set; }
    }

    public class Messages
    {
        public Message[] messages { get; set; }
    }

    public class Message
    {
        public int id { get; set; }

        public string content { get; set; }

        public string createdDate { get; set; }

        public string createdBy { get; set; }

        public bool readFlag { get; set; }

        public Attachment[] attachments { get; set; }
    }

    public class Attachment
    {
        public int id { get; set; }

        public string fileName { get; set; }

        public string mimeType { get; set; }

        public double fileSize { get; set; }
    }

}
