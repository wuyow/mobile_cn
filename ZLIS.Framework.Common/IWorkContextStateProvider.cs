﻿using System;

namespace ZLIS.Framework.Common
{
    public interface IWorkContextStateProvider : IDependency
    {
        Func<WorkContext, T> Get<T>(string name);
    }
}
