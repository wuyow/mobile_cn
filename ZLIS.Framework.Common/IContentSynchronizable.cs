﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Framework.Common
{
    public interface IContentSynchronizable
    {
        Guid? ContentSyncId { get; set; }
    }
}
