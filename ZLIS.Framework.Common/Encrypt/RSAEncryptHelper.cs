﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace ZLIS.Framework.Common.Encrypt
{
    public class RSAEncryptHelper
    {
        private static string _privateKey = "<RSAKeyValue><Modulus>rahB2KCDBzBJ2J5ZPJN4UoSEFw1pz4sz/+HQKyg28COeO69+CSw5yHUkAcphc3kKNMo/+WWjtzKtDtjevPgMMUohl0dgHru9TPXLwd6vl3tRdKtEbon3odZgmsLTftVJtMQes5u7JDd6n6vkNpqqJun7DoZ9UQElCtAbAth5lPumKS+H9cqVb7Kl+gP9GjeeRJS5dWw9jORzld2+cAmvzY220NCVRLmVeciCHrVonHif1+SLZidhktTA3WPhwDFoxiLbVcljyERLprK6h/z+mE8DjGXzbioAeHgH+9mMs+g28hrGVTqFkxWd8ZP0QhnHGFgIujDUaMLw8HEWx/TYKn5oZft5dII047Se3uYurCnlQ/Zjrs/TBWusO3IU1G4nSU7GJh2rYzVJE+KleGAu++GAl1LISfOUtXn3zhI33Y7GXicx1J401nv+Ba6gmvZphc3GZ0l9LoxAE+Oyyi584/dvPqKwI3Fbp5J0/KObqtEsbk2WWusgRmLxtNd9F4yoVg6vSU8Gwk1ygyiSDHbr+ciiLuNTdLY31zwWlK9l0f9H/jf/AsJNZnNzmcrhBvNmd+CXCmOjcmMkvgU23Qr8VaUZoMzfiLyNAVxrJ+cd59gJSTT5wu/P5ETDuVYzpCdLsUJ0tXcqbTW3w0zxlOe/GZtw7ckrs3Qi8i/YGgRHqns=</Modulus><Exponent>AQAB</Exponent><P>4qNlTj3iq3H9blmixcsBqoGMyZA3u8MD8BvREzBbh89v0c1xMtk7wTcjPY5eXWB/XE0WkAot3OjTXTEmHZwO2HpP5gHGKnT9JzyjpeYyC7ahf5iWTKb36nbbfsHrpI+Y6MjL2Id8+PRlvM394VoVEn62eF9g9Ji6LvSk8oRDvkeR5i9kEVKUXMUOAUgsEt3c11d6TOVSGjrdus8H/YdY0FlbLHitr10JgAFAETOx89G9g1dJYR7jDSieSmZS+t4j+zKNh/Qc76oBujMPKDCNtwvgOp/qEx4b/hiHivKo8gBcMWm1oLBEm/dV42IbIDXXkJkTaugAxuKreiQBQWvjqQ==</P><Q>xCe2TPOfjupxYz/1hajC1GsaaVMAZXS68mKyiHfg69mVj5PKY8TXJSNlDQkHotBTNpcfICIRAyCmMjotr6bgV1oK13tGpRG07EUJgiz74APgXe/ekvp9MrKvB2e5M8oxI9xw6DZZ3r4NZLRPt6kkxRoTYuFikOZMVIN9mVcLm/1i1FYBgQcHYmxQQxSmPaOOWreYaimb8myu2dJ7CpeMHtmiIJzIxAReeQ8/36EMy8rCAfnQCM9sKNXp4XSBCLxJraBswXYO5dHtQXAuBupRAWJA3xKwwHnyrj/39i7bXBF2e1vLzi8Nl70XsC67eIiIfeyr+1H3Kr9Mkh26yKuzgw==</Q><DP>WnrJEvd56uBTQR7f8P8e5ydZQY8l8PvmMpGoYnTH1exmw+e07Yv0YyrUEtsnqTfBIORVwaOOs8bvuyDICsIm2x3ZkAXdS03xUxzOaIkCKdL7nqkYN/ngt+ZDstNQdB4v+Z9QYl2U8v7/LuU+Gh2QlBWqW1wYpQ3kJqLcx0UulmSURsVcud2yBWdWNATQM6gHZqEx3kkfAgHGS7Ijin/iYSuLTrHPBVerV0MohLWSsbhmZG0Qiol+M96ncyRutGDDFKIWBtTaSIQitGd2AaGAsjc3ibAWDxPSMRsx4bMG57ZlGAUP0dsNSQbxbxnXooBcUwoJyoBc7wnHU7fUGH4uIQ==</DP><DQ>PH4Ov26Gb3mnnDcWTbD83KtnCsI5JhC4spVzf5iQuctxT0UBroD+5Iok2L9HJpPehcBXLf6zhmZkOqssebYaC3ZOT6PJ5pAuXorv4sBisPQhFncqCePMBn2KctsvB2S8gNWz0DkqwAGWec8NopM8Lrja76yYTB1pvsqKSmbIwsOC3f7kGdmEEntayUUt+2ZcOWfTTjIk0L9psO0zWtHfuYfecvZHCARc6jKkvR+apaKS8C8NVBo7veAyb4ARZxNCGZSW1ZbGA2QZKgwhdap/btDRNro53jH9gHVjz/0lUyBjjKmDkcN7CqebDY9r60EX7tW1OB1P/1PdllYqy0YAZw==</DQ><InverseQ>bHEArtvqEdTkQ7vhlVwU3fnunLJx/orrRlaGJfzQU+OY4nK6z+LAp6I0DQybEBTiH+tS5OIKKxkuikoYHwqIX6yJPf2wNiuhTh+ppw1xNIIgE7By7sxGnPZPwoPBBB2t36EWT0NVNFRueABw+bmdwwaadH94mA6iKBF1EoenxZ+vAyUsbaLRrffFSOvMpUtdmhnvhvB7U6XFFjTM1wBbzC+5aTl00aPpquNd/cvIG5DP5KYO1r4KSjhFvBLQi0cHpWLL4BhqXL0eQsk/bJezXk1+pVWGViX+Jf0deYde8R7a1ZQUJEea1IuW4Dg5KLPnCG13+wT0nALKykn7yiQRiw==</InverseQ><D>LjxTM8Mgv8Q6lSMr7TXyXj4METswBeMxOBq+Ww88+VjyYz/JVdXhmJ6ftJ5WrfD3J7EFsXg/S6KBcyeIvlVooaL0KJwHWodv0lLLKlYI+FMMD/0dnjS7quoyFGJ0/xr4odlptzZqcC0ZR/PzMhqh3PSFNIvYR73/R866slpCnMkXp3WRRH7opLGzAUsYk2dliyrx6fKkvKOCps9ASgI6PYdSumweogqrYp5hDp4MOUTgghHGj69Cil587zUNwScml0JmlYOmPob4gWyqv1Q70P+QmUv3sUgbkfebYJG+pZPZNUKttbl8IPorcEuNSCAm4zNlN3WSyEXG8q2ZfCmTd+RYa5oQ7c/1MMHVAMRnqRDbWpmu1WqNywzxEDZV+WznO1y0dsD4aX5qgt48OOY3/HWCq3gpBcXxV0WOm2tKr0C2PPRi/qPnt6feG4/Pjc2/ZVQMmkNXDhsmq/IsdrVlaRwbJr8qzPxIkb7UzogKqGG6zpjmcwXLFf+BFdNvQqcv0JUhBhKRI2fSrmjYRyA9VLOAfYrHv/bbhRTq1YVtG+chQHpLLmFA8iNRyWtFw9+OEcLzb0J82N3Hg6TYnUrK5YJEqUbs84xN/k+oSrUtkbtJizcgDezdbNBFFEPHrJUglEPUPw1KOemiS6ust16/eaesYlBTvjHz9yWMfn8DV9E=</D></RSAKeyValue>";
        private static string _publicKey = "<RSAKeyValue><Modulus>rahB2KCDBzBJ2J5ZPJN4UoSEFw1pz4sz/+HQKyg28COeO69+CSw5yHUkAcphc3kKNMo/+WWjtzKtDtjevPgMMUohl0dgHru9TPXLwd6vl3tRdKtEbon3odZgmsLTftVJtMQes5u7JDd6n6vkNpqqJun7DoZ9UQElCtAbAth5lPumKS+H9cqVb7Kl+gP9GjeeRJS5dWw9jORzld2+cAmvzY220NCVRLmVeciCHrVonHif1+SLZidhktTA3WPhwDFoxiLbVcljyERLprK6h/z+mE8DjGXzbioAeHgH+9mMs+g28hrGVTqFkxWd8ZP0QhnHGFgIujDUaMLw8HEWx/TYKn5oZft5dII047Se3uYurCnlQ/Zjrs/TBWusO3IU1G4nSU7GJh2rYzVJE+KleGAu++GAl1LISfOUtXn3zhI33Y7GXicx1J401nv+Ba6gmvZphc3GZ0l9LoxAE+Oyyi584/dvPqKwI3Fbp5J0/KObqtEsbk2WWusgRmLxtNd9F4yoVg6vSU8Gwk1ygyiSDHbr+ciiLuNTdLY31zwWlK9l0f9H/jf/AsJNZnNzmcrhBvNmd+CXCmOjcmMkvgU23Qr8VaUZoMzfiLyNAVxrJ+cd59gJSTT5wu/P5ETDuVYzpCdLsUJ0tXcqbTW3w0zxlOe/GZtw7ckrs3Qi8i/YGgRHqns=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        private static UnicodeEncoding _encoder = new UnicodeEncoding();


        public static string Decrypt(string data)
        {

            var rsa = new RSACryptoServiceProvider();
            var dataArray = data.Split(new char[] { ',' });
            byte[] dataByte = new byte[dataArray.Length];
            for (int i = 0; i < dataArray.Length; i++)
            {
                dataByte[i] = Convert.ToByte(dataArray[i]);
            }

            rsa.FromXmlString(_privateKey);
            var decryptedByte = rsa.Decrypt(dataByte, false);
            return _encoder.GetString(decryptedByte);

        }

        public static string Encrypt(string data)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(_publicKey);
            var dataToEncrypt = _encoder.GetBytes(data);
            var encryptedByteArray = rsa.Encrypt(dataToEncrypt, false).ToArray();
            var length = encryptedByteArray.Count();
            var item = 0;
            var sb = new StringBuilder();
            foreach (var x in encryptedByteArray)
            {
                item++;
                sb.Append(x);

                if (item < length)
                    sb.Append(",");
            }

            return sb.ToString();
        }
    }
}
