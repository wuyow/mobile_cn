﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace ZLIS.Framework.Common.Helper
{
    public class JsonUtils
    {
        public static string ObjectToJsonString(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static T JsonStringToObject<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

    }
}
