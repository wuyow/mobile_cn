﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Framework.Common
{
    public class PagingContext
    {
        public int Total { get; set; }
        public int PageSize { get; set; }
        public int PageNo { get; set; }
    }
}
