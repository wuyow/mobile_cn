using System;

namespace ZLIS.Framework.Common.Caching
{
    public interface ICacheHolder : ISingletonDependency {
        ICache<TKey, TResult> GetCache<TKey, TResult>(Type component);
    }
}
