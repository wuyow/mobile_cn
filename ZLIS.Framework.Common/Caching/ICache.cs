﻿using System;

namespace ZLIS.Framework.Common.Caching
{
    public interface ICache<TKey, TResult> {
        TResult Get(TKey key, Func<AcquireContext<TKey>, TResult> acquire);
    }
}
