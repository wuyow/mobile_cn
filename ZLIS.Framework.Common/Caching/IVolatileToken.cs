namespace ZLIS.Framework.Common.Caching
{
    public interface IVolatileToken {
        bool IsCurrent { get; }
    }
}