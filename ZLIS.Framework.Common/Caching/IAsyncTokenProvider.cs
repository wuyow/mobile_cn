﻿using System;

namespace ZLIS.Framework.Common.Caching
{
    public interface IAsyncTokenProvider {
        IVolatileToken GetToken(Action<Action<IVolatileToken>> task);
    }
}