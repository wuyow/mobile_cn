﻿namespace ZLIS.Framework.Common.Caching
{
    public interface ICacheContextAccessor {
        IAcquireContext Current { get; set; }
    }
}