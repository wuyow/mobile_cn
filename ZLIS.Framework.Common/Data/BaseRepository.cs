using System.Linq;
using ZLIS.Lib.Framework.Repository;

namespace ZLIS.Framework.Common.Data 
{
	public class BaseRepository<TEntity, TPrimaryKey> : IGenericRepository<TEntity, TPrimaryKey> where TEntity : class
    {
		/// <summary>
		/// Deletes entity in the storage.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="unitOfWork"></param>
		public void Delete(TEntity entity, IUnitOfWork unitOfWork){
            unitOfWork.Delete(entity);
		}

		/// <summary>
		/// Gets all entities of the type from the storage.
		/// </summary>
		/// <param name="unitOfWork"></param>
		public IQueryable<TEntity> GetAll(IUnitOfWork unitOfWork)
		{
		    return unitOfWork.GetAll<TEntity>();
		}

		/// <summary>
		/// Gets entity from the storage by it's Id.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="unitOfWork"></param>
		public TEntity GetById(TPrimaryKey id, IUnitOfWork unitOfWork){

            return unitOfWork.GetById<TEntity, TPrimaryKey>(id);
		}

		/// <summary>
		/// Inserts entity to the storage.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="unitOfWork"></param>
		public void Insert(TEntity entity, IUnitOfWork unitOfWork){

            unitOfWork.Insert(entity);
		}

		/// <summary>
		/// Updates entity in the storage.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="unitOfWork"></param>
		public void Update(TEntity entity, IUnitOfWork unitOfWork){
            unitOfWork.Update(entity);
		}

        public TEntity Get(System.Linq.Expressions.Expression<System.Func<TEntity, bool>> predicate, IUnitOfWork unitOfWork)
        {
            throw new System.NotImplementedException();
        }

        public int Count(System.Linq.Expressions.Expression<System.Func<TEntity, bool>> predicate, IUnitOfWork unitOfWork)
        {
            return GetAll(unitOfWork).Count(predicate);
        }

        public System.Collections.Generic.IEnumerable<TEntity> Fetch(IUnitOfWork unitOfWork, System.Linq.Expressions.Expression<System.Func<TEntity, bool>> predicate)
        {
            return GetAll(unitOfWork).Where(predicate).ToList().AsReadOnly();
        }

        public System.Collections.Generic.IEnumerable<TEntity> Fetch(IUnitOfWork unitOfWork, System.Linq.Expressions.Expression<System.Func<TEntity, bool>> predicate, System.Action<Orderable<TEntity>> order)
        {
            var orderable = new Orderable<TEntity>(GetAll(unitOfWork).Where(predicate));
            order(orderable);
            return orderable.Queryable.ToList().AsReadOnly();
        }

        public System.Collections.Generic.IEnumerable<TEntity> Fetch(IUnitOfWork unitOfWork, System.Linq.Expressions.Expression<System.Func<TEntity, bool>> predicate, System.Action<Orderable<TEntity>> order, int skip, int count)
        {
            var orderable = new Orderable<TEntity>(GetAll(unitOfWork).Where(predicate));
            order(orderable);
            return orderable.Queryable.Skip(skip).Take(count);
        }
    }//end BaseRepository

}//