﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Cfg;

namespace ZLIS.Framework.Common.Data
{
    public interface ISessionFactoryHolder
    {
        ISessionFactory GetSessionFactory();
        Configuration GetConfiguration();
    }
}
