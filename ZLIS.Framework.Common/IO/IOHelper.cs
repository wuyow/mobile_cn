﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ionic.Zip;
using System.IO;

namespace ZLIS.Framework.Common.IO
{
    public class IOHelper
    {
        public static void SaveMediaFile(string filename, byte[] fileData)
        {
            using (System.IO.FileStream fs = new System.IO.FileStream(
                        filename,
                        System.IO.FileMode.Create,
                        System.IO.FileAccess.ReadWrite))
            {
                using (System.IO.BinaryWriter bw = new System.IO.BinaryWriter(fs))
                {
                    bw.Write(fileData);
                    bw.Close();
                }
            }
        }

        public static void ZipPackage(string folderpath, string filename)
        {
            using (ZipFile zip = new ZipFile(filename, Encoding.UTF8))
            {
                zip.AddDirectory(folderpath);
                zip.Save();
            }
        }

        public static void ExtractZipPackage(string pkgfilepath, string extractfolder)
        {
            if (!Directory.Exists(extractfolder))
            {
                using (var zip = ZipFile.Read(pkgfilepath))
                {
                    zip.ExtractAll(extractfolder, ExtractExistingFileAction.OverwriteSilently);
                }
            }
        }

        public static void ExtractZipPackage(byte[] pkgcontent, string extractfolder)
        {
            if (!Directory.Exists(extractfolder))
            {
                MemoryStream stream = new MemoryStream(pkgcontent);
                using (var zip = ZipFile.Read(stream))
                {
                    zip.ExtractAll( extractfolder, ExtractExistingFileAction.OverwriteSilently);
                }
                stream.Close();
            }
        }

        public static byte[] GetMediaFileConent(string filename)
        {
            using (System.IO.FileStream fs = new System.IO.FileStream(
                        filename,
                        System.IO.FileMode.Open,
                        System.IO.FileAccess.Read))
            {
                byte[] fileData = new byte[fs.Length];
                fs.Read(fileData, 0, System.Convert.ToInt32(fs.Length));
                fs.Close();
                return fileData;
            }

        }

        public static byte[] Compress(byte[] data)
        {
            using (MemoryStream zipStream = new MemoryStream())
            {
                //Zip data
                using (ZipOutputStream compressStream = new ZipOutputStream(zipStream, true))
                {
                    ZipEntry entry = compressStream.PutNextEntry("data");
                    entry.CompressionLevel = Ionic.Zlib.CompressionLevel.Level4;
                    compressStream.Write(data, 0, data.Length);
                }

                return zipStream.ToArray();
            }
        }

        public static byte[] DeCompress(byte[] data)
        {
            MemoryStream output = new MemoryStream();
            using (MemoryStream zipStream = new MemoryStream(data))
            {
                //Zip data
                using (ZipInputStream decompressStream = new ZipInputStream(zipStream, true))
                {
                    decompressStream.GetNextEntry();

                    byte[] buffer = new byte[10240];
                    int len = 0;

                    do
                    {
                        len = decompressStream.Read(buffer, 0, buffer.Length);
                        if (len > 0)
                        {
                            output.Write(buffer, 0, len);
                        }
                    } while (len > 0);

                    if (output.Length > 0)
                    {
                        return output.ToArray();
                    }
                }

                return null;
            }
        }
    }
}
