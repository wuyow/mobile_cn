﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Autofac;

namespace ZLIS.Framework.Common
{
    /// <summary>
    /// A work context for work context scope
    /// </summary>
    public class WorkContext : IDisposable
    {
        public static object WorkScopeTag { get; set; }

        readonly IComponentContext _componentContext;
        readonly ConcurrentDictionary<string, Func<object>> _stateResolvers = new ConcurrentDictionary<string, Func<object>>();
        readonly IEnumerable<IWorkContextStateProvider> _workContextStateProviders;

        public WorkContext(IComponentContext ctx)
        {
            _componentContext = ctx;
            _workContextStateProviders = _componentContext.Resolve<IEnumerable<IWorkContextStateProvider>>();
        }

        public IComponentContext Container { get { return _componentContext; } }
        
        /// <summary>
        /// Resolves a registered dependency type
        /// </summary>
        /// <typeparam name="T">The type of the dependency</typeparam>
        /// <returns>An instance of the dependency if it could be resolved</returns>
        public T Resolve<T>()
        {
            return _componentContext.Resolve<T>();
        }

        /// <summary>
        /// Resolves a registered dependency type
        /// </summary>
        /// <typeparam name="T">The type of the dependency</typeparam>
        /// <returns>An instance of the dependency if it could be resolved</returns>
        public T Resolve<T>(params Autofac.Core.Parameter[] parameters)
        {
            return _componentContext.Resolve<T>(parameters);
        }

        /// <summary>
        /// Tries to resolve a registered dependency type
        /// </summary>
        /// <typeparam name="T">The type of the dependency</typeparam>
        /// <param name="service">An instance of the dependency if it could be resolved</param>
        /// <returns>True if the dependency could be resolved, false otherwise</returns>
        public bool TryResolve<T>(out T service)
        {
            return _componentContext.TryResolve(out service);
        }

        public T GetState<T>(string name)
        {
            var resolver = _stateResolvers.GetOrAdd(name, FindResolverForState<T>);
            return (T)resolver();
        }

        Func<object> FindResolverForState<T>(string name)
        {
            var resolver = _workContextStateProviders.Select(wcsp => wcsp.Get<T>(name)).FirstOrDefault(value => !Equals(value, default(T)));

            if (resolver == null)
            {
                return () => default(T);
            }
            return () => resolver(this);
        }


        public void SetState<T>(string name, T value)
        {
            _stateResolvers[name] = () => value;
        }

        public void Dispose()
        {
        }
    }
}
