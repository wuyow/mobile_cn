﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection.Emit;
using System.Reflection;

namespace ZLIS.Framework.Common.Reflection
{

    public class CustomFieldInfo
    {
        public string FieldName { get; set; }
        public Type FieldType { get; set; }
    }

    public static class CustomTypeBuilder
    {
        public static object CreateNewObject(IList<CustomFieldInfo> lst)
        {
            var customType = CompileResultType(lst);
            var obj = Activator.CreateInstance(customType);

            return obj;
        }

        public static Type CompileResultType(IList<CustomFieldInfo> lst)
        {
            TypeBuilder tb = GetTypeBuilder();
            ConstructorBuilder constructor = tb.DefineDefaultConstructor(MethodAttributes.Public |
                MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);

            foreach (var field in lst)
            {
                CreateProperty(tb, field.FieldName, field.FieldType);
            }

            Type objectType = tb.CreateType();
            return objectType;
        }

        private static TypeBuilder GetTypeBuilder()
        {
            var typeSignature = "CustomType";
            var assemblyName = new AssemblyName(typeSignature);
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName,
                AssemblyBuilderAccess.Run);

            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("MainModule");

            TypeBuilder tb = moduleBuilder.DefineType(typeSignature
                , TypeAttributes.Public | TypeAttributes.Class |
                TypeAttributes.AutoClass | TypeAttributes.AnsiClass |
                TypeAttributes.BeforeFieldInit | TypeAttributes.AutoLayout
                , null);

            return tb;
        }

        private static void CreateProperty(TypeBuilder tb, string propertyName, Type propertyType)
        {
            FieldBuilder fieldBuilder = tb.DefineField("_" + propertyName, propertyType, FieldAttributes.Private);

            PropertyBuilder propertyBuilder = tb.DefineProperty(propertyName, PropertyAttributes.HasDefault, propertyType, null);

            MethodBuilder getPropMthdBldr = tb.DefineMethod("get_" + propertyName, MethodAttributes.Public |
                MethodAttributes.SpecialName | MethodAttributes.HideBySig, propertyType, Type.EmptyTypes);

            ILGenerator getIl = getPropMthdBldr.GetILGenerator();
            getIl.Emit(OpCodes.Ldarg_0);
            getIl.Emit(OpCodes.Ldfld, fieldBuilder);
            getIl.Emit(OpCodes.Ret);

            MethodBuilder setPropMthdBldr =
               tb.DefineMethod("set_" + propertyName, MethodAttributes.Public |
               MethodAttributes.SpecialName | MethodAttributes.HideBySig,
               null, new[] { propertyType });

            ILGenerator setIl = setPropMthdBldr.GetILGenerator();

            Label modifyProperty = setIl.DefineLabel(); Label exitSet = setIl.DefineLabel();

            setIl.MarkLabel(modifyProperty); setIl.Emit(OpCodes.Ldarg_0);
            setIl.Emit(OpCodes.Ldarg_1); setIl.Emit(OpCodes.Stfld, fieldBuilder);
            setIl.Emit(OpCodes.Nop); setIl.MarkLabel(exitSet);
            setIl.Emit(OpCodes.Ret); propertyBuilder.SetGetMethod(getPropMthdBldr);

            propertyBuilder.SetSetMethod(setPropMthdBldr);
        }


    }

}
