﻿using System;

namespace ZLIS.Framework.Common.FileSystems.LockFile
{
    public interface ILockFile : IDisposable {
        void Release();
    }
}
