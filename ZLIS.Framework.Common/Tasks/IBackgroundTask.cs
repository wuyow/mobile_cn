﻿
namespace ZLIS.Framework.Common.Tasks
{
    public interface IBackgroundTask {
        void Sweep();
    }
}
