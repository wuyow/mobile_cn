﻿using System;
using System.Timers;
using ZLIS.Framework.Common.Logging;

namespace ZLIS.Framework.Common.Tasks
{

    public interface ISweepGenerator : ISingletonDependency {
        void Activate();
        void Terminate();
    }

    public class SweepGenerator : ISweepGenerator, IDisposable {
        private readonly Timer _timer;
        private IBackgroundService _backgroundService;

        public SweepGenerator(IBackgroundService backgroundService)
        {
            _backgroundService = backgroundService;
            _timer = new Timer();
            _timer.Elapsed += Elapsed;
            Logger = NullLogger.Instance;
            Interval = TimeSpan.FromMinutes(1);
        }

        public ILogger Logger { get; set; }

        public TimeSpan Interval {
            get { return TimeSpan.FromMilliseconds(_timer.Interval); }
            set { _timer.Interval = value.TotalMilliseconds; }
        }

        public void Activate() {
            lock (_timer) {
                _timer.Start();
            }
        }

        public void Terminate() {
            lock (_timer) {
                _timer.Stop();
            }
        }

        void Elapsed(object sender, ElapsedEventArgs e) {
            // current implementation disallows re-entrancy
            if (!System.Threading.Monitor.TryEnter(_timer))
                return;

            try {
                if (_timer.Enabled) {
                    DoWork();
                }
            }
            catch (Exception ex) {
                Logger.Warning(ex, "Problem in background tasks");
            }
            finally {
                System.Threading.Monitor.Exit(_timer);
            }
        }

        public void DoWork()
        {
            _backgroundService.Sweep();
        }

        public void Dispose() {
            _timer.Dispose();
        }
    }
}
