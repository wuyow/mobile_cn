using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using ZLIS.Framework.Common.Logging;
using System.Reflection;

namespace ZLIS.Framework.Common.Tasks
{
    public interface IBackgroundService : IDependency {
        void Sweep();
    }

    [UsedImplicitly]
    public class BackgroundService : IBackgroundService {
        private readonly IEnumerable<IBackgroundTask> _tasks;

        public BackgroundService(IEnumerable<IBackgroundTask> tasks)
        {
            _tasks = tasks;
            Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        public void Sweep() {
            foreach (var task in _tasks.OfType<IBackgroundTask>())
            {
                Logger.Information("Sweep is called {0}", task.ToString());
                try
                {
                    var loggerProperties = task.GetType()
                        .GetProperties(BindingFlags.SetProperty | BindingFlags.Public | BindingFlags.Instance)
                        .Select(p => new
                        {
                            PropertyInfo = p,
                            p.PropertyType,
                            IndexParameters = p.GetIndexParameters(),
                            Accessors = p.GetAccessors(false)
                        })
                        .Where(x => x.PropertyType == typeof(ILogger)) // must be a logger
                        .Where(x => x.IndexParameters.Count() == 0) // must not be an indexer
                        .Where(x => x.Accessors.Length != 1 || x.Accessors[0].ReturnType == typeof(void)); //must have get/set, or only set

                    foreach (var entry in loggerProperties)
                    {
                        var propertyInfo = entry.PropertyInfo;
                        propertyInfo.SetValue(task, Logger, null);
                    }

                    task.Sweep();
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Error while processing background task");
                }

            }
        }
    }
}
