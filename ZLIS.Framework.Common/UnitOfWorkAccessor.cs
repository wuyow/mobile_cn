﻿using System;
using System.Collections;
using NHibernate;
using NHibernate.SqlCommand;
using NHibernate.Type;
using ZLIS.Framework.Common.Data;
using ZLIS.Framework.Common.Logging;
using ZLIS.Lib.Framework.Repository;
using ZLIS.Lib.Framework.Repository.Providers.EntityFramework;
using ZLIS.Lib.Framework.Repository.Providers.NHibernate;
using ITransaction = NHibernate.ITransaction;
using NHibernate.Cfg;

namespace ZLIS.Framework.Common
{
    public interface IUnitOfWorkAccessor : IDisposable
    {
        IUnitOfWork GetUnitOfWork();
    }

    public interface ISessionFactoryHolder
    {
        ISessionFactory GetSessionFactory();
        Configuration GetConfiguration();
    }

    public interface INhUnitOfWorkAccessor : IUnitOfWorkAccessor
    {

    }

    public interface IEfUnitOfWorkAccessor : IUnitOfWorkAccessor
    {

    }

    public class EfUnitOfWorkAccessor : IEfUnitOfWorkAccessor
    {
        private IUnitOfWork _unitOfWork;
        private EntityFrameworkUnitOfWorkFactory _factory;

        public EfUnitOfWorkAccessor(IDbContextFactory dbContextFactory)
        {
            _factory = new EntityFrameworkUnitOfWorkFactory(dbContextFactory);
        }

        public IUnitOfWork GetUnitOfWork()
        {
            if (_unitOfWork == null)
            {
                _unitOfWork = _factory.BeginUnitOfWork();
            }

            return _unitOfWork;
        }

        public void Dispose()
        {
            if (_unitOfWork != null)
            {
                _factory.EndUnitOfWork(_unitOfWork);
            }
        }
    }

    public class NhUnitOfWorkAccessor : INhUnitOfWorkAccessor
    {
        private readonly ISessionFactoryHolder _sessionFactoryHolder;
        private ISession _session;
        private IUnitOfWork _unitOfWork;

        public NhUnitOfWorkAccessor(ISessionFactoryHolder sessionFactoryHolder)
        {
            _sessionFactoryHolder = sessionFactoryHolder;
            Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        #region IUnitOfWorkAccessor Members

        public IUnitOfWork GetUnitOfWork()
        {
            if (_unitOfWork == null)
                _unitOfWork = new NHibernateUnitOfWork(OpenSession());

            return _unitOfWork;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_unitOfWork != null)
            {
                _unitOfWork.Dispose();
                _unitOfWork = null;
            }
        }

        #endregion

        private ISession OpenSession()
        {
            //Logger.Debug("Acquiring session for {0}", entityType);

            if (_session == null)
            {

                var sessionFactory = _sessionFactoryHolder.GetSessionFactory();
                //Logger.Information("Openning database session");
                _session = sessionFactory.OpenSession(new SessionInterceptor());
            }
            return _session;
        }
    }

    class SessionInterceptor : IInterceptor
    {
        private ISession _session;

        bool IInterceptor.OnLoad(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            return false;
        }

        bool IInterceptor.OnFlushDirty(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, IType[] types)
        {
            return false;
        }

        bool IInterceptor.OnSave(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
            return false;
        }

        void IInterceptor.OnDelete(object entity, object id, object[] state, string[] propertyNames, IType[] types)
        {
        }

        void IInterceptor.OnCollectionRecreate(object collection, object key)
        {
        }

        void IInterceptor.OnCollectionRemove(object collection, object key)
        {
        }

        void IInterceptor.OnCollectionUpdate(object collection, object key)
        {
        }

        void IInterceptor.PreFlush(ICollection entities)
        {
        }

        void IInterceptor.PostFlush(ICollection entities)
        {
        }

        bool? IInterceptor.IsTransient(object entity)
        {
            return null;
        }

        int[] IInterceptor.FindDirty(object entity, object id, object[] currentState, object[] previousState, string[] propertyNames, IType[] types)
        {
            return null;
        }

        object IInterceptor.Instantiate(string entityName, EntityMode entityMode, object id)
        {
            return null;
        }

        string IInterceptor.GetEntityName(object entity)
        {
            return null;
        }

        object IInterceptor.GetEntity(string entityName, object id)
        {
            return null;
        }

        void IInterceptor.AfterTransactionBegin(ITransaction tx)
        {
        }

        void IInterceptor.BeforeTransactionCompletion(ITransaction tx)
        {
        }

        void IInterceptor.AfterTransactionCompletion(ITransaction tx)
        {
        }

        SqlString IInterceptor.OnPrepareStatement(SqlString sql)
        {
            return sql;
        }

        void IInterceptor.SetSession(ISession session)
        {
            _session = session;
        }
    }
}