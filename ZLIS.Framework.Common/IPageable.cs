﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Framework.Common
{
    public interface IPageable
    {
        int Total { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; set; }
    }
}
