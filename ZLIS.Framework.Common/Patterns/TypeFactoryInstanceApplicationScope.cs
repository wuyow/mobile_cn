﻿namespace ZLIS.Framework.Common.Patterns
{
    #region Using

    using System;
    using System.Web;

    #endregion

    /// <summary>
    /// The type application factory instance.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public class TypeFactoryInstanceApplicationScope<T> : ITypeFactoryInstance<T>
    {
        #region Constants and Fields

        /// <summary>
        /// The _type instance key.
        /// </summary>
        private string _typeInstanceKey;

        /// <summary>
        /// The _type name.
        /// </summary>
        private string _typeName;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeFactoryInstanceApplicationScope{T}"/> class.
        /// </summary>
        /// <param name="typeName">
        /// The type name.
        /// </param>
        public TypeFactoryInstanceApplicationScope(string typeName)
        {
            this.TypeName = typeName;
            this.TypeInstanceKey = typeName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TypeFactoryInstanceApplicationScope{T}"/> class. 
        /// For derived classes
        /// </summary>
        protected TypeFactoryInstanceApplicationScope()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets TypeInstanceKey.
        /// </summary>
        protected string TypeInstanceKey
        {
            get
            {
                return this._typeInstanceKey;
            }

            set
            {
                this._typeInstanceKey = value;
            }
        }

        /// <summary>
        /// Gets or sets the TypeName.
        /// </summary>
        public string TypeName
        {
            get
            {
                return this._typeName;
            }

            protected set
            {
                this._typeName = value;
            }
        }

        private object _instance = null;

        /// <summary>
        /// Gets or sets ApplicationInstance.
        /// </summary>
        protected T ApplicationInstance
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return this._instance == null ? default(T) : (T)this._instance;
                }

                return (T)(HttpContext.Current.Application[this.TypeInstanceKey] ?? default(T));
            }

            set
            {
                if (HttpContext.Current == null)
                {
                    _instance = value;
                }
                else
                {
                    HttpContext.Current.Application[this.TypeInstanceKey] = value;
                }
            }
        }

        #endregion

        #region Implemented Interfaces

        #region ITypeFactoryInstance<T>

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// </returns>
        public T Get()
        {
            if (Equals(this.ApplicationInstance, default(T)))
            {
                this.ApplicationInstance = this.CreateTypeInstance();
            }

            return this.ApplicationInstance;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// The create type instance.
        /// </summary>
        /// <returns>
        /// Created type instance.
        /// </returns>
        private T CreateTypeInstance()
        {
            Type typeGetType = Type.GetType(this.TypeName);

            // validate this type is proper for this type instance.
            if (typeof(T).IsAssignableFrom(typeGetType))
            {
                return (T)Activator.CreateInstance(typeGetType);
            }

            return default(T);
        }

        #endregion
    }
}
