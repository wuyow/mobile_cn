﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Framework.Common.Patterns
{
    /// <summary>
    /// The type factory instance interface
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public interface ITypeFactoryInstance<T>
    {
        #region Properties

        /// <summary>
        /// Gets TypeName.
        /// </summary>
        string TypeName { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get an instance of the type.
        /// </summary>
        /// <returns>
        /// </returns>
        T Get();

        #endregion
    }
}
