﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Framework.Common.Patterns
{
    #region Using

    using System;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// The single class instance factory.
    /// </summary>
    public class SingleClassInstanceFactory : IInstanceFactory
    {
        #region Constants and Fields

        /// <summary>
        ///   The _context classes.
        /// </summary>
        private readonly Dictionary<int, object> _contextClasses = new Dictionary<int, object>();

        #endregion

        #region Implemented Interfaces

        #region IInstanceFactory

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// </returns>
        public T GetInstance<T>() where T : class
        {
            int objNameHash = typeof(T).ToString().GetHashCode();

            if (!this._contextClasses.ContainsKey(objNameHash))
            {
                this._contextClasses[objNameHash] = Activator.CreateInstance(typeof(T));
            }

            return (T)this._contextClasses[objNameHash];
        }

        /// <summary>
        /// The set instance.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        public void SetInstance<T>(T instance) where T : class
        {
            int objNameHash = typeof(T).ToString().GetHashCode();
            this._contextClasses[objNameHash] = instance;
        }

        #endregion

        #endregion
    }
}
