﻿namespace ZLIS.Framework.Common.Patterns
{
    /// <summary>
    /// The instance factory interface
    /// </summary>
    public interface IInstanceFactory
    {
        #region Public Methods

        /// <summary>
        /// The get instance.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// </returns>
        T GetInstance<T>() where T : class;

        /// <summary>
        /// The set instance.
        /// </summary>
        /// <param name="instance">
        /// The instance.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        void SetInstance<T>(T instance) where T : class;

        #endregion
    }
}
