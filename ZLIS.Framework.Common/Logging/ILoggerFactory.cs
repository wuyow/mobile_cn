﻿using System;

namespace ZLIS.Framework.Common.Logging
{
    public interface ILoggerFactory
    {
        ILogger CreateLogger(Type type);
    }
}
