﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using ZLIS.Framework.Common.Encrypt;

namespace ZLIS.Framework.Common.Security
{
    public class PasswordUtils
    {
        private static int SALT_BYTES = 16;

        public static string EncodePassword(string password, MembershipPasswordFormat passwordFormat, string salt)
        {
            byte[] password_bytes;
            byte[] salt_bytes;

            switch (passwordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    return password;
                case MembershipPasswordFormat.Hashed:
                    password_bytes = Encoding.Unicode.GetBytes(password);
                    salt_bytes = Convert.FromBase64String(salt);

                    byte[] hashBytes = new byte[salt_bytes.Length + password_bytes.Length];

                    Buffer.BlockCopy(salt_bytes, 0, hashBytes, 0, salt_bytes.Length);
                    Buffer.BlockCopy(password_bytes, 0, hashBytes, salt_bytes.Length, password_bytes.Length);

                    using (HashAlgorithm hash = HashAlgorithm.Create("MD5"))
                    {
                        hash.TransformFinalBlock(hashBytes, 0, hashBytes.Length);
                        return Convert.ToBase64String(hash.Hash);
                    }
                case MembershipPasswordFormat.Encrypted:
                    password_bytes = Encoding.Unicode.GetBytes(password);
                    salt_bytes = Convert.FromBase64String(salt);

                    byte[] buf = new byte[password_bytes.Length + salt_bytes.Length];

                    Array.Copy(salt_bytes, 0, buf, 0, salt_bytes.Length);
                    Array.Copy(password_bytes, 0, buf, salt_bytes.Length, password_bytes.Length);

                    return Convert.ToBase64String(EncryptPassword(buf));
                default:
                    /* not reached.. */
                    return null;
            }
        }

        public static string DecodePassword(string password, MembershipPasswordFormat passwordFormat)
        {
            switch (passwordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    return password;
                case MembershipPasswordFormat.Hashed:
                    throw new Exception("Hashed passwords cannot be decoded.");
                case MembershipPasswordFormat.Encrypted:
                    return Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                default:
                    /* not reached.. */
                    return null;
            }
        }

        private static SymmetricAlgorithm GetAlg(out byte[] decryptionKey)
        {
            //MachineKeySection section = (MachineKeySection)WebConfigurationManager.GetSection("system.web/machineKey");

            //if (section.DecryptionKey.StartsWith("AutoGenerate"))
            //    throw new Exception("You must explicitly specify a decryption key in the <machineKey> section when using encrypted passwords.");

            string alg_type = "AES"; ;//section.Decryption;
            if (alg_type == "Auto")
                alg_type = "AES";

            SymmetricAlgorithm alg = null;
            if (alg_type == "AES")
                alg = Rijndael.Create();
            else if (alg_type == "3DES")
                alg = TripleDES.Create();
            else
                throw new Exception(String.Format("Unsupported decryption attribute '{0}' in <machineKey> configuration section", alg_type));

            decryptionKey = Convert.FromBase64String(/*section.DecryptionKey*/"EF975B401B6327D5D773435F2F0172B81CD19C7EB515FA58");
            return alg;
        }

        public static byte[] DecryptPassword(byte[] encodedPassword)
        {
            byte[] decryptionKey;

            using (SymmetricAlgorithm alg = GetAlg(out decryptionKey))
            {
                alg.Key = decryptionKey;

                using (ICryptoTransform decryptor = alg.CreateDecryptor())
                {

                    byte[] buf = decryptor.TransformFinalBlock(encodedPassword, 0, encodedPassword.Length);
                    byte[] rv = new byte[buf.Length - SALT_BYTES];

                    Array.Copy(buf, 16, rv, 0, buf.Length - 16);
                    return rv;
                }
            }
        }

        public static byte[] EncryptPassword(byte[] password)
        {
            byte[] decryptionKey;
            byte[] iv = new byte[SALT_BYTES];

            Array.Copy(password, 0, iv, 0, SALT_BYTES);
            Array.Clear(password, 0, SALT_BYTES);

            using (SymmetricAlgorithm alg = GetAlg(out decryptionKey))
            {
                using (ICryptoTransform encryptor = alg.CreateEncryptor(decryptionKey, iv))
                {
                    return encryptor.TransformFinalBlock(password, 0, password.Length);
                }
            }
        }

        public static string GeneratePasswordSaltBase64()
        {
            RandomNumberGenerator rng = RandomNumberGenerator.Create();
            byte[] salt = new byte[SALT_BYTES];
            rng.GetBytes(salt);
            return Convert.ToBase64String(salt);
        }

        public static string GeneratePasswordSaltHex()
        {
            RandomNumberGenerator rng = RandomNumberGenerator.Create();
            byte[] salt = new byte[SALT_BYTES];
            rng.GetBytes(salt);
            return EncryptHelper.ByteArrayToHexString(salt);
        }

        public static string EncodePassword(string password)
        {
            string passwordSalt = GeneratePasswordSaltHex();
            return EncodePassword(password, MembershipPasswordFormat.Hashed, passwordSalt);
        }
    }
}
