﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Framework.Common.Security
{
    [Serializable]
    public class SimulatedUser
    {
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string OriginalUserName { get; set; }
        public string OriginalUserFullName { get; set; }
        public string WebSessionId { get; set; }
        public DateTime SimulateDateTime { get; set; }
        public string Url { get; set; }
    }
}
