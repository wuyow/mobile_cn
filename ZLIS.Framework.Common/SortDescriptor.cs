﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZLIS.Framework.Common
{
    public enum SortType
    {
        Asc,
        Desc
    }

    public class SortDescriptor
    {
        public string Field { get; set; }
        public SortType SortType { get; set; }
    }
}
