﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Framework.Common.FileSystems.AppData;
using Aspose.Words;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.Core.Common;
using ZLIS.Cos.Core.Settings.Services;

namespace ZLIS.Cos.PdfGenerator
{
    internal static class MyExtensions
    {
        public static string FormatCurrency(this decimal value, string fomat = "###,###.00", string zeroDisplay = ".00")
        {
             if (value == 0)
                return zeroDisplay;
            else
                return (value).ToString(fomat);
        }

        public static string FormatCurrency(this decimal? value, string fomat = "###,###.00", string zeroDisplay = ".00", string nullDisplay = ".00")
        {
            //ISystemSettingService _service = CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();
            if (!value.HasValue)
                return nullDisplay;
            else if (value.Value == 0)
                return zeroDisplay;
            else
                return (value.Value).ToString(fomat);
        }

        public static string FormatString(this int value, string zeroDisplay = "")
        {
            if (value == 0)
                return zeroDisplay;
            else
                return (value).ToString();
        }
    }

    public class DefaultReportGenerater: BaseReportGenerator, IReportGenerater
    {
        public DefaultReportGenerater(ReportContext context) : base(context)
        {
        }

        /// <summary>
        /// Generate Overdue draft PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public virtual byte[] GenerateOverdueReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            OverDueHeaderModel header = GetBaseReportHeaderModel<OverDueHeaderModel>(new OverDueHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetOverDueTables(contractNo, header, out header);
            return GenerateReport(templatepath,string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        /// <summary>
        /// Generate SOA PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contract"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="rptTxnType"></param>
        /// <param name="sdate"></param>
        /// <param name="edate"></param>
        /// <param name="isloc"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public virtual byte[] GenerateSOAReport(string templatename, string downloadname, CmsContractDetail contract, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_SOA_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            SOAHeaderModel header = GetBaseReportHeaderModel<SOAHeaderModel>(new SOAHeaderModel(), contract.ContractNO, marketcode, isloc);
            List<MergeTableModel> tables = GetSOATables(contract, rptTxnType, sdate, edate, header, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }

        /// <summary>
        /// Generate repayment plan PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="fpg"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public virtual byte[] GenerateRepaymentReport(string templatename, string downloadname, string contractNo, string fpg, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_REPAYMENT_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            RepaymentHeaderModel header = GetBaseReportHeaderModel<RepaymentHeaderModel>(new RepaymentHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetRepaymentTables(contractNo, header, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }

        /// <summary>
        /// Generate ET quote PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="tDate"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public virtual byte[] GenerateETReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, DateTime tDate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            ETHeaderModel header = GetBaseReportHeaderModel<ETHeaderModel>(new ETHeaderModel(), contractNo, marketcode, isloc);

            IRmwService _rmwService = _context.RmwService;
            CmsETEstimateExt cmsEtEstimate = (CmsETEstimateExt)_rmwService.GetETQuotation(contractNo, tDate);
            header.EstimatePayOff = cmsEtEstimate.EarlyTerminationAmount.HasValue ? cmsEtEstimate.EarlyTerminationAmount.Value.ToString("###,###.00") : string.Empty;
            header.TerminationDate = cmsEtEstimate.EarlyTerminationDate == null ? string.Empty : _context.Formatter.FormatData("DATE", cmsEtEstimate.EarlyTerminationDate, true);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, null, SaveFormat.Pdf);
        }

        /// <summary>
        /// Generate receipt PDF. Not required in TW
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="receiptno"></param>
        /// <param name="ctx"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public virtual byte[] GenerateReceipt(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, string receiptno, bool isloc)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generate invoice receipt. Not required in TW
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="tDate"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public virtual byte[] GenerateInvoiceReceipt(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, DateTime tDate, bool isloc)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generate tax invoice PDF. not required in TW
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="rptContractId"></param>
        /// <param name="rptTaxInvoiceNo"></param>
        /// <param name="ctx"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public virtual byte[] GenerateTaxInvoice(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, string rptContractId, string rptTaxInvoiceNo, bool isloc)
        {
            throw new NotImplementedException();
        }

        public virtual byte[] GeneratePaymentHistoryReport(string templatename, string downloadname, CmsContractDetail contract, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc)
        {
            throw new NotImplementedException();
        }

        #region Private Methods

        private List<MergeTableModel> GetOverDueTables(string contractNo, OverDueHeaderModel inheader, out OverDueHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;

            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            decimal paytotal = 0;
            decimal misctotal = 0;
            decimal interesttotal = 0;
            decimal paidtotal = 0;

            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            IList<CmsOverDueRental> overdueInstallments = _rmwService.GetOverdueRentalDetail(businessPartner.BpId, contractNo);
            if (overdueInstallments != null && overdueInstallments.Count > 0)
            {
                var paytable = overdueInstallments.Select((a, index) => new OverDuePayTableItemModel()
                {
                    ID = (index + 1).ToString(),
                    Paid = a.PaidAmount.ToString("###,###.00"),
                    PayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                    RentalNo = a.RentalNum.ToString(),
                    PayMonth = a.RentalAmount.ToString("###,###.00"),
                    Remain = a.DueAmount.ToString("###,###.00")
                });
                paytotal = (from a in overdueInstallments select a.DueAmount).Sum();
                table.Items = paytable;
            }
            else
            {
                var eptlst = new List<OverDuePayTableItemModel>();
                eptlst.Add(new OverDuePayTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OtherPayItems";
            IList<CmsMiscCharge> overdueMiscCharges = _rmwService.GetMiscChargesDetails(businessPartner.BpId, contractNo).Where(x => x.DueAmount > 0).ToList();
            if (overdueMiscCharges != null && overdueMiscCharges.Count > 0)
            {
                var misctable = from a in overdueMiscCharges
                                select new OverDueOtherPayTableItemModel()
                                {
                                    OtherPaid = a.SettledAmount.ToString("###,###.00"),
                                    OtherPayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                                    OtherRemain = a.DueAmount.ToString("###,###.00"),
                                    Project = a.ChargeName
                                };
                misctotal = (from a in overdueMiscCharges select a.DueAmount).Sum();
                table.Items = misctable;
            }
            else
            {
                var eptlst = new List<OverDueOtherPayTableItemModel>();
                eptlst.Add(new OverDueOtherPayTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OverDueItems";
            IList<CmsOverDueInterest> overdueInterests = _rmwService.GetOverdueRentalInterest(businessPartner.BpId, contractNo);
            if (overdueInterests != null && overdueInterests.Count > 0)
            {
                var interesttable = from a in overdueInterests
                                    select new OverDueChargeTableItemModel()
                                    {
                                        OverDueType = a.OverdueType,
                                        OverDueTerm = a.RentalNO.ToString(),
                                        OverDueStartDate = a.FromDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.FromDate, true),
                                        OverDueEndDate = a.ToDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.ToDate, true),
                                        OverDueDate = a.OverdueDays.ToString(),
                                        OverDue = a.DueAmount.ToString("###,###.00"),
                                        OverDuePaid = a.PaidWaive.ToString("###,###.00"),
                                        OverDueRemain = a.DueAmount.ToString("###,###.00")
                                    };
                interesttotal = (from a in overdueInterests select a.DueAmount).Sum();
                paidtotal = (from a in overdueMiscCharges select a.PaidWaive).Sum();
                table.Items = interesttable;
            }
            else
            {
                var eptlst = new List<OverDueChargeTableItemModel>();
                eptlst.Add(new OverDueChargeTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            header.MonthTotal = paytotal.ToString("###,###.00");
            header.OtherTotalPay = misctotal.ToString("###,###.00");
            header.OverDuePaidTotal = paidtotal.ToString("###,###.00");
            header.OverDueRemainTotal = interesttotal.ToString("###,###.00");
            header.TotalOverDue = (paytotal + misctotal + interesttotal).ToString("###,###.00");
            return results;
        }

        private List<MergeTableModel> GetSOATables(CmsContractDetail contract, int rptTxnType, DateTime sDate, DateTime eDate, SOAHeaderModel inheader, out SOAHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;

            MergeTableModel table = new MergeTableModel();
            IEnumerable<SOATableItemModel> soaitems = new List<SOATableItemModel>();
            table.TableName = "Items";
            int i = 1;
            decimal totalbalance = 0, openingbalance = 0, totaldebit = 0, totalcredit = 0, misccharge = 0;
            CmsSoaResultSet statementOfAccount = null;
            decimal misc = contract.MiscellaneousCharges == null || contract.MiscellaneousCharges.Count == 0 ? 0 : contract.MiscellaneousCharges.Select(c => c.DueAmount).Sum();
            do
            {
                statementOfAccount = _rmwService.GetSOA(businessPartner.BpId, contract.ContractNO, rptTxnType, i, ReportProcessConstants.NoOfRecordsPerFetch, sDate, eDate);
                if (i == 1)
                {
                    //if (CosApplicationContext.Current.CurrentCountry != "TH")
                    //{
                    //    soaitems = soaitems.Union(new List<SOATableItemModel>(){new SOATableItemModel(){
                    //    TransactionDate = "",
                    //    Type = "",
                    //    Description = "Opening Balance",
                    //    Debit = "",
                    //    Balance = statementOfAccount.OpeningBalance.ToString("###,###.00"),
                    //    }});
                    //}

                    totalbalance = statementOfAccount.ClosingBalance;
                    openingbalance = statementOfAccount.OpeningBalance;
                    misccharge = statementOfAccount.AmountMiscCharge;
                }
                soaitems = soaitems.Union(statementOfAccount.SoaDetails.Select(x => new SOATableItemModel()
                {
                    TransactionDate = x.TransactionDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.TransactionDate, true),
                    Type = x.TransactionType,
                    Description = x.TransactionDescription,
                    Debit = x.Debit == null ? "-" : x.Debit.Value.ToString("###,###.00"),
                    Credit = x.Credit == null ? "-" : x.Credit.Value.ToString("###,###.00"),
                    Balance = x.Balance == null ? "-" : x.Balance.Value.ToString("###,###.00")
                }));
                
                totaldebit += statementOfAccount.SoaDetails.Sum(a => a.Debit) ?? 0;
                totalcredit += statementOfAccount.SoaDetails.Sum(a => a.Credit) ?? 0;
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < statementOfAccount.Total);

            if (soaitems.Count() > 0)
            {
                table.Items = soaitems;
            }else
            {
                var eptlst = new List<SOATableItemModel>();
                eptlst.Add(new SOATableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            header.TotalBalance = totalbalance.ToString("###,###.00");
            header.OpeningBalance = openingbalance.ToString("###,###.00");
            header.TotalCredit = totalcredit.ToString("###,###.00");
            header.TotalDebit = totaldebit.ToString("###,###.00");
            header.MiscCharge = misccharge.ToString("###,###.00");
            header.TransactionPeriod = _context.Formatter.FormatData("DATE", sDate, true) + " - " + _context.Formatter.FormatData("DATE", eDate, true);
            return results;
        }

        private List<MergeTableModel> GetRepaymentTables(string contractNo, RepaymentHeaderModel inheader, out RepaymentHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;

            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsPaymentScheduleResultSet paymentSchedule;
            IEnumerable<RepaymentTableItemModel> items = new List<RepaymentTableItemModel>();
            int i = 1;
            decimal intotal = 0;
            decimal sttotal = 0;
            decimal ptotal = 0, rentalTotal=0;
            int j = 1;
            do
            {
                paymentSchedule = _rmwService.GetPaymentSchedule(businessPartner.BpId, contractNo, i, ReportProcessConstants.NoOfRecordsPerFetch, null, null);
                if (i == 1)
                {
                    header.StartDate = paymentSchedule.StartDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.StartDate, true);
                    header.MaturityDate = paymentSchedule.MaturityDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.MaturityDate, true);
                    header.Term = paymentSchedule.Term.ToString();
                    header.InterestRate = (paymentSchedule.InterestRate ?? 0).ToString();
                }
                items = items.Union(paymentSchedule.PaymentScheduleDetails.Select(x => new RepaymentTableItemModel
                {
                    ID = (j++).ToString(),
                    RentalNo = x.RentalNum.ToString(),
                    DueDate = x.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.DueDate, true),
                    InstallmentAmount = (x.Amount ?? 0).ToString("###,###.00"),
                    SettledAmount = (x.PaidAmount ?? 0).ToString("###,###.00"),
                    RentalAmount = (x.AmountRental ?? 0).ToString("###,###.00"),
                    InterestAmount = (x.AmountInterest ?? 0).ToString("###,###.00"),
                    PrincipalAmount = (x.AmountPrincipal ?? 0).ToString("###,###.00"),
                    PrincipalBalance = (x.AmountPrincipalOutstanding ?? 0).ToString("###,###.00"),
                }));
                intotal += (from a in paymentSchedule.PaymentScheduleDetails select a.Amount ?? 0).Sum();
                sttotal += (from a in paymentSchedule.PaymentScheduleDetails select a.PaidAmount ?? 0).Sum();
                ptotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountPrincipalOutstanding ?? 0).Sum();
                rentalTotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountRental ?? 0).Sum();
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < paymentSchedule.Total);

            table.Items = items;
            results.Add(table);

            var contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);
            if (contract != null)
            {
                header.FinanceType = contract.FinancialDetail.FinanceType;
            }

            header.InstallmentTotal = intotal.ToString("###,###.00");
            header.SettledTotal = sttotal.ToString("###,###.00");
            header.PrincipalTotal = ptotal.ToString("###,###.00");
            header.RentalTotal = rentalTotal.ToString("###,###.00");
            return results;
        }

        #endregion
    }
}