﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.Core.Formatter;
using ZLIS.Cos.Core.ResourceProvider;

namespace ZLIS.Cos.PdfGenerator
{
    public class ReportContext
    {
        public ReportContext() { }
        public ReportContext(IStringFormatter formatter, IRmwService rmwService)
        {
            RmwService = rmwService;
            Formatter = formatter;
        }

        public IStringFormatter Formatter { get; set; }
        public IRmwService RmwService { get; set; }
    }
}
