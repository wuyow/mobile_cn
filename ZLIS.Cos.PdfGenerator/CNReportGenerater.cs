﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using Aspose.Words;
using ZLIS.Cos.Core.Common;

namespace ZLIS.Cos.PdfGenerator
{
    public class CNReportGenerater : DefaultReportGenerater
    {
        public CNReportGenerater(ReportContext context) : base(context) { }

        public override byte[] GenerateOverdueReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            OverDueHeaderModel header = GetBaseReportHeaderModel<OverDueHeaderModel>(new OverDueHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetOverDueTables(contractNo, header, isloc, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        private List<MergeTableModel> GetOverDueTables(string contractNo, OverDueHeaderModel inheader, bool isLoc, out OverDueHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;

            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            decimal paytotal = 0;
            decimal misctotal = 0;
            decimal interesttotal = 0;
            decimal paidtotal = 0;

            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            IList<CmsOverDueRental> overdueInstallments = _rmwService.GetOverdueRentalDetail(businessPartner.BpId, contractNo);
            if (overdueInstallments != null && overdueInstallments.Count > 0)
            {
                var paytable = overdueInstallments.Select((a, index) => new OverDuePayTableItemModel()
                {
                    ID = (index + 1).ToString(),
                    Paid = a.PaidAmount.ToString("###,###.00"),
                    PayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                    RentalNo = a.RentalNum.ToString(),
                    PayMonth = a.RentalAmount.ToString("###,###.00"),
                    Remain = a.DueAmount.ToString("###,###.00")
                });
                paytotal = (from a in overdueInstallments select a.DueAmount).Sum();
                table.Items = paytable;
            }
            else
            {
                var eptlst = new List<OverDuePayTableItemModel>();
                eptlst.Add(new OverDuePayTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OtherPayItems";
            IList<CmsMiscCharge> overdueMiscCharges = _rmwService.GetMiscChargesDetails(businessPartner.BpId, contractNo).Where(x => x.DueAmount > 0).ToList();
            if (overdueMiscCharges != null && overdueMiscCharges.Count > 0)
            {
                var misctable = from a in overdueMiscCharges
                                select new OverDueOtherPayTableItemModel()
                                {
                                    OtherPaid = a.SettledAmount.ToString("###,###.00"),
                                    OtherPayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                                    OtherRemain = a.DueAmount.ToString("###,###.00"),
                                    Project = a.ChargeName
                                };
                misctotal = (from a in overdueMiscCharges select a.DueAmount).Sum();
                table.Items = misctable;
            }
            else
            {
                var eptlst = new List<OverDueOtherPayTableItemModel>();
                eptlst.Add(new OverDueOtherPayTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OverDueItems";
            string odTypeAccrued = new ZLIS.Cos.Core.ResourceProvider.CosResourceProvider("MyAccount_MyAccount_ContractSummary").GetObject("PageLabel_Accrued", new System.Globalization.CultureInfo("zh-Hans")).ToString();
            string odTypeOutstanding = new ZLIS.Cos.Core.ResourceProvider.CosResourceProvider("MyAccount_MyAccount_ContractSummary").GetObject("PageLabel_Outstanding", new System.Globalization.CultureInfo("zh-Hans")).ToString();

            if (odTypeAccrued == "")
                odTypeAccrued = "Accrued";

            if (odTypeOutstanding == "")
                odTypeOutstanding = "Outstanding";

            IList<CmsOverDueInterest> overdueInterests = _rmwService.GetOverdueRentalInterest(businessPartner.BpId, contractNo);
            if (overdueInterests != null && overdueInterests.Count > 0)
            {
                var interesttable = from a in overdueInterests
                                    select new OverDueChargeTableItemModel()
                                    {
                                        OverDueType = (isLoc ? (a.OverdueType.ToLower() == "accrued" ? odTypeAccrued : (a.OverdueType.ToLower() == "outstanding" ? odTypeOutstanding: a.OverdueType)) : a.OverdueType),
                                        OverDueTerm = a.RentalNO.ToString(),
                                        OverDueStartDate = a.FromDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.FromDate, true),
                                        OverDueEndDate = a.ToDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.ToDate, true),
                                        OverDueDate = a.OverdueDays.ToString(),
                                        OverDue = a.DueAmount.ToString("###,###.00"),
                                        OverDuePaid = a.PaidWaive.ToString("###,###.00"),
                                        OverDueRemain = a.DueAmount.ToString("###,###.00")
                                    };
                interesttotal = (from a in overdueInterests select a.DueAmount).Sum();
                paidtotal = (from a in overdueMiscCharges select a.PaidWaive).Sum();
                table.Items = interesttable;
            }
            else
            {
                var eptlst = new List<OverDueChargeTableItemModel>();
                eptlst.Add(new OverDueChargeTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            header.MonthTotal = paytotal.ToString("###,###.00");
            header.OtherTotalPay = misctotal.ToString("###,###.00");
            header.OverDuePaidTotal = paidtotal.ToString("###,###.00");
            header.OverDueRemainTotal = interesttotal.ToString("###,###.00");
            header.TotalOverDue = (paytotal + misctotal + interesttotal).ToString("###,###.00");
            return results;
        }

        public override byte[] GenerateSOAReport(string templatename, string downloadname, CmsContractDetail contract, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            CNSOAHeaderModel header = GetBaseReportHeaderModel<CNSOAHeaderModel>(new CNSOAHeaderModel(), contract.ContractNO, marketcode, isloc);
            List<MergeTableModel> tables = GetSOATables(contract, rptTxnType, sdate, edate, header, isloc, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        private List<MergeTableModel> GetSOATables(CmsContractDetail contract, int rptTxnType, DateTime sDate, DateTime eDate, CNSOAHeaderModel inheader, bool isloc, out CNSOAHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contract.ContractNO).First();
            MergeTableModel table = new MergeTableModel();
            IEnumerable<CNSOATableItemModel> soaitems = new List<CNSOATableItemModel>();
            table.TableName = "Items";
            int i = 1;
            decimal totaldebit = 0;
            decimal totalcredit = 0;
            decimal resamount = 0;
            decimal paymentamount = 0;
            decimal amountreceived = 0;

            decimal closingbalance = 0;

            decimal totalbalance = 0;
            decimal openingbalance = 0;
            decimal misccharge = 0;

            decimal receipttotal = 0;
            decimal principletotal = 0;
            decimal interesttotal = 0;
            decimal overduetotal = 0;
            decimal othertotal = 0;
            decimal grosstotal = 0;
            
            
            CmsSoaResultSet statementOfAccount = null;
            decimal misc = contract.MiscellaneousCharges == null || contract.MiscellaneousCharges.Count == 0 ? 0 : contract.MiscellaneousCharges.Select(c => c.DueAmount).Sum();
            do
            {
                statementOfAccount = _rmwService.GetSOA(businessPartner.BpId, contract.ContractNO, rptTxnType, i, ReportProcessConstants.NoOfRecordsPerFetch, sDate, eDate);
                if (i == 1)
                {
                    closingbalance = statementOfAccount.ClosingBalance;
                    resamount = statementOfAccount.AmountResidual;
                    paymentamount = statementOfAccount.AmountPayment;
                    amountreceived = statementOfAccount.AmountReceived;

                    totalbalance = statementOfAccount.ClosingBalance;
                    openingbalance = statementOfAccount.OpeningBalance;
                    misccharge = statementOfAccount.AmountMiscCharge;
                }
                soaitems = soaitems.Union(statementOfAccount.SoaDetails.Select(x => new CNSOATableItemModel()
                {
                    TransactionDate = x.TransactionDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.TransactionDate, true),
                    Type = x.TransactionType,
                    Description = x.TransactionDescription,
                    Debit = x.Debit == null ? "-" : x.Debit.Value.ToString("###,###.00"),
                    Credit = x.Credit == null ? "-" : x.Credit.Value.ToString("###,###.00"),
                    Balance = x.Balance == null ? "-" : x.Balance.Value.ToString("###,###.00"),

                    RepaymentDate = x.RepaymentDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.RepaymentDate, true),
                    ReceiptAmount = x.AmountReceipt == null ? "-" : x.AmountReceipt.Value.ToString("###,###.00"),
                    PaymentMode = GetPaymentMode(isloc, x.PaymentMode),// x.PaymentMode, 
                    PrincipleAmount = x.AmountPrincipal == null ? "-" : x.AmountPrincipal.Value.ToString("###,###.00"),
                    InterestAmount = x.AmountInterest == null ? "-" : x.AmountInterest.Value.ToString("###,###.00"),
                    OverDueAmount = x.AmountOverdueInterest == null ? "-" : x.AmountOverdueInterest.Value.ToString("###,###.00"),
                    OtherAmount = x.AmountOthers == null ? "-" : x.AmountOthers.Value.ToString("###,###.00"),
                    GrossOutstanding = x.AmountGrossOutstanding == null ? "-" : x.AmountGrossOutstanding.Value.ToString("###,###.00")
                }));
                totaldebit += statementOfAccount.SoaDetails.Sum(a => a.Debit) ?? 0;
                totalcredit += statementOfAccount.SoaDetails.Sum(a => a.Credit) ?? 0;
                
                receipttotal += statementOfAccount.SoaDetails.Sum(a => a.AmountReceipt) ?? 0;
                principletotal += statementOfAccount.SoaDetails.Sum(a => a.AmountPrincipal) ?? 0;
                interesttotal += statementOfAccount.SoaDetails.Sum(a => a.AmountInterest) ?? 0;
                overduetotal += statementOfAccount.SoaDetails.Sum(a => a.AmountOverdueInterest) ?? 0;
                othertotal += statementOfAccount.SoaDetails.Sum(a => a.AmountOthers) ?? 0;
                grosstotal += statementOfAccount.SoaDetails.Sum(a => a.AmountGrossOutstanding) ?? 0;
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < statementOfAccount.Total);


            if (soaitems.Count() > 0)
            {
                table.Items = soaitems;
            }
            else
            {
                var eptlst = new List<CNSOATableItemModel>();
                eptlst.Add(new CNSOATableItemModel()
                {
                    RepaymentDate = "",
                    ReceiptAmount = "",
                    PaymentMode = "",
                    PrincipleAmount = "",
                    InterestAmount = "",
                    OverDueAmount = "",
                    GrossOutstanding = "",
                    OtherAmount = "" 
                });
                table.Items = eptlst;
            }

            results.Add(table);


            header.TransactionPeriod = _context.Formatter.FormatData("DATE", sDate, true) + " - " + _context.Formatter.FormatData("DATE", eDate, true);

            header.ReceiptTotal = receipttotal.ToString("###,###.00");
            header.PrincipleTotal = principletotal.ToString("###,###.00");
            header.InterestTotal = interesttotal.ToString("###,###.00");
            header.OverDueTotal = overduetotal.ToString("###,###.00");
            header.OtherTotal = othertotal.ToString("###,###.00");
            header.GrossOutstandingTotal = grosstotal.ToString("###,###.00");

            header.TotalCredit = totalcredit.ToString("###,###.00");
            header.TotalDebit = totaldebit.ToString("###,###.00");

            return results;
        }

        public override byte[] GenerateRepaymentReport(string templatename, string downloadname, string contractNo, string fpg, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_REPAYMENT_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            CNRepaymentHeaderModel header = GetBaseReportHeaderModel<CNRepaymentHeaderModel>(new CNRepaymentHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetRepaymentTables(contractNo, header, isloc, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }

        private List<MergeTableModel> GetRepaymentTables(string contractNo, CNRepaymentHeaderModel inheader, bool isLoc, out CNRepaymentHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;

            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsPaymentScheduleResultSet paymentSchedule;
            IEnumerable<RepaymentTableItemModel> items = new List<RepaymentTableItemModel>();
            int i = 1;
            decimal intotal = 0;
            decimal sttotal = 0;
            decimal ptotal = 0;
            decimal rentalTotal = 0;
            decimal interestTotal = 0;

            int j = 1;
            do
            {
                paymentSchedule = _rmwService.GetPaymentSchedule(businessPartner.BpId, contractNo, i, ReportProcessConstants.NoOfRecordsPerFetch, null, null);
                if (i == 1)
                {
                    //header.StartDate = paymentSchedule.StartDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.StartDate, true);
                    //header.MaturityDate = paymentSchedule.MaturityDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.MaturityDate, true);
                    header.Term = paymentSchedule.Term.ToString();
                    //header.InterestRate = (paymentSchedule.InterestRate ?? 0).ToString();

                    header.ResidualAmount = (paymentSchedule.AmountResidual ?? 0).ToString("###,###.00");
                    header.PaymentFrequency = paymentSchedule.PaymentFrequency;
                    header.PaymentFrequency = GetPaymentFrequency(isLoc, paymentSchedule.PaymentFrequency);
                }
                items = items.Union(paymentSchedule.PaymentScheduleDetails.Select(x => new RepaymentTableItemModel
                {
                    ID = (j++).ToString(),
                    RentalNo = x.RentalNum.ToString(),
                    DueDate = x.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.DueDate, true),
                    InstallmentAmount = (x.Amount ?? 0).ToString("###,###.00"),
                    SettledAmount = (x.PaidAmount ?? 0).ToString("###,###.00"),
                    RentalAmount = (x.AmountRental ?? 0).ToString("###,###.00"),
                    InterestAmount = (x.AmountInterest ?? 0).ToString("###,###.00"),
                    PrincipalAmount = (x.AmountPrincipal ?? 0).ToString("###,###.00"),
                    PrincipalBalance = (x.AmountPrincipalOutstanding ?? 0).ToString("###,###.00"),
                }));
                intotal += (from a in paymentSchedule.PaymentScheduleDetails select a.Amount ?? 0).Sum();
                sttotal += (from a in paymentSchedule.PaymentScheduleDetails select a.PaidAmount ?? 0).Sum();
                ptotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountPrincipal ?? 0).Sum();
                rentalTotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountRental ?? 0).Sum();
                interestTotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountInterest ?? 0).Sum();
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < paymentSchedule.Total);

            table.Items = items;
            results.Add(table);

            var contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);
            if (contract != null)
            {
                //header.FinanceType = contract.FinancialDetail.FinanceType;
                header.FinanceAmount = contract.FinancialDetail.FinancedAmount.ToString("###,###.00");
            }

            header.InstallmentTotal = intotal.ToString("###,###.00");
            header.SettledTotal = sttotal.ToString("###,###.00");
            header.PrincipalTotal = ptotal.ToString("###,###.00");
            header.RentalTotal = rentalTotal.ToString("###,###.00");
            header.InterestTotal = interestTotal.ToString("###,###.00");
            return results;
        }

        private string GetPaymentMode(bool isLoc, string paymentMode)
        {
            if (string.IsNullOrEmpty(paymentMode))
                return string.Empty;

            if (!isLoc)
                return paymentMode;

            string paymentModeLoc = "";

            switch (paymentMode.ToLower())
            {
                case "eft-ccb":
                    paymentModeLoc = new ZLIS.Cos.Core.ResourceProvider.CosResourceProvider("MyAccount_SOA_SOA").GetObject("PageLabel_EFTCCB", new System.Globalization.CultureInfo("zh-Hans")).ToString();
                    break;

                case "eft-icbc":
                    paymentModeLoc = new ZLIS.Cos.Core.ResourceProvider.CosResourceProvider("MyAccount_SOA_SOA").GetObject("PageLabel_EFTICBC", new System.Globalization.CultureInfo("zh-Hans")).ToString();
                    break;

                case "dealer deduction":
                    paymentModeLoc = new ZLIS.Cos.Core.ResourceProvider.CosResourceProvider("MyAccount_SOA_SOA").GetObject("PageLabel_DealerDeduction", new System.Globalization.CultureInfo("zh-Hans")).ToString();
                    break;

                case "direct debit":
                    paymentModeLoc = new ZLIS.Cos.Core.ResourceProvider.CosResourceProvider("MyAccount_SOA_SOA").GetObject("PageLabel_DirectDebit", new System.Globalization.CultureInfo("zh-Hans")).ToString();
                    break;
                    
            }

            return (paymentModeLoc == "") ? paymentMode : paymentModeLoc;
        }

        private string GetPaymentFrequency(bool isLoc, string paymentFrequency)
        {
            if (string.IsNullOrEmpty(paymentFrequency))
                return string.Empty;

            if (!isLoc)
                return paymentFrequency;

            string paymentFrequencyLoc = "";

            switch (paymentFrequency.ToLower())
            {
                case "monthly":
                    paymentFrequencyLoc = new ZLIS.Cos.Core.ResourceProvider.CosResourceProvider("MyAccount_MyAccount_RepaymentPlan").GetObject("PageLabel_Monthly", new System.Globalization.CultureInfo("zh-Hans")).ToString();
                    break;
                 
            }

            return (paymentFrequencyLoc == "") ? paymentFrequency : paymentFrequencyLoc;
        }
    }
}