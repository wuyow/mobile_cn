﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZLIS.Cos.PdfGenerator
{
    public class DecimalItem
    {

        private decimal? _val;

        public DecimalItem(decimal? value)
        {
            Value = value ?? 0;
        }

        public decimal Value
        {
            get { return _val ?? 0; }
            set { _val = value; }
        }
        public string Text { get { return Value.ToString("###,###.00"); } }
    }

    public class ReportHeaderModel
    {
        public string CustomerName{get;set;}
        public string Address { get; set; }
        public string PrintDate { get; set; }
        public string IdentifyCode { get; set; }
        public string ContractNo { get; set; }
        public string VehicleRegNo { get; set; }
        public string IdentityType { get; set; }

        public string ContractStartDate { get; set; }
        public string ContractEndDate { get; set; }

        public string FinancialProduct { get; set; } 

        public string FinancialProductGroup { get; set; }

        [Obsolete("re-mapped to FinancialProductGroup", false)]
        public string FinanceProduct { get; set; }
        

        public string VehicleDescription { get; set; }
        
    }

    public class OverDueHeaderModel:ReportHeaderModel
    {
        public string MonthTotal { get; set; }
        public string MonthPaidTotal { get; set; }
        public string MonthGrossTotal { get; set; }
        public string OtherTotalPay { get; set; }
        public string OtherTotalPaid { get; set; }
        public string OtherTotalCharge { get; set; }
        public string TotalOverDue { get; set; }
        public string OverDuePaidTotal { get; set; }
        public string OverDueTotal { get; set; }
        public string OverDueTaxTotal { get; set; }
        public string OverDueRemainTotal { get; set; }
    }

    public class ETHeaderModel : ReportHeaderModel
    {
        public string TerminationDate { get; set; }
        public string EstimatePayOff { get; set; }
    
    }

    public class RepaymentHeaderModel : ReportHeaderModel
    {
        public string FinanceType { get; set; }
        public string StartDate { get; set; }
        public string MaturityDate { get; set; }
        public string Term { get; set; }
        public string InstallmentTotal { get; set; }
        public string SettledTotal { get; set; }
        public string PrincipalTotal { get; set; }
        public string RentalTotal { get; set; }
        public string InterestRate { get; set; }
    }

    public class THRepaymentHeaderModel : RepaymentHeaderModel
    {
        public string StatementDate { get; set; }
        public string FinanceProduct { get; set; }
        public string VehicleDescription { get; set; }
        public string FinanceAmount { get; set; }
        public string TermCharge { get; set; }
        public string RentalMode { get; set; }
        public string ResidualAmount { get; set; }
        public string PaymentFrequency { get; set; }
        public string InterestTotal { get; set; }
        public string NetTotal { get; set; }
        public string InsuranceTotal { get; set; }
        public string RegistrationTotal { get; set; }
        public string NetVatTotal { get; set; }
        public string GrossTotal { get; set; }
        public string GrossPaidTotal { get; set; }
        public string CancelledTotal { get; set; }
        public string SundryTotal { get; set; }
    }

   
    public class SOAHeaderModel : ReportHeaderModel
    {
        public string TransactionPeriod { get; set; }
        public string TotalBalance { get; set; }

        public string AmountResidual { get; set; }
        public string AmountPayment { get; set; }
        public string AmountReceived { get; set; }

        public string OpeningBalance { get; set; }
        
        public string MiscCharge { get; set; }

        public string TotalDebit { get; set; }
        public string TotalCredit { get; set; }
    }

    public class THSOAHeaderModel : ReportHeaderModel
    {
        public string VehicleReg { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string FinanceAmount { get; set; }
        public string ResidualAmount { get; set; }
        public string PaymentAmount { get; set; }
        public string DueDate { get; set; }
        public string PaymentReceived { get; set; }
        public string OverDueAmount { get; set; }
        public string TotalDebit { get; set; }
        public string TotalCredit { get; set; }

    }

    public class ReceiptHeaderModel : ReportHeaderModel
    {
        public string ReceiptDate { get; set; }
        public string ReceiptNo { get; set; }
        public string TaxNo { get; set; }
        public string ProductType { get; set; }
        public string LicensePlate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string TotalAmount { get; set; }
        public string TotalAmountText { get; set; }

    }

    public class MYReceiptHeaderModel : ReportHeaderModel
    {
        public string ReceiptNo { get; set; }
        public string ReceiptStatus { get; set; }
        public string ReceiptDate { get; set; }
        public string TotalAmount { get; set; }
    }

    public class ReceiptTableItemModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string Term { get; set; }
        public string Amount { get; set; }
    }

    public class TaxInvoiceReceiptHeaderModel : ReportHeaderModel
    {
        public string ReceiptDate { get; set; }
        public string ReceiptNo { get; set; }
        public string TaxId { get; set; }
        public string ProductType { get; set; }
        public string LicensePlate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string TotalAmount { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentDetail { get; set; }

    }

    public class TaxInvoiceReceiptTableItemModel
    {
        public string Description { get; set; }
        public string Term { get; set; }
        public string Amount { get; set; }
    }

    public class TaxInvoiceHeaderModel : ReportHeaderModel
    {
        public string InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        //public string TaxId { get; set; }
        public string ProductType { get; set; }
        public string LicensePlate { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string TotalAmount { get; set; }
        public string TaxAddress { get; set; }
        public string TotalAmountText { get; set; }

    }

    public class TaxInvoiceTableItemModel
    {
        public string Description { get; set; }
        public string Term { get; set; }
        public string Amount { get; set; }
    }

    public class MergeTableModel
    {
        public string TableName { get; set; }
        public IEnumerable<object> Items { get; set; }
    }

    public class OverDuePayTableItemModel
    {
        public string ID { get; set; }
        public string RentalNo { get; set; }
        public string PayDate { get; set; }
        public string PayMonth { get; set; }
        public string Paid { get; set; }
        public string Remain { get; set; }
        public string GrossAmount { get; set; }
    }

    public class OverDueOtherPayTableItemModel
    {
        public string ID { get; set; }
        public string Project { get; set; }
        public string OtherPayDate { get; set; }
        public string OtherPaid { get; set; }
        public string OtherRemain { get; set; }
        public string OtherPayCharge { get; set; }
        public string OtherTaxAmount { get; set; }
    }

    public class OverDueChargeTableItemModel
    {
        public string ID { get; set; }
        public string OverDueType { get; set; }
        public string OverDueTerm { get; set; }
        public string OverDueStartDate { get; set; }
        public string OverDueEndDate { get; set; }
        public string OverDueDate { get; set; }
        public string OverDue { get; set; }
        public string OverDuePaid { get; set; }
        public string OverDueRemain { get; set; }
        public string OverDueRate { get; set; }
        public string OverDueAmount { get; set; }
        public string OverDueTaxAmount { get; set; }
        public string OverDuePayDate { get; set; }
    }

    public class SOATableItemModel
    {
        public string TransactionDate { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string Balance { get; set; }
    }

    public class RepaymentTableItemModel
    {
        public string ID { get; set; }
        public string RentalNo { get; set; }
        public string DueDate { get; set; }
        public string InstallmentAmount { get; set; }
        public string SettledAmount { get; set; }
        public string RentalAmount { get; set; }
        public string InterestAmount { get; set; }
        //public string PrincipalRentalAmount { get; set; }
        public string PrincipalAmount { get; set; }
        public string PrincipalBalance { get; set; }
        public string NetRentalAmount { get; set; }
        public string InsuranceAmount { get; set; }
        public string RegistrationAmount { get; set; }
        public string NetVatAmount { get; set; }
        public string GrossRentalAmount { get; set; }
        public string SundryCharge { get; set; }
        public string GrossPaid { get; set; }
        public string Cancelled { get; set; }
        public string SurchargeVAT { get; set; }
        public string ServiceTax { get; set; }
        public string EcessAmount { get; set; }
        public string SAHSAmount { get; set; }
    }

    public class PaymentHistoryHeaderModel : ReportHeaderModel
    {
        public string Term { get; set; }
        public string InterestRate { get; set; }

        public string FinancedAmount { get; set; }
        public string InitSecurityDeposit { get; set; }
        public string Prepayment { get; set; }
        public string GResidualValue { get; set; }
        public string NGResidualValue { get; set; }

        public string TotalTotalByPaidDate { get; set; }
        public string TotalPrincipal { get; set; }
        public string TotalInterest { get; set; }
        public string TotalOverdue { get; set; }
        public string TotalCarTax { get; set; }
        public string TotalFineTicket { get; set; }
        public string TotalChargeAmount { get; set; }
        public string TotalTotal { get; set; }
        public string TotalMonthlyRental { get; set; }
    }

    public class PaymentHistoryTableItemModel
    {
        public string ReceiptDate { get; set; }
        public string TotalByPaidDate { get; set; }
        public string PaymentNo { get; set; }
        public string Principal { get; set; }
        public string Interest { get; set; }
        public string ODInterest { get; set; }
        public string CarTax { get; set; }
        public string FineTicket { get; set; }
        public string ChargeAmount { get; set; }
        public string ChargeType { get; set; }
        public string RentalNo { get; set; }
        public string MonthlyRental { get; set; }
        public string Total { get; set; }
        
    }

    #region TH Model

    public class THETHeaderModel : ETHeaderModel
    {
        //public string IdentifyCode { get; set; }
        public string VehicleDescription { get; set; }
        public string VehicleRegistrationNo { get; set; }

    }

    #endregion

    #region CN Model

    public class CNSOAHeaderModel : SOAHeaderModel
    {
        public string GrossOutstandingTotal { get; set; }
        public string ReceiptTotal { get; set; }
        public string PrincipleTotal { get; set; }
        public string InterestTotal { get; set; }
        public string OverDueTotal { get; set; }
        public string OtherTotal { get; set; }
    }

    public class CNSOATableItemModel : SOATableItemModel
    {
        public string RepaymentDate { get; set; }
        public string ReceiptAmount { get; set; }
        public string PaymentMode { get; set; }
        public string PrincipleAmount { get; set; }
        public string InterestAmount { get; set; }
        public string OverDueAmount { get; set; }
        public string GrossOutstanding { get; set; }
        public string OtherAmount { get; set; }
    }

    public class CNRepaymentHeaderModel : RepaymentHeaderModel
    {
        public string ResidualAmount { get; set; }
        public string PaymentFrequency { get; set; }
        public string FinanceAmount { get; set; }
        public string InterestTotal { get; set; }
        public string NetVatTotal { get; set; }
    }

    #endregion

    #region IN Model

    public class INOverDueHeaderModel : OverDueHeaderModel
    {
        public string RentalReceivable { get; set; }
        public string ReceivableCharges { get; set; }
        public string OverdueRental { get; set; }
    }

    public class INRepaymentHeaderModel : RepaymentHeaderModel
    {
        public string StatementDate { get; set; }
        public string FinanceProduct { get; set; }
        public string VehicleDescription { get; set; }
        public string FinanceAmount { get; set; }
        public string FinancedAmount { get; set; }
        public string TermCharge { get; set; }
        public string RentalMode { get; set; }
        public string ResidualAmount { get; set; }
        public string PaymentFrequency { get; set; }
        public string InterestTotal { get; set; }
        public string NetTotal { get; set; }
        public string InsuranceTotal { get; set; }
        public string RegistrationTotal { get; set; }
        public string NetVatTotal { get; set; }
        public string GrossTotal { get; set; }
        public string GrossPaidTotal { get; set; }
        public string CancelledTotal { get; set; }
        public string SundryTotal { get; set; }
        public string SurchargeVATTotal { get; set; }
        public string PreviousStartDate { get; set; }
        public string ServiceTaxTotal { get; set; }
        public string EcessTotal { get; set; }
        public string SAHSTotal { get; set; }
    }

    public class INSOAHeaderModel : SOAHeaderModel
    {
        public string CoborrowerName { get; set; }
        public string GuarantorName { get; set; }
        public string Tenure { get; set; }
        public string InstallmentMode { get; set; }
        public string InstallmentAmount { get; set; }
        public string InstallmentPaid { get; set; }
        public string FutureInstallment { get; set; }
        public string InstallmentOverdue { get; set; }
        public string InstallmentDueTillDate { get; set; }
        public string InstallmentReceivedTillDate { get; set; }
        //public string VehicleRegNo { get; set; }
        public string EngineNo { get; set; }
        public string ChassisNo { get; set; }
        public string AssetDesc { get; set; }
        public string Linked { get; set; }
        public string LoanStatus { get; set; }
        public string PrincipalOutstandingAmount { get; set; }
        public string FinancedAmount { get; set; }
    }

    #endregion

    #region KR Model

    public class KRRepaymentHeaderModel : RepaymentHeaderModel
    {
        public string FinanceAmount { get; set; }
        public string FinancedAmount { get; set; }
        public string InitSecurityDeposit { get; set; }
        public string Prepayment { get; set; }
        public string GResidualValue { get; set; }
        public string NGResidualValue { get; set; }
        public string InterestTotal { get; set; }
        public string SundryTotal { get; set; }
        public string PrepaymentTotal { get; set; }
        public string NetRentalTotal { get; set; }
        public string CarTaxTotal { get; set; }
        public string InsuranceTotal { get; set; }
    }

    public class KRRepaymentTableItemModel : RepaymentTableItemModel
    {
        
        public string PayDate { get; set; } // Payment Date
        public string CarTax { get; set; }
        public string Prepayment { get; set; }
        public string InsuranceInterest { get; set; }
        public string PrincipalOutstanding { get; set; }
        
    }

    public class KRReceiptTableItemModel : ReceiptTableItemModel
    {
        public string ReceiptDate { get; set; }
        public string RentalNo { get; set; }
        public int RentalNoInt { get; set; }
        internal decimal d_Amount { get; set; }
        internal int Order { get; set; }
    }
    
    #endregion

    #region JP Model

    public class JPRepaymentHeaderModel : RepaymentHeaderModel
    {
        public string PayerName { get; set; }
        public string PayerType { get; set; }
        public string ContractType { get; set; }
        public string VinNo { get; set; }
        public string Model { get; set; }
        public string PaymentMethod { get; set; }
        public string BankAccount { get; set; }
        public string ContractTerm { get; set; }
        public string Bank { get; set; }
        public string TotalMonthlyPaymentVATExcl { get; set; }
        public string TotalMonthlyVAT { get; set; }
        public string TotalAmount { get; set; }
    }

    public class JPRepaymentTableItemModel : RepaymentTableItemModel
    {
        public string YearMonth { get; set; }
        public string PayDate { get; set; }
        public string MonthlyPaymentVATExcl { get; set; }
        public string MonthlyVAT { get; set; }
        public string Amount { get; set; }
        public string PaymentMethod { get; set; }
        public int Sequence { get; set; }
        public int DataType { get; set; }
    }
    #endregion  
}