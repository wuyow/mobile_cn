﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Framework.Common.FileSystems.AppData;
using Aspose.Words;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.Core.Common;

namespace ZLIS.Cos.PdfGenerator
{
    public class KRReportGenerater : DefaultReportGenerater, IReportGenerater
    {
          public KRReportGenerater(ReportContext context)
            : base(context)
        {
        }

        public override byte[] GenerateOverdueReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            OverDueHeaderModel header = GetBaseReportHeaderModel<OverDueHeaderModel>(new OverDueHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetOverDueTables(contractNo, header, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        public override byte[] GenerateSOAReport(string templatename, string downloadname, CmsContractDetail contract, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();

            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, string.Format("{0}_{1}", templatename, GetSubTemplate(contract.ContractNO)), prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_REPAYMENT_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname.Replace(ReportConstants.KEY_SOA, "PaymentHistory"));

            THSOAHeaderModel header = GetBaseReportHeaderModel<THSOAHeaderModel>(new THSOAHeaderModel(), contract.ContractNO, marketcode, isloc);
            List<MergeTableModel> tables = GetSOATables(contract, rptTxnType, sdate, edate, header,isloc, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }

        public override byte[] GenerateRepaymentReport(string templatename, string downloadname, string contractNo, string fpg, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            //var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            //var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);

            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, string.Format("{0}_{1}", templatename, GetSubTemplate(contractNo)), prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_REPAYMENT_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);

            KRRepaymentHeaderModel header = GetBaseReportHeaderModel<KRRepaymentHeaderModel>(new KRRepaymentHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetRepaymentTables(contractNo, header,isloc, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }

        /// <summary>
        /// Generate ET quote PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="tDate"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public override byte[] GenerateETReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, DateTime tDate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            THETHeaderModel header = GetBaseReportHeaderModel<THETHeaderModel>(new THETHeaderModel(), contractNo, marketcode, isloc);

            IRmwService _rmwService = _context.RmwService;
            CmsETEstimateExt cmsEtEstimate = (CmsETEstimateExt)_rmwService.GetETQuotation(contractNo, tDate);
            header.EstimatePayOff = cmsEtEstimate.EarlyTerminationAmount.ToString();
            header.TerminationDate = cmsEtEstimate.EarlyTerminationDate == null ? string.Empty : _context.Formatter.FormatData("DATE", cmsEtEstimate.EarlyTerminationDate, true);
            ISessionState session = CosApplicationContext.Current.Session;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            CmsContractDetail contract = _rmwService.GetContractDetails(businessPartner.BpId,contractNo);
            var vehicle = contract.ContractAsset.FirstOrDefault();
            header.VehicleDescription = isloc ? (string.IsNullOrEmpty(vehicle.VehicleTypeDescription_Local) ? vehicle.VehicleTypeDescription : vehicle.VehicleTypeDescription_Local) : vehicle.VehicleTypeDescription;
            header.VehicleRegistrationNo = isloc ? (string.IsNullOrEmpty(vehicle.VehicleRegistrationNO_Local) ? vehicle.VehicleRegistrationNO : vehicle.VehicleRegistrationNO_Local) : vehicle.VehicleRegistrationNO;
            return GenerateReport(templatepath, string.Empty, downloadpath, header, null, SaveFormat.Pdf);
        }

        /// <summary>
        /// Generate Receipt PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="receiptno"></param>
        /// <param name="ctx"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public override byte[] GenerateReceipt(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge,string receiptno, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            ReceiptHeaderModel header = GetBaseReportHeaderModel<ReceiptHeaderModel>(new ReceiptHeaderModel(), contractNo, marketcode, isloc);

            List<MergeTableModel> tables = GetReceiptTables(contractNo,receiptno, header,isloc, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        public override byte[] GeneratePaymentHistoryReport(string templatename, string downloadname, CmsContractDetail contract, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();

            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, string.Format("{0}_{1}", templatename, GetSubTemplate(contract.ContractNO)), prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_REPAYMENT_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname.Replace(ReportConstants.KEY_SOA, "PaymentHistory"));

            PaymentHistoryHeaderModel header = GetBaseReportHeaderModel<PaymentHistoryHeaderModel>(new PaymentHistoryHeaderModel(), contract.ContractNO, marketcode, isloc);
            List<MergeTableModel> tables = GetPaymentHistoryTables(contract, rptTxnType, sdate, edate, header, isloc, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }
        

        #region Private Methods

        private List<MergeTableModel> GetOverDueTables(string contractNo, OverDueHeaderModel inheader, out OverDueHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            decimal monthtotal = 0;
            decimal monthpaidtotal = 0;
            decimal misctotal = 0;
            decimal misccharge = 0;
            decimal miscpaid = 0;
            decimal interesttotal = 0;
            decimal paidtotal = 0;

            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            IList<CmsOverDueRental> overdueInstallments = _rmwService.GetOverdueRentalDetail(businessPartner.BpId, contractNo);
            if (overdueInstallments != null && overdueInstallments.Count > 0)
            {
                var paytable = overdueInstallments.Select((a, index) => new OverDuePayTableItemModel()
                {
                    ID = (index + 1).ToString(),
                    Paid = a.PaidAmount.FormatCurrency("###,##0","-"),
                    PayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                    RentalNo = a.RentalNum.FormatString(""),
                    PayMonth = a.RentalAmount.FormatCurrency("###,##0","-"),
                    Remain = a.DueAmount.FormatCurrency("###,##0","-")
                });
                monthtotal = (from a in overdueInstallments select a.DueAmount).Sum();
                monthpaidtotal = (from a in overdueInstallments select a.PaidAmount).Sum();
                table.Items = paytable;
            }
            else
            {
                var eptlst = new List<OverDuePayTableItemModel>();
                eptlst.Add(new OverDuePayTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OtherPayItems";
            IList<CmsMiscCharge> overdueMiscCharges = _rmwService.GetMiscChargesDetails(businessPartner.BpId, contractNo).Where(x => x.DueAmount > 0).ToList();
            if (overdueMiscCharges != null && overdueMiscCharges.Count > 0)
            {
                var misctable = overdueMiscCharges.Select((a, index) => new OverDueOtherPayTableItemModel()
                                {
                                    ID = (index + 1).ToString(),
                                    OtherPaid = a.SettledAmount.FormatCurrency("###,##0","-"),
                                    OtherPayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                                    OtherPayCharge = a.ChargeAmount.FormatCurrency("###,##0","-"),
                                    OtherRemain = a.DueAmount.FormatCurrency("###,##0","-"),
                                    Project = a.ChargeName
                                });
                misctotal = (from a in overdueMiscCharges select a.DueAmount).Sum();
                miscpaid = (from a in overdueMiscCharges select a.SettledAmount).Sum();
                misccharge = (from a in overdueMiscCharges select a.ChargeAmount).Sum();
                table.Items = misctable;
            }
            else
            {
                var eptlst = new List<OverDueOtherPayTableItemModel>();
                eptlst.Add(new OverDueOtherPayTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OverDueItems";
            IList<CmsOverDueInterest> overdueInterests = _rmwService.GetOverdueRentalInterest(businessPartner.BpId, contractNo);
            if (overdueInterests != null && overdueInterests.Count > 0)
            {
                var interesttable = overdueInterests.Select((a, index) => new OverDueChargeTableItemModel()
                                    {
                                        ID = (index + 1).ToString(),
                                        OverDueType = a.OverdueType,
                                        OverDueTerm = a.RentalNO == 0 ? "" : a.RentalNO.ToString(),
                                        OverDueStartDate = a.FromDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.FromDate, true),
                                        OverDueEndDate = a.ToDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.ToDate, true),
                                        OverDueDate = a.OverdueDays.ToString(),
                                        OverDue = a.DueAmount.FormatCurrency("###,##0","-"),
                                        OverDuePaid = a.PaidWaive.FormatCurrency("###,##0","-"),
                                        OverDueRemain = a.DueAmount.FormatCurrency("###,##0","-"),
                                        OverDuePayDate = 
                                            overdueInstallments.Where(x=> x.RentalNum == a.RentalNO).Any() ? 
                                            _context.Formatter.FormatData("DATE", overdueInstallments.Where(x => x.RentalNum == a.RentalNO).First().DueDate) : string.Empty
                                    });
                interesttotal = (from a in overdueInterests select a.DueAmount).Sum();
                paidtotal = (from a in overdueInterests select a.PaidWaive).Sum();
                table.Items = interesttable;
            }
            else
            {
                var eptlst = new List<OverDueChargeTableItemModel>();
                eptlst.Add(new OverDueChargeTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);

            header.MonthTotal = monthtotal.FormatCurrency("###,##0","-");
            header.MonthPaidTotal = monthpaidtotal.FormatCurrency("###,##0","-");
            header.OtherTotalPay = misctotal.FormatCurrency("###,##0","-");
            header.OtherTotalPaid = miscpaid.FormatCurrency("###,##0","-");
            header.OtherTotalCharge = misccharge.FormatCurrency("###,##0","-");
            header.OverDueRemainTotal = interesttotal.FormatCurrency("###,##0","-");
            header.OverDuePaidTotal = paidtotal.FormatCurrency("###,##0","-");
            header.TotalOverDue = (monthtotal + misctotal + interesttotal).FormatCurrency("###,##0","-");
            return results;
        }

        private List<MergeTableModel> GetSOATables(CmsContractDetail contract, int rptTxnType, DateTime sDate, DateTime eDate, THSOAHeaderModel inheader,bool isloc, out THSOAHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contract.ContractNO).First();
            MergeTableModel table = new MergeTableModel();
            IEnumerable<SOATableItemModel> soaitems = new List<SOATableItemModel>();
            table.TableName = "Items";
            int i = 1;
            decimal totaldebit = 0;
            decimal totalcredit = 0;
            decimal resamount = 0;
            decimal paymentamount = 0;
            decimal amountreceived = 0;
            //decimal closingbalance = 0;
            CmsSoaResultSet statementOfAccount = null;
            decimal misc = contract.MiscellaneousCharges == null || contract.MiscellaneousCharges.Count == 0 ? 0 : contract.MiscellaneousCharges.Select(c => c.DueAmount).Sum();
            do
            {
                statementOfAccount = _rmwService.GetSOA(businessPartner.BpId, contract.ContractNO, rptTxnType, i, ReportProcessConstants.NoOfRecordsPerFetch, sDate, eDate);
                if (i == 1)
                {
                    //closingbalance = statementOfAccount.ClosingBalance;
                    resamount = statementOfAccount.AmountResidual;
                    paymentamount = statementOfAccount.AmountPayment;
                    amountreceived = statementOfAccount.AmountReceived;
                }
                soaitems = soaitems.Union(statementOfAccount.SoaDetails.Select(x => new SOATableItemModel()
                {
                    TransactionDate = x.TransactionDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.TransactionDate, true),
                    Type = x.TransactionType,
                    Description = x.TransactionDescription,
                    Debit = x.Debit == null ? "-" : x.Debit.Value.FormatCurrency("###,##0","-"),
                    Credit = x.Credit == null ? "-" : x.Credit.Value.FormatCurrency("###,##0","-"),
                    Balance = x.Balance == null ? "-" : x.Balance.Value.FormatCurrency("###,##0","-"),
                }));
                totaldebit += statementOfAccount.SoaDetails.Sum(a => a.Debit) ?? 0;
                totalcredit += statementOfAccount.SoaDetails.Sum(a => a.Credit) ?? 0;
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < statementOfAccount.Total);
            

            if (soaitems.Count() > 0)
            {
                table.Items = soaitems;
            }
            else
            {
                var eptlst = new List<SOATableItemModel>();
                eptlst.Add(new SOATableItemModel());
                table.Items = eptlst;
            }

            results.Add(table);

            header.FinanceAmount = contract.FinancialDetail.FinancedAmount.FormatCurrency("###,##0","-");
            header.ResidualAmount = resamount.FormatCurrency("###,##0","-");
            header.PaymentAmount = paymentamount.FormatCurrency("###,##0","-");
            header.DueDate = summary == null || !summary.DateNextPaymentDue.HasValue ? string.Empty : DayOfTheMonth(summary.DateNextPaymentDue.Value);
            header.PaymentReceived = summary == null ? "0" : amountreceived.FormatCurrency("###,##0","-");
            header.OverDueAmount = summary.AmountOverdue.FormatCurrency("###,##0","-");


            header.VehicleReg = summary == null ? string.Empty : (isloc ? (string.IsNullOrEmpty(summary.VehicleRegNO_Local) ? summary.VehicleRegNO : summary.VehicleRegNO_Local) : summary.VehicleRegNO);
            header.TotalCredit = totalcredit.FormatCurrency("###,##0","-");
            header.TotalDebit = totaldebit.FormatCurrency("###,##0","-");
            header.StartDate = _context.Formatter.FormatData("DATE", contract.StartDate, true);
            header.EndDate = _context.Formatter.FormatData("DATE", contract.MaturityDate ?? contract.EndDate, true);
           
            return results;
        }

        private List<MergeTableModel> GetRepaymentTables(string contractNo, KRRepaymentHeaderModel inheader, bool isloc, out KRRepaymentHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contractNo).First();
            var contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);
            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsPaymentScheduleResultSet paymentSchedule;
            IEnumerable<RepaymentTableItemModel> items = new List<RepaymentTableItemModel>();
            int i = 1;
            decimal ptotal = 0;

            decimal intesttotal = 0;
            decimal nettotal = 0;
            decimal insurancetotal = 0;
            decimal regtotal = 0;
            decimal vattotal = 0;
            decimal grosstotal = 0;
            decimal grosspaidtotal = 0;
            decimal canceltotal = 0;
            decimal suntrytotal = 0;
            decimal installmenttotal = 0;
            decimal prepaymenttotal = 0;
            decimal netrentaltotal = 0;
            decimal cartaxtotal = 0;

            int j = 1;
            do
            {
                paymentSchedule = _rmwService.GetPaymentSchedule(businessPartner.BpId, contractNo, i, ReportProcessConstants.NoOfRecordsPerFetch, null, null);
                if (i == 1)
                {
                    header.ContractStartDate = paymentSchedule.StartDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.StartDate, true);
                    header.ContractEndDate = paymentSchedule.EndDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.EndDate, true);
                    header.MaturityDate = paymentSchedule.MaturityDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.MaturityDate, true);

                    header.Term = paymentSchedule.Term.ToString();
                    //header.TermCharge = (paymentSchedule.AmountTermCharges ?? 0).FormatCurrency("###,##0","-");
                    //header.RentalMode = paymentSchedule.RentalMode;
                    //header.ResidualAmount = (paymentSchedule.AmountResidual ?? 0).FormatCurrency("###,##0","-");
                    //header.PaymentFrequency = paymentSchedule.PaymentFrequency;
                    header.InterestRate = (paymentSchedule.InterestRate ?? 0).ToString("#0.00");
                    header.InitSecurityDeposit = (paymentSchedule.InitialSecurityDeposit ?? 0).FormatCurrency("###,##0","-");// new DecimalItem(paymentSchedule.InitialSecurityDeposit).Text;
                    header.Prepayment = (paymentSchedule.Prepayment ?? 0).FormatCurrency("###,##0","-");// new DecimalItem(paymentSchedule.Prepayment).Text;
                    header.GResidualValue = (paymentSchedule.GResidualValue ?? 0).FormatCurrency("###,##0","-");// new DecimalItem(paymentSchedule.GResidualValue).Text;
                    header.NGResidualValue = (paymentSchedule.NGResidualValue ?? 0).FormatCurrency("###,##0","-");// new DecimalItem(paymentSchedule.NGResidualValue).Text;
                    header.FinanceAmount = (paymentSchedule.FinanceAmount ?? 0).FormatCurrency("###,##0","-");
                    header.FinancedAmount = (paymentSchedule.FinancedAmount ?? 0).FormatCurrency("###,##0", "-");
                }
                items = items.Union(paymentSchedule.PaymentScheduleDetails.Select(x => new KRRepaymentTableItemModel
                {
                    ID = (j++).ToString(),

                    RentalNo = x.RentalNum.FormatString(""),
                    PayDate = x.PayDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.PayDate, true),
                    PrincipalAmount = (x.AmountPrincipal ?? 0).FormatCurrency("###,##0","-"),
                    InterestAmount = (x.AmountInterest ?? 0).FormatCurrency("###,##0","-"),
                    SundryCharge = (x.AmountSundaryCharges ?? 0).FormatCurrency("###,##0","-"),
                    RentalAmount = (x.AmountRental ?? 0).FormatCurrency("###,##0","-"),
                    PrincipalBalance = (x.AmountPrincipalOutstanding ?? 0).FormatCurrency("###,##0","-"),
                    PrincipalOutstanding = (x.PrincipalOutstandingAmount ?? 0).FormatCurrency("###,##0","-"),
                    Prepayment = (x.AmountPrepayment ?? 0).FormatCurrency("###,##0","-"),
                    NetRentalAmount = ((x.AmountPrincipal ?? 0) + (x.AmountInterest ?? 0)).FormatCurrency("###,##0","-"),
                    CarTax = (x.AmountCarTax ?? 0).FormatCurrency("###,##0","-"),
                    InsuranceAmount = (x.AmountInsurance ?? 0).FormatCurrency("###,##0","-"),
                    InstallmentAmount = (x.Amount ?? 0).FormatCurrency("###,##0","-"),
                    // NOT IN USE

                    //PrincipalRentalAmount = (x.AmountPrincipalOutstanding ?? 0).FormatCurrency("###,##0","-"),
                    DueDate = x.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.DueDate, true),
                    RegistrationAmount = (x.AmountRegistration ?? 0).FormatCurrency("###,##0","-"),
                    NetVatAmount = (x.AmountVATOnRental ?? 0).FormatCurrency("###,##0","-"),
                    GrossRentalAmount = (x.AmountGrossRental ?? 0).FormatCurrency("###,##0","-"),
                    
                    GrossPaid = (x.AmountGrossPaid ?? 0).FormatCurrency("###,##0","-"),
                    Cancelled = (x.AmountDishonourCancelled ?? 0).FormatCurrency("###,##0","-"),
                    SettledAmount = (x.PaidAmount ?? 0).FormatCurrency("###,##0","-"),
                    
                    
                }));
                
                ptotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountPrincipal ?? 0).Sum();
                intesttotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountInterest ?? 0).Sum();
                suntrytotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountSundaryCharges ?? 0).Sum();
                installmenttotal += (from a in paymentSchedule.PaymentScheduleDetails select a.Amount ?? 0).Sum();
                prepaymenttotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountPrepayment ?? 0).Sum();
                netrentaltotal += (from a in paymentSchedule.PaymentScheduleDetails select (a.AmountPrincipal ?? 0) + (a.AmountInterest ?? 0)).Sum();
                cartaxtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountCarTax ?? 0).Sum();
                insurancetotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountInsurance ?? 0).Sum();
                //nettotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountRental ?? 0).Sum();
                //regtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountRegistration ?? 0).Sum();
                //vattotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountVATOnRental ?? 0).Sum();
                //grosstotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountGrossRental ?? 0).Sum();
                //grosspaidtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountGrossPaid ?? 0).Sum();
                //canceltotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountDishonourCancelled ?? 0).Sum();
                
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < paymentSchedule.Total);

            table.Items = items;
            results.Add(table);
            


            header.PrincipalTotal = ptotal.FormatCurrency("###,##0","-");
            header.InterestTotal = intesttotal.FormatCurrency("###,##0","-");
            header.SundryTotal = suntrytotal.FormatCurrency("###,##0","-");
            header.InstallmentTotal = installmenttotal.FormatCurrency("###,##0","-");
            header.PrepaymentTotal = prepaymenttotal.FormatCurrency("###,##0","-");
            header.NetRentalTotal = netrentaltotal.FormatCurrency("###,##0","-");
            header.CarTaxTotal = cartaxtotal.FormatCurrency("###,##0","-");
            header.InsuranceTotal = insurancetotal.FormatCurrency("###,##0","-");
            //header.NetTotal = nettotal.FormatCurrency("###,##0","-");
            //header.RegistrationTotal = regtotal.FormatCurrency("###,##0","-");
            //header.NetVatTotal = vattotal.FormatCurrency("###,##0","-");
            //header.GrossTotal = grosstotal.FormatCurrency("###,##0","-");
            //header.GrossPaidTotal = grosspaidtotal.FormatCurrency("###,##0","-");
            //header.CancelledTotal = canceltotal.FormatCurrency("###,##0","-");
            
            
            return results;
        }

        private List<MergeTableModel> GetReceiptTables(string contractNo, string receiptno, ReceiptHeaderModel inheader,bool isloc, out ReceiptHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;


            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsReceiptDetail receipt = _rmwService.GetReceipt(businessPartner.BpId, contractNo,receiptno);
            IList<KRReceiptTableItemModel> items = new List<KRReceiptTableItemModel>();

            string receiptDate = receipt.ValueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", receipt.ValueDate, true);
            header.ReceiptDate = receiptDate;
            header.ReceiptNo = isloc ? (string.IsNullOrEmpty(receipt.ReceiptNo_Local) ? receipt.ReceiptNo : receipt.ReceiptNo_Local) : receipt.ReceiptNo;
            header.TotalAmount = (receipt.ReceiptAmount ?? 0).FormatCurrency("###,##0","-");

            int i = 1;
            int j = 1;
            CmsPaymentHistoryResultSet paymentHistory = null;
            do
            {
                paymentHistory = _rmwService.GetPaymentHistory(contractNo, receiptno, i, ReportProcessConstants.NoOfRecordsPerFetch, null, null);

                items = items.Union(paymentHistory.PaymentHistoryDetails.Select(x => new KRReceiptTableItemModel()
                {
                    Description = isloc ? "원금" : "Principal",
                    ReceiptDate = x.ReceiptDate == null ? receiptDate : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    Amount = (x.PrincipalAmount ?? 0).FormatCurrency("###,##0","-"),
                    RentalNo = x.PaymentNo.FormatString(""),
                    RentalNoInt = x.PaymentNo,
                    d_Amount = (x.PrincipalAmount ?? 0),
                    Order = 1
                })).Union(paymentHistory.PaymentHistoryDetails.Select(x => new KRReceiptTableItemModel()
                {
                    Description = isloc ? "이자" : "Interest",
                    ReceiptDate = x.ReceiptDate == null ? receiptDate : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    Amount = (x.InterestAmount ?? 0).FormatCurrency("###,##0","-"),
                    RentalNo = x.PaymentNo.FormatString(""),
                    RentalNoInt = x.PaymentNo,
                    d_Amount = (x.InterestAmount ?? 0),
                    Order = 2
                })).Union(paymentHistory.PaymentHistoryDetails.Select(x => new KRReceiptTableItemModel()
                {
                    Description = isloc ? "월리스료" : "Monthly Rental",
                    ReceiptDate = x.ReceiptDate == null ? receiptDate : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    Amount = (x.MonthlyRental ?? 0).FormatCurrency("###,##0", "-"),
                    RentalNo = x.PaymentNo.FormatString(""),
                    RentalNoInt = x.PaymentNo,
                    d_Amount = (x.MonthlyRental ?? 0),
                    Order = 1
                })).Union(paymentHistory.PaymentHistoryDetails.Select(x => new KRReceiptTableItemModel()
                {
                    Description = isloc ? "차세" : "Car Tax",
                    ReceiptDate = x.ReceiptDate == null ? receiptDate : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    Amount = (x.CarTaxAmount ?? 0).FormatCurrency("###,##0", "-"),
                    RentalNo = x.PaymentNo.FormatString(""),
                    RentalNoInt = x.PaymentNo,
                    d_Amount = (x.CarTaxAmount ?? 0),
                    Order = 3
                })).Union(paymentHistory.PaymentHistoryDetails.Select(x => new KRReceiptTableItemModel()
                {
                    Description = isloc ? "과태료" : "Fine Ticket",
                    ReceiptDate = x.ReceiptDate == null ? receiptDate : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    Amount = (x.FineTicketAmount ?? 0).FormatCurrency("###,##0", "-"),
                    RentalNo = x.PaymentNo.FormatString(""),
                    RentalNoInt = x.PaymentNo,
                    d_Amount = (x.FineTicketAmount ?? 0),
                    Order = 4
                })).Union(paymentHistory.PaymentHistoryDetails.Select(x => new KRReceiptTableItemModel()
                {
                    Description = isloc ? "연체이자" : "Overdue Interest",
                    ReceiptDate = x.ReceiptDate == null ? receiptDate : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    Amount = (x.OverDueAmount ?? 0).FormatCurrency("###,##0","-"),
                    RentalNo = x.PaymentNo.FormatString(""),
                    RentalNoInt = x.PaymentNo,
                    d_Amount = (x.OverDueAmount ?? 0),
                    Order = 5
                })).Union(paymentHistory.PaymentHistoryDetails.Select(x => new KRReceiptTableItemModel()
                {
                    Description = x.ChargeType,
                    ReceiptDate = x.ReceiptDate == null ? receiptDate : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    Amount = (x.ChargeAmount ?? 0).FormatCurrency("###,##0","-"),
                    RentalNo = x.PaymentNo.FormatString(""),
                    RentalNoInt = x.PaymentNo,
                    d_Amount = (x.ChargeAmount ?? 0),
                    Order = 6
                })).ToList();

            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < paymentHistory.Total);

            items = items.Where(a => a.d_Amount > 0).OrderBy(o => o.RentalNoInt).ThenBy(o => o.Order).Select(x => new KRReceiptTableItemModel()
                {
                    ID = j++,
                    Description = x.Description,
                    ReceiptDate = x.ReceiptDate == null ? receiptDate : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    Amount = x.Amount,
                    RentalNo = x.RentalNoInt == 0 ? "" : x.RentalNo,
                    d_Amount = x.d_Amount
                }).ToList();
            table.Items = items;
            results.Add(table);

            return results;
        }

        private List<MergeTableModel> GetPaymentHistoryTables(CmsContractDetail contract, int rptTxnType, DateTime sDate, DateTime eDate, PaymentHistoryHeaderModel inheader, bool isloc, out PaymentHistoryHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contract.ContractNO).First();
            MergeTableModel table = new MergeTableModel();
            IEnumerable<PaymentHistoryTableItemModel> phitems = new List<PaymentHistoryTableItemModel>();
            table.TableName = "Items";
            int i = 1;
            decimal totalpaiddate = 0;
            decimal totalprincipal = 0;
            decimal totalinterest = 0;
            decimal totaloverdue = 0;
            decimal totalcartax = 0;
            decimal totalfineticket = 0;
            decimal totalcharge = 0;
            decimal totaldue = 0;
            decimal totalrental = 0;

           
            CmsPaymentHistoryResultSet paymentHistory = null;
            decimal misc = contract.MiscellaneousCharges == null || contract.MiscellaneousCharges.Count == 0 ? 0 : contract.MiscellaneousCharges.Select(c => c.DueAmount).Sum();
            do
            {
                paymentHistory = _rmwService.GetPaymentHistory(contract.ContractNO, null, i, ReportProcessConstants.NoOfRecordsPerFetch, sDate, eDate);
                if (i == 1)
                {
                    header.ContractStartDate = paymentHistory.StartDate == null ? string.Empty : _context.Formatter.FormatData("DATE", 
paymentHistory.StartDate, true);
                    header.ContractEndDate = paymentHistory.EndDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentHistory.EndDate, true);
                    header.Term = paymentHistory.Term.ToString();
                    header.FinancedAmount = (paymentHistory.FinancedAmount ?? 0).FormatCurrency("###,##0","-");
                    header.InitSecurityDeposit = (paymentHistory.InitialSecurityDeposit ?? 0).FormatCurrency("###,##0","-");
                    header.Prepayment = (paymentHistory.PrepaymentAmount ?? 0).FormatCurrency("###,##0","-");
                    header.GResidualValue = (paymentHistory.GuaranteedRVamount).FormatCurrency("###,##0","-");
                    header.NGResidualValue = (paymentHistory.NonGuaranteedRVamount ?? 0).FormatCurrency("###,##0","-");
                    header.InterestRate = (paymentHistory.InterestRate ?? 0).FormatCurrency("#0.00", "0.00");
                }
                phitems = phitems.Union(paymentHistory.PaymentHistoryDetails.Select(x => new PaymentHistoryTableItemModel()
                {
                    ReceiptDate = x.ReceiptDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.ReceiptDate, true),
                    TotalByPaidDate = (x.TotalAmountbyPaidDate ?? 0).FormatCurrency("###,##0", "-"),

                    PaymentNo = x.PaymentNo.FormatString(""),
                    Principal = (x.PrincipalAmount ?? 0).FormatCurrency("###,##0","-"),
                    Interest = (x.InterestAmount ?? 0).FormatCurrency("###,##0","-"),
                    ODInterest = (x.OverDueAmount ?? 0).FormatCurrency("###,##0","-"),
                    CarTax = (x.CarTaxAmount ?? 0).FormatCurrency("###,##0","-"),
                    FineTicket = (x.FineTicketAmount ?? 0).FormatCurrency("###,##0","-"),
                    ChargeAmount = (x.ChargeAmount ?? 0).FormatCurrency("###,##0","-"),
                    ChargeType = x.ChargeType,
                    RentalNo = x.PaymentNo.FormatString(""), /*x.RentalNo.ToString(),*/
                    MonthlyRental = (x.MonthlyRental ?? 0).FormatCurrency("###,##0", "-"),
                    Total = (x.TotalAmount ?? 0).FormatCurrency("###,##0", "-"),
                }));
                totalpaiddate += paymentHistory.PaymentHistoryDetails.Sum(a => a.TotalAmountbyPaidDate) ?? 0;
                totalprincipal += paymentHistory.PaymentHistoryDetails.Sum(a => a.PrincipalAmount) ?? 0;
                totalinterest += paymentHistory.PaymentHistoryDetails.Sum(a => a.InterestAmount) ?? 0;
                totaloverdue += paymentHistory.PaymentHistoryDetails.Sum(a => a.OverDueAmount) ?? 0;
                totalcartax += paymentHistory.PaymentHistoryDetails.Sum(a => a.CarTaxAmount) ?? 0;
                totalfineticket += paymentHistory.PaymentHistoryDetails.Sum(a => a.FineTicketAmount) ?? 0;
                totalcharge += paymentHistory.PaymentHistoryDetails.Sum(a => a.ChargeAmount) ?? 0;
                totaldue += paymentHistory.PaymentHistoryDetails.Sum(a => a.TotalAmount) ?? 0;
                totalrental += paymentHistory.PaymentHistoryDetails.Sum(a => a.MonthlyRental) ?? 0;

            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < paymentHistory.Total);


            if (phitems.Count() > 0)
            {
                table.Items = phitems;
            }
            else
            {
                var eptlst = new List<PaymentHistoryTableItemModel>();
                eptlst.Add(new PaymentHistoryTableItemModel());
                table.Items = eptlst;
            }

            results.Add(table);

            header.TotalTotalByPaidDate = totalpaiddate.FormatCurrency("###,##0","-");
            header.TotalPrincipal = totalprincipal.FormatCurrency("###,##0","-");
            header.TotalInterest = totalinterest.FormatCurrency("###,##0","-");
            header.TotalOverdue = totaloverdue.FormatCurrency("###,##0","-");
            header.TotalCarTax = totalcartax.FormatCurrency("###,##0","-");
            header.TotalFineTicket = totalfineticket.FormatCurrency("###,##0","-");
            header.TotalChargeAmount = totalcharge.FormatCurrency("###,##0","-");
            header.TotalTotal = totaldue.FormatCurrency("###,##0","-");
            header.TotalMonthlyRental = totalrental.FormatCurrency("###,##0","-");


            return results;
        }

        private string GetSubTemplate(string contractNo)
        {
            var businessPartner = ((CmsCustomerProfile)(CosApplicationContext.Current.Session.CurrentCmsCustomerProfile)).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contractNo).First();

            switch (summary.LeaseType)
            {
                case "OL":
                    return string.Format("{0}_{1}", summary.LeaseType, summary.ExcIncFlag);
                case "FL":
                case "IF":
                default:
                    return summary.LeaseType;
            }
        }

        #endregion

        public string DayOfTheMonth(System.DateTime dDate)
        {
            string sSuffix = null;
            string sResult = null;

            try
            {
                //get the suffix
                switch (dDate.Day)
                {
                    case 1:
                    case 21:
                    case 31:
                        sSuffix = "st";
                        break;
                    case 2:
                    case 22:
                        sSuffix = "nd";
                        break;
                    case 3:
                        sSuffix = "rd";
                        break;
                    default:
                        sSuffix = "th";
                        break;
                }

                //build the string
                sResult = Convert.ToString(dDate.Day) + sSuffix;

            }
            catch (Exception ex)
            {
                sResult = "Custom Format Error";
            }

            return sResult;
        }
    }
}