﻿using System;
using ZLIS.Cos.RmwService.DomainObjects;

namespace ZLIS.Cos.PdfGenerator
{
    public interface IReportGenerater
    {
        byte[] GenerateOverdueReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, bool isloc);
        byte[] GenerateSOAReport(string templatename, string downloadname, CmsContractDetail contract, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc);
        byte[] GenerateRepaymentReport(string templatename, string downloadname, string contractNo, string fpg, string marketcode, string prilanguge, bool isloc);
        byte[] GenerateETReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, DateTime tDate, bool isloc);
        byte[] GenerateReceipt(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, string receiptno, bool isloc);
        //byte[] GenerateInvoiceReceipt(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, DateTime tDate, bool isloc); // not necessary, previously TH requirement and drop in the middle of UAT
        byte[] GenerateTaxInvoice(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, string rptContractId, string rptTaxInvoiceNo, bool isloc);
        byte[] GeneratePaymentHistoryReport(string templatename, string downloadname, CmsContractDetail contractNo, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc);
    }
}