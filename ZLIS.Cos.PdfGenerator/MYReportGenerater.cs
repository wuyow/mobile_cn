﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.RmwService.DomainObjects;
using Aspose.Words;
using ZLIS.Cos.Core.Common;

namespace ZLIS.Cos.PdfGenerator
{
    public class MYReportGenerater : DefaultReportGenerater
    {
        public MYReportGenerater(ReportContext context) : base(context) { }

        public override byte[] GenerateReceipt(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, string receiptno, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            MYReceiptHeaderModel header = GetBaseReportHeaderModel<MYReceiptHeaderModel>(new MYReceiptHeaderModel(), contractNo, marketcode, isloc);

            List<MergeTableModel> tables = GetReceiptTables(contractNo, receiptno, header,isloc, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        private List<MergeTableModel> GetReceiptTables(string contractNo, string receiptno, MYReceiptHeaderModel inheader,bool isloc, out MYReceiptHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)(session.CurrentCmsCustomerProfile)).BusinessPartner;
            CmsContractDetail contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);


            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsReceiptDetail receipt = _rmwService.GetReceipt(businessPartner.BpId, contractNo, receiptno);
            IList<ReceiptTableItemModel> items = new List<ReceiptTableItemModel>();
            

            var vehicle = contract.ContractAsset.FirstOrDefault();
            header.VehicleRegNo = vehicle != null ?(isloc?(string.IsNullOrEmpty(vehicle.VehicleRegistrationNO_Local )?vehicle.VehicleRegistrationNO:vehicle.VehicleRegistrationNO_Local): vehicle.VehicleRegistrationNO ): string.Empty;
            header.ReceiptStatus =isloc?(string.IsNullOrEmpty(receipt.ReceiptStatus_Local)?receipt.ReceiptStatus:receipt.ReceiptStatus_Local): receipt.ReceiptStatus;
            header.ReceiptDate = _context.Formatter.FormatData("DATE", receipt.ReceiptDate, true);
            header.ReceiptNo = receipt.ReceiptNo;

            items.Add(new ReceiptTableItemModel()
            {
                Description = "Instalment",
                Term = receipt.RentalAllocationDetail == null || receipt.RentalAllocationDetail.Count() == 0 ? "" : string.Join(", ", receipt.RentalAllocationDetail.Where(x => (x.AllocatedAmount ?? 0) > 0).OrderBy(x => x.RentalNo).Select(x => x.RentalNo.ToString())),
                Amount = receipt.RentalAllocationDetail == null || receipt.RentalAllocationDetail.Count() == 0 ? ".00" : (receipt.RentalAllocationDetail.Sum(x => x.AllocatedAmount) ?? 0).ToString("###,###.00")
            });

            items.Add(new ReceiptTableItemModel()
            {
                Description = "Overdue Interest",
                Amount = receipt.OverdueInterestAllocationDetail == null || receipt.OverdueInterestAllocationDetail.Count() == 0 ? ".00" : (receipt.OverdueInterestAllocationDetail.Sum(x => x.AllocatedAmount) ?? 0).ToString("###,###.00")
            });

            items.Add(new ReceiptTableItemModel()
            {
                Description = "Other Charges",
                Amount = receipt.ChargeAllocationDetail == null || receipt.ChargeAllocationDetail.Count() == 0 ? ".00" : (receipt.ChargeAllocationDetail.Sum(x => x.AllocatedAmount) ?? 0).ToString("###,###.00")
            });
            //decimal zerodec = 0;
            //header.TotalAmount = ((receipt.RentalAllocationDetail == null || receipt.RentalAllocationDetail.Count() == 0 ? zerodec : receipt.RentalAllocationDetail.Sum(x => x.AllocatedAmount) ?? zerodec) +
            //    (receipt.OverdueInterestAllocationDetail == null || receipt.OverdueInterestAllocationDetail.Count() == 0 ? zerodec : receipt.OverdueInterestAllocationDetail.Sum(x => x.AllocatedAmount) ?? zerodec) +
            //    (receipt.ChargeAllocationDetail == null || receipt.ChargeAllocationDetail.Count() == 0 ? zerodec : receipt.ChargeAllocationDetail.Sum(x => x.AllocatedAmount) ?? zerodec)).ToString("###,###.00");
            header.TotalAmount = (receipt.ReceiptAmount ?? 0).ToString("###,###.00");

            table.Items = items;
            results.Add(table);
            return results;
        }
    }
}