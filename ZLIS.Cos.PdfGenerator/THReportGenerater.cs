﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Cos.Core;
using Aspose.Words;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.Core.Common;

namespace ZLIS.Cos.PdfGenerator
{
    public class THReportGenerater : DefaultReportGenerater, IReportGenerater
    {
        public THReportGenerater(ReportContext context)
            : base(context)
        {
        }

        public override byte[] GenerateOverdueReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            OverDueHeaderModel header = GetBaseReportHeaderModel<OverDueHeaderModel>(new OverDueHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetOverDueTables(contractNo, header, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        public override byte[] GenerateSOAReport(string templatename, string downloadname, CmsContractDetail contract, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            THSOAHeaderModel header = GetBaseReportHeaderModel<THSOAHeaderModel>(new THSOAHeaderModel(), contract.ContractNO, marketcode, isloc);
            List<MergeTableModel> tables = GetSOATables(contract, rptTxnType, sdate, edate, header,isloc, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        public override byte[] GenerateRepaymentReport(string templatename, string downloadname, string contractNo, string fpg, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            //var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            //var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);

            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, string.Format("{0}_{1}", templatename, fpg), prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_REPAYMENT_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);

            THRepaymentHeaderModel header = GetBaseReportHeaderModel<THRepaymentHeaderModel>(new THRepaymentHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetRepaymentTables(contractNo, header,isloc, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }

        /// <summary>
        /// Generate ET quote PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="tDate"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public override byte[] GenerateETReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, DateTime tDate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            THETHeaderModel header = GetBaseReportHeaderModel<THETHeaderModel>(new THETHeaderModel(), contractNo, marketcode, isloc);

            IRmwService _rmwService = _context.RmwService;
            CmsETEstimateExt cmsEtEstimate = (CmsETEstimateExt)_rmwService.GetETQuotation(contractNo, tDate);
            header.EstimatePayOff = cmsEtEstimate.EarlyTerminationAmount.ToString();
            header.TerminationDate = cmsEtEstimate.EarlyTerminationDate == null ? string.Empty : _context.Formatter.FormatData("DATE", cmsEtEstimate.EarlyTerminationDate, true);
            ISessionState session = CosApplicationContext.Current.Session;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            CmsContractDetail contract = _rmwService.GetContractDetails(businessPartner.BpId,contractNo);
            var vehicle = contract.ContractAsset.FirstOrDefault();
            header.VehicleDescription = isloc ? (string.IsNullOrEmpty(vehicle.VehicleTypeDescription_Local) ? vehicle.VehicleTypeDescription : vehicle.VehicleTypeDescription_Local) : vehicle.VehicleTypeDescription;
            header.VehicleRegistrationNo = isloc ? (string.IsNullOrEmpty(vehicle.VehicleRegistrationNO_Local) ? vehicle.VehicleRegistrationNO : vehicle.VehicleRegistrationNO_Local) : vehicle.VehicleRegistrationNO;
            return GenerateReport(templatepath, string.Empty, downloadpath, header, null, SaveFormat.Pdf);
        }

        /// <summary>
        /// Generate Receipt PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="receiptno"></param>
        /// <param name="ctx"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public override byte[] GenerateReceipt(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, string receiptno, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            ReceiptHeaderModel header = GetBaseReportHeaderModel<ReceiptHeaderModel>(new ReceiptHeaderModel(), contractNo, marketcode, isloc);

            List<MergeTableModel> tables = GetReceiptTables(contractNo,receiptno, header,isloc, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        /// <summary>Receipt
        /// Generate Tax Invoice  PDF. Not required in Thailand
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="tDate"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public override byte[] GenerateInvoiceReceipt(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, DateTime tDate, bool isloc)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generate tax invoice PDF
        /// </summary>
        /// <param name="templatename"></param>
        /// <param name="downloadname"></param>
        /// <param name="contractNo"></param>
        /// <param name="marketcode"></param>
        /// <param name="prilanguge"></param>
        /// <param name="rptContractId"></param>
        /// <param name="rptTaxInvoiceNo"></param>
        /// <param name="ctx"></param>
        /// <param name="isloc"></param>
        /// <returns></returns>
        public override byte[] GenerateTaxInvoice(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, string rptContractId, string rptTaxInvoiceNo, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            TaxInvoiceHeaderModel header = GetBaseReportHeaderModel<TaxInvoiceHeaderModel>(new TaxInvoiceHeaderModel(), contractNo, marketcode, isloc);

            List<MergeTableModel> tables = GetTaxInvoiceTables(contractNo, rptContractId, rptTaxInvoiceNo, header, isloc, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        #region Private Methods

        private List<MergeTableModel> GetOverDueTables(string contractNo, OverDueHeaderModel inheader, out OverDueHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            decimal monthtotal = 0;
            decimal monthpaidtotal = 0;
            decimal misctotal = 0;
            decimal misccharge = 0;
            decimal miscpaid = 0;
            decimal interesttotal = 0;
            decimal paidtotal = 0;

            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            IList<CmsOverDueRental> overdueInstallments = _rmwService.GetOverdueRentalDetail(businessPartner.BpId, contractNo);
            if (overdueInstallments != null && overdueInstallments.Count > 0)
            {
                var paytable = overdueInstallments.Select((a, index) => new OverDuePayTableItemModel()
                {
                    ID = (index + 1).ToString(),
                    Paid = a.PaidAmount.ToString("###,###.00"),
                    PayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                    RentalNo = a.RentalNum.ToString(),
                    PayMonth = a.RentalAmount.ToString("###,###.00"),
                    Remain = a.DueAmount.ToString("###,###.00")
                });
                monthtotal = (from a in overdueInstallments select a.DueAmount).Sum();
                monthpaidtotal = (from a in overdueInstallments select a.PaidAmount).Sum();
                table.Items = paytable;
            }
            else
            {
                var eptlst = new List<OverDuePayTableItemModel>();
                eptlst.Add(new OverDuePayTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OtherPayItems";
            IList<CmsMiscCharge> overdueMiscCharges = _rmwService.GetMiscChargesDetails(businessPartner.BpId, contractNo).Where(x => x.DueAmount > 0).ToList();
            if (overdueMiscCharges != null && overdueMiscCharges.Count > 0)
            {
                var misctable = from a in overdueMiscCharges
                                select new OverDueOtherPayTableItemModel()
                                {
                                    OtherPaid = a.SettledAmount.ToString("###,###.00"),
                                    OtherPayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                                    OtherPayCharge = a.ChargeAmount.ToString("###,###.00"),
                                    OtherRemain = a.DueAmount.ToString("###,###.00"),
                                    Project = a.ChargeName
                                };
                misctotal = (from a in overdueMiscCharges select a.DueAmount).Sum();
                miscpaid = (from a in overdueMiscCharges select a.SettledAmount).Sum();
                misccharge = (from a in overdueMiscCharges select a.ChargeAmount).Sum();
                table.Items = misctable;
            }
            else
            {
                var eptlst = new List<OverDueOtherPayTableItemModel>();
                eptlst.Add(new OverDueOtherPayTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OverDueItems";
            IList<CmsOverDueInterest> overdueInterests = _rmwService.GetOverdueRentalInterest(businessPartner.BpId, contractNo);
            if (overdueInterests != null && overdueInterests.Count > 0)
            {
                var interesttable = from a in overdueInterests
                                    select new OverDueChargeTableItemModel()
                                    {
                                        OverDueType = a.OverdueType,
                                        OverDueTerm = a.RentalNO.ToString(),
                                        OverDueStartDate = a.FromDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.FromDate, true),
                                        OverDueEndDate = a.ToDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.ToDate, true),
                                        OverDueDate = a.OverdueDays.ToString(),
                                        OverDue = a.DueAmount.ToString("###,###.00"),
                                        OverDuePaid = a.PaidWaive.ToString("###,###.00"),
                                        OverDueRemain = a.DueAmount.ToString("###,###.00")
                                    };
                interesttotal = (from a in overdueInterests select a.DueAmount).Sum();
                paidtotal = (from a in overdueInterests select a.PaidWaive).Sum();
                table.Items = interesttable;
            }
            else
            {
                var eptlst = new List<OverDueChargeTableItemModel>();
                eptlst.Add(new OverDueChargeTableItemModel());
                table.Items = eptlst;
            }
            results.Add(table);

            header.MonthTotal = monthtotal.ToString("###,###.00");
            header.MonthPaidTotal = monthpaidtotal.ToString("###,###.00");
            header.OtherTotalPay = misctotal.ToString("###,###.00");
            header.OtherTotalPaid = miscpaid.ToString("###,###.00");
            header.OtherTotalCharge = misccharge.ToString("###,###.00");
            header.OverDueRemainTotal = interesttotal.ToString("###,###.00");
            header.OverDuePaidTotal = paidtotal.ToString("###,###.00");
            header.TotalOverDue = (monthtotal + misctotal + interesttotal).ToString("###,###.00");
            return results;
        }

        private List<MergeTableModel> GetSOATables(CmsContractDetail contract, int rptTxnType, DateTime sDate, DateTime eDate, THSOAHeaderModel inheader,bool isloc, out THSOAHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contract.ContractNO).First();
            MergeTableModel table = new MergeTableModel();
            IEnumerable<SOATableItemModel> soaitems = new List<SOATableItemModel>();
            table.TableName = "Items";
            int i = 1;
            decimal totaldebit = 0;
            decimal totalcredit = 0;
            decimal resamount = 0;
            decimal paymentamount = 0;
            decimal amountreceived = 0;
            //decimal closingbalance = 0;
            CmsSoaResultSet statementOfAccount = null;
            decimal misc = contract.MiscellaneousCharges == null || contract.MiscellaneousCharges.Count == 0 ? 0 : contract.MiscellaneousCharges.Select(c => c.DueAmount).Sum();
            do
            {
                statementOfAccount = _rmwService.GetSOA(businessPartner.BpId, contract.ContractNO, rptTxnType, i, ReportProcessConstants.NoOfRecordsPerFetch, sDate, eDate);
                if (i == 1)
                {
                    //closingbalance = statementOfAccount.ClosingBalance;
                    resamount = statementOfAccount.AmountResidual;
                    paymentamount = statementOfAccount.AmountPayment;
                    amountreceived = statementOfAccount.AmountReceived;
                }
                soaitems = soaitems.Union(statementOfAccount.SoaDetails.Select(x => new SOATableItemModel()
                {
                    TransactionDate = x.TransactionDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.TransactionDate, true),
                    Type = x.TransactionType,
                    Description = x.TransactionDescription,
                    Debit = x.Debit == null ? "-" : x.Debit.Value.ToString("###,###.00"),
                    Credit = x.Credit == null ? "-" : x.Credit.Value.ToString("###,###.00"),
                    Balance = x.Balance == null ? "-" : x.Balance.Value.ToString("###,###.00"),
                }));
                totaldebit += statementOfAccount.SoaDetails.Sum(a => a.Debit) ?? 0;
                totalcredit += statementOfAccount.SoaDetails.Sum(a => a.Credit) ?? 0;
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < statementOfAccount.Total);
            

            if (soaitems.Count() > 0)
            {
                table.Items = soaitems;
            }
            else
            {
                var eptlst = new List<SOATableItemModel>();
                eptlst.Add(new SOATableItemModel());
                table.Items = eptlst;
            }

            results.Add(table);

            header.FinanceAmount = contract.FinancialDetail.FinancedAmount.ToString("###,###.00");
            header.ResidualAmount = resamount.ToString("###,###.00");
            header.PaymentAmount = paymentamount.ToString("###,###.00");
            header.DueDate = summary == null || !summary.DateNextPaymentDue.HasValue ? string.Empty : DayOfTheMonth(summary.DateNextPaymentDue.Value);
            header.PaymentReceived = summary == null ? ".00" : amountreceived.ToString("###,###.00");
            header.OverDueAmount = summary.AmountOverdue.ToString("###,###.00");


            header.VehicleReg = summary == null ? string.Empty : (isloc ? (string.IsNullOrEmpty(summary.VehicleRegNO_Local) ? summary.VehicleRegNO : summary.VehicleRegNO_Local) : summary.VehicleRegNO);
            header.TotalCredit = totalcredit.ToString("###,###.00");
            header.TotalDebit = totaldebit.ToString("###,###.00");
            header.StartDate = _context.Formatter.FormatData("DATE", contract.StartDate, true);
            header.EndDate = _context.Formatter.FormatData("DATE", contract.MaturityDate ?? contract.EndDate, true);
           
            return results;
        }

        private List<MergeTableModel> GetRepaymentTables(string contractNo, THRepaymentHeaderModel inheader,bool isloc, out THRepaymentHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contractNo).First();
            var contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);
            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsPaymentScheduleResultSet paymentSchedule;
            IEnumerable<RepaymentTableItemModel> items = new List<RepaymentTableItemModel>();
            int i = 1;
            decimal ptotal = 0;

            decimal intesttotal = 0;
            decimal nettotal = 0;
            decimal insurancetotal = 0;
            decimal regtotal = 0;
            decimal vattotal = 0;
            decimal grosstotal = 0;
            decimal grosspaidtotal = 0;
            decimal canceltotal = 0;
            decimal suntrytotal = 0;

            int j = 1;
            do
            {
                paymentSchedule = _rmwService.GetPaymentSchedule(businessPartner.BpId, contractNo, i, ReportProcessConstants.NoOfRecordsPerFetch, null, null);
                if (i == 1)
                {
                    header.StartDate = paymentSchedule.StartDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.StartDate, true);
                    header.MaturityDate = paymentSchedule.MaturityDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.MaturityDate, true);

                    header.Term = paymentSchedule.Term.ToString();
                    header.TermCharge = (paymentSchedule.AmountTermCharges ?? 0).ToString("###,###.00");
                    header.RentalMode = paymentSchedule.RentalMode;
                    header.ResidualAmount = (paymentSchedule.AmountResidual ?? 0).ToString("###,###.00");
                    header.PaymentFrequency = paymentSchedule.PaymentFrequency;
                    header.InterestRate = (paymentSchedule.InterestRate ?? 0).ToString();

                }
                items = items.Union(paymentSchedule.PaymentScheduleDetails.Select(x => new RepaymentTableItemModel
                {
                    ID = (j++).ToString(),

                    DueDate = x.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.DueDate, true),
                    RentalNo = x.RentalNum.ToString(),
                    PrincipalBalance = (x.AmountPrincipalOutstanding ?? 0).ToString("###,###.00"),
                    //PrincipalRentalAmount = (x.AmountPrincipalOutstanding ?? 0).ToString("###,###.00"),
                    PrincipalAmount = (x.AmountPrincipal ?? 0).ToString("###,###.00"),
                    InterestAmount = (x.AmountInterest ?? 0).ToString("###,###.00"),
                    NetRentalAmount = (x.AmountRental ?? 0).ToString("###,###.00"),
                    InsuranceAmount = (x.AmountInsurance ?? 0).ToString("###,###.00"),
                    RegistrationAmount = (x.AmountRegistration ?? 0).ToString("###,###.00"),
                    NetVatAmount = (x.AmountVATOnRental ?? 0).ToString("###,###.00"),
                    GrossRentalAmount = (x.AmountGrossRental ?? 0).ToString("###,###.00"),
                    SundryCharge = (x.AmountSundaryCharges ?? 0).ToString("###,###.00"),
                    GrossPaid = (x.AmountGrossPaid ?? 0).ToString("###,###.00"),
                    Cancelled = (x.AmountDishonourCancelled ?? 0).ToString("###,###.00"),

                    // NOT IN USE
                    InstallmentAmount = (x.Amount ?? 0).ToString("###,###.00"),
                    SettledAmount = (x.PaidAmount ?? 0).ToString("###,###.00"),
                    RentalAmount = (x.AmountRental ?? 0).ToString("###,###.00"),
                    
                }));
                
                ptotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountPrincipal ?? 0).Sum();
                intesttotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountInterest ?? 0).Sum();
                nettotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountRental ?? 0).Sum();
                insurancetotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountInsurance ?? 0).Sum();
                regtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountRegistration ?? 0).Sum();
                vattotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountVATOnRental ?? 0).Sum();
                grosstotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountGrossRental ?? 0).Sum();
                suntrytotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountSundaryCharges ?? 0).Sum();
                grosspaidtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountGrossPaid ?? 0).Sum();
                canceltotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountDishonourCancelled ?? 0).Sum();
                
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < paymentSchedule.Total);

            table.Items = items;
            results.Add(table);
            header.StatementDate = header.PrintDate;
            header.FinanceProduct =summary==null?string.Empty:(isloc?(string.IsNullOrEmpty(summary.FinancialProductGroup.Title_Local)?summary.FinancialProductGroup.Title:summary.FinancialProductGroup.Title_Local): summary.FinancialProductGroup.Title);
            var vehicle = contract.ContractAsset.FirstOrDefault();
            
            header.FinanceAmount = contract.FinancialDetail.FinancedAmount.ToString("###,###.00");
            header.VehicleDescription = vehicle == null ? "" : (isloc ? (string.IsNullOrEmpty(vehicle.AssetDescription_Local) ? vehicle.AssetDescription : vehicle.AssetDescription_Local) : vehicle.AssetDescription);

            header.PrincipalTotal = ptotal.ToString("###,###.00");
            header.InterestTotal = intesttotal.ToString("###,###.00");
            header.NetTotal = nettotal.ToString("###,###.00");
            header.InsuranceTotal = insurancetotal.ToString("###,###.00");
            header.RegistrationTotal = regtotal.ToString("###,###.00");
            header.NetVatTotal = vattotal.ToString("###,###.00");
            header.GrossTotal = grosstotal.ToString("###,###.00");
            header.SundryTotal = suntrytotal.ToString("###,###.00");
            header.GrossPaidTotal = grosspaidtotal.ToString("###,###.00");
            header.CancelledTotal = canceltotal.ToString("###,###.00");
            
            
            return results;
        }

        private List<MergeTableModel> GetReceiptTables(string contractNo, string receiptno, ReceiptHeaderModel inheader,bool isloc, out ReceiptHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;


            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsReceiptDetail receipt = _rmwService.GetReceipt(businessPartner.BpId, contractNo,receiptno);
            IList<ReceiptTableItemModel> items = new List<ReceiptTableItemModel>();

            header.ReceiptDate = receipt.ReceiptDate == null ? string.Empty : _context.Formatter.FormatData("DATE", receipt.ReceiptDate, true);
            header.ReceiptNo = isloc ? (string.IsNullOrEmpty(receipt.ReceiptNo_Local) ? receipt.ReceiptNo : receipt.ReceiptNo_Local) : receipt.ReceiptNo;
            header.TaxNo = isloc ? (string.IsNullOrEmpty(businessPartner.TaxNo_Local) ? businessPartner.TaxNo : businessPartner.TaxNo_Local) : businessPartner.TaxNo;
            header.ProductType = isloc ? (string.IsNullOrEmpty(receipt.ProductType_Local) ? receipt.ProductType : receipt.ProductType_Local) : receipt.ProductType;
            header.LicensePlate = isloc ? (string.IsNullOrEmpty(receipt.LicenseNo_Local) ? receipt.LicenseNo : receipt.LicenseNo_Local) : receipt.LicenseNo;
            header.EngineNo = isloc ? (string.IsNullOrEmpty(receipt.EngineNo_Local) ? receipt.EngineNo : receipt.EngineNo_Local) : receipt.EngineNo;
            header.ChassisNo = isloc ? (string.IsNullOrEmpty(receipt.ChasisNo_Local) ? receipt.ChasisNo : receipt.ChasisNo_Local) : receipt.ChasisNo;
            items.Add(new ReceiptTableItemModel()
            {
                Description = isloc ? "ค่างวด" : "Instalment",
                Term = receipt.RentalAllocationDetail == null || receipt.RentalAllocationDetail.Count() == 0 ? "" : string.Join(", ", receipt.RentalAllocationDetail.Where(x => (x.AllocatedAmount ?? 0) > 0).OrderBy(x => x.RentalNo).Select(x => x.RentalNo.ToString())),
                Amount = receipt.RentalAllocationDetail == null || receipt.RentalAllocationDetail.Count() == 0 ? ".00" : (receipt.RentalAllocationDetail.Sum(x => x.AllocatedAmount)??0).ToString("###,###.00")
            });

            items.Add(new ReceiptTableItemModel()
            {
                Description = isloc ? "ดอกเบี้ยค้างชำระ" : "Overdue Interest",
                Amount = receipt.OverdueInterestAllocationDetail == null || receipt.OverdueInterestAllocationDetail.Count() == 0 ? ".00" : (receipt.OverdueInterestAllocationDetail.Sum(x => x.AllocatedAmount)??0).ToString("###,###.00")
            });

            items.Add(new ReceiptTableItemModel()
            {
                Description = isloc ? "ค่าใช้จ่ายอื่น ๆ" : "Other Charges",
                Amount = receipt.ChargeAllocationDetail == null || receipt.ChargeAllocationDetail.Count() == 0 ? ".00" : (receipt.ChargeAllocationDetail.Sum(x => x.AllocatedAmount) ?? 0).ToString("###,###.00")
            });
            table.Items = items;
            results.Add(table);
            //decimal zerodec = 0;
            header.TotalAmount = (receipt.ReceiptAmount ?? 0).ToString("###,###.00");
                //((receipt.RentalAllocationDetail == null || receipt.RentalAllocationDetail.Count() == 0 ? zerodec : receipt.RentalAllocationDetail.Sum(x => x.AllocatedAmount) ?? zerodec) +
                //(receipt.OverdueInterestAllocationDetail == null || receipt.OverdueInterestAllocationDetail.Count() == 0 ? zerodec : receipt.OverdueInterestAllocationDetail.Sum(x => x.AllocatedAmount) ?? zerodec) +
                //(receipt.ChargeAllocationDetail == null || receipt.ChargeAllocationDetail.Count() == 0 ? zerodec : receipt.ChargeAllocationDetail.Sum(x => x.AllocatedAmount) ?? zerodec)).ToString("###,###.00");
            header.TotalAmountText = new Number2TextHelper().TranslateCurrency(receipt.ReceiptAmount ?? 0, "Baht Only");
            return results;
        }

        private List<MergeTableModel> GetTaxInvoiceTables(string contractNo, string rptContractId, string rptTaxInvoiceNo, TaxInvoiceHeaderModel inheader,bool isloc, out TaxInvoiceHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;


            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsTaxInvoiceResultSet taxes = _rmwService.GetTaxInvoice(businessPartner.BpId, contractNo, rptTaxInvoiceNo);
            CmsTaxInvoiceDetail taxDetail = taxes.TaxInvoiceDetails.Where(x => x.TaxInvoiceNo == rptTaxInvoiceNo).FirstOrDefault();
            IList<TaxInvoiceTableItemModel> items = new List<TaxInvoiceTableItemModel>();
            
            header.InvoiceDate = taxDetail.TaxInvoiceDate == null ? string.Empty : _context.Formatter.FormatData("DATE", taxDetail.TaxInvoiceDate, true);
            header.InvoiceNo = isloc ? (string.IsNullOrEmpty(taxDetail.TaxInvoiceNo_Local) ? taxDetail.TaxInvoiceNo : taxDetail.TaxInvoiceNo_Local) : taxDetail.TaxInvoiceNo;
            //header.TaxId = isloc ? (string.IsNullOrEmpty(businessPartner.TaxNo_Local) ? businessPartner.TaxNo : businessPartner.TaxNo_Local) : businessPartner.TaxNo;
            header.ProductType = isloc ? (string.IsNullOrEmpty(taxes.TaxInvoice.ProductType_Local) ? taxes.TaxInvoice.ProductType : taxes.TaxInvoice.ProductType_Local) : taxes.TaxInvoice.ProductType;
            header.LicensePlate = isloc ? (string.IsNullOrEmpty(taxes.TaxInvoice.LicenseNo_Local) ? taxes.TaxInvoice.LicenseNo : taxes.TaxInvoice.LicenseNo_Local) : taxes.TaxInvoice.LicenseNo;
            header.EngineNo = isloc ? (string.IsNullOrEmpty(taxes.TaxInvoice.EngineNo_Local) ? taxes.TaxInvoice.EngineNo : taxes.TaxInvoice.EngineNo_Local) : taxes.TaxInvoice.EngineNo;
            header.ChassisNo = isloc ? (string.IsNullOrEmpty(taxes.TaxInvoice.ChasisNo_Local) ? taxes.TaxInvoice.ChasisNo : taxes.TaxInvoice.ChasisNo_Local) : taxes.TaxInvoice.ChasisNo;
            header.TotalAmount =( taxDetail.TotalAmount??0).ToString("###,###.00");
            header.TotalAmountText = new Number2TextHelper().TranslateCurrency(taxDetail.TotalAmount ?? 0, "Baht Only");

            header.TaxAddress = GetAddress(businessPartner.BpId, "TH", "Registration");

            items.Add(new TaxInvoiceTableItemModel()
            {
                Description = isloc ? "ค่างวด" : "Installment/Rental",
                Term = taxDetail.RentalNo.ToString(),
                Amount = (taxDetail.RentalAmount ?? 0).ToString("###,###.00")
            });

            items.Add(new TaxInvoiceTableItemModel()
            {
                Description = isloc ? "ภาษีมูลค่าเพิ่ม 7%" : "Vat 7%",
                Amount = (taxDetail.VATAmount?? 0).ToString("###,###.00")
            });

            table.Items = items;
            results.Add(table);
            return results;
        }

        #endregion

        public string DayOfTheMonth(System.DateTime dDate)
        {
            string sSuffix = null;
            string sResult = null;

            try
            {
                //get the suffix
                switch (dDate.Day)
                {
                    case 1:
                    case 21:
                    case 31:
                        sSuffix = "st";
                        break;
                    case 2:
                    case 22:
                        sSuffix = "nd";
                        break;
                    case 3:
                        sSuffix = "rd";
                        break;
                    default:
                        sSuffix = "th";
                        break;
                }

                //build the string
                sResult = Convert.ToString(dDate.Day) + sSuffix;

            }
            catch (Exception ex)
            {
                sResult = "Custom Format Error";
            }

            return sResult;
        }
    }

    
    

}