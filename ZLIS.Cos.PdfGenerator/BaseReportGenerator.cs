﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aspose.Words;
using System.Collections;
using System.IO;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.Core.Settings.Services;
using Aspose.Words.Saving;
using ZLIS.Cos.Core.Settings.DomainModel;
using ZLIS.Cos.Core.Settings;
using ZLIS.Cos.RmwService.DomainObjects;
using System.Text;
using ZLIS.Cos.Core.Common;
using Aspose.Words.Fonts;

namespace ZLIS.Cos.PdfGenerator
{
    public class BaseReportGenerator
    {
        protected ReportContext _context;

        public BaseReportGenerator(ReportContext context)
        {
            _context = context;
        }

        public byte[] GenerateReport(string templatepath,string defaulttemplatepath,string downloadpath, object headermodel, List<MergeTableModel> tablemodels,SaveFormat format)
        {
            FileStream stream = null;
            try
            {
                Aspose.Words.License license = new Aspose.Words.License();
                license.SetLicense("Aspose.Words.lic");
                IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
                if (!System.IO.File.Exists(templatepath))
                {
                    if (!string.IsNullOrEmpty(defaulttemplatepath))
                        templatepath = defaulttemplatepath;
                }

                stream = File.Open(templatepath, FileMode.Open, FileAccess.Read, FileShare.Read);

                Document doc = new Document(stream);
                //FontSettings.SetFontsFolders(new string[] { System.Environment.GetFolderPath(System.Environment.SpecialFolder.Fonts), appsrv.MapPath("Fonts") }, true);
                List<object> headers = new List<object>();
                headers.Add(headermodel);
                MyMailMergeDataSource headersource = new MyMailMergeDataSource(headers.AsEnumerable());
                doc.MailMerge.Execute(headersource);
                if (tablemodels != null && tablemodels.Count > 0)
                {
                    foreach (var tablemodel in tablemodels)
                    {
                        if (tablemodel.Items != null)
                        {
                            MyMailMergeDataSource tablesource = new MyMailMergeDataSource(tablemodel.Items, tablemodel.TableName);
                            doc.MailMerge.ExecuteWithRegions(tablesource);
                        }
                    }
                }
                MemoryStream outStream = new MemoryStream();
                if (format == SaveFormat.Pdf)
                {
                    PdfSaveOptions options = new PdfSaveOptions();
                    options.EmbedFullFonts = false;
                    doc.Save(outStream, options);
                }
                else
                    doc.Save(outStream, format);
                doc.Save(downloadpath);

                stream.Close();
                return outStream.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally {
                if (stream != null)
                    stream.Close();
            }
        }

        public  T GetBaseReportHeaderModel<T>(T headermodel, string contractNo,string countrycode, bool isloc) where T : ReportHeaderModel
        {
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            ISystemSettingService _settingService = CosApplicationContext.Current.GetWorkContext().Resolve<ISystemSettingService>();
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            //CustomerName
            string customerName = string.Empty;
            string customerName_Local = customerName_Local = businessPartner.CustomerType == CmsCustomerType.Individual ?
                    string.Format("{0}{1}{2}{3}{4}"
                    , !string.IsNullOrEmpty(businessPartner.FirstName_Local) ? businessPartner.FirstName_Local.Trim() : businessPartner.FirstName
                    , (string.IsNullOrEmpty(businessPartner.FirstName_Local) && string.IsNullOrEmpty(businessPartner.FirstName)) ? "" : " ", !string.IsNullOrEmpty(businessPartner.MiddleName_Local) ? businessPartner.MiddleName_Local.Trim() : businessPartner.MiddleName
                    , (string.IsNullOrEmpty(businessPartner.MiddleName_Local) && string.IsNullOrEmpty(businessPartner.MiddleName)) ? "" : " ", !string.IsNullOrEmpty(businessPartner.LastName_Local) ? businessPartner.LastName_Local.Trim() : businessPartner.LastName).Trim()
                : (!string.IsNullOrEmpty(businessPartner.FirstName_Local) ? businessPartner.FirstName_Local.Trim() : businessPartner.FirstName);

            if (!isloc || string.IsNullOrEmpty(customerName_Local))
            {
                customerName = businessPartner.CustomerType == CmsCustomerType.Individual ?
                    string.Format("{0}{1}{2}{3}{4}"
                        , !string.IsNullOrEmpty(businessPartner.FirstName) ? businessPartner.FirstName.Trim() : businessPartner.FirstName
                        , string.IsNullOrEmpty(businessPartner.FirstName) ? "" : " ", !string.IsNullOrEmpty(businessPartner.MiddleName) ? businessPartner.MiddleName.Trim() : businessPartner.MiddleName
                        , string.IsNullOrEmpty(businessPartner.MiddleName) ? "" : " ", !string.IsNullOrEmpty(businessPartner.LastName) ? businessPartner.LastName.Trim() : businessPartner.LastName).Trim()
                        : (!string.IsNullOrEmpty(businessPartner.FirstName) ? businessPartner.FirstName.Trim() : businessPartner.FirstName);

                if (string.IsNullOrEmpty(customerName) && !string.IsNullOrEmpty(customerName_Local))
                {
                    customerName = customerName_Local;
                }
            }
            else
                customerName = customerName_Local;

             //Identify No
            string identifyNo =
                businessPartner.CustomerType == CmsCustomerType.Individual ?
                (String.IsNullOrEmpty(businessPartner.CompanyRegNO) ? businessPartner.PassportNO : businessPartner.CompanyRegNO)
                : businessPartner.CompanyRegNO;

            headermodel.Address = GetAddress(businessPartner.BpId, countrycode);
            headermodel.IdentifyCode = identifyNo;
            headermodel.PrintDate = _context.Formatter.FormatData("DATE", TimeHelper.Now, true);
            headermodel.ContractNo = contractNo;
            headermodel.CustomerName = customerName;
            headermodel.IdentityType = businessPartner.CustomerType == CmsCustomerType.Individual ? "NRIC / Passport" : "Company Reg. No.";

            var contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);
            if (contract != null)
            {
                if (contract.ContractAsset != null)
                {
                    var vehicle = contract.ContractAsset.FirstOrDefault();

                    headermodel.VehicleRegNo = string.Join(",", contract.ContractAsset.Select(x => x.VehicleRegistrationNO));
                    headermodel.VehicleDescription = vehicle == null ? "" : (isloc ? (string.IsNullOrEmpty(vehicle.AssetDescription_Local) ? vehicle.AssetDescription : vehicle.AssetDescription_Local) : vehicle.AssetDescription);
                }

                headermodel.ContractStartDate = _context.Formatter.FormatData("DATE", contract.StartDate, true);
                headermodel.ContractEndDate = GetContractEndDate(countrycode, contract.MaturityDate, contract.EndDate);
            }

            var summary = _context.RmwService.GetAccountsSummary(businessPartner.BpId, contractNo).FirstOrDefault();
            if (summary != null)
            {
                headermodel.FinancialProduct = isloc ? (string.IsNullOrEmpty(summary.FinancialProduct.Title_Local) ? summary.FinancialProduct.Title : summary.FinancialProduct.Title_Local) : summary.FinancialProduct.Title;
                headermodel.FinancialProductGroup = isloc ? (string.IsNullOrEmpty(summary.FinancialProductGroup.Title_Local) ? summary.FinancialProductGroup.Title : summary.FinancialProductGroup.Title_Local) : summary.FinancialProductGroup.Title;
            }

            return headermodel;
        }

        public string GetContractEndDate(string countryCode, DateTime? maturityDate, DateTime? endDate)
        {
            switch (countryCode.ToUpper())
            {
                case "CN":
                case "JP":
                    return _context.Formatter.FormatData("DATE", endDate, true);
                default:
                    return _context.Formatter.FormatData("DATE", maturityDate ?? endDate, true);
            }
        }

        public string GetAddress(int bpid, string countryCode, string addresstype = "")
        {
            IRmwService _rmwService = _context.RmwService;
            IList<CmsAddress> lstAddress = _rmwService.GetAddressDetails(bpid);//.Where(x => !string.IsNullOrEmpty(x.AddressTypeCode)).ToList();

            if (lstAddress != null && lstAddress.Count > 0)
            {
                lstAddress = lstAddress.Where(x => !string.IsNullOrEmpty(x.AddressTypeCode)).ToList();
            }

            if (lstAddress == null || lstAddress.Count == 0)
            {
                return "";
            }

            CmsAddress displayaddress = null;
            StringBuilder sbAddress = new StringBuilder();
            string tempaddress = "";
            
            switch (countryCode.ToUpper())
            {
                case "SG":
                    displayaddress = lstAddress.Where(x => x.AddressTypeCode.Equals(string.IsNullOrEmpty(addresstype) ? "mailing" : addresstype, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (displayaddress != null)
                    {
                        tempaddress = string.Format("{0} {1}",Trim( displayaddress.StreetBlockNo), Trim(displayaddress.StreetName)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0} {1}", Trim(displayaddress.UnitNo), Trim(displayaddress.BuildingName)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0} {1}", Trim(displayaddress.Country), Trim(displayaddress.PostalCode)).Trim();
                        sbAddress.Append(tempaddress);
                    }
                    break;
                case "MY":
                    displayaddress = string.IsNullOrEmpty(addresstype) ? lstAddress.OrderBy(x => x.AddressSeq).FirstOrDefault() : lstAddress.Where(x => x.AddressTypeCode.Equals(addresstype, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (displayaddress != null)
                    {
                        tempaddress = string.Format("{0}", Trim(displayaddress.Address1)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", Trim(displayaddress.Address2)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0} {1} {2} {3}", Trim(displayaddress.PostalCode), Trim(displayaddress.City),Trim( displayaddress.State), Trim(displayaddress.Country)).Trim();
                        sbAddress.Append(tempaddress);
                    }
                    break;
                case "TH":
                    displayaddress = lstAddress.Where(x => x.AddressTypeCode.Equals(string.IsNullOrEmpty(addresstype) ? "Registration" : addresstype, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (displayaddress != null)
                    {
                        tempaddress = string.Format("{0}", Trim(displayaddress.BuildingName)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0} {1}", Trim(displayaddress.UnitNo), Trim(displayaddress.StreetName)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0} {1}", Trim(displayaddress.SubDistrict), Trim(displayaddress.District)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0} {1}", Trim(displayaddress.City), Trim(displayaddress.PostalCode)).Trim();
                        sbAddress.Append(tempaddress);
                    }
                    break;
                case "TW":
                    displayaddress = string.IsNullOrEmpty(addresstype) ? lstAddress.OrderBy(x => x.AddressSeq).FirstOrDefault() : lstAddress.Where(x => x.AddressTypeCode.Equals(addresstype, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (displayaddress != null)
                    {
                        //tempaddress = string.Format("{0}", displayaddress.Address1).Trim();
                        //tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        //sbAddress.Append(tempaddress);

                        //tempaddress = string.Format("{0}", displayaddress.Address2).Trim();
                        //tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        //sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", Trim(displayaddress.Area)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0} {1}", Trim(displayaddress.County), Trim(displayaddress.PostalCode)).Trim();
                        sbAddress.Append(tempaddress);
                    }
                    break;
                case "CN": 
                    displayaddress = lstAddress.Where(x => x.AddressTypeCode.Equals(string.IsNullOrEmpty(addresstype) ? "Current" : addresstype, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (displayaddress != null)
                    {
                        tempaddress = string.Format("{0} {1}", Trim(displayaddress.Country), Trim(displayaddress.City)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", Trim(displayaddress.BuildingName)).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", Trim(displayaddress.PostalCode)).Trim();
                        sbAddress.Append(tempaddress);
                    }
                    break;
                case "IN":
                    displayaddress = lstAddress.Where(x => x.AddressTypeCode.Equals(string.IsNullOrEmpty(addresstype) ? "mailing" : addresstype, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (displayaddress != null)
                    {
                        tempaddress = string.Format("{0}", displayaddress.Address1).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", displayaddress.Address2).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", displayaddress.Landmark).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0} {1}", displayaddress.City, displayaddress.PostalCode).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", displayaddress.District).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", displayaddress.State).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);
                    }
                    break;
                case "JP":
                    displayaddress = lstAddress.Where(x => x.AddressTypeCode.Equals(string.IsNullOrEmpty(addresstype) ? "Current" : addresstype, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (displayaddress != null)
                    {
                        tempaddress = string.Format("{0}", displayaddress.Address1).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);

                        tempaddress = string.Format("{0}", displayaddress.Address2).Trim();
                        tempaddress = string.IsNullOrEmpty(tempaddress) ? "" : tempaddress + "\n";
                        sbAddress.Append(tempaddress);
                    }
                    break;
            }
            return sbAddress.ToString().Trim();
        }

        private string Trim(string value)
        {  
            return string.IsNullOrEmpty(value) ? "" : value.Trim();
        }
    }

    public static class Extenders
    {
        public static decimal ToDecimal(this string str)
        {
            // you can throw an exception or return a default value here
            if (string.IsNullOrEmpty(str))
                return 0;

            decimal d;

            // you could throw an exception or return a default value on failure
            if (!decimal.TryParse(str, out d))
                return 0;

            return d;
        }
    }
}