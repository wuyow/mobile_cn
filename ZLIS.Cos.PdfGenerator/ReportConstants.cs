﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZLIS.Cos.PdfGenerator
{
    public class ReportConstants
    {
        public static readonly string TEMPLATE_OVERDUE_REPORT = "Overdue";
        public static readonly string TEMPLATE_SOA_REPORT = "SOA";
        public static readonly string TEMPLATE_ET_REPORT = "ET_Estimate";
        public static readonly string TEMPLATE_REPAYMENT_REPORT = "Repayment";
        public static readonly string TEMPLATE_RECEIPT_REPORT = "Receipt";
        public static readonly string TEMPLATE_TAXINVOICE_REPORT = "TaxInvoice";
        public static readonly string TEMPLATE_PAYMENT_HISTORY_REPORT = "PaymentHistory";

        public static readonly string KEY_OVERDUE_BREAKDOWN = "OVERDUE_BREAKDOWN";
        public static readonly string KEY_REPAYMENT_PLAN = "REPAYMENT_SCHEDULE";
        public static readonly string KEY_SOA = "SOA";
        public static readonly string KEY_ET_ESTIMATE = "ET_ESTIMATE";
        public static readonly string KEY_RECEIPT = "RECEIPT";
        public static readonly string KEY_TAX_INVOICE = "TAX_INVOICE";
        public static readonly string KEY_PAYMENT_HISTORY = "PAYMENT_HISTORY";

        public const string FORMAT_PDF = "PDF";
        public const string FORMAT_EXCEL = "Excel";

    }

    public enum ReportType
    { 
        OverDue,
        SOA,
        ET,
        Repayment
    }

    internal class ReportProcessConstants
    {
        public static readonly int NoOfRecordsPerFetch = 100;
    }
}