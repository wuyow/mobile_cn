﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Framework.Common.FileSystems.AppData;
using Aspose.Words;
using ZLIS.Cos.Core;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.Core.Common;

namespace ZLIS.Cos.PdfGenerator
{
    public class JPReportGenerater : DefaultReportGenerater, IReportGenerater
    {
          public JPReportGenerater(ReportContext context)
            : base(context)
        {
        }

        public override byte[] GenerateRepaymentReport(string templatename, string downloadname, string contractNo, string fpg, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();

            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, string.Format("{0}_{1}", templatename, GetSubTemplate(contractNo)), prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_REPAYMENT_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);

            JPRepaymentHeaderModel header = GetBaseReportHeaderModel<JPRepaymentHeaderModel>(new JPRepaymentHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetRepaymentTables(contractNo, header,isloc, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }
        

        #region Private Methods

        private List<MergeTableModel> GetRepaymentTables(string contractNo, JPRepaymentHeaderModel inheader, bool isloc, out JPRepaymentHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contractNo).First();
            var contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);
            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsPaymentScheduleResultSet paymentSchedule;
            IEnumerable<JPRepaymentTableItemModel> items = new List<JPRepaymentTableItemModel>();
            int i = 1;
            decimal totalmonthlyvatexcl = 0;
            decimal totalmonthlyvat = 0;
            decimal totalamount = 0;

            var bank = ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BANK == null ? null : ((CmsCustomerProfile)session.CurrentCmsCustomerProfile).BANK.Where(x => x.ContractNO == contractNo).FirstOrDefault();

            if (bank != null)
            {
                header.BankAccount = string.Format("{0} {1} {2}", bank.AccountType, bank.AccountNO, bank.AccountName);
                header.Bank = !string.IsNullOrEmpty(bank.BankName) ? (string.Format("{0}    {1}", bank.BankName, bank.BranchName)) : string.Format("{0}     {1}", bank.BankNameLocal, bank.BranchNameLocal);
            }

            if (contract.ContractAsset != null && contract.ContractAsset.Count > 0)
            {
                header.Model = string.Format("{0} {1}", contract.ContractAsset[0].Make, contract.ContractAsset[0].Model);
            }

            int j = 1;
            int seq = 1;
            do
            {
                paymentSchedule = _rmwService.GetPaymentSchedule(businessPartner.BpId, contractNo, i, ReportProcessConstants.NoOfRecordsPerFetch, null, null);
                if (i == 1)
                {
                    header.PayerName = string.IsNullOrEmpty(paymentSchedule.PayerName) ? paymentSchedule.PayerNameLocal : paymentSchedule.PayerName;
                    header.PayerType = paymentSchedule.PayerType.ToLower().Equals("corporate") ? "御中" : "様";
                    header.VinNo = paymentSchedule.VinNo;
                    header.CustomerName = string.IsNullOrEmpty(paymentSchedule.Contractor) ? paymentSchedule.ContractorLocal : paymentSchedule.Contractor;
                    header.VehicleRegNo = paymentSchedule.VehicleRegistrationNo;
                    header.PaymentMethod = GetPaymentMethod(contract.FinancialDetail.PaymentMode, isloc);
                    header.ContractTerm = string.Format("{0}{1}", (summary.TotalTerm).ToString(), "ヶ月");
                }
                items = items.Union(paymentSchedule.PaymentScheduleDetails.Select(x => new JPRepaymentTableItemModel
                {
                    ID = (j++).ToString(),
                    RentalNo = string.Format("{0}{1}", ((!string.IsNullOrEmpty(x.PaymentMethod) && (x.PaymentMethod.ToLower() == "offsetting" || x.PaymentMethod == "相殺")) ? "$ " : (((x.MonthlyPaymentVATIncl ?? 0) > 0) ? (((x.MonthlyPaymentVATIncl ?? 0) - (x.ActualMonthly ?? 0) == 0) ? "$ " : "") : "")), x.RentalNum.FormatString("0")),
                    YearMonth = GetYearMonth(x.YearMonth),
                    PayDate = x.PayDate == null ? string.Empty : _context.Formatter.FormatData("PLAINDATE", x.PayDate, true),
                    MonthlyPaymentVATExcl = (x.MonthlyPaymentVATExcl ?? 0).FormatCurrency("###,##0", "-"),
                    MonthlyVAT = (x.MonthlyVAT ?? 0).FormatCurrency("###,##0", "-"),
                    Amount = (x.MonthlyPaymentVATIncl ?? 0).FormatCurrency("###,##0", "-"),
                    PaymentMethod = (x.MonthlyPaymentVATIncl ?? 0) > 0 ? GetPaymentMethod(x.PaymentMethod, isloc) : "",
                    Sequence = (seq++),
                    DataType = 2 
                }).ToList());

                totalmonthlyvatexcl += (from a in paymentSchedule.PaymentScheduleDetails select a.MonthlyPaymentVATExcl ?? 0).Sum();
                totalmonthlyvat += (from a in paymentSchedule.PaymentScheduleDetails select a.MonthlyVAT ?? 0).Sum();
                totalamount += (from a in paymentSchedule.PaymentScheduleDetails select a.MonthlyPaymentVATIncl ?? 0).Sum();                
            } while (i++ * ReportProcessConstants.NoOfRecordsPerFetch < paymentSchedule.Total);

            #region Adding Empty data


            int noOfDataRows = items.Count();
            int totalRowSequence = 0;
            double dataEndedColumn = Math.Ceiling((double)items.Count() / 33);
            double noOfPage = Math.Ceiling((double)items.Count() / 99);
            double noOfColumn = noOfPage * 3;

            
            var emptyData = new List<JPRepaymentTableItemModel>();

            // Adding total rows
            string strTotal = "Total";
            if (isloc)
            {
                strTotal = new ZLIS.Cos.Core.ResourceProvider.CosResourceProvider("MyAccount_MyAccount_RepaymentPlan").GetObject("PageLabel_Total", new System.Globalization.CultureInfo("ja-JP")).ToString();
                if (strTotal == "")
                    strTotal = "Total";
            }

            for (int col = 1; col <= noOfColumn; col++)
            {
                if (dataEndedColumn == col)
                {
                    totalRowSequence = ((33 * col) + 1);
                    emptyData.Add(new JPRepaymentTableItemModel() { RentalNo = strTotal, MonthlyPaymentVATExcl = totalmonthlyvatexcl.FormatCurrency("###,##0", "-"), MonthlyVAT = totalmonthlyvat.FormatCurrency("###,##0", "-"), Amount = totalamount.FormatCurrency("###,##0", "-"), Sequence = totalRowSequence, DataType = 1 });
                    break;
                }
                else
                {
                    emptyData.Add(new JPRepaymentTableItemModel() { Sequence = ((33 * col) + 1), DataType = 1 });
                }
            }

            // Adding empty rows for the data ended column
            for (int rows = noOfDataRows+1; rows < totalRowSequence; rows++)
            {
                emptyData.Add(new JPRepaymentTableItemModel() { Sequence = (rows), DataType = 4 });
            }

            // Adding empty rows for the rest of the columns
            for (int rows = 0; rows < (noOfColumn - dataEndedColumn) * 34; rows++)
            {
                emptyData.Add(new JPRepaymentTableItemModel() { Sequence = (10000), DataType = 3 });
            }

            #endregion

            table.Items = items.Union(emptyData).OrderBy(x => x.Sequence).ThenBy(x => x.DataType);
            results.Add(table);

            string fpg = summary.FinancialProductGroup == null ? "" : summary.FinancialProductGroup.Title;

            if (fpg.ToLower().Contains("kappu"))
            {
                header.ContractType = "割　賦";
                header.ContractTerm = "";
            }
            else
            {
                header.ContractType = "リース";
            }

            header.TotalMonthlyPaymentVATExcl = totalmonthlyvatexcl.FormatCurrency("###,##0", "-");
            header.TotalMonthlyVAT = totalmonthlyvat.FormatCurrency("###,##0", "-");
            header.TotalAmount = totalamount.FormatCurrency("###,##0", "-");
            
            return results;
        }

        private string GetYearMonth(string yearMonth)
        {
            if (string.IsNullOrEmpty(yearMonth))
                return "";

            string[] str = yearMonth.Split(' ');

            if (str.Length < 2)
                return yearMonth;

            string[] months = new string[12] {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

            try
            {
                int month = Array.IndexOf(months, str[0]) + 1;
                int year = DateTime.Now.Year + (int.Parse(str[1].Trim()) - int.Parse(DateTime.Now.Year.ToString().Substring(2)));

                return string.Format("{0} {1}{2}", year.ToString(), (month < 10 ? "  " : ""), month);
            }
            catch
            {
            }

            return yearMonth;
        }

        private string GetSubTemplate(string contractNo)
        {
            var businessPartner = ((CmsCustomerProfile)(CosApplicationContext.Current.Session.CurrentCmsCustomerProfile)).BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contractNo).First();

            switch (summary.LeaseType)
            {
                case "OL":
                    return string.Format("{0}_{1}", summary.LeaseType, summary.ExcIncFlag);
                case "FL":
                case "IF":
                default:
                    return summary.LeaseType;
            }
        }

        private string GetPaymentMethod(string paymentMethod, bool isLoc)
        {
            if (string.IsNullOrEmpty(paymentMethod))
                return "";

            if (isLoc)
            {
                switch (paymentMethod.ToLower())
                {
                    case "cash":
                        return "現金";
                    case "direct debit":
                        return "口座振替";
                    case "transfer from suspense":
                        return "＊";
                    case "atm transfer":
                        return "振込";
                    case "offsetting":
                        return "相殺";
                }
            }
            else
            {
                //switch (paymentMethod)
                //{
                //    case "現金":
                //        return "Cash";
                //    case "现金":
                //        return "Cash";
                //    case "口座振替":
                //        return "Direct Debit";
                //    case "振込":
                //        return "ATM Transfer";
                //    case "相殺":
                //        return "Offsetting";
                //}
                switch (paymentMethod.ToLower())
                {
                    case "transfer from suspense":
                        return "＊";
                }
            }

            return paymentMethod;
        }

        #endregion

        public string DayOfTheMonth(System.DateTime dDate)
        {
            string sSuffix = null;
            string sResult = null;

            try
            {
                //get the suffix
                switch (dDate.Day)
                {
                    case 1:
                    case 21:
                    case 31:
                        sSuffix = "st";
                        break;
                    case 2:
                    case 22:
                        sSuffix = "nd";
                        break;
                    case 3:
                        sSuffix = "rd";
                        break;
                    default:
                        sSuffix = "th";
                        break;
                }

                //build the string
                sResult = Convert.ToString(dDate.Day) + sSuffix;

            }
            catch (Exception ex)
            {
                sResult = "Custom Format Error";
            }

            return sResult;
        }
    }
}