﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Framework.Common.FileSystems.AppData;
using ZLIS.Cos.Core;
using Aspose.Words;
using ZLIS.Cos.RmwService.DomainObjects;
using ZLIS.Cos.RmwService;
using ZLIS.Cos.Core.Common;

namespace ZLIS.Cos.PdfGenerator
{
    public class INReportGenerater : DefaultReportGenerater
    {
        public INReportGenerater(ReportContext context)
            : base(context)
        {
        }

        public override byte[] GenerateOverdueReport(string templatename, string downloadname, string contractNo, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            INOverDueHeaderModel header = GetBaseReportHeaderModel<INOverDueHeaderModel>(new INOverDueHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetOverDueTables(contractNo, header, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        public override byte[] GenerateRepaymentReport(string templatename, string downloadname, string contractNo, string fpg, string marketcode, string prilanguge, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            //var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, templatename, prilanguge.ToUpper()));
            //var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);

            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, string.Format("{0}_{1}", templatename, fpg), prilanguge.ToUpper()));
            var defaultpath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, ReportConstants.TEMPLATE_REPAYMENT_REPORT, prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);

            INRepaymentHeaderModel header = GetBaseReportHeaderModel<INRepaymentHeaderModel>(new INRepaymentHeaderModel(), contractNo, marketcode, isloc);
            List<MergeTableModel> tables = GetRepaymentTables(contractNo, header, isloc, out header);
            return GenerateReport(templatepath, defaultpath, downloadpath, header, tables, SaveFormat.Pdf);
        }

        public override byte[] GenerateSOAReport(string templatename, string downloadname, CmsContractDetail contract, string fpg, string marketcode, string prilanguge, int rptTxnType, DateTime sdate, DateTime edate, bool isloc)
        {
            IAppDataFolder appsrv = CosApplicationContext.Current.GetWorkContext().Resolve<IAppDataFolder>();
            var templatepath = appsrv.MapPath(string.Format("ReportTemplate/{0}/{1}_{2}.docx", marketcode, string.Format("{0}_{1}", templatename, fpg), prilanguge.ToUpper()));
            var downloadpath = appsrv.MapPath("ReportTemp/" + downloadname);
            INSOAHeaderModel header = GetBaseReportHeaderModel<INSOAHeaderModel>(new INSOAHeaderModel(), contract.ContractNO, marketcode, isloc);
            List<MergeTableModel>  tables = GetSOATables(contract, rptTxnType, sdate, edate, header, isloc, out header);
            return GenerateReport(templatepath, string.Empty, downloadpath, header, tables, SaveFormat.Pdf);
        }

        #region Private Methods

        private List<MergeTableModel> GetOverDueTables(string contractNo, INOverDueHeaderModel inheader, out INOverDueHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = session.CurrentCmsCustomerProfile.BusinessPartner;
            decimal monthgrosstotal = 0;
            decimal monthpaidtotal = 0;
            decimal monthtotal = 0;
            decimal misctotal = 0;
            decimal misccharge = 0;
            decimal miscpaid = 0;
            decimal overduepaidtotal = 0;  
            decimal overduetotal = 0;
            decimal overduetaxtotal = 0;
            decimal overdueremaintotal = 0;
            
            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            //CmsPaymentScheduleResultSet paymentSchedule;
            IEnumerable<OverDuePayTableItemModel> items = new List<OverDuePayTableItemModel>();
            IList<CmsOverDueRental> overdueInstallments = _rmwService.GetOverdueRentalDetail(businessPartner.BpId, contractNo);
            //int i = 1;
            //int j = 1;
            //do
            //{
            //    paymentSchedule = _rmwService.GetPaymentSchedule(businessPartner.BpId, contractNo, i, 100, null, null);

            //    items = items.Union(paymentSchedule.PaymentScheduleDetails.Select(a => new OverDuePayTableItemModel
            //    {
            //        ID = (j++).ToString(),
            //        RentalNo = a.RentalNum.ToString(),
            //        PayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
            //        GrossAmount = a.AmountGrossRental == null ? string.Empty : a.AmountGrossRental.Value.ToString("###,##0.00"),
            //        Paid = a.PaidAmount == null ? string.Empty : a.PaidAmount.Value.ToString("###,##0.00"),
            //        Remain = (overdueInstallments == null ? 0 : (overdueInstallments.Where(b => b.RentalNum == a.RentalNum).FirstOrDefault() == null ? 0: (overdueInstallments.Where(b => b.RentalNum == a.RentalNum).FirstOrDefault().DueAmount))).ToString("###,##0.00")
            //    }));

            //    monthgrosstotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountGrossRental ?? 0).Sum();
            //    monthpaidtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.PaidAmount ?? 0).Sum();
            //    //monthtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.Amount ?? 0).Sum();
            //} while (i++ * 100 < paymentSchedule.Total);

            //if (items == null || items.Count() > 0)
            //{
            //    table.Items = items;

            //    monthtotal = (from a in items select a.Remain.ToDecimal()).Sum();
            //}
            //else
            //{
            //    var eptlst = new List<OverDuePayTableItemModel>();
            //    eptlst.Add(new OverDuePayTableItemModel()
            //        {
            //            ID = "",
            //            RentalNo = "",
            //            PayDate = "",
            //            GrossAmount = "",
            //            Paid = "",
            //            Remain = ""
            //        });
            //    table.Items = eptlst;
            //}

            if (overdueInstallments != null && overdueInstallments.Count > 0)
            {
                var paytable = overdueInstallments.Select((a, index) => new OverDuePayTableItemModel()
                {
                    ID = (index + 1).ToString(),
                    RentalNo = a.RentalNum.ToString(),
                    PayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                    GrossAmount = a.RentalAmount.ToString("###,###.00"),
                    Paid = a.PaidAmount.ToString("###,###.00"),
                    Remain = a.DueAmount.ToString("###,###.00")
                    //PayMonth = a.RentalAmount.ToString("###,###.00"),
                });
                monthgrosstotal = (from a in overdueInstallments select a.RentalAmount).Sum();
                monthpaidtotal = (from a in overdueInstallments select a.PaidAmount).Sum();
                monthtotal = (from a in overdueInstallments select a.DueAmount).Sum();
                table.Items = paytable;
            }
            else
            {
                var eptlst = new List<OverDuePayTableItemModel>();
                eptlst.Add(new OverDuePayTableItemModel(){
                    ID = "",
                    RentalNo = "",
                    PayDate = "",
                    GrossAmount = "",
                    Paid = "",
                    Remain = ""
                });
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OtherPayItems";
            IList<CmsMiscCharge> overdueMiscCharges = _rmwService.GetMiscChargesDetails(businessPartner.BpId, contractNo).Where(x => x.DueAmount > 0).ToList();
            if (overdueMiscCharges != null && overdueMiscCharges.Count > 0)
            {
                var misctable = from a in overdueMiscCharges
                                select new OverDueOtherPayTableItemModel()
                                {
                                    Project = a.ChargeName,
                                    OtherPayDate = a.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.DueDate, true),
                                    OtherPayCharge = (a.ChargeAmount - a.EcessAmount - a.SAHSAmount - a.ServiceTax - a.SurchargeVAT).ToString("###,##0.00"),
                                    OtherTaxAmount = (a.EcessAmount + a.SAHSAmount + a.ServiceTax + a.SurchargeVAT).ToString("###,##0.00"),
                                    OtherPaid = a.PaidWaive.ToString("###,##0.00"),
                                    OtherRemain = a.DueAmount.ToString("###,##0.00")                                    
                                };
                misctotal = overdueMiscCharges.Select(a => a.EcessAmount + a.SAHSAmount + a.ServiceTax + a.SurchargeVAT).Sum();
                miscpaid = (from a in overdueMiscCharges select a.PaidWaive).Sum();
                misccharge = (from a in overdueMiscCharges select a.DueAmount).Sum();
                table.Items = misctable;
            }
            else
            {
                var eptlst = new List<OverDueOtherPayTableItemModel>();
                eptlst.Add(new OverDueOtherPayTableItemModel()
                {
                    Project = "",
                    OtherPayDate = "",
                    OtherPayCharge = "",
                    OtherTaxAmount = "",
                    OtherPaid = "",
                    OtherRemain = ""
                    });
                table.Items = eptlst;
            }
            results.Add(table);
            table = new MergeTableModel();
            table.TableName = "OverDueItems";
            IList<CmsOverDueInterest> overdueInterests = _rmwService.GetOverdueRentalInterest(businessPartner.BpId, contractNo);
            if (overdueInterests != null && overdueInterests.Count > 0)
            {
                var interesttable = from a in overdueInterests
                                    select new OverDueChargeTableItemModel()
                                    {
                                        OverDueTerm = a.RentalNO.ToString(),
                                        OverDueType = a.OverdueType,
                                        OverDueStartDate = a.FromDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.FromDate, true),
                                        OverDueEndDate = a.ToDate == null ? string.Empty : _context.Formatter.FormatData("DATE", a.ToDate, true),
                                        OverDueDate = a.OverdueDays.ToString(),
                                        OverDue = a.RentalAmount.ToString("###,##0.00"),
                                        OverDueRate = a.Rate.ToString("###,##0.00"),
                                        OverDueAmount = a.OverDueAmount.ToString("###,##0.00"),
                                        OverDueTaxAmount = (a.EcessAmount + a.SAHSAmount + a.ServiceTax + a.SurchargeVAT).ToString("###,##0.00"),
                                        OverDueRemain = (a.DueAmount).ToString("###,##0.00")
                                    };
                overduepaidtotal = (from a in overdueInterests select a.RentalAmount).Sum();
                overduetotal = (from a in overdueInterests select a.OverDueAmount).Sum();
                overduetaxtotal = overdueInterests.Select(a => a.EcessAmount + a.SAHSAmount + a.ServiceTax + a.SurchargeVAT).Sum();
                overdueremaintotal = overdueInterests.Select(a => a.DueAmount).Sum();
                table.Items = interesttable;
            }
            else
            {
                var eptlst = new List<OverDueChargeTableItemModel>();
                eptlst.Add(new OverDueChargeTableItemModel()
                {
                    OverDueTerm = "",
                    OverDueType = "",
                    OverDueStartDate = "",
                    OverDueEndDate = "",
                    OverDueDate = "",
                    OverDue = "",
                    OverDueRate = "",
                    OverDueAmount = "",
                    OverDueTaxAmount = "",
                    OverDueRemain = "" 
                    });
                table.Items = eptlst;
            }
            results.Add(table);

            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contractNo).First();
            var contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);

            header.RentalReceivable = monthtotal.ToString("###,##0.00");// contract.ReceivableRentals.ToString();
            header.ReceivableCharges = misccharge.ToString("###,##0.00");// contract.ReceivableCharges.ToString("###,##0.00");
            header.OverdueRental = overdueremaintotal.ToString("###,##0.00");// (from a in contract.OverdueRentals select a.DueAmount).Sum().ToString("###,##0.00");

            header.MonthTotal = monthtotal.ToString("###,##0.00");
            header.MonthPaidTotal = monthpaidtotal.ToString("###,##0.00");
            header.MonthGrossTotal = monthgrosstotal.ToString("###,##0.00");
            header.OtherTotalPay = misctotal.ToString("###,##0.00");
            header.OtherTotalPaid = miscpaid.ToString("###,##0.00");
            header.OtherTotalCharge = misccharge.ToString("###,##0.00");
            header.OverDuePaidTotal = overduepaidtotal.ToString("###,##0.00");
            header.OverDueTotal = overduetotal.ToString("###,##0.00");
            header.OverDueTaxTotal = overduetaxtotal.ToString("###,##0.00");
            header.OverDueRemainTotal = overdueremaintotal.ToString("###,##0.00");
            header.TotalOverDue = (monthtotal + misccharge + overdueremaintotal).ToString("###,##0.00");
            return results;
        }

        private List<MergeTableModel> GetRepaymentTables(string contractNo, INRepaymentHeaderModel inheader, bool isloc, out INRepaymentHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = session.CurrentCmsCustomerProfile.BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contractNo).First();
            var contract = _context.RmwService.GetContractDetails(businessPartner.BpId, contractNo);
            MergeTableModel table = new MergeTableModel();
            table.TableName = "Items";
            CmsPaymentScheduleResultSet paymentSchedule;
            IEnumerable<RepaymentTableItemModel> items = new List<RepaymentTableItemModel>();
            int i = 1;
            decimal ptotal = 0;
            decimal interesttotal = 0;
            decimal nettotal = 0;
            decimal insurancetotal = 0;
            decimal regtotal = 0;
            decimal vattotal = 0;
            decimal grosstotal = 0;
            decimal grosspaidtotal = 0;
            decimal canceltotal = 0;
            decimal sundrytotal = 0;
            decimal surchargevattotal = 0;
            decimal servicetaxtotal = 0;
            decimal eduecesstotal = 0;
            decimal sahstotal = 0;

            int j = 1;
            do
            {
                paymentSchedule = _rmwService.GetPaymentSchedule(businessPartner.BpId, contractNo, i, 100, null, null);
                if (i == 1)
                {
                    header.FinanceProduct = paymentSchedule.FinancialProduct;
                    header.VehicleDescription = paymentSchedule.VehicleDescription;
                    header.StartDate = paymentSchedule.StartDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.StartDate, true);
                    header.PreviousStartDate = paymentSchedule.PreviousStartDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.PreviousStartDate, true);
                    header.ContractEndDate = paymentSchedule.EndDate == null ? string.Empty : _context.Formatter.FormatData("DATE", paymentSchedule.EndDate, true);
                    header.FinanceAmount = (paymentSchedule.FinanceAmount ?? 0).ToString("###,##0.00");
                    header.FinancedAmount = (paymentSchedule.FinancedAmount ?? 0).ToString("###,##0.00");
                    header.Term = paymentSchedule.Term.ToString();
                    header.TermCharge = (paymentSchedule.AmountTermCharges ?? 0).ToString("###,##0.00");
                    header.RentalMode = paymentSchedule.RentalMode;
                    header.PaymentFrequency = paymentSchedule.PaymentFrequency;
                    header.ResidualAmount = (paymentSchedule.AmountResidual ?? 0).ToString("###,##0.00");
                    header.InterestRate = (paymentSchedule.InterestRate ?? 0).ToString();
                }
                items = items.Union(paymentSchedule.PaymentScheduleDetails.Select(x => new RepaymentTableItemModel
                {
                    ID = (j++).ToString(),
                    
                    DueDate = x.DueDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.DueDate, true),
                    RentalNo = x.RentalNum.ToString(),
                    PrincipalBalance = (x.AmountPrincipalOutstanding ?? 0).ToString("###,##0.00"),
                    PrincipalAmount = (x.AmountPrincipal ?? 0).ToString("###,##0.00"),
                    InterestAmount = (x.AmountInterest ?? 0).ToString("###,##0.00"),
                    NetRentalAmount = (x.AmountNetRental ?? 0).ToString("###,##0.00"),
                    InsuranceAmount = (x.AmountVATOnRental ?? 0).ToString("###,##0.00"),
                    SurchargeVAT = (x.SurchargeVAT ?? 0).ToString("###,##0.00"),
                    ServiceTax = (x.ServiceTax ?? 0).ToString("###,##0.00"),
                    EcessAmount = (x.AmountEcess ?? 0).ToString("###,##0.00"),
                    SAHSAmount = (x.SAHSAmount ?? 0).ToString("###,##0.00"),
                    SundryCharge = (x.AmountSundaryCharges ?? 0).ToString("###,##0.00"),
                    GrossRentalAmount = (x.AmountGrossRental ?? 0).ToString("###,##0.00"),
                    GrossPaid = (x.AmountGrossPaid ?? 0).ToString("###,##0.00"),
                    Cancelled = (x.AmountDishonourCancelled ?? 0).ToString("###,##0.00")

                    // NOT IN USE
                    //PrincipalBalance = (x.AmountPrincipalOutstanding ?? 0).ToString("###,###.00"),
                    //PrincipalAmount = (x.AmountPrincipal ?? 0).ToString("###,###.00"),
                    //InterestAmount = (x.AmountInterest ?? 0).ToString("###,###.00"),
                    //InsuranceAmount = (x.AmountInsurance ?? 0).ToString("###,###.00"),
                    //RegistrationAmount = (x.AmountRegistration ?? 0).ToString("###,###.00"),
                    //NetVatAmount = (x.AmountVATOnRental ?? 0).ToString("###,###.00"),
                    //InstallmentAmount = (x.Amount ?? 0).ToString("###,###.00"),
                    //SettledAmount = (x.PaidAmount ?? 0).ToString("###,###.00"),
                    //RentalAmount = (x.AmountRental ?? 0).ToString("###,###.00")
                }));

                //todo: future rental total
                ptotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountPrincipal ?? 0).Sum();
                interesttotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountInterest ?? 0).Sum();
                nettotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountRental ?? 0).Sum();
                insurancetotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountVATOnRental ?? 0).Sum();
                surchargevattotal += (from a in paymentSchedule.PaymentScheduleDetails select a.SurchargeVAT ?? 0).Sum();
                servicetaxtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.ServiceTax ?? 0).Sum();
                eduecesstotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountEcess ?? 0).Sum();
                sahstotal += (from a in paymentSchedule.PaymentScheduleDetails select a.SAHSAmount ?? 0).Sum();
                sundrytotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountSundaryCharges ?? 0).Sum();
                grosstotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountGrossRental ?? 0).Sum();
                grosspaidtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountGrossPaid ?? 0).Sum();
                canceltotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountDishonourCancelled ?? 0).Sum();

                //NOT USE
                //ptotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountPrincipal ?? 0).Sum();
                //intesttotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountInterest ?? 0).Sum();
                //insurancetotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountInsurance ?? 0).Sum();
                //regtotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountRegistration ?? 0).Sum();
                //vattotal += (from a in paymentSchedule.PaymentScheduleDetails select a.AmountVATOnRental ?? 0).Sum();
            } while (i++ * 100 < paymentSchedule.Total);

            table.Items = items;
            results.Add(table);
            header.StatementDate = header.PrintDate;
            header.PrincipalTotal = ptotal.ToString("###,##0.00");
            header.InterestTotal = interesttotal.ToString("###,##0.00");
            header.NetTotal = nettotal.ToString("###,##0.00");
            header.InsuranceTotal = insurancetotal.ToString("###,##0.00");
            header.SurchargeVATTotal = surchargevattotal.ToString("###,##0.00");
            header.ServiceTaxTotal = servicetaxtotal.ToString("###,##0.00");
            header.EcessTotal = eduecesstotal.ToString("###,##0.00");
            header.SAHSTotal = sahstotal.ToString("###,##0.00");
            header.SundryTotal = sundrytotal.ToString("###,##0.00");
            header.GrossTotal = grosstotal.ToString("###,##0.00");
            header.GrossPaidTotal = grosspaidtotal.ToString("###,##0.00");
            header.CancelledTotal = canceltotal.ToString("###,##0.00");

            //NOT USE
            //header.FinanceProduct = summary == null ? string.Empty : (isloc ? (string.IsNullOrEmpty(summary.FinancialProductGroup.Title_Local) ? summary.FinancialProductGroup.Title : summary.FinancialProductGroup.Title_Local) : summary.FinancialProductGroup.Title);
            //var vehicle = contract.ContractAsset.FirstOrDefault();
            //header.FinanceAmount = contract.FinancialDetail.FinancedAmount.ToString("###,###.00");
            //header.VehicleDescription = vehicle == null ? "" : (isloc ? (string.IsNullOrEmpty(vehicle.AssetDescription_Local) ? vehicle.AssetDescription : vehicle.AssetDescription_Local) : vehicle.AssetDescription);
            //header.PrincipalTotal = ptotal.ToString("###,###.00");
            //header.InterestTotal = intesttotal.ToString("###,###.00");
            //header.InsuranceTotal = insurancetotal.ToString("###,###.00");
            //header.RegistrationTotal = regtotal.ToString("###,###.00");
            //header.NetVatTotal = vattotal.ToString("###,###.00");

            return results;
        }

        private List<MergeTableModel> GetSOATables(CmsContractDetail contract, int rptTxnType, DateTime sDate, DateTime eDate, INSOAHeaderModel inheader,bool isloc, out INSOAHeaderModel header)
        {
            header = inheader;
            List<MergeTableModel> results = new List<MergeTableModel>();
            ISessionState session = CosApplicationContext.Current.Session;
            IRmwService _rmwService = _context.RmwService;
            var businessPartner = session.CurrentCmsCustomerProfile.BusinessPartner;
            var summaries = _context.RmwService.GetAccountsSummary(businessPartner.BpId, null);
            CmsAccountSummary summary = summaries.Where(x => x.ContractNO == contract.ContractNO).First();
            MergeTableModel table = new MergeTableModel();
            IEnumerable<SOATableItemModel> soaitems = new List<SOATableItemModel>();
            table.TableName = "Items";
            int i = 1;
            decimal totaldebit = 0;
            decimal totalcredit = 0;
            decimal resamount = 0;
            decimal paymentamount = 0;
            decimal amountreceived = 0;
            //decimal closingbalance = 0;
            CmsSoaResultSet statementOfAccount = null;
            decimal misc = contract.MiscellaneousCharges == null || contract.MiscellaneousCharges.Count == 0 ? 0 : contract.MiscellaneousCharges.Select(c => c.DueAmount).Sum();
            do
            {
                statementOfAccount = _rmwService.GetSOA(businessPartner.BpId, contract.ContractNO, rptTxnType, i, 100, sDate, eDate);
                if (i == 1)
                {

                    //closingbalance = statementOfAccount.ClosingBalance;
                    //resamount = statementOfAccount.AmountResidual;
                    //paymentamount = statementOfAccount.AmountPayment;
                    //amountreceived = statementOfAccount.AmountReceived;
                }
                soaitems = soaitems.Union(statementOfAccount.SoaDetails.Select(x => new SOATableItemModel()
                {
                    TransactionDate = x.TransactionDate == null ? string.Empty : _context.Formatter.FormatData("DATE", x.TransactionDate, true),
                    Type = x.TransactionType,
                    Description = x.TransactionDescription,
                    Debit = x.Debit == null ? "-" : x.Debit.Value.ToString("###,##0.00"),
                    Credit = x.Credit == null ? "-" : x.Credit.Value.ToString("###,##0.00"),
                    Balance = x.Balance == null ? "-" : x.Balance.Value.ToString("###,##0.00"),
                }));
                //totaldebit += statementOfAccount.SoaDetails.Sum(a => a.Debit) ?? 0;
                //totalcredit += statementOfAccount.SoaDetails.Sum(a => a.Credit) ?? 0;
            } while (i++ * 100 < statementOfAccount.Total);
            

            if (soaitems.Count() > 0)
            {
                table.Items = soaitems;
            }
            else
            {
                var eptlst = new List<SOATableItemModel>();
                eptlst.Add(new SOATableItemModel());
                table.Items = eptlst;
            }

            results.Add(table);
            header.CoborrowerName = statementOfAccount.CoborrowerName;
            header.GuarantorName = statementOfAccount.GuarantorName;
            header.Tenure = (statementOfAccount.Tenure ?? 0).ToString();
            header.InstallmentMode = statementOfAccount.InstallmentMode;
            header.InstallmentAmount = (statementOfAccount.InstallmentAmount ?? 0).ToString("###,##0.00");
            header.InstallmentPaid = statementOfAccount.InstallmentPaid.ToString();
            header.FutureInstallment = (statementOfAccount.FutureInstallment ?? 0).ToString();
            header.InstallmentOverdue = (statementOfAccount.InstallmentOverdue ?? 0).ToString();
            header.InstallmentDueTillDate = (statementOfAccount.InstallmentDueTillDate ?? 0).ToString("###,##0.00");
            header.InstallmentReceivedTillDate = (statementOfAccount.InstallmentReceivedTillDate ?? 0).ToString("###,##0.00");
            header.PrincipalOutstandingAmount = summary.PrincipalOutstanding.ToString("###,##0.00");
            header.OpeningBalance = statementOfAccount.OpeningBalance.ToString("###,##0.00");
            header.TotalBalance = statementOfAccount.ClosingBalance.ToString("###,##0.00");
            header.EngineNo = statementOfAccount.EngineNo;
            header.ChassisNo = statementOfAccount.ChassisNo;
            header.AssetDesc = statementOfAccount.AssetDesc;
            header.Linked = statementOfAccount.Linked;
            header.LoanStatus = statementOfAccount.LoanStatus;
            header.FinancedAmount = contract.FinancialDetail.FinancedAmount.ToString("###,##0.00");
            header.TransactionPeriod = _context.Formatter.FormatData("DATE", sDate, true) + " - " + _context.Formatter.FormatData("DATE", eDate, true);
            header.VehicleRegNo = statementOfAccount.VehicleRegNo;

            //header.FinanceAmount = contract.FinancialDetail.FinancedAmount.ToString("###,###.00");
            //header.ResidualAmount = resamount.ToString("###,###.00");
            //header.PaymentAmount = paymentamount.ToString("###,###.00");
            //header.DueDate = summary == null || !summary.DateNextPaymentDue.HasValue ? string.Empty : DayOfTheMonth(summary.DateNextPaymentDue.Value);
            //header.PaymentReceived = summary == null ? ".00" : amountreceived.ToString("###,###.00");
            //header.OverDueAmount = summary.AmountOverdue.ToString("###,###.00");
            //header.VehicleReg = summary == null ? string.Empty : (isloc ? (string.IsNullOrEmpty(summary.VehicleRegNO_Local) ? summary.VehicleRegNO : summary.VehicleRegNO_Local) : summary.VehicleRegNO);
            //header.TotalCredit = totalcredit.ToString("###,###.00");
            //header.TotalDebit = totaldebit.ToString("###,###.00");
            //header.StartDate = _context.Formatter.FormatData("DATE", contract.StartDate, true);
            //header.EndDate = _context.Formatter.FormatData("DATE", contract.MaturityDate ?? contract.EndDate, true);
           
            return results;
        }

        #endregion

        public string DayOfTheMonth(System.DateTime dDate)
        {
            string sSuffix = null;
            string sResult = null;

            try
            {
                //get the suffix
                switch (dDate.Day)
                {
                    case 1:
                    case 21:
                    case 31:
                        sSuffix = "st";
                        break;
                    case 2:
                    case 22:
                        sSuffix = "nd";
                        break;
                    case 3:
                        sSuffix = "rd";
                        break;
                    default:
                        sSuffix = "th";
                        break;
                }

                //build the string
                sResult = Convert.ToString(dDate.Day) + sSuffix;

            }
            catch (Exception ex)
            {
                sResult = "Custom Format Error";
            }

            return sResult;
        }
    }

    
    

}