﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aspose.Words.Reporting;
using System.Collections;
using System.Reflection;

namespace ZLIS.Cos.PdfGenerator
{
    //ExStart
    //ExId:LINQtoXMLMailMerge_class
    //ExSummary:The implementation of the IMailMergeDataSource interface.
    public class MyMailMergeDataSource : IMailMergeDataSource
    //ExEnd
    {
        /// <summary>
        /// Creates a new instance of a custom mail merge data source.
        /// </summary>
        /// <param name="data">Data returned from a LINQ query.</param>
        //ExStart
        //ExId:LINQtoXMLMailMerge_constructor_simple
        //ExSummary:Constructor for the simple mail merge.
        public MyMailMergeDataSource(IEnumerable data)
        {
            mEnumerator = data.GetEnumerator();
        }
        //ExEnd

        /// <summary>
        /// Creates a new instance of a custom mail merge data source, for mail merge with regions.
        /// </summary>
        /// <param name="data">Data returned from a LINQ query.</param>
        /// <param name="tableName">Name of the data source is only used when you perform mail merge with regions. 
        /// If you prefer to use the simple mail merge then use constructor with one parameter.</param>
        //ExStart
        //ExId:LINQtoXMLMailMerge_constructor_with_regions
        //ExSummary:Constructor for the mail merge with regions.
        public MyMailMergeDataSource(IEnumerable data, string tableName)
        {
            mEnumerator = data.GetEnumerator();
            mTableName = tableName;
        }
        //ExEnd

        /// <summary>
        /// Aspose.Words calls this method to get a value for every data field.
        /// 
        /// This is a simple "generic" implementation of a data source that can work over 
        /// any IEnumerable collection. This implementation assumes that the merge field
        /// name in the document matches the name of a public property on the object
        /// in the collection and uses reflection to get the value of the property.
        /// </summary>
        //ExStart
        //ExId:LINQtoXMLMailMerge_get_value
        //ExSummary:Getting the field value in the custom data source.
        public bool GetValue(string fieldName, out object fieldValue)
        {
            // Use reflection to get the property by name from the current object.
            object obj = mEnumerator.Current;

            Type curentRecordType = obj.GetType();
            PropertyInfo property = curentRecordType.GetProperty(fieldName);
            if (property != null)
            {
                fieldValue = property.GetValue(obj, null);
                return true;
            }

            // Return False to the Aspose.Words mail merge engine to indicate the field was not found.
            fieldValue = null;
            return false;
        }
        //ExEnd

        /// <summary>
        /// Moves to the next record in the collection.
        /// </summary>
        //ExStart
        //ExId:LINQtoXMLMailMerge_move_next
        //ExSummary:Moving through the data records.
        public bool MoveNext()
        {
            return mEnumerator.MoveNext();
        }
        //ExEnd

        /// <summary>
        /// The name of the data source. Used by Aspose.Words only when executing mail merge with repeatable regions.
        /// </summary>
        //ExStart
        //ExId:LINQtoXMLMailMerge_table_name
        //ExSummary:The table name property.
        public string TableName
        {
            get { return mTableName; }
        }
        //ExEnd

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private readonly IEnumerator mEnumerator;
        private readonly string mTableName;
    }
}