﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZLIS.Cos.Core;

namespace ZLIS.Cos.PdfGenerator
{
    public class ReportGeneraterFactory
    {
        public static IReportGenerater GetReporter(string marketcode)
        {
            ReportContext context = CosApplicationContext.Current.GetWorkContext().Resolve<ReportContext>();
            
            switch (marketcode.ToUpper())
            { 
                case "TW":
                case "SG":
                    return new DefaultReportGenerater(context);
                case "CN":
                    return new CNReportGenerater(context);
                case "TH":
                    return new THReportGenerater(context);
                case "MY":
                    return new MYReportGenerater(context);
                case "IN":
                    return new INReportGenerater(context);
                case "KR":
                    return new KRReportGenerater(context);
                case "JP":
                    return new JPReportGenerater(context);
                default:
                    return new DefaultReportGenerater(context);

            }
        }
    }
}